# Welcome to the Pipeline for ANalysis of DAta (PANDA)

This project provides a framework for building data processing applications.
Its emphasis is to provdide support for compute algoritms on heterogeneous accelerator devices (e.g. GPU, FPGA) at the process level.

Panda is a c++ 2011 framework relying heavily on templates, which allows for very strong typing and compile time optimisation.

Its primary goal is to provide configurable pools of compute resources to which jobs (async tasks) can be
submitted for processing via a queue. 

These async tasks can be composed of more than one algorithm, each optimised to a sepcific device.
As a resource in the pool becomes available, the next task in the queue that supports that device 
will be executed.

# API Documentation

https://ska-tdt.gitlab.io/panda

# Licence

Copyright (C) 2015  The SKA Organisation (Square Kilometre Array)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the MIT License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the MIT License
    along with this program.

# Installation
This is a cmake project. Please see CMakeLists.txt file for build instructions


