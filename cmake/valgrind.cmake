# The MIT License (MIT)
#
# Copyright (c) 2016-2020 The SKA organisation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# adapted from https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake

# -- find valgrind
find_program( VALGRIND_PATH valgrind )
if(NOT VALGRIND_PATH)
    file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/valgrind)
endif(NOT VALGRIND_PATH)

macro(VALGRIND_TARGET _targetname )
    set(valgrind_target valgrind_report_${PROJECT_NAME}_${_targetname})
    if(NOT VALGRIND_PATH)
        # add a valgrind not installed message for the target
        ADD_CUSTOM_TARGET(${valgrind_target}
            COMMAND ${CMAKE_COMMAND} -E echo
            COMMENT "target ${valgrind_target} requires valgrind executable to be available"
        )
    else(NOT VALGRIND_PATH)
        set(_outputdir valgrind)
        set(_outputname valgrind_${PROJECT_NAME}_${_targetname})
        set(valgrind_target valgrind_report_${PROJECT_NAME}_${_targetname})
        ADD_CUSTOM_TARGET(${valgrind_target}
            COMMAND ${VALGRIND_PATH} --xml=yes --xml-file=${_outputdir}/${_outputname} --trace-children=yes ${CMAKE_MAKE_PROGRAM} ${_targetname}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )
        ADD_CUSTOM_COMMAND(TARGET ${valgrind_target} POST_BUILD
            COMMAND ;
            COMMENT "see ${_outputdir}/${_outputname} for the valgrind report."
        )
    endif(NOT VALGRIND_PATH)
endmacro(VALGRIND_TARGET)


# -- targets
valgrind_target(test)
