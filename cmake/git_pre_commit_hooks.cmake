# -- function to join the values in a list into a single string
function(JOIN VALUES GLUE OUTPUT)
  string (REPLACE ";" "${GLUE}" _TMP_STR "${VALUES}")
  set (${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
endfunction()

# -- macro to find the difference between two paths
#    path_a should be the higher level directory
macro(PATH_DIFFERENCE result path_a path_b)
    set(test_path ${path_b})
    while(NOT (("${test_path}" STREQUAL "${path_a}") OR ("${test_path}" STREQUAL "")))
        get_filename_component(dir ${test_path} NAME)
        list(APPEND dpath ${dir})
        get_filename_component(test_path ${test_path} DIRECTORY)
    endwhile()
    if(dpath)
        list(REVERSE dpath)
        join("${dpath}" "/" ${result})
    endif()
endmacro(PATH_DIFFERENCE)

# -- setup the git pre-commit hook
set(HOOKS_PATH "${PROJECT_SOURCE_DIR}/.git/hooks")
if(NOT EXISTS "${HOOKS_PATH}/pre-commit")
    if(EXISTS "${HOOKS_PATH}/pre-commit.sample")
        configure_file("${HOOKS_PATH}/pre-commit.sample" "${HOOKS_PATH}/pre-commit" COPYONLY)
    else()
        # we are probably a sub module
        path_difference(SUBMODULE_PATH "${CMAKE_SOURCE_DIR}" "${PROJECT_SOURCE_DIR}")
        set(HOOKS_PATH "${CMAKE_SOURCE_DIR}/.git/modules/${SUBMODULE_PATH}/hooks")
        if(EXISTS "${HOOKS_PATH}/pre-commit.sample")
            if(NOT EXISTS "${HOOKS_PATH}/pre-commit")
                configure_file("${HOOKS_PATH}/pre-commit.sample" "${HOOKS_PATH}/pre-commit" COPYONLY)
            endif()
        endif()
    endif()
endif()
