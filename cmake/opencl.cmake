#---------------------------------------------------------
# OPENCL development toolkit and configuration
#
# Interface Variables:
#---------------------------------------------------------
option(ENABLE_OPENCL "enable opencl architecture" false)
if(ENABLE_OPENCL)
  find_path(OpenCL_INCLUDE_DIR
    NAMES
      CL/cl.h OpenCL/cl.h
    PATHS
      ${OpenCL_INCLUDE_DIR}
      ${OpenCL_INSTALL_DIR}
      ENV "PROGRAMFILES(X86)"
      ENV AMDAPPSDKROOT
      ENV ALTERAOCLSDKROOT
      ENV INTELFPGAOCLSDKROOT
      ENV NVSDKCOMPUTE_ROOT
      ENV ATISTREAMSDKROOT
      /usr/local
      /usr
    PATH_SUFFIXES
      include
      host/include
      OpenCL/common/inc
      "AMD APP/include"
  )
  find_library(OpenCL_LIBRARY
    NAMES OpenCL
    PATHS
      ${OpenCL_LIBRARY_DIR}
      ${OpenCL_INSTALL_DIR}
      ENV ALTERAOCLSDKROOT
      ENV INTELFPGAOCLSDKROOT
    PATH_SUFFIXES
      lib64
      lib
      "AMD APP/lib/x86"
      lib/x86
      lib/Win32
      linux64/lib
      OpenCL/common/lib/Win32
  )
  set(OpenCL_LIBRARIES ${OpenCL_LIBRARY})
  set(OpenCL_INCLUDE_DIRS ${OpenCL_INCLUDE_DIR})

  INCLUDE(FindPackageHandleCompat)
  FIND_PACKAGE_HANDLE_STANDARD_ARGS(OpenCL DEFAULT_MSG OpenCL_LIBRARIES OpenCL_INCLUDE_DIRS)

  IF(NOT OPENCL_FOUND)
      SET( OpenCL_LIBRARIES )
      SET( OpenCL_TEST_LIBRARIES )
  ENDIF(NOT OPENCL_FOUND)

  MARK_AS_ADVANCED(OpenCL_LIBRARIES OpenCL_TEST_LIBRARIES OpenCL_INCLUDE_DIR)

  include_directories(${OpenCL_INCLUDE_DIRS})

  set(CMAKE_CXX_FLAGS "-DENABLE_OPENCL ${CMAKE_CXX_FLAGS}")
  list(APPEND DEPENDENCY_LIBRARIES ${OpenCL_LIBRARIES})
  message("OpenCL Libraries: ${OpenCL_LIBRARY}")
endif(ENABLE_OPENCL)
