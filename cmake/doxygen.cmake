# The MIT License (MIT)
#
# Copyright (c) 2016 The SKA organisation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

if(CMAKE_BUILD_TYPE MATCHES documentation)
  set(ENABLE_DOC "true")
  set(doc_all_target "ALL")
else()
  set(doc_all_target "")
endif()
# Search for the DoxyfileAPI.in by looking through CMAKE_MODULE_PATH
# This will pull it from either the local cmake modules (In preference)
# or, from the installed Panda, or finally from the Panda Source
find_file(DOXYFILE_IN "DoxyfileAPI.in" ${CMAKE_MODULE_PATH})

message("Doxyfile set to " ${DOXYFILE_IN})

set(PROJECT_DOC_TARGET "doc_${PROJECT_NAME}")
if(ENABLE_DOC)
  # add a target to generate API documentation with Doxygen
  find_package(Doxygen)
  if(DOXYGEN_FOUND)
    set(DOXYGEN_BUILD_DIR ${CMAKE_BINARY_DIR}/doc)
    file(MAKE_DIRECTORY ${DOXYGEN_BUILD_DIR})
    configure_file(${DOXYFILE_IN} ${CMAKE_BINARY_DIR}/DoxyfileAPI @ONLY)
    add_custom_target(${PROJECT_DOC_TARGET}
      ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/DoxyfileAPI
      WORKING_DIRECTORY ${DOXYGEN_BUILD_DIR}
      COMMENT "Generating API documentation with Doxygen" VERBATIM)

    install(DIRECTORY ${DOXYGEN_BUILD_DIR}/
            DESTINATION ${DOC_INSTALL_DIR}
            PATTERN "${DOXYGEN_BUILD_DIR}/*"
           )
   else(DOXYGEN_FOUND)
       add_custom_target(${PROJECT_DOC_TARGET}
      COMMAND ${CMAKE_COMMAND} -E echo
      COMMENT "No doc target configured. Doxygen not found" VERBATIM
    )
   endif(DOXYGEN_FOUND)
else(ENABLE_DOC)
    add_custom_target(${PROJECT_DOC_TARGET}
      COMMAND ${CMAKE_COMMAND} -E echo
      COMMENT "No doc target configured. Rebuild with 'cmake -DENABLE_DOC=true .'" VERBATIM
    )
endif(ENABLE_DOC)

list(APPEND PROJECT_DOC_TARGETS ${PROJECT_DOC_TARGET})
get_directory_property(hasParent PARENT_DIRECTORY)
if(hasParent)
    set(PROJECT_DOC_TARGETS ${PROJECT_DOC_TARGETS} PARENT_SCOPE)
else()
    set(PROJECT_DOC_TARGETS ${PROJECT_DOC_TARGETS})
endif(hasParent)

# --- add top level doc target only when there can be no clashes
if("${PROJECT_NAME}" STREQUAL "${CMAKE_PROJECT_NAME}")
    add_custom_target(doc ${doc_all_target} DEPENDS ${PROJECT_DOC_TARGETS})
endif()

