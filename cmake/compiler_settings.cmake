#
# Compiler defaults for panda
# This file is included in the top level pelican CMakeLists.txt.
#

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "release")
endif ()

# Set compiler flags
#set(CMAKE_CXX_FLAGS "-fPIC")

if(CMAKE_CXX_COMPILER MATCHES icpc OR CMAKE_CXX_COMPILER MATCHES icc)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcheck -wd2259 -wd1125 -wd161")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()
if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Werror")
    if(CMAKE_BUILD_TYPE MATCHES profile)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg -g -O0  -fprofile-arcs -ftest-coverage")
    endif()
endif ()
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=gnu++11")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wextra -Wcast-align -Wno-unknown-pragmas")
    if(CMAKE_BUILD_TYPE MATCHES profile)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg -O0 -fprofile-arcs -ftest-coverage")
    endif()
endif ()

set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -Wall -Wextra")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g -Wall -Wextra -pedantic -Wno-unknown-pragmas")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -Wall -Wextra -pedantic  -Wno-unknown-pragmas")

# Set include directories for dependencies
include_directories(
    ${Boost_INCLUDE_DIRS}
)
