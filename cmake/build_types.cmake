if(CMAKE_BUILD_TYPE MATCHES documentation)
  include(cmake/doxygen.cmake)
else()
  # -- normal build operations
  include(cmake/dependencies.cmake)
  include(cmake/compiler_settings.cmake)
endif()

