#---------------------------------------------------------
# CUDA development toolkit and configuration
#
# Interface Variables:
# CUDA_NVCC_FLAGS  : additional flags to send to the nvcc compiler
#---------------------------------------------------------
option(ENABLE_CUDA "Enable CUDA algorithms. Requires CUDA toolkit to be installed" OFF)
if(ENABLE_CUDA)

  cmake_minimum_required(VERSION 2.8) # CUDA package only available from this version
  find_package(CUDA REQUIRED)
  include_directories(${CUDA_TOOLKIT_INCLUDE})

  set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
  set(CUDA_PROPAGATE_HOST_FLAGS OFF)

  # options for NVCC
  list(APPEND CUDA_NVCC_FLAGS -DENABLE_CUDA --std c++11)

  if(CUDA_VERSION_MAJOR EQUAL "11")
    add_definitions(-DTHRUST_IGNORE_DEPRECATED_CPP_DIALECT)
  endif()
  if(CUDA_VERSION_MAJOR LESS "9" )
      list(APPEND CUDA_NVCC_FLAGS_DEBUG --generate-line-info)
  endif(CUDA_VERSION_MAJOR LESS "9" )
  list(APPEND CUDA_NVCC_FLAGS_DEBUG --debug; --device-debug; -Xcompiler "-Werror")


  set(CMAKE_CXX_FLAGS "-DENABLE_CUDA ${CMAKE_CXX_FLAGS}")

  macro(CUDA_GENERATE_LINK_FILE cuda_link_file cuda_target)

      # Compute the file name of the intermedate link file used for separable
      # compilation.
      CUDA_COMPUTE_SEPARABLE_COMPILATION_OBJECT_FILE_NAME(cuda_link_file ${cuda_target} "${${cuda_target}_SEPARABLE_COMPILATION_OBJECTS}")

      # Add a link phase for the separable compilation if it has been enabled.  If
      # it has been enabled then the ${cuda_target}_SEPARABLE_COMPILATION_OBJECTS
      # variable will have been defined.
      CUDA_LINK_SEPARABLE_COMPILATION_OBJECTS("${cuda_link_file}" ${cuda_target} "${_options}" "${${cuda_target}_SEPARABLE_COMPILATION_OBJECTS}")

  endmacro()

  #
  # @macro CUDA_SUBPACKAGE_COMPILE
  # Use to specify that certain objects should be compiled with
  # alternative flags (e.g. a specific architecture)
  # @example
  # cuda_subpackage_compile(${my_cuda_files} OPTIONS "-arch compute_35")
  #
  macro(CUDA_SUBPACKAGE_COMPILE)
    FILE(APPEND ${SUBPACKAGE_FILENAME}
      "CUDA_ADD_CUDA_INCLUDE_ONCE()\n"
      "cuda_compile(_cuda_objects "
    )
    foreach(arg ${ARGN})
        if(EXISTS "${arg}")
            set(_file "${arg}")
        else()
            set(_file "${CMAKE_CURRENT_SOURCE_DIR}/${arg}")
            if(NOT EXISTS "${_file}")
                set(_file ${arg})
            endif()
        endif()
        FILE(APPEND ${SUBPACKAGE_FILENAME}
            "${_file}\n"
        )
    endforeach(arg ${ARGV})
    FILE(APPEND ${SUBPACKAGE_FILENAME}
      ")\n"
      "list(APPEND lib_obj_cuda \${_cuda_objects})\n"
    )
  endmacro(CUDA_SUBPACKAGE_COMPILE)

else(ENABLE_CUDA)
  # dummies
  macro(CUDA_SUBPACKAGE_COMPILE)
  endmacro(CUDA_SUBPACKAGE_COMPILE)

endif(ENABLE_CUDA)
