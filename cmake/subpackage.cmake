#===============================================================================
# Macro for managing arch subpackages
# -----------------------------------
# Usage:
#
#     subpackage(module_name)
#
# the packages are expected to exist in the module_name sub directory
#
# Creates install targets for the header files (including detail directory)
#===============================================================================

macro(SUBPACKAGE name)
    add_subdirectory(${name})
    file(GLOB include_files "${CMAKE_CURRENT_SOURCE_DIR}/${name}/*.h")
    install(FILES ${include_files} DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/${name})
    install(DIRECTORY ${name}/detail DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/${name})
endmacro(SUBPACKAGE)
