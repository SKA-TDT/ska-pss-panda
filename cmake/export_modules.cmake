# cmake modules that are exported for use externally when Panda is installed.
# APPEND the required files to  INSTALLABLE_CMAKE_FILES

# -------------------------------------------------------------------------------
# function to install a cmake module file into the MODULES_INSTALL_DIR location
# MODULE_FILE the cmake module file to export
# The EXPORT_VARIABLES passed will be expanded with current values and inserted as
# default values for the module.
# -------------------------------------------------------------------------------
function(export_cmake_module MODULE_FILE EXPORT_VARIABLES)
    set(_EXPORT_VARIABLES ${EXPORT_VARIABLES} ${ARGN})
    set(OUT_FILE "${MODULE_FILE}")
    set(OUT_FILE_TMP "${CMAKE_BINARY_DIR}/${OUT_FILE}.in")
    file(WRITE ${OUT_FILE_TMP} "# ---- begin system specific default values -----\n")
    foreach(CACHEVAR ${_EXPORT_VARIABLES})
        if(${CACHEVAR})
            file(APPEND ${OUT_FILE_TMP} "if(NOT ${CACHEVAR})\n")
            file(APPEND ${OUT_FILE_TMP} "  set(${CACHEVAR} \"${${CACHEVAR}}\")\n")
            file(APPEND ${OUT_FILE_TMP} "endif(NOT ${CACHEVAR})\n")
        endif()
    endforeach()
    file(APPEND ${OUT_FILE_TMP} "# ---- end system specific default values -----\n\n")
    file(READ "${CMAKE_SOURCE_DIR}/${MODULE_FILE}" FILE_CONTENTS)
    file(APPEND ${OUT_FILE_TMP} "${FILE_CONTENTS}")
    configure_file(${OUT_FILE_TMP} ${OUT_FILE} COPYONLY)
    install(FILES ${CMAKE_BINARY_DIR}/${OUT_FILE} DESTINATION ${MODULES_INSTALL_DIR}/cmake)
endfunction()

# Doxygen
list(APPEND INSTALLABLE_CMAKE_FILES cmake/doxygen.cmake cmake/DoxyfileAPI.in)

# Boost
export_cmake_module("cmake/boost.cmake" "Boost_INCLUDE_DIR" "Boost_LIBRARY_DIR" "Boost_LIBRARY_DIR_RELEASE" "Boost_LIBRARY_DIR_DEBUG" "Boost_DIR")

# Cuda
export_cmake_module("cmake/cuda.cmake" "CUDA_TOOLKIT_ROOT_DIR")

# Code Coverage
list(APPEND INSTALLABLE_CMAKE_FILES cmake/code_coverage.cmake)

# Git Version
list(APPEND INSTALLABLE_CMAKE_FILES cmake/git_version.cmake)

# Git Pre-Commit Hooks
list(APPEND INSTALLABLE_CMAKE_FILES cmake/git_pre_commit_hooks.cmake)

# Google Test
list(APPEND INSTALLABLE_CMAKE_FILES cmake/googletest.cmake)

# OpenCl
export_cmake_module("cmake/opencl.cmake" "OpenCL_INCLUDE_DIR" "OpenCL_LIBRARY")

# Valgrind
list(APPEND INSTALLABLE_CMAKE_FILES cmake/valgrind.cmake)


# And finally add this list of files to the cmake subdir of the install directory
install(FILES ${INSTALLABLE_CMAKE_FILES} DESTINATION ${MODULES_INSTALL_DIR}/cmake)
