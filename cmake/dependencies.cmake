include(cmake/boost.cmake)
include(cmake/googletest.cmake)
include(cmake/numa.cmake)
include(cmake/cuda.cmake)
include(cmake/opencl.cmake)
include(cmake/doxygen.cmake)
include(cmake/thread_affinities.cmake)

list(APPEND DEPENDENCY_LIBRARIES ${CUDA_LIBRARIES})
