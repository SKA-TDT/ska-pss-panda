
# -- find threading library that allows to set affinities
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads)
if(CMAKE_USE_PTHREADS_INIT)
    if(NOT APPLE)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DHAS_PTHREAD_SET_AFFINITY")
        list(APPEND PANDA_DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT})
    endif(NOT APPLE)
endif(CMAKE_USE_PTHREADS_INIT)
