\anchor Main
\mainpage Introduction

# PANDA - the Pipeline for ANalysis of DAta (PANDA)

This project provides a framework for building data processing applications.
Its emphasis is to provdide support for compute algoritms on heterogeneous accelerator devices (e.g. GPU, FPGA) at the process level.

Panda is a c++ 2011 framework relying heavily on templates, which allows for very strong typing and compile time optimisation.

Its primary goal is to provide configurable pools of compute resources to which jobs (async tasks) can be
submitted for processing via a queue.

These async tasks can be composed of more than one algorithm, each optimised to a sepcific device.
As a resource in the pool becomes available, the next task in the queue that supports that device
will be executed.

## Other documents

- [Developer Guide](@ref md_doc_developers_guide)
- [Pipeline Documentation](@ref PDPPipeline)
- [User Guide](@ref md_doc_user_guide)

\defgroup config Configuration
\defgroup connectivity Network Utilities
\defgroup resource Tasks and Resource Management
\defgroup sinks Data Sinks
\defgroup concepts Concepts
\defgroup testutil Test Utilities
