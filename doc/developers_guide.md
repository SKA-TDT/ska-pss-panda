# Developers Guide

## Dependencies
Do not introduce any new dependencies on any other product without consultation

## Build Products
The project will build two c++ libraries, and the associated header files.
One for the main product and the second a library of utilitites to aid in unit testing code that uses the main library.

## Repository Structure
cmake          : project specific files for configuring the build system
tools          : useful things to aid the developer such as a class template generator
doc            : all documentation, with the exception of the API doc of the classes, should go in here
panda          : this is the src code for the panda library. In this directory only put header files that you
                 wish to export as part of the library
panda/src      : place all other header files and src code for the library here
panda/test     : the location of all tests and the public headers for the panda test utility library
panda/test/src : the location of all unnit tests and the src for the test utility library

# The Build System
This project uses cmake as is build system. See the notes in the CMakeLists.txt file in the top directory

## Adding a src file
    - Each time you add a new class, in order for it to build you will need to add a reference to it in the appropriate CMakeLists.txt file
      (either in panda/CMakeLists.txt or panda/test/CMakeLists.txt)

## Adding a dependency
    - add a new file cmake file for that product and include this from cmake/dependencies.cmake
    - Try and stick to a standard convention for naming the product location.
      TODO - decide what these should be
      i.e. <NAME>_INCLUDE_DIRECTORIES, <NAME>_LIBRARIES, FOUND_<NAME>
    - Test these can be overriden succesfully from the command line

# Coding Conventions
Coding conventions are used in this project to ensure conformity in style and to help
reduce any of the very many pitfalls c++ programming can introduce,
Remember that these are just general guidelines. Try and stick to them as much as possible,
but there will always be cases where doing it differently is justified.

## Naming Conventions
- class names shall start with a capital letter and be CamelCased
- method and functions shall be all lower case, with word seperation with an "_" e.g. do_something()
- variable nnd member names shall be all lower case
- all members shall be prefixed with an underscore "_"

## C++ Files
Each class shall have its own separate cpp and header file. The name of the file should be the same as the class with the .h or .cpp extension
Do not mix two classes together in the same file.
An exception to this rule can be made where there is a private Internal Implementation class unique to the
class being implemented AND it is not confusing to do so.

- All classes must be in the ska, panda namespace
- Header files must start with.
'''
#ifdef SKA_PANDA_<CLASS_NAME>_H
#define SKA_PANDA_<CLASS_NAME>_H
... body of header
#endif // SKA_PANDA_<CLASS_NAME>_H
'''
  In the tool directory you will find a class template generator that will do this for you.

## Documentation
- Add doxygen style comments for each method and each parameter in every public header file.
- Think about adding an example of the use of each method
- Document the bounds of any parameter
- Add comments as appropriate in the cpp file and non-public headers.

## Constructors
- Never use new inside a constructor parameter list e.g. dodgy # MyClass(new BadThingToDo());.
  This could easily lead to a memory leak if the constructor of MyClass should fail.
- Do not create long lists of parameters with the same types. Use strong typeing as much as possible to
  get your compiler to do the debugging for you.
    e.g. thing(int x_dimension, int y_dimension, int subject_index)
         could become:
         thing( Dimension x, Dimension y, SubjectIndex i );
         or even:
         thing( DimensionX x, DimensionY y, SubjectIndex i );

## Destructors
- destructors should be marked virtual if they are public
- non-virtual destructructors must be protected

## Namespaces
- all classes shall be in the ska::panda or ska::panda::test namespace as appropriate

## Class Declarations
- all members shall be prefixed with an underscore "_"
- all members shall be made either private or protected as appropriate
- always use the override keyword when you inherit a method

## Unit Tests
- gtest shall be used as the unit test framework
- each class should have its own unit test suite which shall be in files <CLASS_NAME>Test.cpp and <CLASS_NAME>Test.h in the unit test directory
- any common functionality should be considered for inclusion in the test utility library, and provded with appropriate headers and documentation
- all unit tests should be verified to pass before pushing code to the master repository

## Comments

Multiline comments should start with `/*` and end with `*/`. All lines between should start with a `*` indented by one space (to line up with the * in the comment start).
