# The Panda User Guide

- [Anatomy of A Pipeline](@ref PDPPipelines)
- [Producer Tutorial](@ref ProducersTutorial)
- [Understanding Architectures](@ref ArchTutorial)

## Functionality Overview by Class

The panda framework provides a number of classes that aid in developing data processing pipelines.
A brief summary of these classes:

### Tasks and Resource Management
* DataChunk        : base class for all data types
* DataManager      : syncronyise independent data streams and provide a queueing mechanism to separate independent units.
    * data producers push onto the data manager
    * data consumers pull from the data manager
* DataSwitch       : Allows you to assign asyncronous handlers to named channels. These channels can then be activated or deactivated at runtime.
    * strongly typed
    * will keep the data in scope as long as needed
    * allows you to assign processing resources to each channel separately (see ProcessingEngine)
* Pipeline         : Convenience class that maps the output from a DataManager to a data processing pipeline (Functor).
    * Functor guaranteed to be run by a single thread
    * Data passed guaranteed to be kept in scope during Functor lifetime
    * shared_from_this() function allows Functor to retain data between invocations if required.
* ProcessingChain  : A description of a series of asyncronous processing events. Use to generate a series of Tasks for execution.
    * allows you to assign how resources shall be mapped to each
* ProcessingEngine : an event loop with a number of dedicated threads associated with it.
    * will automatically use sync calls if no dedicated threads are allocated.
* ResourcePool     : A collection of compute resources (e.g. threads, GPU, FPGA, etc ) and a queueing system to assign Tasks to them.
* System           : Device discovery and provisioning of ResourcePools
* Task             : An architecture independent description of an asyncronous task that needs to be executed by a ResourcePool.
    * when the type of resource available is known, task will be converted to a specific job tailored to that architecture.

### Utilities
* AggregationBuffer : aggregates a collection of DataChunks together.
    * handler notified when buffer full
    * maintains references to individual objects that make up the aggregation
    * allows you to copy data form each chunk to an alternative format (e.g. contiguous memory)
* ConfigFile       : Read in XML and JSON configuration files.
* ConfigModule     : Base class takes features from boost::program_options combined with boost::property_tree structure (e.g XML, JSON)
                     - separate definition of options requiring documentation
                     - automatically picks up options from either command line or config file
                     - 2 stage config file parsing
                     - tree hierarcy of ConfigModules easily developed
* MixInTimer        : a mixin class that wraps any Functor and times it. Allows you to specify custom output formats etc.
    * by selecting the underlying type or the MixInTimer wrapped version allows timing when required with zero runtime cost when not required.

### Network Utilities
* Server                      : manage network connections and transfers data from these to a suitable application.
    * single or multi-threaded
    * supply functionality through a ServerApp class.
    * ConnectionTraits allow static customisation of:
        * error policies
        * communication protocols
        * socket types (e.g. tcp, udp)
* ServerClient                : A simple network client to connect to a Server and send/read unformatted data from the socket.
* StreamerApp                 : A ServerApp that will stream data to connected clients
    * dynamic mode where client must connect to the server to subscribe to the data stream
    * fixed mode where the data is sent out to a fixed IP
* CumulativeDelayErrorPolicy  : An error policy that will keep trying to reconnect, each attempt at an increasingly long time interval.
    * fully configurable time delays


## Tutorials
- [Producers Tutorial](@ref ProducersTutorial)
