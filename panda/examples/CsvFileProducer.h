/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CSVFILEPRODUCER_H
#define SKA_PANDA_CSVFILEPRODUCER_H

#include "panda/Producer.h"
#include "panda/Log.h"
#include <string>
#include <vector>
#include <fstream>

namespace ska {
namespace panda {
namespace examples {

/**
 * @brief
 *     A producer that reads from a list of CSV files
 *
 * @details
 *     @ref ProducerTutorial
 */

template<class DerivedType>
class CsvFileProducerCRTP : public ska::panda::Producer<DerivedType, std::vector<int>, ska::panda::ServiceData<std::vector<std::string>>>
{
    private:
        typedef ska::panda::Producer<DerivedType, std::vector<int>, ska::panda::ServiceData<std::vector<std::string>>> BaseT;

        typedef std::vector<int> Data;
        typedef std::vector<std::string> Headers;

    public:
        typedef std::vector<std::string> FileNameContainer;
        typedef typename BaseT::DataSetType DataSetType; // Producer requirement

    public:
        CsvFileProducerCRTP(FileNameContainer const& file_names);
        ~CsvFileProducerCRTP();

        // initialisation to perform when a DataManager becomes available
        void init() override;

        // called by a single thread to do the work.
        // return true when all data is consumed
        bool process();

        // stop called by the DataManager to pause or shutdown the data stream
        void stop();

    private:
        // methods
        // returns true if succesful
        bool next_file();
        bool read_headers(Headers&);

    private:
        // members
        FileNameContainer _file_names;
        typename FileNameContainer::const_iterator _file_name_it; // points to the next file to open
        std::ifstream _file_stream;
};

class CsvFileProducer : public examples::CsvFileProducerCRTP<CsvFileProducer>
{
    using CsvFileProducerCRTP<CsvFileProducer>::CsvFileProducerCRTP;
};

} // examples
} // namespace panda
} // namespace ska

#include "CsvFileProducer.cpp"

#endif // SKA_PANDA_CSVFILEPRODUCER_H
