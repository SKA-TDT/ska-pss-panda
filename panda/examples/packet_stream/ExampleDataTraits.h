/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_EXAMPLEDATATRAITS_H
#define SKA_PANDA_EXAMPLEDATATRAITS_H
#include "ExamplePacketInspector.h"
#include <vector>
#include <limits>

namespace ska {
namespace panda {
namespace examples {

/**
 * @brief
 *    DataTraits implementation for the example PacketStream
 */
class ExampleDataTraits
{
    public: // required typedefs
        // specify we want to use the ExamplePacketInspector
        typedef ExamplePacketInspector PacketInspector;

        // we want to unpack the data from the packets into a std::vector
        typedef std::vector<uint32_t> DataType;

    public:
        ExampleDataTraits();
        ~ExampleDataTraits();

         // required method
         static bool align_packet(PacketInspector const& packet);

         // required method
         inline static constexpr uint32_t max_sequence_number() { return std::numeric_limits<uint32_t>::max(); };

         // required method
         static constexpr std::size_t packet_size() { return ExamplePacket::payload_size(); }

         // required method
         static std::size_t chunk_size(DataType const&);

         // required method
         static uint32_t sequence_number(PacketInspector const&);

         // required method
         static void packet_stats(std::size_t missing_packets, std::size_t expected_packets);

         // required method
         template<typename ContextType>
         static void deserialise_packet(ContextType& context, PacketInspector const&);
 
         // required method
         template<typename MissingSliceContextType>
         static void process_missing_slice(MissingSliceContextType& context);

         // required method
         template<typename ResizeContextType>
         static void resize_chunk(ResizeContextType& context);
};

} // namespace examples
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_EXAMPLEDATATRAITS_H 
