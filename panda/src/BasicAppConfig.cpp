/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/BasicAppConfig.h"
#include "panda/Version.h"
#include "panda/Log.h"
#include "panda/ConfigFile.h"
#include "panda/PropertyTreeUtils.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/filesystem.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/version.hpp>
#pragma GCC diagnostic pop
#include <iostream>

namespace ska {
namespace panda {


BasicAppConfig::BasicAppConfig(std::string app_name, std::string short_description)
    : ConfigModule("")
    , _init(false)
    , _short_description(std::move(short_description))
    , _app_name(app_name)
{
    // generic options
    _desc.add_options()
        //("config,c", boost::program_options::value<std::string>(), "specify the configuration file")
        ("help,h", "this help message")
        ("help-config-file", "information about the configuration file and its format")
        ("help-module", boost::program_options::value<std::vector<std::string>>(), "display help message for the specified modules (see --list-modules)")
        ("list-modules", "a list of the configurable modules (see --help-module)")
        ("log-level", boost::program_options::value<std::vector<std::string>>()->notifier(
                    [](std::vector<std::string> const& levels) {
                        for(auto const& level : levels) {
                            if(level == "debug") {
                                panda::log_debug.enable(true);
                            }
                            else if(level == "error") {
                                panda::log_error.enable(true);
                                panda::log_warn.enable(false);
                                panda::log.enable(false);
                            }
                            else if(level == "warn") {
                                panda::log_warn.enable(true);
                                panda::log.enable(false);
                            }
                            else if(level == "log") {
                                panda::log_error.enable(true);
                                panda::log_warn.enable(true);
                                panda::log.enable(true);
                            }
                        }
                    }
                ), "set the level of logging (error, warn, log, debug)")
        ("config", boost::program_options::value<std::string>(), "specify a configuration file (see --help-config-file)")
        ("version", "print out the program version and exit")
    ;

}


void BasicAppConfig::add_options(OptionsDescriptionEasyInit&)
{
}


void BasicAppConfig::init()
{
    if(!_init) {
        for(auto p : _add_modules) {
            ConfigModule::add(p.first, *p.second);
        }
        _all_desc.add(_desc);
        _all_desc.add(cli_options_description());
        _init = true;

        // now modify top level description to ensure top level help exists
        // for he local options. n.b. must happen after add to _all_desc
        OptionsDescriptionEasyInit init(&_desc, "");
        this->add_options(init);
    }
}


BasicAppConfig::~BasicAppConfig()
{
}

void BasicAppConfig::add(ConfigModule& config)
{
    add(config.name(), config);
}

void BasicAppConfig::add(std::string section_name, ConfigModule& config)
{
    _add_modules.emplace_back(std::make_pair(section_name, &config));
}

std::string BasicAppConfig::version() const
{
    std::string v("Panda Version: ");
    v += panda::full_version;
    v += "\n";
    v += std::string("Panda Boost Version: ") + panda::boost_version + "\n";
    return v;
}


int BasicAppConfig::parse(int argc, char** argv)
{
    init();
    std::string app_name=_app_name;
    if(app_name == "" && argc > 0) app_name= boost::filesystem::basename(argv[0]);

     // parse the command line to find the config file
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
                                  .options(_all_desc)
                                  .positional(_options_pod)
                                  .run(), vm);


    if( vm.count("version") ) {
        std::cout << version() << "\n";
        return 1;
    }
    if( vm.count("help") ) {
        std::cout << "Name\n\t" << app_name << " " <<  _short_description << "\n\n";
        std::cout << "Synopsis\n\t" << app_name << " [OPTIONS] [config_file]\n\n";
        std::cout << _desc << "\n";
        if(subsections().size() > 2) {
            // split up help messages
            std::cout << "Modules:\n";
            std::cout << "  Module specific help can be accessed via the --help-module flag.\n" << "\n";
            for( auto const& mod : subsections() ) {
                std::cout << "\t" << mod << "\n";
            }
        }
        else {
            // keep all modules help at the top level if short enough
            for( auto const& module : subsections() ) {
                std::cout << help(module) << "\n";
            }
        }
        return 1;
    }
    if( vm.count("help-module") ) {
        for(auto const& module : vm["help-module"].as<std::vector<std::string>>() ) {
            std::cout << "Name\n\t" << app_name << " " <<  _short_description << " : " << module << "\n\n";
            //std::cout << command_line_options(module) << "\n";
            std::cout << help(module) << "\n";
        }
        return 1;
    }
    if( vm.count("help-config-file") ) {
        std::cout << "Name\n\t" << app_name << " " << _short_description << " - config files\n\n";
        std::cout << "Description\n\tconfig files should be in XML format and the filename should have the .xml extension." << "\n";
        std::cout << "\tAny command line option can be set in the config file using the long name of the option as a tag." << "\n";
        boost::property_tree::ptree config_pt;
        config_pt.put_child(app_name, config_tree());
#if BOOST_VERSION < 105600
        boost::property_tree::write_xml(std::cout, config_pt,boost::property_tree::xml_writer_make_settings(' ', 2));
#else
        boost::property_tree::write_xml(std::cout, config_pt,boost::property_tree::xml_writer_make_settings<std::string>(' ', 2));
#endif
        std::cout << "\n";
        return 1;
    }
    if( vm.count("list-modules") ) {
        for( auto const& mod : subsections() ) {
            std::cout << mod << "\n";
        }
        return 1;
    }
    if( vm.count("config") ) {
        // configuration file specified
        panda::ConfigFile config_file(vm["config"].as<std::string>()); config_file.parse();
        // <cheetah> section corresponds to generic options
        auto top_it = config_file.property_tree().find(_app_name);
        if(top_it != config_file.property_tree().not_found() ) {
            boost::program_options::store(panda::parse_property_tree<char>(top_it->second, _desc), vm);
            parse_property_tree(top_it->second, vm);
        }
    }

    int rv = this->parse_vm(vm);
    if(rv != 0) 
        return rv;

    boost::program_options::notify(vm);
    return 0;
}


int BasicAppConfig::parse_vm(boost::program_options::variables_map&)
{
    return 0;
}

} // namespace panda
} // namespace ska
