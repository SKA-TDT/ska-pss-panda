/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ResourceJob.h"
#include <iostream>

namespace ska {
namespace panda {


ResourceJob::ResourceJob()
    : _processing(true)
    , _wait_condition(nullptr)
{
    set_status( JobStatus::None );
}

ResourceJob::~ResourceJob()
{
    if( _wait_condition )
    {
        _wait_condition->notify_all();
        delete _wait_condition;
    }
}

void ResourceJob::reset() {
    std::unique_lock<std::mutex> lock(_mutex);
    _processing = true;
    set_status( JobStatus::None );
    set_error("");
    _callbacks.clear();
}

void ResourceJob::wait() const {
    std::unique_lock<std::mutex> lock(_mutex);
    if( _processing  ) {
        if(!_wait_condition)
            _wait_condition=new std::condition_variable;
        _wait_condition->wait(lock, [&]() { return !_processing; });
    }
}

void ResourceJob::emit_finished() {
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _processing = false;
    }
    if( _wait_condition ) {
        _wait_condition->notify_all();
    }
}

} // namespace panda
} // namespace ska
