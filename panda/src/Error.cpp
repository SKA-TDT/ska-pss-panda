/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Error.h"
#include <cstring>

namespace ska {
namespace panda {


Error::Error()
    : std::runtime_error("")
    , _exception_ptr(nullptr)
{
}

Error::Error(Error const & e)
    : std::runtime_error(e)
    , _exception_ptr(e._exception_ptr)
    , _msg(e._msg)
{
}

Error::Error(Error&& e)
    : std::runtime_error(std::move(e))
    , _exception_ptr(e._exception_ptr)
    , _msg(std::move(e._msg))
{
}

Error::Error(std::string const & msg)
    : std::runtime_error(msg)
    , _exception_ptr(nullptr)
{
    set(std::runtime_error::what());
}

Error::Error(boost::system::error_code const & ec)
    : std::runtime_error(std::string(ec?ec.message():""))
    , _exception_ptr(nullptr)
{
    set(std::runtime_error::what());
}

Error::Error(std::exception const & e)
    : std::runtime_error(e.what())
{
    set(std::runtime_error::what());
    set(std::make_exception_ptr(e));
}

Error::~Error()
{
}

Error& Error::operator=(Error const& error)
{
    std::exception::operator=(error);
    _exception_ptr = error._exception_ptr;
    _msg = error._msg;
    return *this;
}

void Error::set( std::string const & msg )
{ 
    try {
        _msg = msg;
    }
    catch(...) {
        // must not throw
    }
}

void Error::append( std::string const & msg ) 
{ 
    try {
        _msg += msg;
    }
    catch(...) {
        // must not throw
    }
}

const char* Error::what() const noexcept
{
    try {
        // string allocation could fail
        try {
            if(_exception_ptr) std::rethrow_exception(_exception_ptr);
        }
        catch(std::exception& e) {
            // add any message from exception
            _msg += std::string(": ") + e.what();
        }
        catch(...) {
            _msg += ": exception of unknown type";
        }
        return _msg.c_str();
    }
    catch(...) {
        return std::runtime_error::what();
    }
}

Error::operator bool() const
{
    if( !_msg.empty() || _exception_ptr != nullptr  || (std::runtime_error::what() != nullptr && *std::runtime_error::what() != '\0')) return true;
    return false;
}

bool operator==(Error const& e1, Error const& e2)
{
    if(!e1 && !e2) return true;
    return (std::strcmp(e1.what(), e2.what()) == 0)?true:false;
}

bool operator!=(Error const& e1, Error const& e2)
{
    return !(e1==e2);
}

std::ostream& operator<<(std::ostream& os, const Error& e)
{
    os << e.what();
    return os;
}

Error& Error::operator<<(std::string const& val)
{
    append(val);
    return *this;
}

} // namespace panda
} // namespace ska
