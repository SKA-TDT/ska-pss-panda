/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataSwitchConfig.h"
#include "panda/ProcessingEngine.h"
#include "panda/ChannelId.h"
#include "panda/Error.h"
#include <iostream>


namespace ska {
namespace panda {


DataSwitchConfig::DataSwitchConfig()
    : _holder(new DataConfigHolder())
{
}

DataSwitchConfig::~DataSwitchConfig()
{
}

void DataSwitchConfig::activate_channel(ChannelId const& channel_name)
{
    _holder->_active[channel_name] = true;
}

void DataSwitchConfig::deactivate_channel(ChannelId const& channel_name)
{
    _holder->_active[channel_name] = false;
}

bool DataSwitchConfig::is_active(ChannelId const& channel_name) const
{
    auto it = _holder->_active.find(channel_name);
    if(it != _holder->_active.end()) {
        return it->second;
    }
    return true; // default true unless explicitly turned off
}

ProcessingEngineConfig const& DataSwitchConfig::engine_config() const
{
    return _holder->_default_engine_config;
}

void DataSwitchConfig::set_engine_config(ProcessingEngineConfig const& config)
{
    if( _holder->_default_engine)
        throw panda::Error("DataSwitchConfig: attempt to set configuration on an already existing engine");

    _holder->_default_engine_config = config;
}

void DataSwitchConfig::set_engine_config(ChannelId const& channel_id, ProcessingEngineConfig const& config)
{
    auto it = _holder->_engines.find(channel_id);
    if( it != _holder->_engines.end() )
        throw panda::Error("DataSwitchConfig: attempt to set configuration on an already existing engine");

    _holder->_engine_config[channel_id] = config;
}

ProcessingEngineConfig const& DataSwitchConfig::engine_config(ChannelId const& channel_id) const
{
    auto c_it = _holder->_engine_config.find(channel_id);
    if( c_it != _holder->_engine_config.end() )
        return c_it->second;

    return _holder->_default_engine_config;
}

ProcessingEngine& DataSwitchConfig::engine(ChannelId const& channel_id) const
{
    auto it = _holder->_engines.find(channel_id);
    if( it != _holder->_engines.end() )
        return *(it->second);

    // initialise a special engine if we have a specialised configuration
    auto config_it = _holder->_engine_config.find(channel_id);
    if(config_it != _holder->_engine_config.end()) {
        return *(_holder->_engines.emplace(std::make_pair(channel_id, std::make_shared<ProcessingEngine>(config_it->second))).first->second);
    }

    // otherwise use the default engine
    if(! _holder->_default_engine) {
        _holder->_default_engine.reset(new ProcessingEngine(_holder->_default_engine_config));
    }
    _holder->_engines[channel_id] = _holder->_default_engine;

    return *_holder->_default_engine;
}

DataSwitchConfig::DataConfigHolder::~DataConfigHolder()
{
    // wait for all engines to stop
    for(auto engine_pair : _engines) {
        engine_pair.second->join();
    }
}

} // namespace panda
} // namespace ska
