/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdlib.h>
#include "panda/Buffer.h"

namespace ska {
namespace panda {

/*
 * "void" pointers are wierd, so we need special cases
 */ 

template<>
Buffer<void>::Buffer(std::size_t size)
    : _size(size)
{
    // TODO use a pool of pre-allocated memory
    void* data = malloc(size);
    if(!data) {
        throw std::bad_alloc();
    }

    _data = std::shared_ptr<char>(reinterpret_cast<char*>(data),
                                  [](char* data) { 
                                    free(reinterpret_cast<void*>(data));
                                  });
}

template<>
void* Buffer<void>::data(unsigned offset){
    return reinterpret_cast<void*>(_data.get() + offset);
}

template<>
const void* Buffer<void>::data(unsigned offset) const{
    return reinterpret_cast<const void*>(_data.get() + offset);
}

} // namespace panda
} // namespace ska
