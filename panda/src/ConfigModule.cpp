/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PropertyTreeUtils.h"
#include "panda/ConfigModule.h"
#include "panda/Error.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/shared_ptr.hpp>
#include <boost/property_tree/xml_parser.hpp>
#pragma GCC diagnostic pop
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <set>

namespace ska {
namespace panda {

unsigned long ConfigModule::_id_generator = 0U;

ConfigModule::ConfigModule(std::string name)
    : _init(false)
{
    set_name(std::move(name));
}

ConfigModule::~ConfigModule()
{
}

void ConfigModule::set_name(std::string name)
{
    _name=std::move(name);
    std::transform(_name.begin(), _name.end(), _name.begin(), [](char ch) {
        return ch == ' ' ? '_' : ch;
    });
    if(_name != "") {  _prefix = _name + "."; }
}

std::string const& ConfigModule::id() const
{
    if(_id == "") {
        _id = "__" + std::to_string(++_id_generator);
    }
    return _id;
}

std::string const& ConfigModule::name() const
{
    return _name;
}

std::string const& ConfigModule::prefix() const
{
    return _prefix;
}

ConfigModule const* ConfigModule::get_representative_section(typename boost::property_tree::ptree::path_type const& path) const
{
    boost::property_tree::ptree::path_type p(path);
    if(p.empty()) {
        return this;
    }

    auto const tag = p.reduce();
    if(tag.empty()) {
        return this;
    }

    // check if it matches submodules
    auto it=_subsections.find(tag);
    if(it != _subsections.end()) {
        return it->second[0]->get_representative_section(p);
    }
    // check if it matches factories
    auto fit=_factory.find(tag);
    if(fit != _factory.end()) {
        return get_default_section(tag).get_representative_section(p);
    }

    return nullptr;
}

ConfigModule* ConfigModule::generate_section(std::string const& tag, boost::optional<std::string> const& id)
{
    auto factory=get_factory(tag);
    if(!factory) {
        return nullptr;
    }
    std::unique_ptr<ConfigModule> node((factory)());
    if(id) {
        node->_id = *id;
    }
    node->set_name(tag);
    node->_prefix+=node->id()+".";
    //OptionsDescriptionEasyInit init(cli_options_description(), prefix() + node->prefix() + node->id() + ".");
    //node->add_options(init);
    auto node_ptr = node.get();
    _subsections[tag].push_back(node_ptr);
    _dynamic_subs.emplace_back(std::move(node));
    return node_ptr;
}

boost::program_options::options_description ConfigModule::command_line_options(typename boost::property_tree::ptree::path_type const& path) const
{
    boost::property_tree::ptree::path_type p(path);
    if(p.empty()) {
        return command_line_options();
    }

    auto const tag = p.reduce();
    if(tag.empty()) {
        return command_line_options();
    }

    // check if it matches submodules
    auto it=_subsections.find(tag);
    if(it != _subsections.end()) {
        if(p.empty())
            return cli_options_description(tag);
        return it->second[0]->command_line_options(p);
    }
    // check if it matches factories
    auto fit=_factory.find(tag);
    if(fit != _factory.end()) {
        return get_default_section(tag).command_line_options(p);
    }

    return boost::program_options::options_description();
}

std::set<std::string> const& ConfigModule::tags() const
{
    return _tags;
}

std::string ConfigModule::help(std::string const& module_path) const {

    std::stringstream out;
    ConfigModule const* module = get_representative_section(module_path);
    if(module) {
        boost::program_options::options_description opt;
        OptionsDescriptionEasyInit init(&opt, module_path + "." );
        const_cast<ConfigModule *>(module)->add_options(init);
        if(opt.options().size()) {
            out << opt;
        }
    }
    else {
        out << "help for unknown module " << module_path << " requested\n";
        return out.str();
    }
    if(module->tags().size()) {
        out << "Submodules:\n  More help is available on the following (--help-module <module_name>)\n";
        for(auto const& tag : module->tags()) {
            out << "\t" << module_path << "." << tag << "\n";
        }
    }

    return out.str();
}

boost::program_options::options_description ConfigModule::command_line_options() const
{
    return const_cast<ConfigModule*>(this)->cli_options_description();
}

boost::program_options::options_description ConfigModule::cli_options_description()
{
    return prefixed_cli_options_description(prefix());
}

boost::program_options::options_description ConfigModule::prefixed_cli_options_description(std::string const& prefix)
{
    boost::program_options::options_description opts(_name + std::string(" Options"));
    OptionsDescriptionEasyInit init(&opts, prefix);
    this->add_options(init);

    add_subsection_options(opts, prefix);
    return opts;
}

void ConfigModule:: add_subsection_options(boost::program_options::options_description& opts, std::string const& prefix) const
{
    auto sub_it = _subsections.begin();
    while(sub_it != _subsections.end()) {
        std::string pre= prefix + sub_it->first + ".";
        for( auto const mod : sub_it->second) {
            OptionsDescriptionEasyInit init(&opts, pre);
            mod->add_options(init);
            mod->add_subsection_options(opts, pre);
        }
        ++sub_it;
    }
}

boost::program_options::options_description ConfigModule::cli_options_description(std::string const& section_name) const
{
    boost::program_options::options_description opts(section_name + std::string(" Options"));
    auto sub_it = _subsections.find(section_name);
    if(sub_it != _subsections.end()) {
        std::string pre= prefix() + sub_it->first + ".";
        for( auto const mod : sub_it->second) {
            OptionsDescriptionEasyInit init(&opts, pre);
            mod->add_options(init);
            mod->add_subsection_options(opts, pre);
        }
    }
    return opts;
}

void ConfigModule::add(std::string section_name, ConfigModule& config)
{
    if(section_name == "") return;
    _tags.insert(section_name);
    _subsections[section_name].push_back(&config);
}

void ConfigModule::add(ConfigModule& config)
{
    add(config.name(), config);
}

ConfigModule const& ConfigModule::get_subsection(std::string const& section_name, std::string const& id) const
{
    auto it=_subsections.find(section_name);
    while(it != _subsections.end()) {
        for(auto const& s : it->second ) {
            if(s->id() == id) {
                return *s;
            }
        }
        ++it;
    }
    return get_default_section(section_name);
}

ConfigModule& ConfigModule::get_subsection(std::string const& section_name, std::string const& id)
{
    auto it=_subsections.find(section_name);
    while(it != _subsections.end()) {
        for(auto const& s : it->second ) {
            if(s->id() == id) {
                return *s;
            }
        }
        ++it;
    }
    return get_default_section(section_name);
}

std::function<ConfigModule*()> ConfigModule::get_factory(std::string const& section_name) const
{
    auto fit=_factory.find(section_name);
    if(fit != _factory.end()) {
        return fit->second;
    }
    if(_default_factory) {
        return [=]() { return _default_factory(section_name); };
    }
    if(_default_function) {
        _default_function(section_name);
    }
    return std::function<ConfigModule*()>();
}

ConfigModule& ConfigModule::get_default_section(std::string const& section_name) const
{
    // none has been set, so return a default version
    auto dit = _default_section.find(section_name);
    if( dit == _default_section.end()) {
        auto fit=get_factory(section_name);
        if(fit) {
            std::unique_ptr<ConfigModule> node((fit)());
            auto rv = node.get();
            _default_section.insert(std::pair<std::string, std::unique_ptr<ConfigModule>>(section_name, std::move(node)));
            return *rv;
        } else {
            panda::Error e("unknown configuration section : '");  e << section_name << "'";
            throw e;
        }
    }
    return *(dit->second);
}

ConfigModule& ConfigModule::get_subsection(std::string const& section_name) const
{
    auto it=_subsections.find(section_name);
    if(it != _subsections.end()) {
        return *((it->second)[0]);
    }

    panda::Error e("unknown configuration section : '");  e << section_name << "'";
    throw e;
}

void ConfigModule::parse_property_tree(boost::property_tree::ptree const& tree, boost::program_options::variables_map& vm)
{
    parse_property_subtree(tree, vm, "");
}

void ConfigModule::parse_property_subtree(boost::property_tree::ptree const& tree, boost::program_options::variables_map& vm, std::string const& parent_path)
{
    std::string parent_path_root = ((parent_path=="")?"":(parent_path + "."));
    boost::program_options::options_description options_description = prefixed_cli_options_description(parent_path_root);
    if(parent_path == "")
    {
        boost::program_options::store(panda::parse_property_tree<char>(tree, options_description), vm);
    }
    else {
        boost::property_tree::ptree my_tree;
        // parse the options for this node
        my_tree.put_child(parent_path, tree);
        //boost::property_tree::xml_parser::write_xml(std::cout , my_tree);
        //std::cout << "\n";
        //std::cout << options_description;
        boost::program_options::store(panda::parse_property_tree<char>(my_tree, options_description), vm);
    }

    std::map<std::string, const_iterator> sect_map; // keep track of static modules to fill
    auto tr_it = tree.begin();
    while(tr_it != tree.end()) {
       std::string const& section = tr_it->first;

       // -- remove options that have already been parsed --
       for(auto const& opt : options_description.options()) {
          if(opt->long_name() == prefix() + section) continue;
       }

       // -- find the next static modules to be filled
       auto sect_map_it = sect_map.find(section);
       if(sect_map_it== sect_map.end()) {
           sect_map.emplace(std::make_pair(section, subsection(section)));
       }
       auto sect_it=sect_map.at(section);

       std::string new_path = parent_path_root + section;
       if(sect_it != subsection_end()) {
           // -- fill any static objects if there are any still empty
           const_cast<ConfigModule&>(*sect_it).parse_property_subtree(tr_it->second, vm, new_path );
           ++sect_it;
       }
       else {
           // -- generate a section object to fill if there is a suitable factory
           boost::optional<std::string> id = tr_it->second.get_optional<std::string>("id");
           auto node = generate_section(section, id);
           if(node) {
               std::string id_path = new_path + "." + node->id();
               boost::property_tree::ptree sub_tree(tr_it->second);
               node->parse_property_subtree(sub_tree, vm, id_path);
           }
       }
       ++tr_it;
    } // subsections loop
}

boost::property_tree::ptree ConfigModule::config_tree() const
{
    boost::property_tree::ptree pt;

    // iterate over options
    boost::program_options::options_description desc;
    OptionsDescriptionEasyInit init(&desc, "");
    const_cast<ConfigModule&>(*this).add_options(init);
    for(auto const& opt : desc.options()) {
        if(! opt->long_name().empty())
            pt.add<std::string>(opt->long_name(), opt->description() );
    }

    // iterate over subsections
    for(auto const& p : _subsections) {
        for(auto const& mod : p.second ) {
            pt.add_child(p.first, mod->config_tree());
        }
    }
    // factories
    for(auto const& p : _factory) {
        if(_subsections.find(p.first) == _subsections.end()) {
            // add factory generator options (if there is no exisitng submodule)
            boost::property_tree::ptree fpt = get_default_section(p.first).config_tree();
            if(!fpt.get_optional<std::string>("id"))
                fpt.add<std::string>("id", "identifier to distinguish between other similar type blocks");
            pt.add_child(p.first, fpt);
        }
    }
    if(_default_factory) {
        std::string tag_name="_any_name_";
        boost::property_tree::ptree fpt = get_default_section(tag_name).config_tree();
        if(!fpt.get_optional<std::string>("id"))
            fpt.add<std::string>("id", "identifier to distinguish between other similar type blocks");
        pt.add_child(tag_name, fpt);
    }

    return pt;
}

boost::program_options::options_description ConfigModule::get_subsection_options(std::string const& section_name) const
{
    return get_subsection(section_name).command_line_options();
}

std::set<std::string> ConfigModule::subsections() const
{
    std::set<std::string> subs;
    for(auto const& p : _subsections) {
        subs.insert(p.first);
    }
    for(auto const& p : _factory) {
        subs.insert(p.first);
    }
    return subs;
}

ConfigModule::const_iterator ConfigModule::subsection(std::string const& section_name) const
{
    auto it=_subsections.find(section_name);
    if(it == _subsections.end()) return subsection_end();

    return const_iterator(&(it->second));
}

ConfigModule::const_iterator ConfigModule::subsection_end() const
{
    return ConfigModule::const_iterator();
}

void ConfigModule::add_factory(std::string section_name, std::function<ConfigModule*()> const& fn)
{
    _tags.insert(section_name);
    _factory.insert(std::make_pair(std::move(section_name), fn));
}

void ConfigModule::add_default_factory(std::function<ConfigModule*(std::string const&)> const& fn)
{
    _default_factory = fn;
}

void ConfigModule::add_default_function(std::function<void(std::string const&)> const& fn)
{
    _default_function = fn;
}

ConfigModule::OptionsDescriptionEasyInit::OptionsDescriptionEasyInit(boost::program_options::options_description* opts, std::string prefix)
    : _opts(opts)
    , _prefix(std::move(prefix))
{}

ConfigModule::OptionsDescriptionEasyInit& ConfigModule::OptionsDescriptionEasyInit::operator()(const char* name, const char* description)
{
    boost::shared_ptr<boost::program_options::option_description> d(
        new boost::program_options::option_description(ext_name(name).c_str(), new boost::program_options::untyped_value(true), description));

    _opts->add(d);

    return *this;
}

ConfigModule::OptionsDescriptionEasyInit& ConfigModule::OptionsDescriptionEasyInit::operator()(const char* name, const boost::program_options::value_semantic* s)
{
    boost::shared_ptr<boost::program_options::option_description> d(new boost::program_options::option_description(ext_name(name).c_str(), s));
    _opts->add(d);
    return *this;
}

ConfigModule::OptionsDescriptionEasyInit& ConfigModule::OptionsDescriptionEasyInit::operator()(const char* name, const boost::program_options::value_semantic* s, const char* description)
{
    boost::shared_ptr<boost::program_options::option_description> d(new boost::program_options::option_description(ext_name(name).c_str(), s, description));

    _opts->add(d);
    return *this;
}

std::string ConfigModule::OptionsDescriptionEasyInit::ext_name(const char* name) const
{
    return _prefix + name;
}

ConfigModule::ConstIterator::ConstIterator() : _v(nullptr) {}
ConfigModule::ConstIterator::ConstIterator(std::vector<ConfigModule*> const* v) : _v(v) { _it = v->begin(); }

ConfigModule::ConstIterator& ConfigModule::ConstIterator::operator++() {
    if(_v) {
        ++_it;
    }
    return *this;
}

ConfigModule::ConstIterator ConfigModule::ConstIterator::operator++(int) {
    ConstIterator tmp;
    if(_v) {
        tmp._v = _v;
        tmp._it = _it;
        _it++;
    }
    return tmp;
}

bool ConfigModule::ConstIterator::operator==(ConstIterator const& i) const {
    if( i._v == _v ) {
        if (_v == nullptr) return true;
        return _it == i._it;
    }
    else {
        if(_v == nullptr && i._it == i._v->end()) return true;
        if(i._v == nullptr && _it == _v->end()) return true;
        return false;
    }
}
bool ConfigModule::ConstIterator::operator!=(ConstIterator const& i) const
{
    return !(*this == i);
}

ConfigModule const& ConfigModule::ConstIterator::operator*() const
{
    return **_it;
}

ConfigModule const* ConfigModule::ConstIterator::operator->() const
{
    return *_it;
}

} // namespace panda
} // namespace ska
