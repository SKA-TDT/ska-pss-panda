/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGURABLETASKIMPL_H
#define SKA_PANDA_CONFIGURABLETASKIMPL_H
#include "panda/Method.h"
#include "panda/ResourcePool.h"
#include "panda/AlgorithmInfo.h"
#include "panda/AlgorithmTuple.h"
#include "panda/Error.h"
#include <tuple>
#include <type_traits>

namespace ska {
namespace panda {

template<typename... DataTypes>
class SubmitMethod;

namespace detail {

namespace {
template<typename T, typename Enable=void>
struct DataTypeForwarder
{
    typedef T& type;
};

template<typename T>
struct DataTypeForwarder<T, typename std::enable_if<std::is_rvalue_reference<T>::value>::type>
{
    typedef T&& type;
};

template<typename T>
struct DataTypeForwarder<T, typename std::enable_if<
                                        std::is_move_constructible<typename std::decay<T>::type>::value
                                    && !std::is_copy_constructible<typename std::decay<T>::type>::value
                                    && !std::is_rvalue_reference<T>::value
                                                   >::type>
{
    typedef T&& type;
};

template<typename T>
using DataTypeFwd = typename DataTypeForwarder<T>::type;

template<typename Arch>
void throw_bad_algo() {
    panda::Error e("algorithm not enabled for architecture ");
    e << to_string<Arch>();
    throw e;
}

} // namespace

/**
 * @brief
 *   Implementation details for ConfigurableTask
 * @details
 *
 */
template<class ArchTuple, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp
{
    public:
        template<class Arch>
        void exec_algo(panda::PoolResource<Arch>&, DataTypes&...)
        {
            throw_bad_algo<Arch>();
        }
};

template<class Arch1, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp<std::tuple<Arch1>, DataTypes...>
{
    public:
        virtual void exec_algo(panda::PoolResource<Arch1>&, DataTypes&...) const {
            throw_bad_algo<Arch1>();
        }
};

template<class Arch1, class...Archs, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp<std::tuple<Arch1, Archs...>, DataTypes...>
    : public ConfigurableTaskImplBaseDirectOp<std::tuple<Archs...>, DataTypes...>
{
        typedef ConfigurableTaskImplBaseDirectOp<std::tuple<Archs...>, DataTypes...> BaseT;

    public:
        using BaseT::exec_algo;
        virtual void exec_algo(panda::PoolResource<Arch1>&, DataTypes&...) const {
            throw_bad_algo<Arch1>();
        }
};

template<class PoolManager, typename... DataTypes>
class ConfigurableTaskImplBase : public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, DataTypes...>
{
    public:
        ConfigurableTaskImplBase() {};
        virtual ~ConfigurableTaskImplBase() {};

        virtual std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<DataTypes>...) { return std::shared_ptr<panda::ResourceJob>(); };
};

template<class PoolManager, typename... SubmitSignature, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, SubmitMethod<SubmitSignature...>, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, DataTypes...>
    , public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...>
{
        typedef ConfigurableTaskImplBase<PoolManager, DataTypes...> BaseT;
        typedef ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...> Base2T;

    public:
        virtual std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<SubmitSignature>...) { return std::shared_ptr<panda::ResourceJob>(); };
        using BaseT::submit;
        using Base2T::exec_algo;

};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, MethodArgs...>, DataTypes...> : public ConfigurableTaskImplBase<PoolManager, DataTypes...>
{
    public:
        virtual void exec_functor(MethodArgs...) {}
};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename MethodFunctor2, typename... MethodArgs2, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, MethodArgs...>, Method<MethodFunctor2, MethodArgs2...>, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor2, MethodArgs2...>, DataTypes...>
{
        typedef ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor2, MethodArgs2...>, DataTypes...> BaseT;

    public:
        using BaseT::exec_functor;
        virtual void exec_functor(MethodArgs...) {}
};

template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
class ExecAlgoGenerator : public BaseType
{
    public:
        template<typename... BaseArgs>
        ExecAlgoGenerator(BaseArgs&&...);
        using BaseType::exec_algo;
};


template<typename BaseType, class TaskType, class Arch, class... Archs, typename... DataTypes>
class ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataTypes...>
    : public ExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataTypes...>
{
        typedef ExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataTypes...> BaseT;

    public:
        template<typename... BaseArgs>
        ExecAlgoGenerator(BaseArgs&&...);
        using BaseT::exec_algo;
        void exec_algo(panda::PoolResource<Arch>& a, DataTypes&... data) const;
};

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
class ConfigurableTaskImpl : public ExecAlgoGenerator<typename MixedAlgoTraits<AlgorithmTuple>::Architectures, BaseType, ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>, DataTypes...>
{
    private:
        typedef ExecAlgoGenerator<typename MixedAlgoTraits<AlgorithmTuple>::Architectures, BaseType, ConfigurableTaskImpl, DataTypes...> BaseT;
        typedef MixedAlgoTraits<AlgorithmTuple> AlgorithmTraitsType;

    public:
        typedef typename AlgorithmTraitsType::Architectures Architectures;
        typedef typename AlgorithmTraitsType::Capabilities Capabilities;

    public:
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&);
        ConfigurableTaskImpl(ConfigurableTaskImpl const&) = delete;
        ~ConfigurableTaskImpl() override;

        std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<DataTypes>...) override;
        using BaseT::submit;
        using BaseT::exec_algo;

        template<typename Architecture, typename... Ts>
        inline void operator()(panda::PoolResource<Architecture>& a, Ts&&... data);

        template<typename Architecture, typename... DataT>
        inline void exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const;

    private:
        template<typename AlgoType, typename ResourceType, typename ReturnType>
        struct CallHandler {
            template<typename... DataT>
            static inline void exec(AlgoType algo, ResourceType& a, Handler&, DataT&&... data);
        };

        template<typename AlgoType, typename ResourceType>
        struct CallHandler<AlgoType, ResourceType, void>;

    protected:
        template<typename... Ts>
        std::shared_ptr<panda::ResourceJob> do_submit(Ts&&...);

    protected:
        mutable AlgorithmTuple _algos;
        PoolManager _pool;
        Handler& _handler;
        std::atomic<unsigned> _ref_count;
};

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... SubmitSignature, typename... DataTypes>
class ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, SubmitMethod<SubmitSignature...>, DataTypes...>
    : public ExecAlgoGenerator<typename MixedAlgoTraits<AlgorithmTuple>::Architectures
                             , ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>
                             , ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>
                             , SubmitSignature...>
{
        typedef ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...> ImplBaseT;
        typedef ExecAlgoGenerator<typename MixedAlgoTraits<AlgorithmTuple>::Architectures, ImplBaseT, ImplBaseT, SubmitSignature...> BaseT;

    public:
        using BaseT::exec_algo;
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, Args&&...);

        std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<SubmitSignature>...) override;
        using BaseT::submit;
};

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, Method<MethodFunctor, MethodArgs...>, DataTypes...>
    : public ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>
{
        typedef ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, MethodFunctor&, Args&&...);
        void exec_functor(MethodArgs...) override;
        using BaseT::exec_functor;

    private:
        MethodFunctor& _functor;
};

} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGURABLETASKIMPL_H
