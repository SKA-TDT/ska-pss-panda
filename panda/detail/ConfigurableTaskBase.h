/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGURABLETASKBASE_H
#define SKA_PANDA_CONFIGURABLETASKBASE_H

namespace ska {
namespace panda {

template<class ImplType, class PoolManager, typename AsyncHandler, typename... DataTypes>
class ConfigurableTaskBase
{
      //  template<class Arch, typename... Ts>
      //  using ResultType = decltype(std::declval<ImplType&>().exec_algo(std::declval<panda::PoolResource<Arch>>(), std::declval<Ts>()...));

    public:
        ConfigurableTaskBase(PoolManager, AsyncHandler handler);
        virtual ~ConfigurableTaskBase();

        /**
         * @brief submit the configured task to the pool
         * @param DataTypeArgs should match the DataTypes type parameters, although they may be r or l-values
         */
        template<typename... DataTypeArgs>
        inline
        std::shared_ptr<ResourceJob> submit(DataTypeArgs&&...);

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch, typename... DataTypeArgs>
        inline
        //ResultType<Arch, DataTypeArgs...> operator()(std::shared_ptr<panda::PoolResource<Arch>> const& resource, DataTypeArgs&&...) const;
        void operator()(std::shared_ptr<panda::PoolResource<Arch>> const& resource, DataTypeArgs&&...) const;

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch, typename... DataTypeArgs>
        inline
        //ResultType<Arch, DataTypeArgs...> operator()(panda::PoolResource<Arch>& resource, DataTypeArgs&&...) const;
        void operator()(panda::PoolResource<Arch>& resource, DataTypeArgs&&...) const;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

        template<class ImplSpecialisation, typename Algorithms, typename... Functors>
        void do_set_algorithms(Algorithms&&, Functors&&...);

    protected:
        AsyncHandler _handler;
        PoolManager _pool;
        ImplType _null_default;
        ImplType* _impl;
};

template<class ImplType, class PoolManager, typename AsyncHandler, typename... SubmitSignature, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSignature...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...> BaseT;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

    public:
        using ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>::ConfigurableTaskBase;

};

template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

        // Call the Functor
        void operator()(FunctorArgs...);
        void operator()(FunctorArgs...) const;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

        template<typename ImplSpecialisation, typename Algorithms, typename... Functors>
        inline
        void do_set_algorithms(Algorithms&&, Functors&&...);

    private:
        Functor _functor;
};

template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor2, Functor2Args...>, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor2, Functor2Args...>, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

        // Call the Functor
        void operator()(FunctorArgs...);
        void operator()(FunctorArgs...) const;

        // Expose the operator() of the base class
        using BaseT::operator();

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

        template<typename ImplSpecialisation, typename Algorithms, typename... Functors>
        inline
        void do_set_algorithms(Algorithms&&, Functors&&...);

    private:
        Functor _functor;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGURABLETASKBASE_H
