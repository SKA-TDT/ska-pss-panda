/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Atomic.h"

namespace ska {
namespace panda {

template<typename T, std::memory_order MemoryOrder>
Atomic<T, MemoryOrder>::Atomic(T val) noexcept
{
    _atom.store(val);
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator=(T val) noexcept
{
    this->store(val);
    return val;
}

template<typename T, std::memory_order MemoryOrder>
Atomic<T, MemoryOrder>::operator T() const noexcept
{
    return static_cast<T>(_atom);
}

template<typename T, std::memory_order MemoryOrder>
void Atomic<T, MemoryOrder>::store( T desired, std::memory_order order) noexcept
{
    _atom.store(desired, order);
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::load(std::memory_order order) const noexcept
{
    return _atom.load(order);
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator++() noexcept
{
    return _atom.fetch_add(1, MemoryOrder) + 1;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator++(int) noexcept
{
    return _atom.fetch_add(1, MemoryOrder);
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator--() noexcept
{
    return _atom.fetch_sub(1, MemoryOrder) - 1;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator--(int) noexcept
{
    return _atom.fetch_sub(1, MemoryOrder);
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator+=(T val) noexcept
{
    return _atom.fetch_add(val, MemoryOrder) + val;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator-=(T val) noexcept
{
    return _atom.fetch_sub(val, MemoryOrder) - val;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator&=(T val) noexcept
{
    return _atom.fetch_and(val, MemoryOrder) & val;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator|=(T val) noexcept
{
    return _atom.fetch_or(val, MemoryOrder) | val;
}

template<typename T, std::memory_order MemoryOrder>
T Atomic<T, MemoryOrder>::operator^=(T val) noexcept
{
    return _atom.fetch_xor(val, MemoryOrder) ^ val;
}

} // namespace panda
} // namespace ska
