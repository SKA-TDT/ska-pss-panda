/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Pipeline.h"
#include "panda/Log.h"
#include <utility>
#include <exception>


namespace ska {
namespace panda {


template<class PipelineTraits>
Pipeline<PipelineTraits>::Pipeline(ProducerType& chunked_stream, FunctionType const& fn)
    : _data(chunked_stream)
    , _run(false)
    , _dispatcher(DispatchFunctionType(fn))
{
}

template<class PipelineTraits>
Pipeline<PipelineTraits>::~Pipeline()
{
    stop();
}

template<class PipelineTraits>
void Pipeline<PipelineTraits>::stop()
{
    _run = false;
    _data.stop();
}

template<class PipelineTraits>
int Pipeline<PipelineTraits>::exec()
{
    try {
        start();
        return 0;
    }
    catch(Abort const & e) {
        PANDA_LOG << "End of stream";
        return 0; // abort is non-errorbut terminating condition
    }
    catch(std::exception const & e) {
        PANDA_LOG_ERROR << e.what();
    }
    catch(...)
    {
        PANDA_LOG_ERROR << "unknown exception caught";
    }
    return 1;

}

template<class PipelineTraits>
void Pipeline<PipelineTraits>::start()
{
    if(!_run) {
        _run = true;
        do {
            try {
                _dispatcher.exec(_data.next());
            }
            catch(...)
            {
                _run = false;
                throw;
            }
        }
        while(_run);
    }
}

template<class PipelineTraits>
bool Pipeline<PipelineTraits>::is_running() const
{
    return _run;
}

template<class PipelineTraits>
void Pipeline<PipelineTraits>::set_function(Pipeline<PipelineTraits>::FunctionType const& fn)
{
    _dispatcher.reset(DispatchFunctionType(fn));
}

} // namespace panda
} // namespace ska
