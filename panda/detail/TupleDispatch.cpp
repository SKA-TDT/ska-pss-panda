/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TupleDispatch.h"


namespace ska {
namespace panda {

// dispatcher class
template<typename FunctionType, typename... Args>
TupleDispatch<FunctionType, Args...>::TupleDispatch(FunctionType const& fn)
    : _fn(fn)
{
}

template<typename FunctionType, typename... Args>
template<int ...S>
void TupleDispatch<FunctionType, Args...>::_dispatch(TupleType&& tuple, seq<S...>)
{
    _fn(std::forward<typename std::tuple_element<S, TupleType>::type>(std::get<S>(tuple))...);
}

template<typename FunctionType, typename... Args>
void TupleDispatch<FunctionType, Args...>::exec(TupleType&& tuple) 
{
    _dispatch(std::forward<TupleType>(tuple), UnpackSequence());
}

template<typename FunctionType, typename... Args>
void TupleDispatch<FunctionType, Args...>::reset(FunctionType const& fn) 
{
    _fn = fn;
}

} // namespace panda
} // namespace ska
