/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Launcher.h"
#include "panda/Error.h"
#include <cstring>
#include <vector>
#include <algorithm>
#include <string>
#include <system_error>
#include <type_traits>


namespace ska {
namespace panda {

template<typename LauncherTraits>
Launcher<LauncherTraits>::Launcher()
    //: _sink(_config.sink_config())
    : _source_factory(_config.source_config())
    , _parsed(false)
{
    // configure the config object
    _config.source_options(_source_factory.available());
}

template<typename LauncherTraits>
Launcher<LauncherTraits>::~Launcher()
{
}

template<typename LauncherTraits>
int Launcher<LauncherTraits>::parse(int argc, char** argv)
{
    // parse the configuration file and command line arguments
    try {
        int rv=_config.parse(argc, argv);
        if(rv != 0) return rv;
        if(_config.info_options_run()) return 0;

        // initialise the sink object
        _sink.reset(new Sink(_config.sink_config()));
        _compute_factory.reset(new ComputeFactory(_config, *_sink));
        _parsed = true;

        // create the appropriate computation unit
        //if(_config.time_handler_invocation()) {
        //    _compute.reset(_compute_factory.create_timed(_config.compute_name()));
        //}
        //else {
            _compute.reset(_compute_factory->create(_config.compute_name()));
        //}
        return rv;
    } catch (std::system_error const& e) {
        std::cerr << e.what() <<  " (code=" << e.code() << ")" << std::endl;
        return 1;
    } catch (boost::system::system_error const& e) {
        std::cerr << e.what() <<  ": (code= " << e.code().value() << ")" << std::endl;
        return 1;
    } catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}

template<typename LauncherTraits>
int Launcher<LauncherTraits>::exec()
{
    try {
        if(_parsed)
            return _source_factory.exec(_config.source_name(), *_compute);
    } catch (std::system_error const& e) {
        std::cerr << e.what() <<  " (code=" << e.code() << ")" << std::endl;
        return 1;
    } catch (boost::system::system_error const& e) {
        std::cerr << e.what() <<  ": (code= " << e.code().value() << ")" << std::endl;
        return 1;
    } catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}

} // namespace panda
} // namespace ska
