/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/FinishTask.h"


namespace ska {
namespace panda {


template<typename TaskType>
FinishTask<TaskType>::FinishTask(TaskType& task)
    : _finished(false)
    , _task(task)
{
}

template<typename TaskType>
FinishTask<TaskType>::~FinishTask()
{
}

template<typename TaskType>
template<typename Arch, typename... DataTypes>
void FinishTask<TaskType>::operator()(panda::PoolResource<Arch>& device, DataTypes&&... data)
{
    try {
        _task(device, std::forward<DataTypes>(data)...);
    } 
    catch(...) {
        _finished = true;
        throw;
    }
    _finished = true;
}

template<typename TaskType>
void FinishTask<TaskType>::wait() {
    while(! _finished) {
        std::this_thread::sleep_for(std::chrono::microseconds(2));
    }
}

template<typename TaskType>
FinishTask<std::shared_ptr<TaskType>>::FinishTask(std::shared_ptr<TaskType> task)
    : BaseT(*task)
    , _task_ptr(std::move(task))
{
}

template<typename TaskType>
void FinishTask<TaskType>::finished() {
    _finished = true;
}


} // namespace panda
} // namespace ska
