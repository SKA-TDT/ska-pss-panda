/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PoolModuleConfig.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {

namespace {
template<typename ConfigModule>
struct AddToConfigModule {
    AddToConfigModule(ConfigModule& cfg)
        : _cfg(cfg)
    {
    }

    template<typename T>
    void operator()(T& cfg) {
        _cfg.add(*cfg);
    }

    private:
        ConfigModule& _cfg;
};

} // namespace

template<typename PoolManagerType, typename... Configs>
PoolModuleConfig<PoolManagerType, Configs...>::PoolModuleConfig(PoolManagerType& pm, ConfigModule& parent)
    : _configs( std::unique_ptr<PoolSelector<PoolManagerType, Configs>>(new PoolSelector<PoolManagerType, Configs>(pm))... )
{
    AddToConfigModule<ConfigModule> adder(parent);
    panda::for_each(_configs, adder); 
}

template<typename PoolManagerType, typename... Configs>
template<typename... Arch>
PoolModuleConfig<PoolManagerType, Configs...>::PoolModuleConfig(Config<Arch...>& parent)
    : _configs( std::unique_ptr<PoolSelector<PoolManagerType, Configs>>(new PoolSelector<PoolManagerType, Configs>(parent.pool_manager()))... )
{
    AddToConfigModule<ConfigModule> adder(static_cast<ConfigModule&>(parent));
    panda::for_each(_configs, adder); 
}

template<typename PoolManagerType, typename... Configs>
PoolModuleConfig<PoolManagerType, Configs...>::~PoolModuleConfig()
{
}

template<typename PoolManagerType, typename... Configs>
template<typename T>
PoolSelector<PoolManagerType, T> const& PoolModuleConfig<PoolManagerType, Configs...>::config() const
{
    return *std::get<panda::Index<std::unique_ptr<PoolSelector<PoolManagerType, T>>, decltype(_configs)>::value>(_configs);
}

template<typename PoolManagerType, typename... Configs>
template<typename T>
PoolSelector<PoolManagerType, T>& PoolModuleConfig<PoolManagerType, Configs...>::config()
{
    return *std::get<panda::Index<std::unique_ptr<PoolSelector<PoolManagerType, T>>, decltype(_configs)>::value>(_configs);
}

} // namespace panda
} // namespace ska
