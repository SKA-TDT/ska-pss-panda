/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/MixInTimer.h"

namespace ska {
namespace panda {

template<typename T, typename MixInTimerTraits, class Enable>
template<typename... Args>
MixInTimer<T,MixInTimerTraits,Enable>::MixInTimer(Handler const& handler, Args&&... args)
    : T(std::forward<Args>(args)...), _output_handler(handler)
{
}

template<typename T, typename MixInTimerTraits, class Enable>
template<typename... Args>
MixInTimer<T,MixInTimerTraits,Enable>::MixInTimer(Args&&... args)
    : T(std::forward<Args>(args)...)
{
}

template<typename T, typename MixInTimerTraits, class Enable>
MixInTimer<T,MixInTimerTraits,Enable>::MixInTimer(Handler const & handler)
    : _output_handler(handler)
{
}

template<typename T, typename MixInTimerTraits, class Enable>
MixInTimer<T,MixInTimerTraits,Enable>::MixInTimer(Handler handler)
    : _output_handler(std::move(handler))
{
}

template<typename T, typename MixInTimerTraits, class Enable>
MixInTimer<T,MixInTimerTraits,Enable>::~MixInTimer()
{
}

template<typename R, typename T, typename ClockType> 
struct MixInTimerHelper {
    template<typename Handler>
    inline static R call_operator(T& obj, Handler& handler)
    {
        auto start = ClockType::now();
        try {
            R rv = obj.T::operator()();
            handler(start, ClockType::now());
            return rv;
        }
        catch(...)
        {
            handler(start, ClockType::now());
            throw;
        }
    }

    template<typename Handler, typename... Args>
    inline static R call_operator(T& obj, Handler& handler, Args&&... args)
    {
        auto start = ClockType::now();
        try {
            R rv = obj.T::operator()(std::forward<Args>(args)...);
            handler(start, ClockType::now());
            return rv;
        }
        catch(...)
        {
            handler(start, ClockType::now());
            throw;
        }
    }
};

template<typename T, typename ClockType> 
struct MixInTimerHelper<void, T, ClockType>
{
    template<typename Handler>
    inline static void call_operator(T& obj, Handler& handler)
    {
        auto start = ClockType::now();
        try {
            obj.T::operator()();
            handler(start, ClockType::now());
        }
        catch(...)
        {
            handler(start, ClockType::now());
            throw;
        }
    }

    template<typename Handler, typename... Args>
    inline static void call_operator(T& obj, Handler& handler, Args&&... args)
    {
        auto start = ClockType::now();
        try {
            obj.T::operator()(std::forward<Args>(args)...);
            handler(start, ClockType::now());
        }
        catch(...)
        {
            handler(start, ClockType::now());
            throw;
        }
    }
};

template<typename T, typename MixInTimerTraits>
auto MixInTimer<T, MixInTimerTraits, typename std::enable_if<has_operator_no_args<T>::value>::type>::operator()() -> decltype( std::declval<T>()() )
{
    return MixInTimerHelper<decltype(T::operator()()), T, typename MixInTimerTraits::ClockType>::call_operator(static_cast<T&>(*this), _output_handler);
}

template<typename T, typename MixInTimerTraits, typename Enable>
template<typename... Args>
auto MixInTimer<T, MixInTimerTraits, Enable>::operator()(Args&&... args) -> decltype( std::declval<T>()(args...) )
{
    return MixInTimerHelper<decltype(T::operator()(args...)), T, typename MixInTimerTraits::ClockType>::call_operator(static_cast<T&>(*this), _output_handler, std::forward<Args>(args)...);
}


} // namespace panda
} // namespace ska
