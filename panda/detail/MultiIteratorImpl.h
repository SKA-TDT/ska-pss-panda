/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MULTIITERATORIMPL_H
#define SKA_PANDA_MULTIITERATORIMPL_H

#include "panda/TupleUtilities.h"
#include <utility>
#include <type_traits>
#include <memory>

namespace ska {
namespace panda {
namespace detail {

// ensure we can instantiate
template<typename T, template <typename> class ConfigurationTemplate, int Depth, typename ParentType=void, class Enable=void>
struct ContainerInspector {
    typedef ContainerInspector<T, ConfigurationTemplate, Depth, ParentType, Enable> SelfType;
    typedef ConfigurationTemplate<typename std::remove_reference<T>::type> Configuration;
    typedef decltype(std::declval<Configuration>().begin(std::declval<T&>())) Iterator;
    static_assert(!std::is_same<void, Iterator>::value, "Iterator should not be void");
    typedef decltype(*std::declval<Iterator>()) ReferenceType;
    
    typedef ContainerInspector<ReferenceType, ConfigurationTemplate, Depth+1, SelfType> Next; // depth 0 -> max_depth
    typedef ParentType Parent;
    static constexpr std::size_t max_depth = 1 + Next::max_depth;
    static constexpr std::size_t depth = Depth;
};

// only instantiate if the Iterator is void
template<typename T, template <typename> class ConfigurationTemplate, typename ParentType, int Depth>
struct ContainerInspector<T, ConfigurationTemplate, Depth, ParentType, typename std::enable_if<std::is_same<void, 
                        decltype(std::declval<ConfigurationTemplate<typename std::remove_reference<T>::type>>().begin(std::declval<T&>()))>::value>::type>
{
    typedef ConfigurationTemplate<typename std::remove_reference<T>::type> Configuration;
    static constexpr std::size_t max_depth = 0;
    typedef ParentType Parent;
};

// utiltiy function to return the containerInspector at the specified depth
template<typename T, int Depth, class Enable=void>
struct FindInspector {
    typedef typename FindInspector<typename T::Next, Depth>::type type;
};

template<typename T, int Depth>
struct FindInspector<T, Depth, typename std::enable_if<T::depth == Depth>::type>
{
    typedef T type;
};

// traits adapter for initalising end iterators
template<typename T, typename TraitsType>
struct BeginEndAdapter : public TraitsType {
    BeginEndAdapter() {} 
    BeginEndAdapter(TraitsType& t) 
        : TraitsType(t)
    {
    }
    typedef decltype(std::declval<TraitsType>().end(std::declval<T&>())) ReturnType;
    ReturnType begin(T& t) { return TraitsType::end(t); }  // replace begin with end call
};

template<typename T>
struct RemoveBeginEndAdapter
{
    typedef T type;
};

template<typename T, typename TraitsType>
struct RemoveBeginEndAdapter<BeginEndAdapter<T, TraitsType>>
{
    typedef TraitsType type;
};

template<class MultiIteratorTraitsType>
struct EndMultiIteratorTraits : public MultiIteratorTraitsType
{
    template<typename T> using ConfigurationTemplate = BeginEndAdapter<T, typename MultiIteratorTraitsType::template ConfigurationTemplate<T>>;
};

template<typename DepthZeroIteratorType, int I, class Traits>
struct MultiIteratorImpl
{
        typedef MultiIteratorImpl<DepthZeroIteratorType, I, Traits> SelfType;
        template<typename, int, class> friend struct MultiIteratorImpl;
        typedef typename Traits::Configuration ImplConfiguration;
        typedef typename RemoveBeginEndAdapter<typename Traits::Configuration>::type Configuration;
        typedef typename Traits::Iterator Iterator;
        typedef typename Traits::ReferenceType ReferenceType;

    public:
        MultiIteratorImpl(DepthZeroIteratorType& z_begin, DepthZeroIteratorType& z_end)
            : _next(z_begin, z_end)
            , _it_pair((z_begin!=z_end)?std::make_pair(_cfg.begin(*_next), _cfg.end(*_next)):std::pair<Iterator, Iterator>())
        {
        }

        template<typename... Args, typename std::enable_if<!panda::HasType<Configuration, std::tuple<typename std::decay<Args>::type...>>::value>::type>
        MultiIteratorImpl(DepthZeroIteratorType& z_begin, DepthZeroIteratorType& z_end, Args&&... args)
            : _next(z_begin, z_end, std::forward<Args>(args)...)
            , _it_pair((z_begin!=z_end)?std::make_pair(_cfg.begin(*_next), _cfg.end(*_next)):std::pair<Iterator, Iterator>())
        {
        }

        template<typename... Args>
        MultiIteratorImpl(DepthZeroIteratorType& z_begin, DepthZeroIteratorType& z_end, Configuration config, Args&&... args)
            : _cfg(config)
            , _next(z_begin, z_end, std::forward<Args>(args)...)
            , _it_pair((z_begin!=z_end)?std::make_pair(_cfg.begin(*_next), _cfg.end(*_next)):std::pair<Iterator, Iterator>())
        {
        }

        template<typename TraitsType, typename Algo>
        void apply_algo(MultiIteratorImpl<DepthZeroIteratorType, I, TraitsType> const& end, Algo const& algo)
        {
            if(end._it_pair.first > _it_pair.first && end._it_pair.first < _it_pair.second)
            {
                algo(_it_pair.first, end._it_pair.first);
            }
            else {
                // bigger than this range so apply to current range
                algo(_it_pair.first, _it_pair.second);
                _cfg.handler();
                if(++_next) {
                    _it_pair = std::make_pair(_cfg.begin(*_next), _cfg.end(*_next));
                }
            }
        }

        ReferenceType operator*() const
        {
            return *_it_pair.first;
        }

        // enabled if iterators match
        template<typename TraitsType>
        typename std::enable_if<std::is_same<typename TraitsType::Iterator, Iterator>::value, bool>::type operator==(MultiIteratorImpl<DepthZeroIteratorType, I, TraitsType> const& o) const
        {
            // depth 0->max check order in case inner layers have not be initialised
            return _next == o._next && (_it_pair.first == o._it_pair.first);
        }

        // enabled if iterators match
        template<typename TraitsType>
        typename std::enable_if<std::is_same<typename TraitsType::Iterator, Iterator>::value, bool>::type operator!=(MultiIteratorImpl<DepthZeroIteratorType, I, TraitsType> const& o) const
        {
            // depth 0->max check order in case inner layers have not be initialised
            return _next != o._next || (_it_pair.first != _it_pair.first);
        }

        inline bool operator!=(DepthZeroIteratorType const& o) const
        {
            return _next != o;
        }

        inline bool operator==(DepthZeroIteratorType const& o) const
        {
            return _next == o;
        }
        // returns true if OK to continue, false to halt
        bool operator++()
        {
            ++(_it_pair.first);
            if(_it_pair.first != _it_pair.second) {
                return true;
            }
            _cfg.handler();
            if(!++_next) {
                return false;
            };
            _it_pair = std::make_pair(_cfg.begin(*_next), _cfg.end(*_next));
            return true;
        }

    private:
        ImplConfiguration _cfg;
        MultiIteratorImpl<DepthZeroIteratorType, I-1, typename Traits::Parent> _next;
        std::pair<Iterator, Iterator> _it_pair;
};

// corresponds to depth 0 (i.e. the top level iterator)
template<typename DepthZeroIteratorType, class Traits>
struct MultiIteratorImpl<DepthZeroIteratorType, 0, Traits>
{
        template<typename, int, class> friend struct MultiIteratorImpl;
        typedef DepthZeroIteratorType Iterator;
        typedef typename Traits::ReferenceType ReferenceType;

    public:
        MultiIteratorImpl(Iterator& begin, Iterator& end)
            : _it(begin)
            , _end(end)
        {
        }

        ReferenceType operator*() const
        {
            return *_it;
        }

        template<typename TraitsType>
        typename std::enable_if<std::is_same<typename TraitsType::Iterator, Iterator>::value, bool>::type
        operator==(MultiIteratorImpl<DepthZeroIteratorType, 0, TraitsType> const& o) const
        {
            return _it == o._it;
        }

        template<typename TraitsType>
        typename std::enable_if<std::is_same<typename TraitsType::Iterator, Iterator>::value, bool>::type
        operator!=(MultiIteratorImpl<DepthZeroIteratorType, 0, TraitsType> const& o) const
        {
            return _it != o._it;
        }

        bool operator++()
        {
            ++_it;
            if(_it != _end)
            {
                return true;
            }
            return false;
        }

        inline bool operator!=(Iterator const& o) const
        {
            return _it != o;
        }

        inline bool operator==(Iterator const& o) const
        {
            return _it == o;
        }

    private:
        Iterator _it;
        Iterator _end;
};


} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_MULTIITERATORIMPL_H 
