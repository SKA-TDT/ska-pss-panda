/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/ConfigurableTaskImpl.h"
#include <functional>
#include <thread>

namespace ska {
namespace panda {
namespace detail {


template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::ConfigurableTaskImpl(PoolManager pool, AlgorithmTuple&& algos, Handler& handler)
    : _algos(std::move(algos))
    , _pool(pool)
    , _handler(handler)
    , _ref_count(0U)
{
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::~ConfigurableTaskImpl()
{
    while(_ref_count > 0) { std::this_thread::yield(); }
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
std::shared_ptr<panda::ResourceJob> ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::submit(DataTypeFwd<DataTypes>... data) {
    return this->do_submit(std::forward<DataTypeFwd<DataTypes>>(data)...);
    /*
    ++_ref_count;
    try {
        return _pool.submit(*this, std::forward<DataTypeFwd<DataTypes>>(data)...);
    }
    catch(...) {
        --_ref_count;
        throw;
    }
    */
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename... Ts>
std::shared_ptr<panda::ResourceJob> ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::do_submit(Ts&&... data)
{
    ++_ref_count;
    try {
        return _pool.submit(*this, std::forward<Ts>(data)...);
    }
    catch(...) {
        --_ref_count;
        throw;
    }
}

template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
template<typename... BaseArgs>
ExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataTypes...>::ExecAlgoGenerator(BaseArgs&&... args)
    : BaseType(std::forward<BaseArgs>(args)...)
{
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename... DataTypes>
template<typename... BaseArgs>
ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataTypes...>::ExecAlgoGenerator(BaseArgs&&... args)
    : BaseT(std::forward<BaseArgs>(args)...)
{
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename... DataTypes>
void ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataTypes...>::exec_algo(panda::PoolResource<Arch>& a, DataTypes&... data) const
{
    static_cast<TaskType const&>(*this).exec_algo(a, std::forward<DataTypes>(data)...);
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename Architecture, typename... DataT>
inline void ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const
{
    typedef typename AlgorithmTraitsType::AlgoArchMapType AlgoArchMapType;
    typedef typename boost::mpl::at<AlgoArchMapType, Architecture>::type AlgoType;
    auto& algo = std::get<panda::Index<AlgoType, decltype(_algos)>::value>(_algos);
    CallHandler<decltype(algo),typename std::decay<decltype(a)>::type, decltype(algo(a, std::forward<DataT>(data)...))>::exec(algo, a, _handler, std::forward<DataT>(data)...);
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename Architecture, typename... Ts>
inline void ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::operator()(panda::PoolResource<Architecture>& a, Ts&&... data) {
    try {
        exec_algo(a, std::forward<Ts>(data)...);
    } catch(...) {
        --_ref_count;
        throw;
    }
    --_ref_count;
}

// Generic CallHandler
template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename AlgoType, typename ResourceType, typename ReturnType>
template<typename... DataTs>
inline void ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::CallHandler<AlgoType, ResourceType, ReturnType>::exec(AlgoType algo, ResourceType& a, Handler& handler, DataTs&&... data)
{
    handler(algo(a, std::forward<DataTs>(data)...));
}

// CallHandler specialisation for void return types
template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename AlgoType, typename ResourceType>
struct ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::CallHandler<AlgoType, ResourceType, void>
{
    template<typename... DataTs>
    static inline void exec(AlgoType algo, ResourceType& a, Handler&, DataTs&&... data);
};

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... DataTypes>
template<typename AlgoType, typename ResourceType>
template<typename... DataTs>
inline void ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, DataTypes...>::CallHandler<AlgoType, ResourceType, void>::exec(AlgoType algo, ResourceType& a, Handler& handler, DataTs&&... data)
{
    algo(a, std::forward<DataTs>(data)...);
    handler();
}


template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... SubmitSignature, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, SubmitMethod<SubmitSignature...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, std::forward<Args>(args)...)
{
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename... SubmitSignature, typename... DataTypes>
std::shared_ptr<panda::ResourceJob> ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, SubmitMethod<SubmitSignature...>, DataTypes...>::submit(DataTypeFwd<SubmitSignature>... data)
{
    return this->do_submit(std::forward<DataTypeFwd<SubmitSignature>>(data)...);
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, Method<MethodFunctor, MethodArgs...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, MethodFunctor& method, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, std::forward<Args>(args)...)
    , _functor(method)
{
}

template<class BaseType, class PoolManager, typename AlgorithmTuple, typename Handler, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
void ConfigurableTaskImpl<BaseType, PoolManager, AlgorithmTuple, Handler, Method<MethodFunctor, MethodArgs...>, DataTypes...>::exec_functor(MethodArgs... args)
{
    panda::for_each(this->_algos, _functor, std::forward<MethodArgs>(args)...);
}

} // namespace detail
} // namespace panda
} // namespace ska
