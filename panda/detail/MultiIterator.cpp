/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/MultiIterator.h"

namespace ska {
namespace panda {


template<typename MultiIteratorTraits>
template<typename... Configurations>
MultiIterator<MultiIteratorTraits>::MultiIterator(Iterator begin, Iterator end, Configurations&&... configs)
    : _impl(begin, end, std::forward<Configurations>(configs)...)
    , _continue(begin!=end)
{
}

template<typename MultiIteratorTraits>
MultiIterator<MultiIteratorTraits>::~MultiIterator()
{
}

template<typename MultiIteratorTraits>
template<typename TraitsType>
bool MultiIterator<MultiIteratorTraits>::operator==(MultiIterator<TraitsType> const& o) const
{
    return _impl == o._impl;
}

template<typename MultiIteratorTraits>
template<typename TraitsType>
bool MultiIterator<MultiIteratorTraits>::operator!=(MultiIterator<TraitsType> const& o) const
{
    return _impl != o._impl;
}

template<typename MultiIteratorTraits>
bool MultiIterator<MultiIteratorTraits>::operator!=(Iterator const& o) const
{
    return _impl != o;
}

template<typename MultiIteratorTraits>
bool MultiIterator<MultiIteratorTraits>::operator==(Iterator const& o) const
{
    return _impl == o;
}

template<typename MultiIteratorTraits>
typename MultiIterator<MultiIteratorTraits>::ReferenceType MultiIterator<MultiIteratorTraits>::operator*() const
{
    return *_impl;
}

template<typename MultiIteratorTraits>
typename MultiIterator<MultiIteratorTraits>::SelfType& MultiIterator<MultiIteratorTraits>::operator++()
{
    _continue = ++_impl;
    return *this;
}

template<typename MultiIteratorTraits>
typename MultiIterator<MultiIteratorTraits>::SelfType MultiIterator<MultiIteratorTraits>::operator++(int)
{
    SelfType tmp(*this);
    _continue = ++_impl;
    return tmp;
}

template<typename MultiIteratorTraits>
bool MultiIterator<MultiIteratorTraits>::end() const
{
    return !_continue;
}

template<typename MultiIteratorTraits>
template<typename TraitsType, typename Algo>
void MultiIterator<MultiIteratorTraits>::apply_algorithm(MultiIterator<TraitsType> const& end, Algo const& algo)
{
    if(*this != end) {
        _impl.apply_algo(end._impl, algo);
        apply_algorithm(end, algo);
    }
}

// specialisation for panda::copy
template<typename TraitsTypeA, typename TraitsTypeB, typename DstIteratorTypeTag>
struct DeviceCopy<Cpu, Cpu, MultiIterator<TraitsTypeA>, MultiIterator<TraitsTypeB>, DstIteratorTypeTag>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType& begin, EndIteratorType const& end, DstIteratorType&& dst)
    {
        typedef typename MultiIterator<TraitsTypeA>::InnerIteratorType Iterator;
        begin.apply_algorithm(end, [&](Iterator& begin, Iterator const& end)
                                   { 
                                        panda::copy(begin
                                                   ,end
                                                   ,std::forward<DstIteratorType>(dst));
                                   });
        return dst;
    }
};

} // namespace panda
} // namespace ska
