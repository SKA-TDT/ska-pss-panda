/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceMemory.h"

namespace ska {
namespace panda {

template<class Arch, typename DataType, typename Allocator>
template<typename... AllocatorArgs>
DeviceMemory<Arch, DataType, Allocator>::DeviceMemory(std::size_t size, AllocatorArgs&&... args)
    : _size(size)
    , _allocator(std::forward<AllocatorArgs>(args)...)
    , _d_ptr(_allocator.allocate(size))
{
}

template<class Arch, typename DataType, typename Allocator>
DeviceMemory<Arch, DataType, Allocator>::DeviceMemory(DeviceMemory&& other)
    : _size(other.size())
    , _allocator(std::move(other._allocator))
    , _d_ptr(std::move(other._d_ptr))
{
    other._d_ptr=nullptr;
}

template<class Arch, typename DataType, typename Allocator>
DeviceMemory<Arch, DataType, Allocator>::~DeviceMemory()
{
    _allocator.deallocate(_d_ptr, _size);
}

template<class Arch, typename DataType, typename Allocator>
std::size_t const& DeviceMemory<Arch, DataType, Allocator>::size() const
{
    return _size;
}

template<class Arch, typename DataType, typename Allocator>
void DeviceMemory<Arch, DataType, Allocator>::reset(std::size_t size)
{
    _allocator.deallocate(_d_ptr, _size);
    _size=size;
    _d_ptr=_allocator.allocate(size);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::Iterator DeviceMemory<Arch, DataType, Allocator>::begin()
{
    return static_cast<Iterator>(_d_ptr);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::Iterator DeviceMemory<Arch, DataType, Allocator>::end()
{
    return static_cast<Iterator>(_d_ptr + _size);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::ConstIterator DeviceMemory<Arch, DataType, Allocator>::begin() const
{
    return static_cast<ConstIterator>(_d_ptr);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::ConstIterator DeviceMemory<Arch, DataType, Allocator>::cbegin() const
{
    return static_cast<ConstIterator>(_d_ptr);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::ConstIterator DeviceMemory<Arch, DataType, Allocator>::end() const
{
    return static_cast<ConstIterator>(_d_ptr + _size);
}

template<class Arch, typename DataType, typename Allocator>
typename DeviceMemory<Arch, DataType, Allocator>::ConstIterator DeviceMemory<Arch, DataType, Allocator>::cend() const
{
    return static_cast<ConstIterator>(_d_ptr + _size);
}

} // namespace panda
} // namespace ska
