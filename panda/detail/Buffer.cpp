/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdlib.h>
#include <utility>

namespace ska {
namespace panda {

template <typename T>
Buffer<T>::Buffer(std::size_t size)
    : _size(size)
{
    // TODO use a pool of pre-allocated memory
    void* data = malloc(sizeof(T)*size);
    if(!data) {
        throw std::bad_alloc();
    }

    _data = std::shared_ptr<char>(reinterpret_cast<char*>(data),
                                  [](char* data) { 
                                    free(reinterpret_cast<void*>(data));
                                  });
}

template <typename T>
Buffer<T>::Buffer(Buffer const& b)
    : _data(b._data)
    , _size(b._size)
{
}

template <typename T>
Buffer<T>::Buffer(Buffer&& b)
    : _data(std::move(b._data))
    , _size(std::move(b._size))
{
}

template <typename T>
Buffer<T>::~Buffer()
{
}

template <typename T>
void Buffer<T>::swap(Buffer& b)
{
    std::swap(b._data, _data);
    std::swap(b._size, _size);
}

template<typename T>
T* Buffer<T>::data(unsigned offset)
{
    return reinterpret_cast<T*>(_data.get()) + offset;
}


template<typename T>
const T* Buffer<T>::data(unsigned offset) const
{
    return reinterpret_cast<const T*>(_data.get()) + offset;
}

template<typename T>
typename Buffer<T>::iterator Buffer<T>::begin()
{
    return data(0);
}

template<typename T>
typename Buffer<T>::const_iterator Buffer<T>::begin() const
{
    return data(0);
}

template<typename T>
typename Buffer<T>::const_iterator Buffer<T>::cbegin() const
{
    return data(0);
}

template<typename T>
typename Buffer<T>::iterator Buffer<T>::end()
{
    return data(_size);
}

template<typename T>
typename Buffer<T>::const_iterator Buffer<T>::end() const
{
    return data(_size);
}

template<typename T>
typename Buffer<T>::const_iterator Buffer<T>::cend() const
{
    return data(_size);
}

template<typename T>
std::size_t Buffer<T>::size() const {
    return _size;
}

} // namespace panda
} // namespace ska
