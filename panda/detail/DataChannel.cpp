/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataChannel.h"
#include "panda/Error.h"
#include "panda/Log.h"

#include <utility>

namespace ska {
namespace panda {

template<typename DataType>
DataChannel<DataType>::DataChannel(DataSwitchConfig const& config)
    : _config(config)
    , _handle_count(0)
{
}

template<typename DataType>
DataChannel<DataType>::~DataChannel()
{
    for(auto& info : _channel_info)
    {
        info.second._handlers.clear();
    }
    // wait until all instances of the handler are complete
    while(_handle_count !=0) {
        for(auto& info : _channel_info)
        {
            info.second._engine.poll_one();
        }
    }
}

template<typename DataType>
template<typename ChannelHandlerType>
typename DataChannel<DataType>::DataChannelHandler& DataChannel<DataType>::add(ChannelId const& channel_id, ChannelHandlerType handler)
{
    // create the channel if it doesn't already exist
    auto it = _channel_info.find(channel_id);
    if(it == _channel_info.end()) {
        it = _channel_info.emplace(std::make_pair(channel_id, ChannelInfo(_config.engine(channel_id)))).first;
        if(_config.is_active(channel_id))  { activate(channel_id); }
        else { deactivate(channel_id); }
    }

    ChannelInfo& info = it->second;
    HandleWrapperType wrapper( new HandlerWrapper(std::move(handler)), [this](HandlerWrapper* data)
                                                                     {
                                                                        delete data;
                                                                        --(this->_handle_count);
                                                                     } );
    ++_handle_count;
    info._handlers.emplace_back(std::move(wrapper));
    return *info._handlers.back();
}

template<typename DataType>
void DataChannel<DataType>::deactivate(ChannelId const& channel_id)
{
    auto it = _channel_info.find(channel_id);
    if( it != _channel_info.end() ) {
        it->second._active = false;
    }
}

template<typename DataType>
void DataChannel<DataType>::activate(ChannelId const& channel_id)
{
    auto it = _channel_info.find(channel_id);
    if(it == _channel_info.end()) {
        panda::Error e("unknown channel : '");
        e << channel_id << "'";
        throw e;
    }

    ChannelInfo& info = it->second;
    info._active = true;
}

template<typename DataType>
bool DataChannel<DataType>::is_active(ChannelId const& channel_id) const
{
    auto it = _channel_info.find(channel_id);
    return ( it != _channel_info.end()) && it->second._active;
}

template<typename DataType>
void DataChannel<DataType>::send(ChannelId const& channel_id, DataType& data)
{
    send(channel_id, data.shared_from_this());
}

template<typename DataType>
void DataChannel<DataType>::send(ChannelId const& channel_id, std::shared_ptr<DataType> const& data)
{
    auto info_it = _channel_info.find(channel_id);
    if(info_it != _channel_info.end()) {
        ChannelInfo & info = info_it->second;
        if(!info._active) return;
        for(HandleWrapperType& h : info._handlers) {
            info._engine.post([h, data]() mutable { (*h)(*data); } );
        }
    }
}

template<typename DataType>
ProcessingEngine& DataChannel<DataType>::engine(ChannelId const& channel_id)
{
    auto info_it = _channel_info.find(channel_id);
    if(info_it != _channel_info.end()) {
        return info_it->second._engine;
    }
    throw panda::Error("channel does not exist");
}

} // namespace panda
} // namespace ska
