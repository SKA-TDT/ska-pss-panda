/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TypeTraits.h"
#include <type_traits>

namespace ska {
namespace panda {
namespace {

template<typename T>
using has_active_t = decltype(std::declval<T&>().active(std::declval<bool>()));

template<typename T, typename Type = void>
using EnableIfHasActive = typename std::enable_if<HasMethod<T, has_active_t>::value, Type>::type;

template<class T, typename Enable=void>
struct DeactvateHelper
{
    static void exec(T&) {}
};

template<class T>
struct DeactvateHelper<T, EnableIfHasActive<T>>
{
    static void exec(T& node) {
        node.active(false);
    }
};

template<typename T>
inline
void deactivate_config(T& node)
{
    DeactvateHelper<T>::exec(node);
}

} // namespace


template<typename BaseType, typename ConfigModuleT, typename ConfigModuleT2, typename... ConfigModuleTypes>
class MultipleConfigModule<BaseType, ConfigModuleT, ConfigModuleT2, ConfigModuleTypes...> : public MultipleConfigModule<BaseType, ConfigModuleT2, ConfigModuleTypes...>
{
        typedef MultipleConfigModule<BaseType, ConfigModuleT2, ConfigModuleTypes...> BaseT;

    public:
        template<typename... BaseTypeArgs>
        MultipleConfigModule(BaseTypeArgs&&... args)
            : BaseT(std::forward<BaseTypeArgs>(args)...)
        {
            BaseT::add(_config);
        }

        template<typename ConfigType, typename std::enable_if<std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType const& config() const { return _config; }

        template<typename ConfigType, typename std::enable_if<std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType& config() { return _config; }

        template<typename ConfigType, typename std::enable_if<!std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType const& config() const { return BaseT::template config<ConfigType>(); }

        template<typename ConfigType, typename std::enable_if<!std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType& config() { return BaseT::template config<ConfigType>(); }

        void deactivate_all() {
            deactivate_config(_config);
            BaseT::deactivate_all();
        }

    private:
        ConfigModuleT _config;
};

template<typename BaseType, typename ConfigModuleT>
class MultipleConfigModule<BaseType, ConfigModuleT> : public MultipleConfigModule<BaseType>
{
        typedef MultipleConfigModule<BaseType> BaseT;

    public:
        template<typename... BaseTypeArgs>
        MultipleConfigModule(BaseTypeArgs&&... args)
            : BaseT(std::forward<BaseTypeArgs>(args)...)
        {
            BaseT::add(_config);
        }

        template<typename ConfigType, typename std::enable_if<std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType const& config() const { return _config; }

        template<typename ConfigType, typename std::enable_if<std::is_same<ConfigType, ConfigModuleT>::value, bool>::type = true>
        ConfigType& config() { return _config; }

        void deactivate_all() {
            deactivate_config(_config);
        }

    private:
        ConfigModuleT _config;
};

template<typename BaseT>
class MultipleConfigModule<BaseT> : public BaseT
{
    public:
        template<typename... BaseTypeArgs>
        MultipleConfigModule(BaseTypeArgs&&... args)
            : BaseT(std::forward<BaseTypeArgs>(args)...)
        {
        }

};

} // namespace panda
} // namespace ska
