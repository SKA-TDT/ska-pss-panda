/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace panda {
namespace packet_stream {


template<typename ContextT>
template<typename... ConfigArgs>
ContextBlock<ContextT>::ContextBlock(panda::Engine& engine, ChunkFactory& factory
                                    , std::size_t number_of_contexts
                                    , ConfigArgs&&... args)
    : _chunks(number_of_contexts)
    , _next(this)
    , _purge(this)
    , _factory(factory)
    , _engine(&engine)
    , _allowed_missing_packets(number_of_contexts)
    , _live_contexts(number_of_contexts)
{
    // setup the contexts
    _contexts.reserve(number_of_contexts);
    _chunks[0].reset(new ChunkHolderType(*_engine, std::forward<ConfigArgs>(args)...));
    auto clean_up_lambda = [this](ContextType* ctx) {
                               delete ctx;
                               --_live_contexts;
                           };
    _contexts.emplace_back(std::shared_ptr<ContextType>(new ContextType(*this), clean_up_lambda));
    for(std::size_t i=1; i<number_of_contexts; ++i)
    {
        _chunks[i].reset(new ChunkHolderType(*_chunks[i-1]));
        _contexts.emplace_back(std::shared_ptr<ContextType>(new ContextType(*_contexts[i-1]), clean_up_lambda));
    }
    _contexts[number_of_contexts-1]->_last=true;
    activate();
}

template<typename ContextT>
ContextBlock<ContextT>::~ContextBlock()
{
    destroy_contexts();
}

template<typename ContextT>
void ContextBlock<ContextT>::destroy_contexts()
{
    for(auto& ctx : _contexts) {
        ctx.reset();
    }
    // wait for all ctxs to go out of scope before we shutdown
    while(_live_contexts) {
        _engine->poll_one();
    }
}

template<typename ContextT>
void ContextBlock<ContextT>::missing_packets_before_realign(LimitType num)
{
    _allowed_missing_packets = num;
}

template<typename ContextT>
typename ContextBlock<ContextT>::LimitType ContextBlock<ContextT>::missing_packets_before_realign() const
{
    return _allowed_missing_packets;
}


template<typename ContextT>
void ContextBlock<ContextT>::purge_cadence(LimitType cadence)
{
    for(std::size_t i=0; i<_contexts.size(); ++i) {
        _contexts[i]->purge_cadence(cadence);
    }
}

template<typename ContextT>
typename ContextBlock<ContextT>::ChunkHolderType& ContextBlock<ContextT>::next_chunk_holder() const
{
    std::lock_guard<std::mutex> lock(_chunk_holder_mutex);
    return *_chunks[0]; // TODO get rid of this method
}

template<typename ContextT>
ContextT* ContextBlock<ContextT>::front() const
{
    return &*_contexts[0];
}

template<typename ContextT>
template<typename PostJob>
void ContextBlock<ContextT>::post(PostJob&& job)
{
    _engine->post(std::forward<PostJob>(job));
}

template<typename ContextT>
void ContextBlock<ContextT>::engine(panda::Engine& engine)
{
    _engine = &engine;
}

template<typename ContextT>
void ContextBlock<ContextT>::next_block(ContextBlock& block)
{
    _next=&block; 
    // join contexts between blocks
    _contexts.back()->_next = &*block._contexts.front();
    assert(block._chunks.size()>0);
    assert(_chunks.size()>0);
    block._chunks.front()->prev(*_chunks.back());;
}

template<typename ContextT>
void ContextBlock<ContextT>::prev_block(ContextBlock& block)
{
    // join contexts between blocks
    _purge = &block;
    _contexts.front()->_prev = &*block._contexts.back();
    assert(block._chunks.size()>0);
    assert(_chunks.size()>0);
    _chunks.front()->prev(*block._chunks.back());
}

template<typename ContextT>
void ContextBlock<ContextT>::next()
{
    // mark next block ready for use
    if(_full) return;
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if(_full) return;
        _next->activate();
        _full = true; // must be last thing
    }
}

template<typename ContextT>
std::size_t ContextBlock<ContextT>::size() const
{
    return _contexts.size();
}

template<typename ContextT>
void ContextBlock<ContextT>::activate()
{
    _full = false;
    for(auto const& context : _contexts)
    {
        context->recycle();
    }
}

} // namespace packet_stream
} // namespace panda
} // namespace ska
