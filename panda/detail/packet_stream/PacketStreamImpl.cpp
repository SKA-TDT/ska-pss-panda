/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Error.h"
#include "panda/Log.h"
#include "panda/Compiler.h"
#include <iostream>
#include <algorithm>


namespace ska {
namespace panda {
namespace packet_stream {

template<typename EngineType>
static
std::size_t default_number_of_buffers(EngineType const&) {
    return 3;
}

template<>
PANDA_SUPPRESS_NOT_USED_WARNING std::size_t default_number_of_buffers<panda::ProcessingEngine>(panda::ProcessingEngine const& engine) {
    return engine.number_of_threads();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
template<typename... ConnectionArgs>
PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::PacketStreamImpl(DerivedType& derived, DataTraits const& chunk_data, ConnectionArgs&&... args)
    : _connection(std::forward<ConnectionArgs>(args)...)
    , _engine(_connection.engine())
    , _context_mgr(_engine, panda::packet_stream::detail::ContextDataFactory<SelfType, DataTraits>(&derived), 80, 10, chunk_data)
    , _post_thread(0)
{
    init_buffers();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
template<typename... ConnectionArgs>
PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::PacketStreamImpl(
    DerivedType& derived, ConnectionArgsTag, ConnectionArgs&&... args)
    : _connection(std::forward<ConnectionArgs>(args)...)
    , _engine(_connection.engine())
    , _context_mgr(_engine, panda::packet_stream::detail::ContextDataFactory<SelfType, DataTraits>(&derived), 80, 10)
    , _post_thread(0)
{
    init_buffers();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::~PacketStreamImpl()
{
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::init_buffers()
{
    std::size_t num_buffers = std::max(std::size_t(1UL), default_number_of_buffers<typename ConnectionTraits::EngineType>(_engine)); // at least one
    _max_bmgr = num_buffers;
    _buffer_mgr.reserve(num_buffers);
    for(std::size_t i=0; i< num_buffers; ++i) {
        _buffer_mgr.emplace_back(std::unique_ptr<ResourceManager<BufferType>>(new ResourceManager<BufferType>(_engine)));
        for(unsigned j=0; j < 3; ++j) { // 1 each for wait, process, fill
            _buffer_mgr[i]->add_resource(std::move(BufferType(sizeof(PacketType))));
        }
    }
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::stop()
{
    std::lock_guard<std::mutex> lock(_halt_mutex);
    if(!_halt) { // stop() must not exit until the stream has actually stopped
        _connection.close();
    }

    // wait for all buffer managers to return their resources
    // i.e. only purges can be in progress
    for(auto& buf_mgr : _buffer_mgr ) {
        buf_mgr->stop();
    }

    // ensure any unfilled chunks are released
    if(!_halt) {
        _halt = true;
        std::this_thread::yield(); // give other threads chance to catch up
        _context_mgr.release();
    }

    PANDA_LOG << "stop: shut down packet stream";
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::number_of_buffers(unsigned n)
{
    while( n < _buffer_mgr.free_resources()) {
        _buffer_mgr.add_resource(std::move(BufferType(sizeof(PacketType))));
    }
    PANDA_LOG << "number of ingest buffers: " << n;
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::start()
{
    _halt = false;
    PANDA_LOG << "start packet stream listening on:" << _connection.socket().local_endpoint();
    start_chunk();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::set_remote_end_point(typename ConnectionTraits::EndPointType const& endpoint)
{
    _connection.set_remote_end_point(endpoint);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::set_remote_end_point(IpAddress const& endpoint)
{
    _connection.set_remote_end_point(endpoint);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
typename ConnectionTraits::EndPointType PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::local_end_point() const
{
    return _connection.socket().local_endpoint();
}

namespace detail {

    template<typename EngineType>
    void do_run_engine(EngineType& e) {
        e.poll_one();
    }

    template<>
    void do_run_engine<panda::ProcessingEngine>(panda::ProcessingEngine& e); // impl in src/PacketStreamImpl.cpp

} // namespace detail

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
bool PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::process()
{
    detail::do_run_engine<typename ConnectionTraits::EngineType>(_engine);
    return false;
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::missing_packets_before_realign(LimitType num)
{
    _context_mgr.missing_packets_before_realign(num);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::purge_cadence(LimitType num)
{
    _context_mgr.purge_cadence(num);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::init()
{
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::start_chunk()
{
    if(!_halt) {
        // round robin assignment to buffer managers toreduce lock wait
        //std::size_t post_thread = (++_post_thread)%_max_bmgr;
        auto& bm = *_buffer_mgr[0]; // TODO find out why we often drop packets if we switch buffer managers (packet dropped is at switch e.g 4,7)
                                    // hence use only one buffer mamanger for now
        bm.async_request(
                [this] (ResourceBufferType buffer)
                {
                    try {
                        KeepAliveContextPtr keep_alive = std::make_shared<KeepAliveContext>(this, std::move(buffer));
                        _connection.receive(
                              keep_alive->buffer()
                            , std::bind(&SelfType::get_packet, this, keep_alive
                                , std::placeholders::_1
                                , std::placeholders::_2
                                , 0
                                , std::placeholders::_3)
                              ); // end std::bind
                    }
                    catch(panda::Error const& e) {
                        // cancellations can occur when shutting down - ignore them
                    }
                    catch(...) {
                        PANDA_LOG_ERROR << "unexpected error";
                    }
                });
    }
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::get_packet(
            KeepAliveContextPtr self
          , panda::Error const& ec
          , std::size_t bytes_read
          , std::size_t total_bytes_read
          , typename PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::BufferType buffer
          )
{
    try {
        if(!ec) {
            total_bytes_read += bytes_read;
            // ensure we have a full datagram to process
            if(total_bytes_read < sizeof(PacketType) && !_halt) {
                // n.b. we use std::bind as lambdas do not yet support move semantics
                _connection.socket().async_receive( boost::asio::buffer(buffer.data(total_bytes_read), sizeof(PacketType) - total_bytes_read),
                      std::bind(&SelfType::get_packet, this, self, std::placeholders::_1, std::placeholders::_2, total_bytes_read, std::move(buffer)));
                      return;
            }
            else {
                // process the datagram
                PacketInspectorType packet(*reinterpret_cast<PacketType const*>(buffer.data()));
                //PANDA_LOG_DEBUG << "process_packet job posted for packet=" << _context_mgr.context().sequence_number(packet) << _context_mgr.context();
                _engine.post(std::bind(&PacketStreamImpl::process_packet, this, std::move(self), std::move(buffer)));
                start_chunk();
                return;
            }
        } else {
            _connection.policy().error(ec);
        }
    }
    catch(std::exception const& e) {
        _connection.policy().error(e);
    }
    catch(...) {
        _connection.policy().error(panda::Error("PacketStreamImpl: unknown exception"));
    }
    // schedule the next packet read
    start_chunk();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerDerivedType>
void PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerDerivedType>::process_packet(KeepAliveContextPtr self, BufferType buffer)
{
    PacketInspectorType packet(*reinterpret_cast<PacketType const*>(buffer.data()));
    if(packet.ignore()) return;

    Context* new_context = _context_mgr.get_context(packet);
    if(new_context) {
        // attempt to deserialize the existing buffer in to a packet
        // only if we have somewhere to store the data
        // normal ops
        new_context->deserialise_packet(packet);
    }
    self.reset(); // allow buffer to go out of scope
}

} // namespace packet_stream
} // namespace panda
} // namespace ska
