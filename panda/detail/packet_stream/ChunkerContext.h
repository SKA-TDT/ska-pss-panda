/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PACKET_STREAM_CHUNKERCONTEXT_H
#define SKA_PANDA_PACKET_STREAM_CHUNKERCONTEXT_H

#include "ChunkHolder.h"
#include "panda/concepts/ChunkerContextDataTraitsConcept.h"
#include <memory>
#include <forward_list>
#include <mutex>
#include <utility>
#include <ostream>

namespace ska {
namespace panda {
namespace packet_stream {
/**
 * @brief
 *    A context for use in chucked stream readers, where the packet has an indicator of ordering.
 *    In particular it contains:
 *    - a reference to a single packet
 *    - the matching chunk to be filled
 *    - offset information of the where the data from the packets payload fits within the chunk
 *
 * @tparam DataTraits : The Data class that defines the payload context. An object of this type will be created if it has any members
 *        The traits class should implement the methods:
 *
 *                      LimitType packet_size() const;  // the data expected to be extracted from a packet
 *                      LimitType chunk_size(DataType const&) const;  // QuotiantType & RemainderType = unsigned, size_t etc.
 *                      LimitType max_sequence_number()
 *                      void process_missing_slice(ContextType);
 *                      void packet_stats(LimitType packets_received, LimitType packets_expected);
 *
 *                  the DataTraits must also typdef a suitable class as PacketInspector. This class must support:
 *
 *                     Consrtuctor(PacketInspector const&)
 *                     bool ignore();    // early stage check to avoid wasting time processing
 *
 *
 * @tparam ChunkFactory : functor to create a new chunk (shared_ptr)
 *
 * @details
 * @code
 *    [ packet ][ packet ][ packet ] ... incoming packets
 *    [    chunk    ][    chunk    ] ... data chunks to fill
 *    [  ctx   ][ctx][ctx][  ctx   ] ... a context for each packet and chunk boundary and contain a reference/pointer to both
 * @endcode
 *
 *    This context manages the lifetime of a chunk of data whilst it is being filled after its creation by the DataManager.
 *    Chunks are not released to the pipeline for processing until they are marked as full.
 */

// The Base class is used to allow us to specialise for cases where there are data memebrs in the DataTraits class
template<typename DerivedType, typename DataTraits, bool is_empty>
class ChunkerContextBase : public std::enable_shared_from_this<DerivedType>
{
        //BOOST_CONCEPT_ASSERT((ChunkerContextDataTraitsConcept<DataTraits>));

    protected:
        typedef ChunkHolder<DataTraits> ChunkHolderType;
        typedef typename ChunkHolderType::LimitType LimitType;

    protected:
        ChunkerContextBase(ChunkHolderType* chunk)
            : _chunk_traits(chunk)
        { assert(_chunk_traits != nullptr); }

    public:

        /**
         * @brief if the DataTraits has any members then an object of that tyoe will be instantiated
         * @returns the reference to this object
         */
        DataTraits& data();
        DataTraits const& data() const { return _chunk_traits->data(); }

        /**
         * @brief call to deserialize a packet (forwarded to the DataTraits class)
         */
        void deserialise_packet(typename DataTraits::PacketInspector const&);

        /**
         * @brief call to handle missing packets
         */
        void process_missing_slice();

        /**
         * @brief call to handle packet counting
         */
        void packet_stats(LimitType packets_per_chunk, LimitType purged);

        /**
         * @brief call to indicate that a resize of the datachunk is required
         */
        void resize_chunk();

    protected:
        ChunkHolderType* _chunk_traits;
};

// specialization where DataTraits has no members therfore no data() method can be provided
template<typename DerivedType, typename DataTraits>
class ChunkerContextBase<DerivedType, DataTraits, true> : public std::enable_shared_from_this<DerivedType>
{
    protected:
        typedef ChunkHolder<DataTraits> ChunkHolderType;
        typedef typename ChunkHolderType::LimitType LimitType;

    protected:
        ChunkerContextBase(ChunkHolderType* chunk)
            : _chunk_traits(chunk)
        {}

        /**
         * @brief call to deserialize a packet (forwarded to the DataTraits class)
         */
        void deserialise_packet(typename DataTraits::PacketInspector const&);

        /**
         * @brief call to process the data
         */
        void process_missing_slice();

        /**
         * @brief call to handle packet loss info
         */
        void packet_stats(LimitType packets_per_chunk, LimitType purged);

        /**
         * @brief call to indicate that a resize of the datachunk is required
         */
        void resize_chunk();

    protected:
        //std::shared_ptr<ChunkHolderType> _chunk; // self memory managing object
        ChunkHolderType* _chunk_traits;
};

template<typename>
class ContextBlock;

template<typename DataTraitsT, typename ChunkFactoryT>
class ChunkerContext : public ChunkerContextBase<ChunkerContext<DataTraitsT, ChunkFactoryT>, DataTraitsT, std::is_empty<DataTraitsT>::value>
{
    private:
        typedef ChunkerContext<DataTraitsT, ChunkFactoryT> SelfType;
        typedef ChunkerContextBase<SelfType, DataTraitsT, std::is_empty<DataTraitsT>::value> BaseT;
        friend BaseT;

    public:
        typedef DataTraitsT DataTraits;
        typedef typename DataTraits::PacketInspector PacketInspector;
        typedef ChunkFactoryT ChunkFactory;

    private:
        /**
         * @brief not strictly a copy, except the data chunk & chunk number os the same as the original
         * @detail ncreases the offset by the packet_size, does not make the passed context the prev member.
        ChunkerContext(ChunkerContext const&);
         */
        ChunkerContext(ChunkerContext const&) = delete;

    protected:
        friend class ContextBlock<ChunkerContext>;

    public:
        typedef typename DataTraits::DataType DataType;
        typedef typename BaseT::ChunkHolderType::LimitType LimitType;
        typedef typename BaseT::ChunkHolderType::PacketSizeType PacketSizeType;
        typedef double PacketPerChunkType;

    public:
        ChunkerContext(ContextBlock<ChunkerContext>& mgr);

    protected:
        // don't let contexts be copied as they have lots of dynamic info that needs to be managed
        ChunkerContext(ChunkerContext& prev);

    public:
        ~ChunkerContext();

        /**
         * @brief return true if the packet number is before that of the other
         */
        bool operator<(ChunkerContext const& other) const;

        /**
         * @brief return the context block associated with the packet provided
         * @details this is the primary method for accessing the correct context
         * if the context has already gone out of scope then a nullptr will be returned
         */
        SelfType* get_context(typename DataTraits::PacketInspector const& packet);

        template<typename... Args>
        SelfType* get_context(LimitType sequence_number, typename DataTraits::PacketInspector const& packet, Args&&... chunk_construction_args);

        /**
         * @brief function to create the initial context
         * @details MUST call init() before using the returned context
         */
        template<typename... DataArgs>
        static std::shared_ptr<SelfType> create(ChunkFactory factory, DataArgs&& ...);

        /**
         * @brief return the chunk data associated with this context
         */
        std::shared_ptr<DataType> const& chunk();
        std::shared_ptr<DataType> const& chunk() const;

        /**
         * @brief call to find the context sequence number (forwarded to the DataTraits class)
         */
        LimitType sequence_number(typename DataTraits::PacketInspector const&);

        /**
         * @brief call to find the context sequence number (forwarded to the DataTraits class)
         */
        PacketSizeType size() const;

        /**
         * @brief call to find if the packet should be processed during alignment
         */
        inline
        bool align_packet(typename DataTraits::PacketInspector const&);

        /**
         * @brief return true if deserialise_packet() or process_missing_slice() has been called
         */
        bool complete() const;

        /**
         * @brief release all contexts associted with the current packet
         *        @returns the next packet context
         *        the context will be in an undefined state after this call and the returned value
         *        should be used for all subsequent operations
         */
        SelfType& deserialise_packet(typename DataTraits::PacketInspector const& packet);

        /**
         * @brief return the position of the data offset to write to in the chunk
         * @details this will be either a reference to the offset of the packet start value from the chunk start
         *          or in the case of sequential access (via next()) the next location to be written to.
         */
        inline PacketSizeType offset() const;

        /**
         * @brief return the position of the offset from the beginning of the packet that this ctx refers to
         */
        inline PacketSizeType packet_offset() const;

        /**
         * @brief return the packet number that this ctx is assoiciated with
         */
        LimitType packet_number() const;

        /// write out a debug string to the stream provided
        void debug_string(std::ostream& os) const;

        /// return the context inmmediately previous to this one
        ChunkerContext* prev() const;

        // return true if the context is configured for use
        bool is_ready() const;

        /// releases the current chunk, resizing it if necessary with this as the last valid context
        void release();

        inline PacketSizeType packet_size() const;

    protected:
        /**
         * @brief returns the next logical context block
         * @details when finished, you should call release on the block
         *          to free all references
         */
        template<typename... Args>
        SelfType& next(Args&&... args);

        // purge and release chunk to DataManager
        void clear_chunk();

        // purge and release chunk to DataManager and call
        // DataTraits::resize(ChunkerContext&) to adjust chunk to reduced data received
        void clear_chunk_resize();

        /**
         * @brief returns the context based on the underlying chunk number and offset
         * @details lower level than the public version
         */
        template<typename... Args>
        SelfType* get_context_internal( LimitType packet_number, Args&&... chunk_args);
        LimitType max_sequence_number() const;

        /**
         * @brief number of packets required to completely fill a chunk
         */
        inline PacketPerChunkType packets_per_chunk() const;

        /**
    `    * @brief set the delay (in number of contexts) to wait before purgeing
         * @param cadence the distance between this ctx and the block to be purged (where 0=purge previous block)
         */
        void purge_cadence(LimitType cadence);

        /**
         * @brief return the highest packet numbered context available (in DataReady state)
         */
        SelfType* last();

    private:
        // return true if less_than other
        bool less_than(LimitType const& other) const;
        std::pair<bool,LimitType> greater_than(LimitType const& other) const;
        void increment_packet_number(LimitType&);

        template<typename... ChunkArgs>
        void reset(ChunkerContext& prev, ChunkArgs&&... args);

        // finds the next unset chunk and realigns it the packetnumber
        template<typename... ChunkArgs>
        SelfType* next_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& packet, ChunkArgs&&... args);

        // if uninitialised will initialis it with new values realigned to sequence_number
        // return true if state change has occured
        template<typename... ChunkArgs>
        bool init_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& p, ChunkArgs&&... args);

        /**
         * @brief copy the chunk and associated meta data, whilst increasing the offset
         */
        void copy_chunk(ChunkerContext const&);

        // flushes all previous elements to be processed with the process_missing_slice() handler
        LimitType purge();

        // purges just this chunk(i.e call process_missing_slice() if necessary) without any regression
        void purge_one();

        // blocks until context is initialised with a data chunk ready for use
        template<typename... ChunkArgs>
        void init(ChunkArgs&&... chunk_construction);

        inline void set_prev(std::shared_ptr<SelfType>);
        inline void reset_offset();

        template<typename... ChunkArgs>
        SelfType* do_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& packet, ChunkArgs&&... args);

        void recycle();

        void reset_next(SelfType& next);
        void reset_prev(SelfType& next);

        SelfType* get_prev(LimitType packet_number);

        inline DataType* chunk_ptr();

    private:
        // args passed to chunk factory
        template<typename... ChunkArgs>
        inline void new_chunk(PacketInspector const&, ChunkArgs&&...);

        // resets the packet, but will not create a new DataChunk if required
        // return true if a new chunk has been created
        bool partial_reset();

        // get a next chunk without creating any new DataChunk
        ChunkerContext& next_preview();

        template<typename... ChunkArgs>
        void setup_next_chunk_locked(ChunkArgs&&... args);

        void set_first_offset_new_chunk(PacketSizeType next_offset);

        // setup a new contecxt for an existing chunk
        void setup_same_chunk();

        inline bool data_written() const;

    private:
        enum class Status { Empty, Reset, DataReady, Complete, Purged };
        std::string static status_string(Status const& status);

    private:
        ContextBlock<ChunkerContext>& _mgr;
        ChunkerContext* _next;
        ChunkerContext* _prev;
        ChunkerContext* _purge;
        const bool _first;
        bool _last;

        std::mutex _data_mutex;
        std::mutex _realign_mutex;
        std::mutex _reset_mutex;
        bool _is_aligned;

        LimitType _purge_count;

        LimitType _packet_number;
        PacketSizeType _packet_offset;
        LimitType _aligned_packet_number;
        PacketPerChunkType _packets_per_chunk;
        PacketSizeType _size;           // size of this context
        PacketSizeType _chunk_size;
        PacketSizeType _offset;         // offset from start of the chunk
        PacketSizeType _first_offset;   // the position corresponding to the start of a packet (i.e. _offset of the 1st ctx of that packet)
        bool      _realigned;           // indicates if a realign has taken place on this context
        Status    _status;
        bool      _requested;
};

template<typename BaseT>
class CurrentChunkContext : protected BaseT
{
    public:
        typedef typename BaseT::LimitType LimitType;
        typedef typename BaseT::DataType DataType;

    public:
        /**
         * @brief return the chunk data associated with this context
         */
        DataType& chunk() const;

        /// write out a debug string to the stream provided
        void debug_string(std::ostream& os) const;
};

// Type passed to process_missing_slice
template<typename BaseT>
class MissingPacketContext : public CurrentChunkContext<BaseT>
{
    public:
        typedef typename BaseT::LimitType LimitType;
        typedef typename BaseT::PacketSizeType PacketSizeType;
        typedef typename BaseT::DataType DataType;

    public:
        /**
         * @brief call to find the size of the current context
         */
        PacketSizeType size() const;

        /**
         * @brief return the position of the data offset to write to in the chunk
         * @details this will be either a reference to the offset of the packet start value from the chunk start
         *          or in the case of sequential access (via next()) the next location to be written to.
         */
        inline PacketSizeType offset() const;

        /**
         * @brief return the position of the offset to read from in the packet.
         * @details note in the case of missing packet, this will be indicative.
         */
        inline PacketSizeType packet_offset() const;

        /**
         * @brief return the packet number that this ctx is associated with
         */
        LimitType packet_number() const;

        /**
         * @brief return the size of the packet (from the DataTraits)
         */
        PacketSizeType packet_size() const;
};

template<typename BaseT>
class ResizeChunkContext final : public CurrentChunkContext<BaseT>
{
    public:
        typedef typename BaseT::LimitType LimitType;
        typedef typename BaseT::PacketSizeType PacketSizeType;
        typedef typename BaseT::DataType DataType;

    public:
        /**
         * @brief call to find the expected size of the chunk
         */
        PacketSizeType size() const;
};

template<typename BaseT>
class DeserialisePacketContext final : public MissingPacketContext<BaseT>
{
    public:
        typedef typename BaseT::LimitType LimitType;
        typedef typename BaseT::PacketSizeType PacketSizeType;
        typedef typename BaseT::DataType DataType;
        typedef typename BaseT::PacketInspector PacketInspector;

    public:
        /**
         * @brief call to find the context sequence number
         */
        LimitType sequence_number(PacketInspector const&);
};

template<typename DataTraits, typename ChunkFactory>
std::ostream& operator<<(std::ostream& os, ChunkerContext<DataTraits, ChunkFactory> const& ctx);

template<typename BaseT>
std::ostream& operator<<(std::ostream& os, CurrentChunkContext<BaseT> const& ctx);

} // namespace packet_stream
} // namespace panda
} // namespace ska
#include "panda/detail/packet_stream/ChunkerContext.cpp"

#endif // SKA_PANDA_PACKET_STREAM_CHUNKERCONTEXT_H
