/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ChunkerContext.h"
#include "IoFormatting.h"
#include "panda/Log.h"
#include "panda/detail/packet_stream/ContextBlock.h"
#include <cmath>
#include <iostream>
#include <iomanip>

namespace ska {
namespace panda {
namespace packet_stream {

template<typename DerivedType, typename DataTraits, bool is_empty>
void ChunkerContextBase<DerivedType, DataTraits, is_empty>::deserialise_packet(typename DataTraits::PacketInspector const& packet)
{
    data().deserialise_packet(reinterpret_cast<DeserialisePacketContext<DerivedType>&>(*this), packet);
}

template<typename DerivedType, typename DataTraits>
void ChunkerContextBase<DerivedType, DataTraits, true>::deserialise_packet(typename DataTraits::PacketInspector const& packet)
{
    DataTraits::deserialise_packet(reinterpret_cast<DeserialisePacketContext<DerivedType>&>(*this), packet);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
void ChunkerContextBase<DerivedType, DataTraits, is_empty>::process_missing_slice()
{
    data().process_missing_slice(reinterpret_cast<MissingPacketContext<DerivedType>&>(*this));
}

template<typename DerivedType, typename DataTraits>
void ChunkerContextBase<DerivedType, DataTraits, true>::process_missing_slice()
{
    DataTraits::process_missing_slice(reinterpret_cast<MissingPacketContext<DerivedType>&>(*this));
}

template<typename DerivedType, typename DataTraits, bool is_empty>
void ChunkerContextBase<DerivedType, DataTraits, is_empty>::packet_stats(LimitType packets_per_chunk, LimitType purged)
{
    data().packet_stats(purged, packets_per_chunk);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
DataTraits& ChunkerContextBase<DerivedType, DataTraits, is_empty>::data()
{
    assert(_chunk_traits); return _chunk_traits->data();
}

template<typename DerivedType, typename DataTraits>
void ChunkerContextBase<DerivedType, DataTraits, true>::packet_stats(LimitType packets_per_chunk, LimitType purged)
{
    DataTraits::packet_stats(purged, packets_per_chunk);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
void ChunkerContextBase<DerivedType, DataTraits, is_empty>::resize_chunk()
{
    data().resize_chunk(reinterpret_cast<ResizeChunkContext<DerivedType>&>(*this));
}

template<typename DerivedType, typename DataTraits>
void ChunkerContextBase<DerivedType, DataTraits, true>::resize_chunk()
{
    DataTraits::resize_chunk(reinterpret_cast<ResizeChunkContext<DerivedType>&>(*this));
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>::ChunkerContext(ContextBlock<ChunkerContext>& mgr)
    : BaseT(&mgr.next_chunk_holder())
    , _mgr(mgr)
    , _prev(nullptr)
    , _purge(nullptr)
    , _first(true)
    , _last(false)
    , _packet_number(0)
    , _packet_offset(0)
    , _aligned_packet_number(0)
    , _size(0)
    , _first_offset(0)
{
    recycle();
    reset_offset();
    _is_aligned = false;
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>::ChunkerContext(ChunkerContext& prev)
    : BaseT(prev)
    , _mgr(prev._mgr)
    , _prev(&prev)
    , _purge(_prev)
    , _first(false)
    , _last(true)
    , _packet_number(0)
    , _packet_offset(0)
    , _aligned_packet_number(0)
    , _size(0)
    , _first_offset(0)
{
    recycle();
    prev._last=false;
    prev._next = this;
    reset_offset();
    _is_aligned = false;
    assert(this->_chunk_traits!=nullptr);
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>::~ChunkerContext()
{
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::copy_chunk(ChunkerContext<DataTraits, ChunkFactory> const& other)
{
    BaseT::operator=(other);
    _is_aligned = other._is_aligned;
    _aligned_packet_number=other._aligned_packet_number;
    _packets_per_chunk=other._packets_per_chunk;
    _chunk_size=other._chunk_size;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::PacketSizeType ChunkerContext<DataTraits, ChunkFactory>::packet_size() const
{
    return this->_chunk_traits->packet_size();
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::purge_one()
{
    // check if purge needed
    switch(_status) {
        case Status::Empty:
            return;
        case Status::Complete:
            return;
        case Status::Purged:
            return;
        default:
            break;
    };
    // need to purge
    std::lock_guard<std::mutex> lock(_data_mutex);
    if(_status >= Status::Complete) return;

    assert(_status==Status::DataReady);
    this->process_missing_slice();
    if(_prev->_chunk_traits == this->_chunk_traits) {
        _purge_count = _prev->_purge_count;
        if(_purge_count) ++_purge_count; // unless set leave as zero as marker to fetch stats
    }
    else {
        ++_purge_count;
    }
    _status = Status::Purged;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::LimitType ChunkerContext<DataTraits, ChunkFactory>::purge()
{
    // process earlier contexts first for two reasons:
    // 1) gives more time for the most likely packets to arrive
    // 2) allows purge() to block until the entire purge chain
    //    has completed when called my multiple threads.
    //    if this didn't happen then the first thread will return
    //    whilst the second is still purging contexts, instead of helping
    //    out.
    if(_status == Status::Empty) return 0;
    if(_status==Status::Purged)
    {
        if(_prev->_chunk_traits == this->_chunk_traits)
        {
            if(_purge_count==0) { // need to collect the stats
               _purge_count=_prev->_purge_count;
            }
            return _purge_count;
        }
        else {
            return 0;
        }
    }

    // purge previous chunk
    if(_prev->_chunk_traits  == this->_chunk_traits) {
        _purge_count+=_prev->purge();
    }
    // lambda to purge the previous chunk and update the purge count
    /*
    auto purge_prev = [this]()
    {
        if(_prev->_chunk_traits  == this->_chunk_traits) {
            _purge_count+=_prev->purge();
        }
    };
    */

    // now deal with this context
    {
        // lock scope
        std::unique_lock<std::mutex> lock(_data_mutex, std::defer_lock);
        if(lock.try_lock()) {
            if(_status < Status::Complete && !_requested)
            {
                assert(_status==Status::DataReady);
                this->process_missing_slice();
                if(_packet_offset == 0) ++_purge_count;
                _status = Status::Purged;
            }
            lock.unlock();
        }
        else {
            // Another proceess is completing so carry on the purge chain instead of waiting
            // ensure we don't leave until Complete
            if(_status < Status::Complete) {
                // will block until other process Completes
                std::lock_guard<std::mutex> lock(_data_mutex);
                assert(_status>=Status::Complete);
            }
        }
    } // end lock scope
    return _purge_count;
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>& ChunkerContext<DataTraits, ChunkFactory>::deserialise_packet(typename DataTraits::PacketInspector const& packet)
{
    // check status and get lock
    if(_status >= Status::Complete) return *_next; //quick exit
    std::lock_guard<std::mutex> lock(_data_mutex);
    if(_status >= Status::Complete) return *_next; // have things changed since lock aquired?
    assert(_status >= Status::DataReady);

    if(_last) _mgr.next();
    bool new_chunk=false;
    if(_next->_status == Status::Empty) {
        // called before the deserialise to determine size of ctx
        new_chunk=_next->partial_reset();
    }
    BaseT::deserialise_packet(packet);
    _status = Status::Complete; // should be after anything that accesses the chunk
    ChunkerContext* next_ctx=_next;
    if(next_ctx->_packet_number == _packet_number) {
        next_ctx=&next(packet).deserialise_packet(packet);
    }
    if(new_chunk) {
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
        PANDA_LOG_DEBUG << "deserialise: posting clear_chunk job";
#endif // PANDA_PACKETSTREAM_LOG_DEBUG
        std::shared_ptr<ChunkerContext> self = this->shared_from_this();
        _mgr.post([self]()
                  {
                      self->clear_chunk();
                  });
    }
    return *next_ctx;
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::reset_next(SelfType& link)
{
    _next = link;
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::reset_prev(SelfType& link)
{
    _prev = link;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
void ChunkerContext<DataTraits, ChunkFactory>::new_chunk(PacketInspector const& packet_inspector, ChunkArgs&&... args)
{
    this->_chunk_traits = &_prev->_chunk_traits->next();
    assert(this->_chunk_traits);
    this->_chunk_traits->reset(_mgr.chunk_factory()(_packet_number, packet_inspector, std::forward<ChunkArgs>(args)...));
    _chunk_size = this->_chunk_traits->chunk_size();
    _packets_per_chunk = (double)_chunk_size/(double)packet_size();
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::prev() const
{
    return this->_prev;
}

template<typename DataTraits, typename ChunkFactory>
inline bool ChunkerContext<DataTraits, ChunkFactory>::is_ready() const
{
    return this->_status >= Status::Reset;
}

template<typename DataTraits, typename ChunkFactory>
std::shared_ptr<typename ChunkerContext<DataTraits,ChunkFactory>::DataType> const& ChunkerContext<DataTraits, ChunkFactory>::chunk()
{
    return this->_chunk_traits->chunk();
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits,ChunkFactory>::DataType* ChunkerContext<DataTraits, ChunkFactory>::chunk_ptr()
{
    return this->_chunk_traits->chunk().get();
}

template<typename DataTraits, typename ChunkFactory>
std::shared_ptr<typename ChunkerContext<DataTraits,ChunkFactory>::DataType> const& ChunkerContext<DataTraits, ChunkFactory>::chunk() const
{
    return this->_chunk_traits->chunk();
}

template<typename DataTraits, typename ChunkFactory>
bool ChunkerContext<DataTraits, ChunkFactory>::complete() const
{
    return this->_status >= Status::Complete;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::PacketSizeType ChunkerContext<DataTraits, ChunkFactory>::offset() const
{
    return _offset;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::PacketSizeType ChunkerContext<DataTraits, ChunkFactory>::packet_offset() const
{
    return _packet_offset;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::LimitType ChunkerContext<DataTraits, ChunkFactory>::packet_number() const
{
    return _packet_number;
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::PacketPerChunkType ChunkerContext<DataTraits, ChunkFactory>::packets_per_chunk() const
{
    return _packets_per_chunk;
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::purge_cadence(LimitType cadence)
{
    _purge=_prev;
    assert(_purge!=nullptr);
    for(LimitType i=0; i<cadence; ++i) {
        _purge=_purge->_prev;
        assert(_purge!=nullptr);
    }
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::reset_offset()
{
    _offset = 0;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
ChunkerContext<DataTraits, ChunkFactory>& ChunkerContext<DataTraits, ChunkFactory>::next(ChunkArgs&&... args )
{
    assert(_next);

    init(std::forward<ChunkArgs>(args)...);
    if(_last) {
        // prepare the next block for use
        _mgr.next();
    }

    _next->init(std::forward<ChunkArgs>(args)...);

    return *_next;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
bool ChunkerContext<DataTraits, ChunkFactory>::init_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& p, ChunkArgs&&... args)
{
    if(_realigned) return true;
    std::lock_guard<std::mutex> lock(_reset_mutex);
    if(_realigned) return true;

    if(_status<Status::DataReady) {
        // safe to realign
        std::shared_ptr<ChunkerContext> prev=this->_prev->shared_from_this();
        _mgr.post([prev]{ prev->clear_chunk_resize(); });
        do_realign(sequence_number, p, std::forward<ChunkArgs>(args)...);
        _realigned = true;
        return true;
    }

    return _realigned;;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::next_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& p, ChunkArgs&&... args)
{
    PANDA_LOG_DEBUG << "next_realign() sequence_number=" << sequence_number << *this;
    if(_last) {
        // prepare the next block for use
        _mgr.next();
    }

    // zip past all current completed contexts

    if(init_realign(sequence_number, p, std::forward<ChunkArgs>(args)...))
    {
        return this;
    }
    _next->init_realign(sequence_number, p, std::forward<ChunkArgs>(args)...);

    return _next;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
void ChunkerContext<DataTraits, ChunkFactory>::setup_next_chunk_locked(ChunkArgs&&... args)
{
    new_chunk(std::forward<ChunkArgs>(args)...);
    _status = Status::DataReady;
    _purge->purge_one(); // cleanup as we go
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::clear_chunk()
{
    // purge missing packets in the previous chunk
    PANDA_LOG_DEBUG << "clearing chunk " << (void*)this->chunk().get();
    std::unique_lock<std::mutex> lk(this->_chunk_traits->mutex(), std::defer_lock);
    if(_next->_status == Status::Empty) _next->partial_reset(); // ensure _size is set correctly for purge
    if(lk.try_lock())
    {
        std::shared_ptr<DataType> chunk = this->_chunk_traits->chunk(); // keep it in scope whilst we purge
        if(!chunk) return;
        this->packet_stats(this->packets_per_chunk(), purge());
        chunk.reset();
        this->_chunk_traits->reset(); // safe to remove chunk
    }
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::clear_chunk_resize()
{
    PANDA_LOG_DEBUG << "clearing and resizing chunk " << (void*)this->chunk().get();
    std::unique_lock<std::mutex> lk(this->_chunk_traits->mutex(), std::defer_lock);
    if(_next->_status == Status::Empty) _next->partial_reset(); // ensure _size is set correctly for purge

    if(lk.try_lock())
    {
        std::shared_ptr<DataType> chunk = this->_chunk_traits->chunk(); // keep it in scope whilst we purge
        if(!chunk) return;

        this->packet_stats(this->packets_per_chunk(), purge());
        // clean up the chunk
        this->resize_chunk();
        chunk.reset();
        this->_chunk_traits->reset(); // safe to remove chunk
    }
}


template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::release()
{
    ChunkerContext* ctx=this->last();
    PANDA_LOG_DEBUG << "release() backwards from ctx " << ctx;
    if(ctx->chunk().get()==nullptr) return;
    ctx->clear_chunk_resize();

    // block until done
    volatile bool complete;
    do {
        complete = ctx->_chunk_traits->reset_complete();
    }
    while(!complete);
}


template<typename DataTraits, typename ChunkFactory>
inline void ChunkerContext<DataTraits, ChunkFactory>::set_first_offset_new_chunk(PacketSizeType next_offset)
{
    if(_prev->_first_offset >= _prev->_chunk_size ) {
        _first_offset = _prev->_first_offset - _prev->_chunk_size;
    }
    else {
        _first_offset = next_offset - _prev->_chunk_size;
    }
    _prev->_size = _prev->_chunk_size - _prev->_offset;
    _offset =0;
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::setup_same_chunk()
{
    // next context must share the same chunk as this one, only offset changes
    this->_chunk_traits = _prev->_chunk_traits; // point to chunk traits data
    this->copy_chunk(*_prev);
    increment_packet_number(_packet_number);
    _first_offset = _prev->_first_offset;
    _prev->_size=_offset-_prev->_offset;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
void ChunkerContext<DataTraits, ChunkFactory>::reset(ChunkerContext& prev, ChunkArgs&&... args)
{
    // thread safety
    std::lock_guard<std::mutex> lock(_reset_mutex);
    if(_status >= Status::DataReady) return; // some other thread has already done the work
    if(_status == Status::Reset) {
        setup_next_chunk_locked(std::forward<ChunkArgs>(args)...);
        return;
    }

    _is_aligned = true; // TODO always true!
    _packet_number = _prev->_packet_number;

    // setup
    PacketSizeType next_offset;
    const PacketSizeType first_offset=prev._first_offset;
    if(prev._offset < first_offset && first_offset < prev._chunk_size) {
        next_offset = first_offset;
    }
    else {
        next_offset = prev._offset + prev.packet_size();
    }
    // must create a new context
    {
        if(next_offset >= prev._chunk_size)
        {
            // need a new chunk
            set_first_offset_new_chunk(next_offset);
            if(_first_offset == 0) {
                increment_packet_number(_packet_number);
                _aligned_packet_number = _packet_number; // TODO was next._packet_number - _next->_packet_number
            }

            new_chunk(std::forward<ChunkArgs>(args)...);
            assert(this->_chunk_traits);
            assert(this->chunk());

            if(_first_offset > 0) {
                _aligned_packet_number = _prev->_aligned_packet_number;
                _packet_offset += _prev->size();
            }
            if(_prev->chunk()) {
                // we cannot purge directly here as we may be called from a _data_lock condition (i.e. deserialise_packet handler calling next())
                // which would cause the system to hang during the purge operation
                std::shared_ptr<ChunkerContext> prev = _prev->shared_from_this();
                _mgr.post([prev]()
                          {
                              prev->clear_chunk();
                          });
            }

        }
        else {
            // move the current chunk offsets
            _offset = next_offset;
            setup_same_chunk();
        }
    }

    // only safe to set this at the end
    _status = Status::DataReady;

    _purge->purge_one(); // cleanup as we go
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
    PANDA_LOG_DEBUG << "reset =" << *this;
#endif // PANDA_PACKETSTREAM_LOG_DEBUG
}

// simialar to reset but will not create a new chunk
template<typename DataTraits, typename ChunkFactory>
bool ChunkerContext<DataTraits, ChunkFactory>::partial_reset()
{
    // thread safety
    std::lock_guard<std::mutex> lock(_reset_mutex);
    if(_status != Status::Empty) return false; // some other thread has already done the work

    _is_aligned = true;
    _packet_number = _prev->_packet_number;

    // setup
    PacketSizeType next_offset;
    PacketSizeType first_offset=_prev->_first_offset;
    if(_prev->_offset < first_offset && first_offset < _prev->_chunk_size) {
        next_offset = first_offset;
    }
    else {
        next_offset = _prev->_offset + _prev->packet_size();
    }
    _offset = next_offset;

    if(next_offset < _prev->_chunk_size) {
        setup_same_chunk();
        _status=Status::DataReady;
        if(_last) {
            // ensure the contexts status is cleared indicative of our progress.
            // this will ensure that last() cannot enter a condition that will loop indefinitely
            _mgr.next();
        }
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
        PANDA_LOG_DEBUG << " partial reset =" << *this;
#endif
        return false;
    }
    else {
        // we will need a new chunk - set all we can without actaully getting one
        //this->_chunk_traits = nullptr; // flag we need a new chunk

        set_first_offset_new_chunk(next_offset);

        if(_first_offset > 0) {
            _aligned_packet_number = _prev->_aligned_packet_number;
            _packet_offset += _prev->size();
        } else {
            increment_packet_number(_packet_number);
            _aligned_packet_number = _packet_number; // TODO was next._packet_number - _next->_packet_number
        }

        // it is safe to purge previous chunk here as we will not get lock conflicts
        // but we still schedule the purge to reduce the chance of chunks getting out
        // of order
        _status = Status::Reset;
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
        PANDA_LOG_DEBUG << " partial reset =" << *this;
#endif // PANDA_PACKETSTREAM_LOG_DEBUG
        return true;
    }

}

template<typename DataTraits, typename ChunkFactory>
inline void ChunkerContext<DataTraits, ChunkFactory>::recycle()
{
    _status=Status::Empty;
    _requested = false;
    _packet_offset=0;
    _purge_count=0;
    _is_aligned=false;
    _realigned=false;
}


template<typename DataTraits, typename ChunkFactory>
template<typename... ChunkArgs>
inline void ChunkerContext<DataTraits, ChunkFactory>::init(ChunkArgs&&... chunk_construction)
{
    if(_status < Status::DataReady) {
        reset(*_prev, std::forward<ChunkArgs>(chunk_construction)...); // basic packet/offset info
    }
}

template<typename DataTraits, typename ChunkFactory>
inline ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::get_context(typename DataTraits::PacketInspector const& packet)
{
    // packet information required setup
    return this->get_context(this->sequence_number(packet), packet);
}

template<typename DataTraits, typename ChunkFactory>
template<typename... Args>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::do_realign(LimitType sequence_number, typename DataTraits::PacketInspector const& packet, Args&&... chunk_construction)
{
    // alignment rejection
    if(!this->align_packet(packet)) {
         return nullptr; // CAN RETURN nullptr
    }
    _packet_number = sequence_number; // must be set before new_chunk
    _aligned_packet_number = _packet_number;
    _first_offset = 0;
    _is_aligned = true;
    reset_offset();

    this->new_chunk(packet, std::forward<Args>(chunk_construction)...);

    _status = Status::DataReady;
    PANDA_LOG_WARN << "realign to packet number: " << _packet_number;
    return this;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... Args>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::get_context(LimitType sequence_number, typename DataTraits::PacketInspector const& packet, Args&&... chunk_construction)
{
    if(!_is_aligned) {
        std::lock_guard<std::mutex> lock(_reset_mutex);
        if(!_is_aligned) {
            return do_realign(sequence_number, packet, std::forward<Args>(chunk_construction)...);
        }
    }

    init(packet, std::forward<Args>(chunk_construction)...);

#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
    PANDA_LOG_DEBUG << "get_context(" << sequence_number << ") aligned=" << _is_aligned << *this;
#endif // PANDA_PACKETSTREAM_LOG_DEBUG

    SelfType* ctx;
    try {
        ctx = get_context_internal(sequence_number, packet, std::forward<Args>(chunk_construction)...);
        if(ctx) ctx->_requested = true;
        return ctx;
    } catch(std::exception const& e) {
        PANDA_LOG_WARN << "exception thrown getting context: " << e.what();
    } catch(...) {
        PANDA_LOG_WARN << "unknown exception thrown getting context";
    }
    return nullptr;
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::last()
{
    if(_next->_status <= Status::Reset ) return this; // quick check
    if(_next->_packet_number < this->_packet_number) return this;
    return _next->last();
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::LimitType ChunkerContext<DataTraits, ChunkFactory>::max_sequence_number() const
{
    return this->_chunk_traits->max_sequence_number();
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::LimitType ChunkerContext<DataTraits, ChunkFactory>::sequence_number(typename DataTraits::PacketInspector const& p)
{
    return this->_chunk_traits->sequence_number(p);
}

template<typename DataTraits, typename ChunkFactory>
typename ChunkerContext<DataTraits, ChunkFactory>::PacketSizeType ChunkerContext<DataTraits, ChunkFactory>::size() const
{
    return _size;
}

template<typename DataTraits, typename ChunkFactory>
bool ChunkerContext<DataTraits, ChunkFactory>::align_packet(typename DataTraits::PacketInspector const& p)
{
    return this->_chunk_traits->align_packet(p);
}

template<typename DataTraits, typename ChunkFactory>
inline bool ChunkerContext<DataTraits, ChunkFactory>::less_than(LimitType const& other) const {
    if(other == _packet_number) return false;
    if( _packet_number < other ) {
        LimitType delta=other - _packet_number;
        return ((max_sequence_number() - other) + _packet_number > delta)?true:false;
    }
    LimitType delta=_packet_number - other;
    return ((max_sequence_number() - _packet_number) + other <= delta)?true:false;
}

template<typename DataTraits, typename ChunkFactory>
inline std::pair<bool, typename ChunkerContext<DataTraits, ChunkFactory>::LimitType> ChunkerContext<DataTraits, ChunkFactory>::greater_than(LimitType const& other) const {
    if(other == _packet_number) return std::pair<bool, LimitType>(false, 0);
    if( _packet_number < other ) {
        LimitType delta=other - _packet_number;
        return ((max_sequence_number() - other) + _packet_number > delta)?std::pair<bool, LimitType>(false, delta):std::pair<bool, LimitType>(true, delta);
    }
    LimitType delta=_packet_number - other;
    return ((max_sequence_number() - _packet_number) + other <= delta)?std::pair<bool, LimitType>(false, delta):std::pair<bool, LimitType>(true, delta);
}

template<typename DataTraits, typename ChunkFactory>
inline void ChunkerContext<DataTraits, ChunkFactory>::increment_packet_number(LimitType& value)
{
    _packet_offset = 0;
    ++value;
    LimitType max_sequence_number = this->max_sequence_number();
    if(value > max_sequence_number) value -= max_sequence_number;
}

template<typename DataTraits, typename ChunkFactory>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::get_prev(LimitType packet_number)
{
    ChunkerContext* ctx=this;
    do {
        if(ctx->_status == Status::Empty) return nullptr;
        if(packet_number == ctx->_packet_number && ctx->_packet_offset==0) return this;
        if(ctx->_packet_number < ctx->_prev->_packet_number) return nullptr;
        ctx=ctx->_prev;
    } while(ctx!=this->_purge);
    return nullptr;
}

template<typename DataTraits, typename ChunkFactory>
template<typename... Args>
ChunkerContext<DataTraits, ChunkFactory>* ChunkerContext<DataTraits, ChunkFactory>::get_context_internal(LimitType packet_number, Args&&... chunk_args)
{
    ChunkerContext* ctx=this;
    while(ctx->_packet_number != packet_number) {

        // check existing ctxs
        std::pair<bool, LimitType> gt=ctx->greater_than(packet_number);
        if(gt.first) {
            // previous packet_numbers should be back the way
            ChunkerContext* new_ctx=ctx->_prev->get_prev(packet_number);
            if(new_ctx) return new_ctx;
            // packet arrived late
            PANDA_LOG_WARN << "packet arrived late: current context=" << ctx->_packet_number << " ignoring pn=" << packet_number << " " << *ctx;
            return nullptr;
        }

        // safe to do relative packet number calculations
        LimitType rel_packet_number = (packet_number-ctx->_aligned_packet_number);
        if(rel_packet_number > ctx->max_sequence_number()) rel_packet_number -= ctx->max_sequence_number();
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
        PANDA_LOG_DEBUG << "get_context_internal(packet_number=" << packet_number
                        << ") _aligned_packet_number=" << ctx->_aligned_packet_number
                        << " rel_packet_number=" << rel_packet_number
                        << *ctx;
#endif
        if(gt.second>_mgr.missing_packets_before_realign() && ctx->_next->_status < Status::Complete) {
            ctx=ctx->next_realign(packet_number, std::forward<Args>(chunk_args)...);
        }
        else {
            ctx = &ctx->next(std::forward<Args>(chunk_args)...);
        }
    }
    return ctx;
}

template<typename DataTraits, typename ChunkFactory>
bool ChunkerContext<DataTraits, ChunkFactory>::operator<(ChunkerContext const& other) const
{
    return less_than(other._packet_number);
}

template<typename DataTraits, typename ChunkFactory>
std::string ChunkerContext<DataTraits, ChunkFactory>::status_string(Status const& status)
{
    typedef typename ChunkerContext<DataTraits, ChunkFactory>::Status Status;
    switch(status) {
        case Status::Empty:
            return "Empty";
        case Status::Reset:
            return "Reset";
        case Status::DataReady:
            return "DataReady";
        case Status::Complete:
            return "Complete";
        case Status::Purged:
            return "Purged";
    };
    return {};
}

template<typename DataTraits, typename ChunkFactory>
bool ChunkerContext<DataTraits, ChunkFactory>::data_written() const
{
    return _status == Status::Complete;
}

template<typename DataTraits, typename ChunkFactory>
void ChunkerContext<DataTraits, ChunkFactory>::debug_string(std::ostream& os) const
{
    os << "\n    " << "| prev(" << _prev << ")        <- ctx(" << this << ")          -> next(" << _next << ")        |\n";
    row_output(os, "pn", _prev->_packet_number , _packet_number, _next->_packet_number);
    row_output(os, "chunk", _prev->_chunk_traits?(void*)&(*_prev->chunk()):(void*)0, (this->_chunk_traits?(void*)&(*chunk()):(void*)0), (_next->_chunk_traits?(void*)&(*_next->chunk()):(void*)0));
    row_output(os,"status", status_string(_prev->_status), status_string(_status), status_string(_next->_status));
    row_output(os, "data_write", _prev->data_written(), data_written(), _next->data_written());
    row_output(os, "offset", _prev->offset(), offset(), _next->offset());
    row_output(os, "packet_offset", _prev->packet_offset(), packet_offset(), _next->packet_offset());
#ifdef PANDA_PACKETSTREAM_LOG_DEBUG
    row_output(os, "size", _prev->_size, this->_size, _next->_size);
    row_output(os, "chunk_size", _prev->_chunk_size, this->_chunk_size, _next->_chunk_size);
    row_output(os, "chunk_tr", _prev->_chunk_traits?(void*)(_prev->_chunk_traits):(void*)0, (this->_chunk_traits?(void*)(this->_chunk_traits):(void*)0), (_next->_chunk_traits?(void*)(_next->_chunk_traits):(void*)0));
    row_output(os, "first_offset", _prev->_first_offset, _first_offset, _next->_first_offset);
    row_output(os, "_purge", _prev->_purge, _purge, _next->_purge);
#endif // PANDA_PACKETSTREAM_LOG_DEBUG
}

template<typename DataTraits, typename ChunkFactory>
std::ostream& operator<<(std::ostream& os, ChunkerContext<DataTraits, ChunkFactory> const& ctx)
{
    ctx.debug_string(os);
    return os;
}

template<typename BaseT>
std::ostream& operator<<(std::ostream& os, CurrentChunkContext<BaseT> const& ctx)
{
    ctx.debug_string(os);
    return os;
}

template<typename BaseT>
inline typename CurrentChunkContext<BaseT>::DataType& CurrentChunkContext<BaseT>::chunk() const
{
    return *BaseT::chunk();
}

template<typename BaseT>
inline typename MissingPacketContext<BaseT>::PacketSizeType MissingPacketContext<BaseT>::size() const
{
    return BaseT::size();
}

template<typename BaseT>
inline typename MissingPacketContext<BaseT>::PacketSizeType MissingPacketContext<BaseT>::offset() const
{
    return BaseT::offset();
}

template<typename BaseT>
inline typename MissingPacketContext<BaseT>::LimitType MissingPacketContext<BaseT>::packet_number() const
{
    return BaseT::packet_number();
}

template<typename BaseT>
inline typename MissingPacketContext<BaseT>::PacketSizeType MissingPacketContext<BaseT>::packet_offset() const
{
    return BaseT::packet_offset();
}

template<typename BaseT>
inline typename MissingPacketContext<BaseT>::PacketSizeType MissingPacketContext<BaseT>::packet_size() const
{
    return BaseT::packet_size();
}

template<typename BaseT>
inline void CurrentChunkContext<BaseT>::debug_string(std::ostream& os) const
{
    BaseT::debug_string(os);
}

template<typename BaseT>
inline typename DeserialisePacketContext<BaseT>::LimitType DeserialisePacketContext<BaseT>::sequence_number(PacketInspector const& packet_in)
{
    return BaseT::sequence_number(packet_in);
}

template<typename BaseT>
inline typename ResizeChunkContext<BaseT>::PacketSizeType ResizeChunkContext<BaseT>::size() const
{
    return BaseT::offset() + BaseT::size();
}

} // namespace packet_stream
} // namespace panda
} // namespace ska
