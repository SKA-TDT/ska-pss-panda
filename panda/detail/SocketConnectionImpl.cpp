/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/SocketConnectionImpl.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include "panda/Buffer.h"
#include "panda/IpAddress.h"


namespace ska {
namespace panda {
namespace detail {

// -------------------------------------------------------------------
// -- Helper functions to write out data to a specific socket type ---
// -------------------------------------------------------------------

template<class SocketType, class EndPointType, class BufferType>
struct do_write
{
    // default assumes an unsyncronised socket type
    static inline
    void exec(SocketType& socket, EndPointType const& endpoint, SocketConnection::State& state
             , BufferType const& buffer, unsigned offset, std::function<void(panda::Error const&)> const& handler) {
        try {
            if(state == SocketConnection::State::Close)
            {
                handler(panda::Error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category())));
            }
            socket.async_send_to(
                    boost::asio::buffer(buffer.data(offset), buffer.size() - offset),
                    endpoint,
                    [handler, offset, buffer, &socket, &endpoint, &state](boost::system::error_code ec, std::size_t length)
                    {
                        try {
                            if(ec) {
                                throw panda::Error(ec);
                            }
                            if( buffer.size() - offset < length ) {
                                do_write<SocketType, EndPointType, BufferType>::exec(socket, endpoint, state, buffer, buffer.size() - offset + length, handler);
                            }
                            else {
                                handler(panda::Error());
                            }
                        } catch(panda::Error const& e) {
                            handler(e);
                        } catch(std::exception const& e) {
                            handler(panda::Error(e));
                        } catch(...) {
                            handler(panda::Error("unknown exception caught"));
                        }
                    });
        } catch(panda::Error const& e) {
            handler(e);
        } catch(std::exception const& e) {
            handler(e);
        } catch(...) {
            handler(panda::Error("unknown exception caught"));
        }
    }
};

template<class SocketType>
struct do_shutdown {
    static inline
    void exec(SocketType&) {
    }
};

template<>
struct do_shutdown<boost::asio::ip::udp::socket> {
    typedef boost::asio::ip::udp::socket SocketType;

    static inline
    void exec(SocketType& ) {
    }
};

// tcp specialization
template<>
struct do_shutdown<boost::asio::ip::tcp::socket> {
    static inline
    void exec(boost::asio::ip::tcp::socket& socket) {
        socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
    }
};

template<class BufferType, class EndPointType>
struct do_write<boost::asio::ip::tcp::socket, EndPointType, BufferType>
{
    // default assumes an unsyncronised socket type
    static inline
    void exec(boost::asio::ip::tcp::socket& socket, EndPointType const& endpoint, SocketConnection::State& state, BufferType const& buffer, unsigned offset, std::function<void(panda::Error const&)> const& handler) {
        try {
            if(state == SocketConnection::State::Close)
            {
                handler(panda::Error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category())));
            }
            socket.async_write_some(
                boost::asio::buffer(buffer.data(offset), buffer.size() - offset),
                [handler, offset, buffer, &socket, &endpoint, &state](boost::system::error_code ec, std::size_t length)
                {
                    try {
                        if(ec) {
                            throw panda::Error(ec);
                        }
                        if( buffer.size() - offset < length ) {
                            do_write<boost::asio::ip::tcp::socket, EndPointType, BufferType>::exec(socket, endpoint, state, buffer, buffer.size() - offset + length, handler);
                        }
                        else {
                            handler(panda::Error());
                        }
                    } catch(panda::Error const& e) {
                        handler(e);
                    } catch(std::exception const& e) {
                        handler(panda::Error(e));
                    } catch(...) {
                        handler(panda::Error("unknown exception caught"));
                    }
                });
        } catch(panda::Error const& e) {
            handler(e);
        } catch(std::exception const& e) {
            handler(e);
        } catch(...) {
            handler(panda::Error("unknown exception caught"));
        }
    }
};

// -------------------------------------------------------------------
// --- class implementation details                                ---
// -------------------------------------------------------------------

template<typename DerivedType, typename ConnectionTraits>
SocketConnectionImpl<DerivedType, ConnectionTraits>::SocketConnectionImpl(EngineType& engine, SocketType socket)
    : _state(State::NotConnected)
    , _socket(std::move(socket))
    , _engine(engine)
    , _safe_exit(0)
{
    open();
}


template<typename DerivedType, typename ConnectionTraits>
SocketConnectionImpl<DerivedType, ConnectionTraits>::SocketConnectionImpl(EngineType& engine)
    : _state(State::NotConnected)
    , _socket(engine)
    , _engine(engine)
    , _safe_exit(0)
{
    open();
}

template<typename DerivedType, typename ConnectionTraits>
SocketConnectionImpl<DerivedType, ConnectionTraits>::~SocketConnectionImpl()
{
    close();
    while(_safe_exit != 0) {
        _engine.poll_one();
    }
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::open()
{
    if(!_socket.is_open()) {
        boost::system::error_code ec;
        _socket.open(_remote_endpoint.protocol(), ec);
        if(ec) {
            panda::Error e(ec);
            e << " " << _remote_endpoint.address() << ":" << _remote_endpoint.port();
            throw e;
        }
    }
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::set_remote_end_point(typename SocketConnectionImpl<DerivedType, ConnectionTraits>::EndPointType const& endpoint)
{
    PANDA_LOG << "[this=" << this << "] setting remote endpoint:" << endpoint;
    _remote_endpoint = endpoint;
    _state=State::NotConnected;
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnectionImpl<DerivedType, ConnectionTraits>::EndPointType const& SocketConnectionImpl<DerivedType, ConnectionTraits>::remote_end_point() const
{
    return _remote_endpoint;
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnectionImpl<DerivedType, ConnectionTraits>::ErrorPolicyType& SocketConnectionImpl<DerivedType, ConnectionTraits>::policy()
{
    return _error_policy;
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnectionImpl<DerivedType, ConnectionTraits>::SocketType& SocketConnectionImpl<DerivedType, ConnectionTraits>::socket()
{
    return _socket;
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnectionImpl<DerivedType, ConnectionTraits>::SocketType const& SocketConnectionImpl<DerivedType, ConnectionTraits>::socket() const
{
    return _socket;
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnectionImpl<DerivedType, ConnectionTraits>::EngineType& SocketConnectionImpl<DerivedType, ConnectionTraits>::engine()
{
    return _engine;
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::send(BufferType const buffer, SendHandler const& handler)
{
    assert(buffer.data() != nullptr);
    auto self=this->shared_from_this();
    async_connect(
        [self, buffer, handler](panda::Error e) {
            if(e) {
                handler(std::move(e));
                return;
            }

            do_write<decltype(self->_socket), EndPointType, decltype(buffer)>::exec(self->_socket, self->_remote_endpoint, self->_state, buffer, 0,
                [self, buffer, handler](panda::Error const& e) // keep the buffer alive in the handler
                {
                    if(e && self->_state != State::Close) {
                        self->_state = State::NotConnected;
                        panda::Error err(e);
                        err << " (" << self->_remote_endpoint << ")";
                        if(self->_error_policy.send_error(err))
                        {
                            // try again
                            self->_engine.post([handler, buffer, self]() { self->send(buffer, handler); });
                            return;
                        }
                    }
                    self->_engine.post([handler, e]() { handler(std::move(e)); });
                }
            );
        });
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::connect(std::function<void(boost::system::error_code)> const& handler)
{
    _socket.async_connect(_remote_endpoint, handler);
}

template<typename DerivedType, typename ConnectionTraits>
template<typename HandlerType>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::recv(BufferType& buffer, HandlerType handler)
{
    _socket.async_receive(boost::asio::mutable_buffers_1(
              static_cast<void*>(buffer.data())
            , buffer.size())
            , std::move(handler));
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::receive(BufferType buffer, ReceiveHandler handler)
{
    auto self=this->shared_from_this();
    this->_receive(std::move(self), std::move(buffer), handler);
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::_receive(std::shared_ptr<SelfType> self, BufferType buffer, ReceiveHandler handler)
{
    async_connect(
        [self, buffer, handler](panda::Error e) mutable {
            if(e) {
                handler(std::move(e), 0, std::move(buffer));
                return;
            }

            static_cast<DerivedType&>(*self).recv(
                      buffer
                    , [self, handler, buffer](boost::system::error_code const& ec , std::size_t bytes_read)
                      {
                          panda::Error err(ec);
                          if(ec && self->_state != State::Close) {
                              PANDA_LOG_DEBUG << "[this=" << &*self << "] recv error: " << self->_remote_endpoint;
                              self->_state = State::NotConnected;
                              if(self->_error_policy.error(err))
                              {
                                  self->_engine.post(std::bind(&SocketConnectionImpl<DerivedType, ConnectionTraits>::_receive,
                                                     &*self, std::move(self), std::move(buffer), handler));
                                  return;
                              }
                          }
                          handler(std::move(err), bytes_read, std::move(buffer));
                      }
                    );
        });
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::async_connect(std::function<void(panda::Error)> const& handler)
{
    switch(_state) {
        case State::Connected:
            handler(panda::Error());
            break;
        case State::Connecting:
        {
            {
                std::unique_lock<std::mutex> lock(_mutex);
                // things might of changed by time we get the lock
                if(_state == State::Connecting) {
                    _awaiting_connection.push_back(handler);
                    break;
                }
            } // make sure lock is released before calling self to avoid deadlock
            async_connect(handler);
            break;
        }
        case State::NotConnected:
            if(_error_policy.try_connect()) {
                _state = State::Connecting;
                static_cast<DerivedType&>(*this).connect(
                        [this, handler](boost::system::error_code const& ec)
                        {
                            if(ec) {
                                panda::Error err(ec);
                                if( _state == State::Close) {
                                    _engine.post([handler, err]() { handler(err); });
                                    return;
                                } else {
                                    _state=State::NotConnected;
                                    err << " (" << _remote_endpoint << ") trying to connect";
                                    if(!_error_policy.connection_error(err)) {
                                        // don't try again, and send error to all awaiting handlers
                                        _engine.post([handler, err]() { handler(err); });
                                        std::unique_lock<std::mutex> lock(_mutex);
                                        for(auto const& h : _awaiting_connection) {
                                            _engine.post([h, err]() { h(err); });
                                        }
                                        _awaiting_connection.clear();
                                        return;
                                    }
                                    // retry to connect
                                    return async_connect(handler);
                                }
                            }
                            _state=State::Connected;
                            _error_policy.connected();
                            // inform all awaiting handlers they can continue
                            _engine.post([handler]() { handler(panda::Error()); });
                            std::unique_lock<std::mutex> lock(_mutex);
                            for(auto const& h : _awaiting_connection) {
                                _engine.post([h]() { h(std::move(panda::Error())); });
                            }
                            _awaiting_connection.clear();
                        });
                return;
            }
            break;
        case State::Close:
            handler(panda::Error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category())));
            break;
    }; // switch
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnectionImpl<DerivedType, ConnectionTraits>::close()
{
    auto clear_awaiting_connections = [&]() {
        // cancel all waiting connections
        std::unique_lock<std::mutex> lock(_mutex);
        for(auto const& h : _awaiting_connection) {
            ++_safe_exit;
            try {
                _engine.post([&, h]() {
                            --_safe_exit;
                            h(std::move(panda::Error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category()))));
                                   });
            } catch(...)
            {
                --_safe_exit;
            }
        }
        _awaiting_connection.clear();
    };

    std::lock_guard<std::mutex> lock(_close_mutex);
    if(_state == State::Close) {
        // already being closed
        clear_awaiting_connections();
        return;
    }

    _state = State::Close;
    PANDA_LOG << "closing connection: " << _remote_endpoint;
    try {
        do_shutdown<SocketType>::exec(_socket);
    } catch(...) {}
    _socket.close();
    clear_awaiting_connections();
}

} // namespace detail
} // namespace panda
} // namespace ska
