/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ConfigurableTask.h"
#include "panda/detail/ConfigurableTaskImpl.cpp"
#include "panda/detail/ConfigurableTaskBase.cpp"
#include <boost/mpl/unique.hpp>
#include <boost/mpl/vector.hpp>
#include <array>
#include <tuple>
#include <algorithm>
#include <type_traits>

namespace ska {
namespace panda {

template<class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename... Args>
ConfigurableTask<PoolManagerType, AsyncHandler, DataTypes...>::ConfigurableTask(PoolManagerType pool
                                                                              , AsyncHandler handler
                                                                              , Args&&... args)
    : BaseT(pool, handler, std::forward<Args>(args)...)
{
}

template<class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename... DataTypeArgs>
std::shared_ptr<ResourceJob> ConfigurableTask<PoolManagerType, AsyncHandler, DataTypes...>::submit(DataTypeArgs&&... data)
{
    return this->_impl->submit(std::forward<DataTypeArgs>(data)...);
}

namespace { // helpers for configure() functionality

template<class T, typename... Ts>
using HasExecuteMethodHelper = decltype(std::declval<T&>().template exec<Ts...>());


template<typename FactoryT, typename TrueFalse>
struct ConfigureUserCallBackExec
{
    public:
        template<typename... Ts>
        inline static
        void exec(FactoryT&) {}
};

template<typename FactoryT>
struct ConfigureUserCallBackExec<FactoryT, std::true_type>
{
    public:
        template<typename... Ts>
        inline static
        void exec(FactoryT& factory)
        {
            factory.template exec<Ts...>();
        }
};

template<typename FactoryT, typename... Ts>
struct ConfigureUserCallBack
{
    template<typename T> using HasExecuteMethodHelperT = HasExecuteMethodHelper<T, Ts...>;

    inline static
    void exec(FactoryT& factory)
    {
        ConfigureUserCallBackExec<FactoryT, typename HasMethod<FactoryT, HasExecuteMethodHelperT>::type>::template exec<Ts...>(factory);
    }
};

template<typename T>
using NotSelectedT = decltype(std::declval<T>().none_selected());

template<typename FactoryT, typename Enable=void>
struct ConfigureUserNullCallBack
{
    inline static void exec(FactoryT&) {}
};

template<typename FactoryT>
struct ConfigureUserNullCallBack<FactoryT, typename std::enable_if<HasMethod<FactoryT, NotSelectedT>::value>::type>
{
    inline static void exec(FactoryT& factory) { factory.none_selected(); }
};

template<typename TaskT, typename FactoryT, typename BoostArchitecturesVector>
struct ConfigureHelper {
    public:
        ConfigureHelper(TaskT& task, FactoryT& factory)
            : _task(task)
            , _factory(factory)
        {
            std::fill(_arch_count.begin(), _arch_count.end(), 0);
        }

        template<typename... Ts>
        inline
        void exec() {
            // ensure only one algorithm for each architecture
            this->template increment_algo_count<typename Ts::Architecture...>();

            // set up the selected algorithms
            _task.template set_algorithms<Ts...>(std::move(this->_factory.template create<Ts>())...);

            // call a user supplied exec function if it exists
            ConfigureUserCallBack<FactoryT, Ts...>::exec(_factory);
        }

        void exec() { ConfigureUserNullCallBack<FactoryT>::exec(_factory); }

    private:

        template<typename T>
        std::size_t& count()
        {
            typedef typename boost::mpl::find<BoostArchitecturesVector, T>::type iter;
            std::size_t index = iter::pos::value;
            return _arch_count[index];
        }

        template<typename T, typename T2, typename... Ts>
        void increment_algo_count()
        {
            this->template increment_algo_count<T>();
            this->template increment_algo_count<T2, Ts...>();
        }

        template<typename T>
        void increment_algo_count()
        {
            if(++this->template count<T>() > 1 )
            {
                panda::Error e("Multiple algorithms specified for architecture :");
                e << panda::to_string<T>();
                throw e;
            }
        }

    private:
        TaskT& _task;
        FactoryT& _factory;
        std::array<std::size_t, boost::mpl::size<BoostArchitecturesVector>::value> _arch_count;
};
} // namespace

template<class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename AlgorithmFactory, typename... AlgoT>
bool ConfigurableTask<PoolManagerType, AsyncHandler, DataTypes...>::configure(AlgorithmFactory& factory
                                                                            , Select<AlgoT> const&... algos)
{
    namespace mpl = boost::mpl;
    ConfigureHelper<ConfigurableTask, AlgorithmFactory, typename mpl::unique<mpl::vector<typename AlgoT::Architecture...>, std::is_same<mpl::_1, mpl::_2>>::type> helper(*this, factory);
    return panda::SelectMethod::exec(helper, algos...);
}

template<class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename... Algorithms>
void ConfigurableTask<PoolManagerType, AsyncHandler, DataTypes...>::set_algorithms(Algorithms&&... algos)
{
    BaseT::template set_algorithms(std::tuple<typename std::decay<Algorithms>::type...>(std::forward<Algorithms>(algos)...));
}

} // namespace panda
} // namespace ska
