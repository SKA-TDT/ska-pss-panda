/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace panda {

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::ConfigurableTaskBase(PoolManagerType pool, AsyncHandler handler)
    : _handler(handler)
    , _pool(pool)
    , _impl(&_null_default)
{
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::~ConfigurableTaskBase()
{
    if(_impl != &_null_default) delete _impl;
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<class Arch, typename... DataTypeArgs>
//typename ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::template ResultType<Arch, DataTypeArgs...>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::operator()(panda::PoolResource<Arch>& resource, DataTypeArgs&&... args) const
{
    _impl->exec_algo(resource, std::forward<DataTypeArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<class Arch, typename... DataTypeArgs>
//typename ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::template ResultType<Arch, DataTypeArgs...>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::operator()(std::shared_ptr<panda::PoolResource<Arch>> const& resource, DataTypeArgs&&... args) const
{
    (*this)(*resource, std::forward<DataTypeArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename AlgoTuple>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::set_algorithms(AlgoTuple&& algos)
{
    typedef detail::ConfigurableTaskImpl<ImplType, PoolManagerType, typename std::decay<AlgoTuple>::type, AsyncHandler, DataTypes...> SImplType;
    this->template do_set_algorithms<SImplType>(std::forward<AlgoTuple>(algos));
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... DataTypes>
template<typename ImplSpecialisation, typename Algorithms, typename... Functors>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, DataTypes...>::do_set_algorithms(Algorithms&& algos, Functors&&... args)
{
    static_assert(std::is_base_of<ImplType, ImplSpecialisation>::value, "Expecting a specialisation that inherits from the ImplType");
    if(_impl != &_null_default) delete _impl;
    _impl = new ImplSpecialisation(_pool, std::forward<Algorithms>(algos), _handler, std::forward<Functors>(args)...);
}

// -- SubmitMethod specialisations
template<class ImplType, class PoolManagerType, typename AsyncHandler, typename... SignatureArgs, typename... DataTypes>
template<typename AlgoTuple>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, SubmitMethod<SignatureArgs...>, DataTypes...>::set_algorithms(AlgoTuple&& algos)
{
    typedef detail::ConfigurableTaskImpl<ImplType, PoolManagerType, typename std::decay<AlgoTuple>::type, AsyncHandler, SubmitMethod<SignatureArgs...>, DataTypes...> SImplType;
    this->template do_set_algorithms<SImplType>(std::forward<AlgoTuple>(algos));
}

// -- Single Method implementation
template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>::ConfigurableTaskBase(PoolManagerType pool
                                                                                                                                 , AsyncHandler handler
                                                                                                                                 , Functor const& functor
                                                                                                                                 , Args&&... args)
    : BaseT(pool, handler, std::forward<Args>(args)...)
    , _functor(functor)
{
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
template<typename AlgoTuple>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>::set_algorithms(AlgoTuple&& algos)
{
    typedef detail::ConfigurableTaskImpl<ImplType, PoolManagerType, typename std::decay<AlgoTuple>::type, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...> SImplType;
    this->do_set_algorithms<SImplType>(std::forward<AlgoTuple>(algos));
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>::operator()(FunctorArgs... args)
{
    this->_impl->exec_functor(std::forward<FunctorArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>::operator()(FunctorArgs... args) const
{
    this->_impl->exec_functor(std::forward<FunctorArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
template<typename ImplSpecialisation, typename AlgoTuple, typename... Functors>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>::do_set_algorithms(AlgoTuple&& algos, Functors&&... args)
{
    BaseT::template do_set_algorithms<ImplSpecialisation>(std::forward<AlgoTuple>(algos), std::forward<Functors>(args)..., _functor);
}

// -- Multi Method implementation
template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
template<typename... Args>
ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>::ConfigurableTaskBase(PoolManagerType pool
                                                                                                                                 , AsyncHandler handler
                                                                                                                                 , Functor const& functor
                                                                                                                                 , Args&&... args)
    : BaseT(pool, handler, std::forward<Args>(args)...)
    , _functor(functor)
{
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
template<typename AlgoTuple>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>::set_algorithms(AlgoTuple&& algos)
{
    typedef detail::ConfigurableTaskImpl<ImplType, PoolManagerType, typename std::decay<AlgoTuple>::type, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...> SImplType;
    this->do_set_algorithms<SImplType>(std::forward<AlgoTuple>(algos));
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>::operator()(FunctorArgs... args)
{
    this->_impl->exec_functor(std::forward<FunctorArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>::operator()(FunctorArgs... args) const
{
    this->_impl->exec_functor(std::forward<FunctorArgs>(args)...);
}

template<class ImplType, class PoolManagerType, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
template<typename ImplSpecialisation, typename AlgoTuple, typename... Functors>
void ConfigurableTaskBase<ImplType, PoolManagerType, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>::do_set_algorithms(AlgoTuple&& algos, Functors&&... args)
{
    BaseT::template do_set_algorithms<ImplSpecialisation>(std::forward<AlgoTuple>(algos), std::forward<Functors>(args)..., _functor);
}


} // namespace panda
} // namespace ska
