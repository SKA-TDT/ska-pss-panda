/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TupleUtilities.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/mpl/map.hpp>
#include <boost/mpl/pair.hpp>
#pragma GCC diagnostic pop
#include <tuple>
#include <type_traits>

namespace ska {
namespace panda {

TYPEDEF_TESTER(HasArchitectureType, Architecture);
TYPEDEF_TESTER(HasArchitecturesType, Architectures);
TYPEDEF_TESTER(HasArchitectureCapabilityTrait, ArchitectureCapability);
TYPEDEF_TESTER(HasCapabilitesTrait, Capabilities);

template<typename Traits, typename Enable=void>
struct ArchitectureInfo;

template<typename Traits, typename Enable=void>
struct ArchitectureInfoBase;

template<typename Traits>
struct ArchitectureInfoBase<Traits, typename std::enable_if<HasArchitecturesType<Traits>::value>::type>
{
    typedef typename Traits::Architectures Architectures;
};

template<typename Traits>
struct ArchitectureInfoBase<Traits, typename std::enable_if<HasArchitectureType<Traits>::value>::type>
{
    typedef std::tuple<typename Traits::Architecture> Architectures;
};

template<typename Traits>
struct ArchitectureInfo<Traits, typename std::enable_if<HasCapabilitesTrait<Traits>::value>::type>
    : public ArchitectureInfoBase<Traits>
{
    private:
        typedef ArchitectureInfoBase<Traits> BaseT;

    public:
        typedef typename BaseT::Architectures Architectures;
        typedef typename Traits::Capabilities Capabilities;
};

template<typename Traits>
struct ArchitectureInfo<Traits, typename std::enable_if<!HasCapabilitesTrait<Traits>::value>::type>
    : public ArchitectureInfoBase<Traits>
{
    private:
        typedef ArchitectureInfoBase<Traits> BaseT;

    public:
        typedef typename BaseT::Architectures Architectures;
        typedef Architectures Capabilities;
};


template<typename Algorithm>
struct AlgorithmInfo<Algorithm, typename std::enable_if<HasArchitectureCapabilityTrait<Algorithm>::value>::type> {
    typedef typename Algorithm::Architecture Architecture;
    typedef typename Algorithm::ArchitectureCapability ArchitectureCapability;
};

template<typename Algorithm>
struct MixedAlgoTraits<std::tuple<Algorithm>> {
    public:
        typedef boost::mpl::map<boost::mpl::pair<typename AlgorithmInfo<Algorithm>::Architecture, Algorithm>> AlgoArchMapType;
        typedef std::tuple<typename AlgorithmInfo<Algorithm>::Architecture> Architectures;
        typedef std::tuple<typename AlgorithmInfo<Algorithm>::ArchitectureCapability> Capabilities;
        typedef std::tuple<boost::mpl::pair<typename AlgorithmInfo<Algorithm>::Architecture, typename AlgorithmInfo<Algorithm>::ArchitectureCapability>> ArchCapability;
};

template<typename Algorithm, typename... Algorithms>
struct MixedAlgoTraits<std::tuple<Algorithm, Algorithms...>> {
        typedef MixedAlgoTraits<std::tuple<Algorithms...>> MixinType;

    public:
        typedef boost::mpl::map<boost::mpl::pair<typename Algorithm::Architecture, Algorithm>, boost::mpl::pair<typename Algorithms::Architecture, Algorithms>...> AlgoArchMapType;
        // require unique pairs of architecture/capability
        template <typename T>
        using SingleArchCapability = boost::mpl::pair<typename AlgorithmInfo<T>::Architecture, typename AlgorithmInfo<T>::ArchitectureCapability>;
        typedef std::tuple<SingleArchCapability<Algorithm>, SingleArchCapability<Algorithms>...> ArchCapability;

        typedef typename std::conditional<panda::HasType<SingleArchCapability<Algorithm>, typename MixinType::ArchCapability>::value,
                                          typename MixinType::Architectures,
                                          decltype(std::tuple_cat(std::tuple<typename  AlgorithmInfo<Algorithm>::Architecture>(), typename MixinType::Architectures()))>::type Architectures;

        typedef typename std::conditional<panda::HasType<SingleArchCapability<Algorithm>, typename MixinType::ArchCapability>::value,
                                          typename MixinType::Capabilities,
                                          decltype(std::tuple_cat(std::tuple<typename  AlgorithmInfo<Algorithm>::ArchitectureCapability>(), typename MixinType::Capabilities()))>::type Capabilities;

        static_assert( std::tuple_size<Architectures>::value ==  std::tuple_size<Capabilities>::value,
                       "Length of Capabilities tuple must match length of Architectures tuple" );
};


} // namespace panda
} // namespace ska
