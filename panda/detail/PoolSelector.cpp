/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PoolSelector.h"
#include "panda/PoolManager.h"


namespace ska {
namespace panda {


template<typename PoolManagerType, typename ConfigModuleType>
template<typename... Args>
PoolSelector<PoolManagerType, ConfigModuleType>::PoolSelector(PoolManagerType& pm, Args&&... args)
    : ConfigModuleType(std::forward<Args>(args)...)
    , _pool_manager(pm)
    , _priority(0U)
{
}

template<typename PoolManagerType, typename ConfigModuleType>
PoolSelector<PoolManagerType, ConfigModuleType>::~PoolSelector()
{
}

// specialization where inheritance is used - does not call base class in infinite loop
// as we expect to be called by the inheriting class
/*
template<typename PoolManagerType, typename ConfigModuleType>
void PoolSelector<PoolManagerType, PoolSelector<PoolManagerType, ConfigModuleType>>::add_options(ConfigModule::OptionsDescriptionEasyInit& add_options)
{
    std::string pool_help = "associate a specifc pool to use to run jobs in" + ConfigModuleType::name();
    add_options
    ("pool-id", boost::program_options::value<std::string>()->notifier( [this](std::string const& pool_id) { _pool_manager.reserve(pool_id); }) , pool_help.c_str() );
}
*/

template<typename PoolManagerType, typename ConfigModuleType>
void PoolSelector<PoolManagerType, ConfigModuleType>::add_options(ConfigModule::OptionsDescriptionEasyInit& add_options)
{
    std::string pool_help = "associate a specifc pool to use to run jobs in " + this->name();
    std::string pool_priority_help = "set the absolute priority level of " + this->name() + " jobs (0=top priority)";
    add_options
    ("pool-id", boost::program_options::value<std::string>()->notifier( [this](std::string const& pool_id) { _pool_manager.reserve(pool_id); _pool_id = pool_id; }) , pool_help.c_str() )
    ("priority", boost::program_options::value<unsigned>()->notifier( [this](unsigned priority) 
                                                                      { 
                                                                          _priority = priority;
                                                                      } ) , pool_priority_help.c_str() );

    ConfigModuleType::add_options(add_options);
}

template<typename PoolManagerType, typename ConfigModuleType>
std::string const& PoolSelector<PoolManagerType, ConfigModuleType>::pool_id() const
{
    return _pool_id;
}

template<typename PoolManagerType, typename ConfigModuleType>
void PoolSelector<PoolManagerType, ConfigModuleType>::set_priority(unsigned p)
{
    _priority = p;
}

template<typename PoolManagerType, typename ConfigModuleType>
typename PoolSelector<PoolManagerType, ConfigModuleType>::PoolType PoolSelector<PoolManagerType, ConfigModuleType>::pool() const
{
    if( _pool_id == "") {
        return _pool_manager.default_pool();
    }
    auto pool = _pool_manager.pool(_pool_id); // make a copy
    pool.set_priority(_priority);             // configure the copy
    return pool;
}

} // namespace panda
} // namespace ska
