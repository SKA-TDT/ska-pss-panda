/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceLocal.h"


namespace ska {
namespace panda {


template<typename DeviceType, typename T_Factory>
DeviceLocal<DeviceType, T_Factory>::DeviceLocal(T_Factory factory)
    : _factory(factory)
{
}

template<typename DeviceType, typename T_Factory>
DeviceLocal<DeviceType, T_Factory>::DeviceLocal(DeviceLocal&& other)
    : _objects(std::move(other._objects))
    , _factory(std::move(other._factory))
{
}

template<typename DeviceType, typename T_Factory>
DeviceLocal<DeviceType, T_Factory>::~DeviceLocal()
{
    for(auto obj : _objects) {
        delete obj.second;
    }
}

template<typename DeviceType, typename T_Factory>
typename DeviceLocal<DeviceType, T_Factory>::T& DeviceLocal<DeviceType, T_Factory>::operator()(DeviceType const& d)
{
    auto id = DeviceId<DeviceType>::id(d);
    auto it = this->_objects.find(id);
    if(it== this->_objects.end()) {
        _objects.insert(std::make_pair(id, _factory(d)));
        it=this->_objects.find(id);
    }
    return *it->second;
}

} // namespace panda
} // namespace ska
