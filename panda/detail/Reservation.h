/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESERVATION_H
#define SKA_PANDA_RESERVATION_H
#include "panda/ConfigModule.h"
#include <tuple>


namespace ska {
namespace panda {
namespace detail {

/**
 * @brief base class of generic properties relating to PoolResources
 */
class ReservationBase {
    public:
        ReservationBase(unsigned number = 0);
        virtual ~ReservationBase() = 0;

        void set_number(unsigned n);
        unsigned number() const;

        void set_config(ConfigModule const& c);
        ConfigModule const& config() const;

    private:
        unsigned _num;
        ConfigModule const* _config;
};

/**
 * @brief Assoicate an Architecture tag with specific reservation parameters
 * @details can be specialized for Arch specific reservation parameters
 */
template<typename Arch>
struct Reservation : public ReservationBase {
    public:
        typedef Arch Architecture;
};

/**
 * @brief constucts a std::tuple of reservations froma std::tupke of Archricterure tags
 */
template<typename ArchitectureTuple>
struct ReservationTuple;

template<typename... Architectures>
struct ReservationTuple<std::tuple<Architectures...>> {
    typedef std::tuple<Reservation<Architectures>...> type;
};


} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_RESERVATION_H 
