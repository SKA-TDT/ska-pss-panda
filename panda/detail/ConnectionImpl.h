/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONNECTIONIMPL_H
#define SKA_PANDA_CONNECTIONIMPL_H


namespace ska {
namespace panda {
namespace detail {

/*
 * @brief   general class allows you to specialize by Protocol
 * @details
 * if your specialization Inherits from SocketConnection then you can create
 * your cown connect(ihandler) function for any protocol specific connection
 * traits.
 */
template<typename ConnectionTraits, typename Protocol = typename ConnectionTraits::ProtocolType>
class ConnectionImpl : public detail::SocketConnectionImpl<ConnectionImpl<ConnectionTraits>, ConnectionTraits>
{
        typedef detail::SocketConnectionImpl<ConnectionImpl<ConnectionTraits, Protocol>, ConnectionTraits> BaseT;

    public:
        template<typename... Args>
        ConnectionImpl(Args&&...);

};

template<typename ConnectionTraits>
class ConnectionImpl<ConnectionTraits, panda::Udp> : public detail::SocketConnectionImpl<ConnectionImpl<ConnectionTraits, panda::Udp>, ConnectionTraits>
{
        typedef detail::SocketConnectionImpl<ConnectionImpl<ConnectionTraits, panda::Udp>, ConnectionTraits> BaseT;
        friend BaseT;
        using typename BaseT::EngineType;

        BOOST_CONCEPT_ASSERT((EngineConcept<EngineType>));

    public:
        template<typename ConfigType>
        ConnectionImpl(ConfigType const& config);
        ConnectionImpl(EngineType& engine, typename ConnectionTraits::SocketType socket);
        ConnectionImpl(EngineType& engine);
        ConnectionImpl(EngineType& engine, typename ConnectionTraits::EndPointType bind_address);

    protected:
        // specialisation for UDP
        template<typename HandlerType>
        void recv(typename BaseT::BufferType& buffer, HandlerType handler);

        void connect(std::function<void(boost::system::error_code)> const& handler);
};

} //namespace detail
} // namespace panda
} // namespace ska
#include "panda/detail/ConnectionImpl.cpp"

#endif // SKA_PANDA_CONNECTIONIMPL_H 
