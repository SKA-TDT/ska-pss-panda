/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PacketStream.h"
#include "panda/Error.h"
#include "panda/Compiler.h"
#include <iostream>
#include <algorithm>

namespace ska {
namespace panda {

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
template<typename... ConnectionArgs>
PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::PacketStream(DataTraits const& chunk_data, ConnectionArgs&&... args)
    : _pimpl(new PimplType(*static_cast<DerivedType*>(this), chunk_data, std::forward<ConnectionArgs>(args)...))
{
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
template<typename FirstArg,
         typename... ConnectionArgs,
         typename std::enable_if<!std::is_same<DataTraits,typename std::decay<FirstArg>::type>::value, int>::type>
PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::PacketStream(
      FirstArg&& arg
    , ConnectionArgs&&... args
)
    : _pimpl(new PimplType(*static_cast<DerivedType*>(this), typename PimplType::ConnectionArgsTag(), std::forward<FirstArg>(arg), std::forward<ConnectionArgs>(args)...))
{
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::~PacketStream()
{
    _pimpl->stop();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::missing_packets_before_realign(LimitType num)
{
    _pimpl->missing_packets_before_realign(num);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::purge_cadence(LimitType num)
{
    _pimpl->purge_cadence(num);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::number_of_buffers(unsigned n)
{
    _pimpl->number_of_buffers(n);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::start()
{
    _pimpl->start();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::set_remote_end_point(typename ConnectionTraits::EndPointType const& endpoint)
{
    _pimpl->set_remote_end_point(endpoint);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
typename PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::PimplType::Context const& PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::context() const
{
    return _pimpl->_context_mgr.context();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
typename PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::PimplType::Context& PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::context()
{
    return _pimpl->_context_mgr.context();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::set_remote_end_point(IpAddress const& endpoint)
{
    _pimpl->set_remote_end_point(endpoint);
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::stop()
{
    _pimpl->stop();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
typename ConnectionTraits::EndPointType PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::local_end_point() const
{
    return _pimpl->local_end_point();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
bool PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::process()
{
    return _pimpl->process();
}

template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
void PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>::init()
{
    _pimpl->init();
}

} // namespace panda
} // namespace ska
