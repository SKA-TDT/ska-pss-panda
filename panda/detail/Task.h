/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TASK_H
#define SKA_PANDA_TASK_H

#include "panda/TupleDispatch.h"
#include "panda/AlgorithmInfo.h"
#include "panda/detail/TaskBase.h"
#include <tuple>
#include <memory>


namespace ska {
namespace panda {
namespace detail {

/**
 * @brief
 *    Description of an asyncronous task that can support multiple arhitectures
 *
 * @details
 *    Retains the data to be moved to the task when a resource is ready
 *    as well as calling the operator()(PoolResource<Arch>&) function of
 *    TaskTraits class whenever the resource becomes available.
 *
 * @code
 *
 * struct MyTraits {
 *     typedef std::tuple<ArchA, ArchB> Architectures;
 *     typedef std::tuple<Capability, ArchB> Capabilities;
 *     void operator()(PoolResource<ArchA>&) {  // do something on ArchA resource }
 *     void operator()(PoolResource<ArchB>&) {  // do something on ArchB resource }
 * };
 *
 * MyTraits traits;
 *
 * resource_manager.submit(std::make_shared<Task<MyTraits>>(traits, data));
 *
 * @endcode
 *
 * If you need runtime selection of algorithms you can use the class ConfigurableTask class
 * n.b. This will introduce the overhead of a virtual call.
 */

template<typename TaskTraits, typename... DataTypes>
class Task : public detail::TaskBase<typename ArchitectureInfo<TaskTraits>::Architectures>
{
    public:
        typedef typename ArchitectureInfo<TaskTraits>::Architectures Architectures;
        typedef typename ArchitectureInfo<TaskTraits>::Capabilities Capabilities;

    private:
        typedef detail::TaskBase<Architectures> BaseT;
        typedef std::tuple<typename std::decay<DataTypes>::type...> DataType;

    public:
        template<typename... DataTypeArgs>
        Task(TaskTraits&, DataTypeArgs&&...data);
        ~Task();

        template<typename Capability, typename Arch>
        bool is_compatible(PoolResource<Arch> const&) const;

        template<typename Arch, int... S>
        inline void launch_with_data(PoolResource<Arch>& r, seq<S...>);

        template<typename Arch>
        inline void launch(PoolResource<Arch>& r);

    private:
        TaskTraits& _tasks;
        DataType _data;
};

/**
 * @brief speicialisation that allows you to pass the Traits class as a shared_ptr rather than a reference
 * @details
 * The shared_ptr will be kept during the lifetime of the asyncronous task, allowing you to use the shared_ptr
 * to control the lifetime of the trits object
 *
 * If you are finding lots of SEGFAULTS on destruction then you should look carefully at lifetimes and consider
 * using this model.
 */
template<typename TaskTraits, typename... DataTypes>
class Task<std::shared_ptr<TaskTraits>, DataTypes...> : public Task<TaskTraits, DataTypes...>
{
        typedef Task<TaskTraits, DataTypes...> BaseT;

    public:
        template<typename... DataTypeArgs>
        Task(std::shared_ptr<TaskTraits>, DataTypeArgs&&...data);

    private:
        std::shared_ptr<TaskTraits> _traits_ptr;
};

} // namespace detail
} // namespace panda
} // namespace ska
#include "panda/detail/Task.cpp"

#endif // SKA_PANDA_TASK_H
