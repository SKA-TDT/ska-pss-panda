/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/ConnectionImpl.h"
#include "panda/Error.h"


namespace ska {
namespace panda {
namespace detail {

// generic socket
template<typename ConnectionTraits, typename Protocol>
template<typename... Args>
ConnectionImpl<ConnectionTraits, Protocol>::ConnectionImpl(Args&&... args)
    : BaseT(std::forward<Args>(args)...)
{
}

/// -------- UDP specializations ----------
template<typename ConnectionTraits>
template<typename ConfigType>
ConnectionImpl<ConnectionTraits, panda::Udp>::ConnectionImpl(ConfigType const& config)
    : BaseT(config.engine())
{
    this->set_remote_end_point(config.remote_end_point());
    boost::system::error_code ec;
    auto const& endpoint = config.local_endpoint();
    this->_socket.bind(endpoint.template end_point<typename ConnectionTraits::EndPointType>(), ec);
    if(ec) {
        panda::Error e(ec);
        e << endpoint;
        throw e;
    }
}

template<typename ConnectionTraits>
ConnectionImpl<ConnectionTraits, panda::Udp>::ConnectionImpl(EngineType& engine)
    : BaseT(engine)
{
}

template<typename ConnectionTraits>
ConnectionImpl<ConnectionTraits, panda::Udp>::ConnectionImpl(EngineType& engine, typename ConnectionTraits::SocketType socket)
    : BaseT(engine, std::move(socket))
{
}

template<typename ConnectionTraits>
ConnectionImpl<ConnectionTraits, panda::Udp>::ConnectionImpl(EngineType& engine, typename ConnectionTraits::EndPointType bind_address)
    : BaseT(engine)
{
    this->socket().bind(std::move(bind_address));
}

template<typename ConnectionTraits>
void ConnectionImpl<ConnectionTraits, panda::Udp>::connect(std::function<void(boost::system::error_code)> const& handler)
{
    handler(boost::system::error_code());
}

template<typename ConnectionTraits>
template<typename HandlerType>
void ConnectionImpl<ConnectionTraits, panda::Udp>::recv(typename BaseT::BufferType& buffer, HandlerType handler)
{
    this->_socket.async_receive_from(boost::asio::mutable_buffers_1(
              static_cast<void*>(buffer.data())
            , buffer.size())
            , this->_remote_endpoint
            , std::move(handler));
}

} // namespace detail
} // namespace panda
} // namespace ska
