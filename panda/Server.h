/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SERVER_H
#define SKA_PANDA_SERVER_H

#include "panda/Stream.h"
#include "panda/ServerConfig.h"
#include "panda/ServerTraits.h"
#include "panda/ServerClient.h"
#include <memory>
#include <vector>

namespace ska {
namespace panda {

/**
 * @brief
 *    A Basic Server template. Manages connections and standard settings.
 *    Can be single or multi-threaded server depending on the engine passed from
 *    the ServerConfig.
 *
 * @details
 *
 * Running A Server
 * ----------------
 *
 * @code
 *    ServerConfig settings;                      // see api for configuration options
 *    MyApp app;
 *    Server<ServerTraits<Tcp>, MyApp> my_app_server(settings, app);
 *    // app specific settings here
 *    return server.run();                          // run the server
 * @endcode
 *
 * The Server Application
 * ----------------------
 *    The Server's job is to manage connections. It is the job of the ServerApp to
 * do actually do something. The app will be notified when the server is starting up, stopping
 * and every time a client connects or an error occurs whilst communicating with a client.
 * Note that the Session type is defined by the ServerTraits.
 *
 * Server stop
 * -----------
 *
 *
 * \verbatim
 *     +--------+  stop()   +---------+
 *     | Server | ------->  | MyApp   |   // MyApp should finish/abort its current tasks.
 *     +--------+           +---------+   // It is important that the App posts no more events to the engine.
 *
 * \endverbatim
 *
 * Normal Connection From A Client
 * -------------------------------
 *
 *
 * \verbatim
 *     +--------+  on_connect(Session)    +---------+
 *     | Server | --------------------->  | MyApp   |   // MyApp should use the Session object to communicate
 *     +--------+                         +---------+   // The connection will be closed as soon as you let Session go
 *                                                      // out of scope, so keep a copy if you want to maintain the connection
 *
 * \endverbatim
 *
 * Error Condition From A Client
 * ------------------------------
 *
 *
 * \verbatim
 *     +--------+  on_error(Session, Error)   +---------+
 *     | Server | ------------------------->  | MyApp   |   // MyApp should use the Session object to communicate
 *     +--------+                             +---------+
 *
 * \endverbatim
 *
 * \ingroup connectivity
 * @{
 */


template<typename ServerTraits, class ServerApp>
class Server
{   
        typedef typename ServerTraits::Protocol Protocol;

    public:
        typedef typename ServerTraits::EndPoint EndPointType;
        typedef typename ServerTraits::Socket SocketType;

        Server(ServerConfig const &, ServerApp& app);
        ~Server();

        Server(Server const&) = delete;

        /**
         * @brief the end point (socket ip/port) that the server is listening on
         */
        EndPointType end_point() const;

        /**
         * @brief start the server. Blocks until stop() is called
         * @see process_events
         */
        int run();

        /**
         * @brief returns true if the server is running
         */
        bool is_running() const;

        /**
         * @brief process outstandng requests. Return when done.
         */
        int process_events();

        /**
         * @brief stops a running server.
         */
        void stop();

    protected:
        void _start_receive();

    protected:
        Engine& engine();

    protected:
        const ServerConfig& _settings;

    private:
        bool _run;
        ServerApp& _app;
        SocketType  _listen_socket;
        std::vector<char> _recv_buffer;
};

} // namespace panda
} // namespace ska

#include "panda/detail/Server.cpp"

#endif // SKA_PANDA_SERVER_H
/**
 *  @}
 *  End Document Group
 **/
