/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONNECTIONTRAITS_H
#define SKA_PANDA_CONNECTIONTRAITS_H

#include "panda/ErrorPolicyConcept.h"
#include "panda/ConnectionTraitsConcept.h"
#include "panda/CumulativeDelayErrorPolicy.h"
#include "panda/PassiveErrorPolicy.h"
#include "panda/ProcessingEngine.h"
#include "panda/NetworkTypes.h"
#include "panda/Engine.h"
#include <boost/asio/ip/basic_endpoint.hpp>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *    Default Traits class definiton for use with the Connection class
 */

template<typename Protocol>
struct DefaultErrorPolicy {
    typedef CumulativeDelayErrorPolicy Policy;

    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<Policy>));
};

template<>
struct DefaultErrorPolicy<Udp> {
    typedef PassiveErrorPolicy Policy;

    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<Policy>));
};

template<typename Protocol,
         typename ErrorPolicy = boost::use_default,       /// use DefaultErrorPolicy<Protocol>::Policy by default
         typename Engine = boost::use_default,            /// use panda::ProcessingEngine by default
         typename Socket = boost::use_default,            /// typename Protocol::socket by default
         typename EndPoint = typename Protocol::endpoint
         >
struct ConnectionTraits
{
    typedef Protocol ProtocolType;
    typedef typename std::conditional<std::is_same<ErrorPolicy, boost::use_default>::value
                        , typename DefaultErrorPolicy<Protocol>::Policy
                        , ErrorPolicy>::type ErrorPolicyType;
    typedef typename std::conditional<std::is_same<Engine, boost::use_default>::value
                        , panda::Engine
                        , Engine>::type EngineType;
    typedef typename std::conditional<std::is_same<Socket, boost::use_default>::value
                        , typename Protocol::socket
                        , Socket>::type SocketType;
    typedef EndPoint EndPointType;

    BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));
};

template<typename ErrorPolicy>
struct ConnectionTraits<Udp, ErrorPolicy, void> : public ConnectionTraits<Udp, ErrorPolicy, boost::use_default, boost::asio::ip::udp::socket>
{
    BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));
};

template<typename ErrorPolicy>
struct ConnectionTraits<Tcp, ErrorPolicy, void> : public ConnectionTraits<Tcp, ErrorPolicy, boost::use_default, boost::asio::ip::tcp::socket>
{
    BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONNECTIONTRAITS_H 
