/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MIXINTIMERCONCEPT_H
#define SKA_PANDA_MIXINTIMERCONCEPT_H

#include "panda/Concept.h"




/**
 * @brief The MixInTimerConcept class check the requirements on an MixInTimer type
 *
 * @details The MixInTimer concept must support a non-const functor operator()() with an access level of at least 'protected'
 *
 * MixInTimer examples:
 *
 * example 1
 * @code
 * struct MyClass {
 *     void operator()();
 * };
 *
 * MixInTimer<MyClass> my_timed_class;
 * my_timed_class(); // MyClass::operator() will now be timed
 * @endcode
 *
 * example 2
 * @code
 * struct MyClass {
 *     double operator()(int);
 * };
 *
 * MixInTimer<MyClass> my_timed_class;
 * double result = my_timed_class(99); // MyClass::operator(int) will now be timed
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct MixInTimerConcept: Concept<X> {
        typedef Concept<X> Base;

        // Helper class to test 'protected' functionality
        struct MixInSub: X {};

    public:
        BOOST_CONCEPT_USAGE(MixInTimerConcept) {
            /// Classes modelling the MixInTimer concept must support a non-const functor operator()() with an access level of at least 'protected'
            Base::reference<MixInSub>()();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_MIXINTIMERCONCEPT_H
