/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_BUFFER_H
#define SKA_PANDA_BUFFER_H
#include <memory>
#include <cstddef>

namespace ska {
namespace panda {

/**
 * @brief
 *   Primary interface to access memory buffers in panda
 *
 * @details
 *   Memory is guaranteed to exist while the object exists and
 *   will be freed on destruction.
 *   The memory is not initialised in any way
 */
template <typename T = void>
class Buffer
{
    public:
        typedef T* iterator;
        typedef const T* const_iterator;

    public:
        /**
         * @details
         * Create a buffer that holds size elements. If T=void
         * then will allocate size bytes.
         *
         * @arg size the number of elements to allocate.
         */
        Buffer(std::size_t size);
        Buffer(Buffer const& b); // copy constructor
        Buffer(Buffer&& b); // move constructor
        ~Buffer();

        /// return a pointer to the actual data (or nullptr)
        T* data(unsigned offset=0);
        const T* data(unsigned offset=0) const;

        // return iterator to the first element
        iterator begin();
        const_iterator begin() const;
        const_iterator cbegin() const;

        // return iterator to the last element + 1
        iterator end();
        const_iterator end() const;
        const_iterator cend() const;

        /**
         *  @brief returns the number of elements in the
         *  buffer. if T==void then the number of bytes.
         */
        std::size_t size() const;

        /**
         *  @brief swap the contents of this buffer with that of the provided
         */
        void swap(Buffer<T>&);

    private:
        std::shared_ptr<char> _data;
        std::size_t _size;
};

/**
 * @brief specialisation for void types
 */
template<>
void* Buffer<void>::data(unsigned offset);

/**
 * @brief specialisation for void types
 */
template<>
const void* Buffer<void>::data(unsigned offset) const;

/**
 * @brief specialisation for void types
 */
template<>
Buffer<void>::Buffer(std::size_t size);

} // namespace panda
} // namespace ska

#include "panda/detail/Buffer.cpp"

#endif // SKA_PANDA_BUFFER_H
