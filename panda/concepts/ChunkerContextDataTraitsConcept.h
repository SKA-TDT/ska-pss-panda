/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CHUNKERCONTEXTDATATRAITSCONCEPT_H
#define SKA_PANDA_CHUNKERCONTEXTDATATRAITSCONCEPT_H

#include "panda/Concept.h"
#include "panda/detail/packet_stream/ChunkHolder.h"
#include <type_traits>



/**
 * @brief The ChunkerContextDataTraitsConcept class check the requirements on an ChunkerContextDataTraits type
 *
 * @details The Data class that defines the payload context. It provides a number of methods
 *
 * @code
 *     PacketSizeType packet_size() const; // returns the size of a packets payload
 *     ChunkSizeType chunk_size(DataType const&) const; // returns the size of the provided chunk
 *     LimitType sequence_number(PacketInspector const&); // return the packet number of the provded packet
 *     LimitType max_sequence_number(); // the max value the sequence number can take before resetting to zero
 * @endcode
 *
 * ChunkSizeType and PacketSizeType are diagnosed, so the values returned packet_size() and chunk_size() may be any reasonable integer type.
 * LimitType also is diagnosed from sequence_number(Packet const&) and must be a reasonable integer type.
 * The only limitation is that max_sequence_number() must also return a LimitType.
 *
 * LimitType should be a suitable type that is capable of representing the full range of the sequence, e.g. std::size_t or unsigned
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct ChunkerContextDataTraitsConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(ChunkerContextDataTraitsConcept) {
            /// Must proved a public DataType type member supporting a default constructor
            typedef typename X::DataType DataType;
            DataType data=DataType();
            Base::use(data);

            typedef typename packet_stream::ChunkHolder<X>::LimitType LimitType;
            typedef typename packet_stream::ChunkHolder<X>::PacketSizeType PacketSizeType;
            typedef typename packet_stream::ChunkHolder<X>::ChunkSizeType ChunkSizeType;

            LimitType n(0);
            PacketSizeType ps(0);
            ChunkSizeType cs(0);
            bool is_aligned(false);

            typedef typename X::PacketInspector Packet;

            Packet& packet(Base::template reference<Packet>());

            X& x(Base::reference());

            /// Must provide:
            /// @code
            /// public: LimitType packet_size();
            /// @endcode
            Base::is_same(ps,x.packet_size());

            /// Must provide:
            /// @code
            /// public: LimitType chunk_size(DataType const&);
            /// @endcode
            Base::is_same(cs,x.chunk_size(data));

            /// Must provide:
            /// @code
            /// public: LimitType sequence_number(Packet const&);
            /// @endcode
            Base::is_same(n,x.sequence_number(packet));

            /// Must provide:
            /// @code
            /// public: LimitType max_sequence_number() const;
            /// @endcode
            Base::is_same(n,x.max_sequence_number());

            /// Must provide:
            /// @code
            /// public: bool align_packet(PacketInspector const&);
            /// @endcode
            Base::is_same(is_aligned, x.align_packet(packet));

            /// Must provide:
            /// @code
            /// public: void packet_stats(LimitType missing_packets, LimitType expected_packets);
            /// @endcode
            static_assert(std::is_same<void, decltype(x.packet_stats(0,100))>::value, "packet_stats(missing_packets, expected_packets) not defined in provided DataTraits class");

//         Do not have access to DerivedType (ContextType) - not sure what to do about this yet
//          x.deserialise_packet(PacketType, ContextType);
//          x.process_missing_slice(ContextType);
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_CHUNKERCONTEXTDATATRAITSCONCEPT_H
