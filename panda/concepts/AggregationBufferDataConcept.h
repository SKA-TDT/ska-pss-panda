/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_AGGREGATIONBUFFERDATACONCEPT_H
#define SKA_PANDA_AGGREGATIONBUFFERDATACONCEPT_H

#include "panda/Concept.h"



namespace ska {
namespace panda {

/**
 * @brief AggregationBufferDataConcept checks data type has begin and end methods that produce dereferencable iterators
 *
 * @details The type passed to an AggregationBufferFiller must support begin() and end() methods returning an appropriate iterator that can dereferenced and supports the pre-increment operator
 *
 * \ingroup concepts
 * @{
 */

template <class X>
struct AggregationBufferDataConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(AggregationBufferDataConcept) {
            X& x(Base::reference());

            /// Test the pre increment;
            auto i=x.begin();
            ++i;

            /// The AggregationBufferData concept classes must be iterable
            Base::is_same(x.begin(),x.end());

            /// The Iterator provided by an AggregationBufferData concept class must be dereferencable
            *x.begin(); // dereference iterator
        }
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_AGGREGATIONBUFFERDATACONCEPT_H
