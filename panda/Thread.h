/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_THREAD_H
#define SKA_PANDA_THREAD_H

#include <thread>
#include <vector>
#include <mutex>
#include <functional>
#include <deque>
#include <condition_variable>

namespace ska {
namespace panda {
class ThreadConfig;

/**
 * @brief
 *    Thread - extension of std::thread to permit NUMA optimisation when available
 *
 * @details
 *
 */
class Thread
{
    public:
        Thread();
        Thread(Thread const&) = delete;
        Thread(Thread&&) = delete;

        /**
         * @brief construct the thread and bind to the core specified by 'affinity'. If affinity == -1 the thread is not bound to any particular core
         */
        template<typename Function, typename... Args>
        Thread(std::vector<unsigned> const& affinities, Function&&);
        Thread(std::vector<unsigned> const& affinities);

        template<typename Function, typename... Args>
        Thread(ThreadConfig const& affinities, Function&&);
        Thread(ThreadConfig const& affinities);
        ~Thread();

        /**
         * @brief instruct the thread to run on a specific core - forwards to pthread_setaffinity_np on LINUX otherwise has no effect
         */
        void set_core_affinity(std::vector<unsigned> const& affinities);

        /**
         * @brief return true if this thread is known to be running on core 'affinity'. (This means it returns false if not LINUX)
         */
        bool has_core_affinity(std::vector<unsigned> const& affinities) const;

        /**
         * @brief run the supplied function in the thread
         */
        template<typename Job>
        void exec(Job const&);

        /**
         * @brief wait for thread to terminate
         */
        void join();

    public:
        static void set_core_affinity(std::vector<unsigned> const& affinities, std::thread::native_handle_type);

    protected:
        void wait();

    private:
        inline void exec() {};

    private:
        bool _terminate;
        std::condition_variable _cv;
        std::mutex _mutex;
        std::deque<std::function<void()>> _jobs;
        std::thread _thread;
};

} // namespace panda
} // namespace ska
#include "detail/Thread.cpp"

#endif // SKA_PANDA_THREAD_H
