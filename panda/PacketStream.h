/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PACKETSTREAM_H
#define SKA_PANDA_PACKETSTREAM_H

#include "panda/Producer.h"
#include "panda/detail/packet_stream/ChunkerContext.h"
#include "panda/Connection.h"
#include "panda/ProcessingEngine.h"
#include "panda/Buffer.h"
#include "panda/concepts/ProducerConcept.h"
#include "panda/concepts/ChunkerContextDataTraitsConcept.h"
#include "panda/detail/packet_stream/PacketStreamImpl.h"
#include <memory>

namespace ska {
namespace panda {
class IpAddress;

/**
 * @brief
 * A chunker that reads in data from a packet stream and passes this to a mutli-threaded backend for processing
 * Suitable only for cases where there are a fixed number of samples per packet.
 *
 * @details
 * The PacketStream system is a collection of classes designed to scale with the number of available threads. Functionally
 * it must:
 *  1) Ingest packets
 *  2) Extract the data in the packet into arbitarily sized data structures (called chunks) in user space using packet header info appropriately
 *  3) Handle all error conditions in regard to the packets (missing packets, connection failures etc)
 *
 *  The lifetime management of the chunks is the responsibility of the panda::DataManager with which this class is designed to work
 *  as an InputStream.
 *
 *  Each component of the PacketStream is a c++ template that requires user supplied Traits classes to specify the types of
 *  connection, define the packet formats and perform the various
 *  processing tasks.
 *
 *  +----------------+        +--------------------+
 *  | Packet Injest  |  --->  | Payload Extraction |
 *  +----------------+        +--------------------+
 *   ConnectionTraits          DataTraits
 *
 * @tparam ConnectionTraits
 *         must provide the following TypeDefs
 *
 *         SocketType - the type of socket for the connection
 *         ErrorPolicy - the type of error handling to apply (see panda/ConnectionTraits.h for some examples, and ready to use traits classes)
 *
 * @tparam DataTraits
 *         must provide the following typedefs:
 *
 *         DataType - the type of chunk that is being created
 *         Packet   - a struct representing the exact bit pattern of the packet received
 *         PacketInspector   - a class to interpret the Packet
 *                             The PacketInspector Must proveide the following methods:
 *                                  constructor(Packet const&)
 *                                  bool ignore(); // early stage rejection of processing
 *
 *         If any members are defined in DataTraits then then a DataTraits object will be generated
 *         and will have the lifetime of a single data chunk. Creation of a new chunk will call the
 *         copy operator of DataTraits so state can be transferred if required.
 *         Generally you should provide static members for the following function calls for efficiency
 *         gains, but where needed regular members can be used.
 *
 *         The following calls must be provided by the DataTraits class:
 *
 *         @code
 *               template<typename ContextType>
 *               void deserialise_packet(ContextType& context, PacketInspector const& packet);
 *         @endcode
 *         This must transfer the contents of the packet into the chunk. The context will provide
 *         methods to access the chunk, the chunk offset(to write to),  the packet offset
 *         position (to read from), and the size - i.e. the number of elements to read until the end of
 *         either a packet or chunk.
 *
 *         @code
 *               template<typename MissingContextType>
 *               void process_missing_slice(MissingContextType& context);
 *         @endcode
 *         When a packet is dropped, this function will be called. The MissingSliceContextType will
 *         provide methods for accessing the chunk, the size, and the chunk_offset
 *
 *         @code
 *               template<typename ResizeContextType>
 *               void resize_chunk(ResizeContextType& context);
 *         @endcode
 *         The resize_chunk function will be called whenever there is a break in the stream and there
 *         is not a contiguous stream of data to fill an entire chunk. The ResizeContextType will provide
 *         access to the chunk, and the size required.
 *
 *         Processing for any packet will only happen if the ignore() method returns false
 *         @code
 *              bool ignore(PacketInspector const& packet);
 *         @endcode
 *         If all packets are to be processed it is most efficient to make this a static constexpr
 *         e.g.
 *         @code
 *              static constexpr bool ignore() { return false; }
 *         @endcode
 *
 *         // LimitType should be a suitable type that is capable of representing the full
 *         // range of the sequence
 *         // e.g. std::size_t or unsigned
 *         // n.b. could also be a static mehtod if required
 *         @code
 *                LimitType packet_size() const;                // the size of a packets payload (i.e bytes to extract into a chunk)
 *                LimitType chunk_size(DataType const&) const;  // the size of the provided chunk
 *         @endcode
 *
 *         The following must be static functions:
 *         @code
 *                static LimitType sequence_number(PacketInspector const& packet);  // return the packet number of the provded packet
 *                static LimitType max_sequence_number(); // the max value the sequence number can take before resetting to zero
 *                bool align_packet(PacketInspector const&); // return true to indeicate that the packet is suitable for aligning with the beginning of a chunk
 *         @endcode
 *
 *         If you define a DataTraits class that has members, an instance will be available in the context object (accessible via the method data())
 *         The instance will have the lifetime of the underlying data chunk. i.e. whenever a new data chunk is created, a new DataTraits instance will
 *         also be created via the copy constructor.
 *
 * example: A UDP stream
 *
 * We first need to provide a class to describe the UDP packet. This class must match the bit paatern of the packet and so it cannot
 * contain any additional state.
 * \include "packet_stream/ExamplePacket.h"
 *
 * The PacketInspector in this case is very simple. It provides a wrapper around the ExamplePacket to interpret the packet data and
 * provide the necessary interface for the PacketStream.
 * this class can contain additional state as required.
 *
 * \include "packet_stream/ExamplePacketInspector.h"
 *
 * The DataTraits will now declare the ExamplePacketInspector as our PacketInspector as well as implementing the required interface
 * methods.
 * \include "packet_stream/ExampleDataTraits.h"
 *
 * Now we have defined our DataTraits we can define a PacketStream to use them.
 *
 * \include "packet_stream/ExamplePacketStream.h"
 *
 * @code
 *     panda::Engine engine;
 *     unsigned port; // set to the port number data is coming in on
 *
 *     // create a socket listening to the required port
 *     typename MyConnectionTraits::SocketType socket(engine, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port));
 *
 *     // setup the PacketStream
 *     PacketStream<MyConnectionTraits, ExampleDataTraits> stream(std::move(socket));
 *
 * @endcode
 *
 * @details
 * The stream maintains a single thread to fill packets which are then delegated to
 * any number of other threads for processing.
 *
 * ## Logical Flow
 *
 * @verbatim
 *    start_chunk()
 *         |--- wait for a Buffer to become free in the buffer pool
 *         |--- register a callback to get_packet(buffer) to be called when data arrives on the socket
 *
 *    get_packet(buffer)
 *         |--- fill the buffer with the data received up to the size of the packet
 *         |--- packet worth of data? (n.b should be a guaranteed yes if packet is equivalent to the underlying socket datagram)
 *                           |- yes - schedule process_packet(buffer) to be run
 *                           |- no  - register self as callback for any new data
 *
 *    process_packet(context, buffer)
 *         |-- execute packet deserialisation command
 *         |-- update context information
 *         |-- release buffer back to the pool
 *         |-- register a call to get_packet()
 *
 * @endverbatim
 */

/**
 * @class PacketStream
 */
template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType=DerivedType>
class PacketStream : public Producer<ProducerType, typename DataTraits::DataType>
{
    BOOST_CONCEPT_ASSERT((ProducerConcept<ProducerType>));
    BOOST_CONCEPT_ASSERT((ChunkerContextDataTraitsConcept<DataTraits>));

    private:
        typedef packet_stream::PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerType> PimplType;
        /// DataTraits must define the PacketType - a struct representing the bit pattern of the Packet
        //  and supporting the size() method
    protected:
        typedef typename DataTraits::DataType DataType;
        typedef typename PimplType::ContextPtr ContextPtr;
        typedef typename PimplType::ContextManager ContextManagerType;
        typedef typename PimplType::LimitType LimitType;

    public:
        typedef DerivedType DerivedClass; // required for factory generator

    protected:

        // Consturctor to allow initialisation of chunk data, forwardiing remaining arguments to the connection object
        template<typename... ConnectionArgs>
        PacketStream(DataTraits const& initialised_data, ConnectionArgs&&... args);

        // forward arguments to the connection object (enable_if disables matching if a DataTraits type is passed
        template<typename FirstConnectionArg,
                 typename... ConnectionArgs,
                 typename std::enable_if<!std::is_same<DataTraits,typename std::decay<FirstConnectionArg>::type>::value, int>::type = 0>
        PacketStream(FirstConnectionArg&& arg, ConnectionArgs&&... args);

        PacketStream(PacketStream&&);


    public:
        void init() override;
        ~PacketStream();

        /**
         * @brief set the number of ingest buffers
         * @details should roughly match the number of threads available
         */
        void number_of_buffers(unsigned n);

        /**
         * @brief start listening for data
         */
        void start();

        /**
         * @brief set the endpoint of the peer to connect to
         */
        void set_remote_end_point(typename ConnectionTraits::EndPointType const&);
        void set_remote_end_point(IpAddress const&);

        /**
         * @brief  return the local end point (i.e. port bound to)
         */
        typename ConnectionTraits::EndPointType local_end_point() const;

        /**
         * @brief execute a single event in the event loop before returning
         * @details non-blocking. If there are no events will return immediately
         */
        bool process();

        /**
         * @brief halts data taking
         */
        void stop();

        /**
         * @brief set the number of missing packets that are allowed (i.e will receive missing_packet notifiactions)
         *        before the current chunk should be resized and flushed and a new one started
         */
        void missing_packets_before_realign(LimitType num);

        /*
         * @brief setup the purge cadence (number of contexts to wait before a context is purged)
         * @param cadence 0=prev ctx, 1=ctx prior to this etc.
         */
        void purge_cadence(LimitType cadence);

    protected:
       /**
        * @brief return the current context
        */
        typename PimplType::Context const& context() const;
        typename PimplType::Context& context();

    private:
        std::shared_ptr<PimplType> _pimpl;
};

} // namespace panda
} // namespace ska
#include "panda/detail/PacketStream.cpp"

#endif // SKA_PANDA_PACKETSTREAM_H
