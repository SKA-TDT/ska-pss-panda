/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MIXINTIMERTRAITSCONCEPT_H
#define SKA_PANDA_MIXINTIMERTRAITSCONCEPT_H

#include "panda/ClockTypeConcept.h"
#include "panda/ConceptChecker.h"




/**
 * @brief The MixInTimerTraitsConcept class check the requirements on an MixInTimerTraits type
 *
 * @details A MixinTimerTraits concept class must provide:-
 *       ClockType - use a different clock than the default high_resolution_clock. ClockType must support the now() function
 *       Handler   - The handler of timing information. The handler will be passed the start and end times of the operator. The default handler echos the elapsed time to std::out
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct MixInTimerTraitsConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(MixInTimerTraitsConcept) {
            /// Classes modelling the MixInTimerTraits concept must provide a ClockType class satisfying the requirements of ClockTypeConcept
            ConceptChecker<ClockTypeConcept>::Check<typename X::ClockType>::use();

            /// Classes modelling the MixInTimerTraits concept must provide a Handler class providing a functor operator()()
            typedef typename X::Handle Handler;
            Handler& handler(Base::template reference<Handler>());
            handler();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_MIXINTIMERTRAITSCONCEPT_H
