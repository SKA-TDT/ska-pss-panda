/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LAUNCHER_H
#define SKA_PANDA_LAUNCHER_H

#include "panda/detail/LauncherConfig.h"
#include <memory>

namespace ska {
namespace panda {

/**
 * @brief
 *    Templatable Launcher for configuring and launching pipelines
 *
 * @details
 *    This template encapsulates a pattern of constructing and launching pipelines
 *    complete with suitable help options etc.
 *
 *    To use provide a suitable LauncherTraits class with the following components
 *
 *    @tparam ComputeFactory  : a class that can generate your compute pipelines (in this ex: ComputeModule)
 *                                  constructor must accept the Config type, and a Sink.
 *                              Must support the methods:
 *                              @code
 *                                  std::vector<std::string> available() const;
 *                                  ComputeModule* create(std::string const& type);
 *                              @endcode
 *
 *    @tparam SourceFactory   : a class that creates, and launches, the pipelins with
 *                              a apecific data source in place.
 *                                  constructor must accept the Config type.
 *                              @code
 *                                  std::vector<std::string> available() const;
 *                                  int Factory::exec(std::string const& source_name, ComputeModule& runtime_handler)
 *                              @endcode
 *
 *    @tparam Sink            : the class that handles all data export
 *                              Must support the methods:
 *                              @code
 *                                  std::vector<std::string> available() const;   // types of sink available
 *                              @endcode
 *
 *    @tparam SinkConfig      : the configuration class type for the Sink class
 *    @tparam SourceConfig    : the configuration class type for the SourceFactory
 *    @tparam Config          : the configuration class for the processing pipelines
 *                              This would normally be bassed on panda::Config or anything that inherits
 *                              from panda::ConfigModule if pool management is not required.
 *
 *    To create an executable with the standard launcher help etc:
 *
 *    @code
 *    int main(int argc, char** argv) {
 *        ska::panda::Launcher<MyLauncherTraits> launcher;
 *        if(int rv=launcher.parse(argc, argv)) return rv;
 *        return launcher.exec();
 *    }
 *    @endcode
 *
 */
template<typename LauncherTraits>
class Launcher
{
        typedef typename LauncherTraits::ComputeFactory ComputeFactory;
        typedef typename LauncherTraits::SourceFactory SourceFactory;
        typedef typename LauncherTraits::Sink Sink;
        typedef typename LauncherTraits::SinkConfig SinkConfigType;
        typedef typename LauncherTraits::Config ConfigType;
        typedef typename LauncherTraits::SourceConfig SourceConfigType;

    public:
        Launcher();
        ~Launcher();

        /**
         * @brief parse the configuration options from config files and the command line
         */
        int parse(int argc, char** argv);

        /**
         * @brief Launch the pipeline according to the confiuration
         * @details call parse before calling this method
         */
        int exec();

    private:
        detail::LauncherConfig<ConfigType, SinkConfigType, SourceConfigType, ComputeFactory, Sink>  _config;
        SourceFactory                     _source_factory;
        std::unique_ptr<Sink>             _sink;
        std::unique_ptr<ComputeFactory>   _compute_factory;
        bool                              _parsed;

        std::unique_ptr<typename ComputeFactory::Type> _compute;
};

} // namespace panda
} // namespace ska
#include "panda/detail/Launcher.cpp"

#endif // SKA_PANDA_LAUNCHER_H
