/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LOG_H
#define SKA_PANDA_LOG_H

#include "panda/detail/LogLine.h"
#include "panda/detail/LogLineHolder.h"
#include "panda/detail/LogLineSentry.h"
#include "panda/detail/LoggingThread.h"
#include <string>
#include <iostream>
#include <array>
#include <vector>
#include <mutex>
#include <atomic>
#include <limits>

namespace ska {
namespace panda {

/**
 * @brief
 *    Basic multi-threaded logging
 *
 * @details
 *    Logging has to be fast and safe to ensure we get the messages
 *    even in the event of exceptions or alloc failures.
 *
 *    This logging framework reserves a buffer for each log message in the code
 *    and this buffer can be filled dynamically with the usual << operators.
 *    If any of these streaming operator results in an exception then the exisiting message buffer
 *    will be logged with a message that the log is truncated.
 *
 * ## compile time options
 * Compiling with PANDA_LOG_DEBUG set to true e.g.
 * @code
 * #define PANDA_LOGGING_THREAD true
 * @endcode
 * will assign writing of log messages to a seperate thread.
 * By default the messages are sent immediately blocking the processing thread
 *
 * example use to log messages
 * @code
 *    PANDA_LOG_ERROR << "some error message";
 *    PANDA_LOG_WARN << "some waring message";
 *    PANDA_LOG << "general log info message";
 *    PANDA_LOG_DEBUG << "a debug message";
 * @endcode
 *
 * example activating/deactivating logging levels
 * @code
 *    // shows the default states
 *    panda::log.enable(true);
 *    panda::log_warn.enable(true);
 *    panda::log_error.enable(true);
 *    panda::log_debug.enable(false);
 * @endcode
 */

class Logger
{
    public:
        Logger(std::string&& level_msg, bool enabled);
        ~Logger();

        /**
         * @brief summary of the logger type as a string
         */
        std::string const& prefix() const;

        /**
         * @brief return true if logging is required
         */
        bool enabled() const;

        /**
         * @brief set to true to activate logging, false to deactivate
         */
        void enable(bool);

        /**
         * @brief send msg to be logged
         */
        void send(LogLineHolder& msg);

    protected:
        friend class LoggingThread;
        static void process(); // TODO remove to thread class

    private:
        const std::string _prefix;
        bool _enabled;
        static std::vector<std::ostream*> _outputstreams;

        static std::mutex _mutex;
        static std::atomic<unsigned char> _pos;
        static unsigned char _send_pos;
        static unsigned char _written;
        typedef std::array<LogLineHolder, 256> ArrayType;

        static ArrayType _msg_queue;
#ifdef PANDA_LOGGING_THREAD
        static LoggingThread _write_thread;
#endif
};

template<typename T>
LogLineSentry operator<<(Logger& logger, T&& t)
{
    static panda::LogLineHolder default_logline("","");
    ska::panda::LogLineSentry sentry(logger, default_logline);
    sentry.send(t);
    return sentry;
}

#define PANDA_LOG PANDA_GENERATE_LOG_ENTRY(ska::panda::log)
#define PANDA_LOG_WARN PANDA_GENERATE_LOG_ENTRY(ska::panda::log_warn)
#define PANDA_LOG_ERROR PANDA_GENERATE_LOG_ENTRY(ska::panda::log_error)
#define PANDA_LOG_DEBUG PANDA_GENERATE_LOG_ENTRY(ska::panda::log_debug)

extern Logger log;
extern Logger log_warn;
extern Logger log_error;
extern Logger log_debug;

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_LOG_H
