/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATARATEMONITOR_H
#define SKA_PANDA_DATARATEMONITOR_H

#include "panda/DataRate.h"
#include "panda/TickTockTimer.h"
#include "panda/ClockTypeConcept.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#pragma GCC diagnostic pop
#include <functional>
#include <memory>


namespace ska {
namespace panda {

/**
 * @brief
 *    class to collect statistics and aid in measuring of data rates
 *
 * @details
 * 
 * @code
 * panda::DataRateMonitor monitor;
 * std::vector<float> some_data(100);
 *
 * // collect data
 * for(unsigned i=0; i<100; ++i) {
 *    auto timer = monitor(some_data.size()*sizeof(float));
 *    do_something_with_data(some_data);
 *    timer();
 * }
 *
 * // extract stats
 * std::cout << monitor.mean();
 * @endcode
 */
template<typename ClockType>
class DataRateMonitor : public std::enable_shared_from_this<DataRateMonitor<ClockType>>
{
    BOOST_CONCEPT_ASSERT((ClockTypeConcept<ClockType>));

    protected:
        /**
         * @brief calculate a new data rate given the size and time duration and add this to the stats
         */
        void operator()(std::size_t number_of_bytes, typename ClockType::duration const& time);

    public:
        typedef TickTockTimer<ClockType, std::function<void(typename ClockType::duration const&)>> TimerType;
        typedef DataRate<double> DataRateType;

    public:
        DataRateMonitor();
        ~DataRateMonitor();

        /**
         * @brief start a timed probe for the processing of the provided data
         * @return an object that will be a running timer. The timer will be stopped either
         *         on destruction or a call to its tock() (c.f tick()/tick() timer) function.
         */
        TimerType operator()(std::size_t number_of_bytes);

        /**
         * @brief return the rolling mean of the data collected
         */
        DataRateType mean() const;



    private:
        typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::rolling_mean> > RateAccumulatorType;
        RateAccumulatorType _data_rates;
};


} // namespace panda
} // namespace ska
#include "panda/detail/DataRateMonitor.cpp"

#endif // SKA_PANDA_DATARATEMONITOR_H 
