/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TUPLEUTILITIES_H
#define SKA_PANDA_TUPLEUTILITIES_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
//#pragma GCC diagnostic ignored "-Wnon-template-friend"
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/list.hpp>
#pragma GCC diagnostic pop
#include <tuple>

namespace ska {
namespace panda {

/**
 * @brief A collection of utilities to manipulate the std::tuple
 */

///--------------------------------------------------------------------
// @brief Index
// @details Static helper function template to return the index of a
//          given type in a tuple.
// @member value : the index of the type
// --------------------------------------------------------------------
template <class T, class Tuple>
struct Index;

template <class T, class... Types>
struct Index<T, std::tuple<T, Types...>> {
    static const std::size_t value = 0;
};

template <class T, class U, class... Types>
struct Index<T, std::tuple<U, Types...>> {
    static const std::size_t value = 1 + Index<T, std::tuple<Types...>>::value;
};

template<typename T, typename TupleType>
constexpr std::size_t index(TupleType&&) { return panda::Index<T, typename std::decay<TupleType>::type>::value; }

///--------------------------------------------------------------------
// @brief IndexIgnoreConst
// @details Static helper function template to return the index of a
//          given type in a tuple, where a type T and const T are considered
//          to be equivalent
///--------------------------------------------------------------------
template <class T, class Tuple, class Enable=void>
struct IndexIgnoreConst;

template <class T, class... Types>
struct IndexIgnoreConst<T, std::tuple<T, Types...>> {
    static const std::size_t value = 0;
};

template <class T, class... Types>
struct IndexIgnoreConst<T, std::tuple<const T, Types...>, typename std::enable_if<!std::is_const<T>::value>::type> {
    static const std::size_t value = 0;
};

template <class T, class U, class... Types>
struct IndexIgnoreConst<T, std::tuple<U, Types...>, typename std::enable_if<!std::is_same<typename std::remove_const<U>::type, typename std::remove_const<T>::type>::value>::type > {
    static const std::size_t value = 1 + IndexIgnoreConst<T, std::tuple<Types...>>::value;
};

///--------------------------------------------------------------------
// @brief HasType
// @details Static helper function template to indicate wheter a
//          given type is present in a tuple.
// @member value : true if the type is in the tuple
// --------------------------------------------------------------------
template <class T, class Tuple>
struct HasType {
    static const bool value = false;
};

template <class T>
struct HasType<T, T> {
    static const bool value = true;
};

template <class T, class... Types>
struct HasType<T, std::tuple<T, Types...>> {
    static const bool value = true;
};

template <class T, class U, class... Types>
struct HasType<T, std::tuple<U, Types...>> {
    static const bool value = HasType<T, std::tuple<Types...>>::value;
};

template <typename T, template <typename...> class Type, typename... TTypes>
struct HasType<T, Type<T, TTypes...>> {
    static const bool value = true;
};

template <typename T, template <typename...> class Type, typename U, typename... TTypes>
struct HasType<T, Type<U, TTypes...>> {
    static const bool value = HasType<T, Type<TTypes...>>::value;
};

///--------------------------------------------------------------------
// @brief CovertTupleParamsToConst
// --------------------------------------------------------------------
template<typename T>
struct CovertTupleParamsToConst;
///--------------------------------------------------------------------
// @brief for_each
// @details iterate over a tuple calling a suitable function f for each Type
//          The FuncType must be a type that defines the operator()(T) for
//          each type.
// --------------------------------------------------------------------

template<std::size_t I=0, typename FuncType, typename... Tp, typename... Args>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(std::tuple<Tp...>&, FuncType&&, Args&&...)
{ }

template<std::size_t I=0, typename FuncType, typename... Tp, typename... Args>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(std::tuple<Tp...> const&, FuncType&&, Args&&...)
{ }

template<std::size_t I = 0, typename FuncType, typename... Tp, typename... Args>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(std::tuple<Tp...>& t, FuncType&& f, Args&&... args)
{
    typedef std::tuple<Tp...> TupleType;
    f(std::get<I>(t), std::forward<Args>(args)...);
    for_each<I + 1, FuncType, Tp...>(std::forward<TupleType&>(t), std::forward<FuncType>(f), std::forward<Args>(args)...);
}

template<std::size_t I = 0, typename FuncType, typename... Tp, typename... Args>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(std::tuple<Tp...> const& t, FuncType&& f, Args&&... args)
{
    typedef std::tuple<Tp...> const TupleType;
    f(std::get<I>(t), std::forward<Args>(args)...);
    for_each<I + 1, FuncType, Tp...>(std::forward<TupleType const&>(t), std::forward<FuncType>(f), std::forward<Args>(args)...);
}

/**
 * @brief iterate over each type in a tuple, without actually having a concrete instance of the tuple
 * @details for each Type will call the passed function template operator<Type>() method
 *
 * @code
 * typedef std::tuple<A, B, C> MyTupleType;
 *
 * temaplte<typename T>
 * struct MyFunctionTemplate {
 *    inline void operator()() {
 *          // do something here T specific
 *    }
 * };
 *
 * panda::ForEach<MyTupleType, MyFunctionTemplate>::exec();
 * @endcode
 *
 * n.b. you can pass in other params through exec to call the corresponding
 * operator()(...) on your template.
 */
template<typename TupleType, template <typename> class>
struct ForEach {
    template<typename... DataTypes>
    static inline void exec(DataTypes&&...) {}
};

template<template <typename> class FunctionTemplate, typename T, typename... Tp>
struct ForEach<std::tuple<T, Tp...>, FunctionTemplate> {
    template<typename... DataTypes>
    static inline void exec(DataTypes&&... data) {
        FunctionTemplate<T>()(std::forward<DataTypes>(data)...);
        ForEach<std::tuple<Tp...>, FunctionTemplate>::exec(std::forward<DataTypes>(data)...);
    }
};

/**
 * @brief as ForEach except will only continue the iteration if false is returned by
 *           the operator()(...).
 * @details i.e the loop will be aborted on the first return of true
 *          will return false if the entire loop was completed, true otherwie
 */
template<typename TupleType, template <typename> class>
struct ForEachIf {
    template<typename... DataTypes>
    static inline bool exec(DataTypes&&...) { return false; }
};

template<template <typename> class FunctionTemplate, typename T, typename... Tp>
struct ForEachIf<std::tuple<T, Tp...>, FunctionTemplate> {
    template<typename... DataTypes>
    static inline bool exec(DataTypes&&... data) {
        if(FunctionTemplate<T>()(std::forward<DataTypes>(data)...)) return true;
        return ForEachIf<std::tuple<Tp...>, FunctionTemplate>::exec(std::forward<DataTypes>(data)...);
    }
};

///--------------------------------------------------------------------
// @brief delete_tuple
// @details delete all pointers in a tuple
// --------------------------------------------------------------------
template<typename... Types>
struct Deleter {
    template <class T>
    void operator()(T ptr) {
        delete ptr;
    }
};

template<typename... Types>
inline void delete_tuple(std::tuple<Types...>& tp)
{
    panda::for_each(tp, Deleter<Types...>());
}

///--------------------------------------------------------------------
// @brief Transfer the types defined in one template param pack into another template
//
// @code
// typedef typename TypeTransfer<std::tuple<A,B,C>, MyTemplateClass>::type type;
// @endcode
// --------------------------------------------------------------------
template<typename From, template <typename...> class To>
struct TypeTransfer;

template<template <typename...> class From, template <typename...> class To, typename... Tp>
struct TypeTransfer<From<Tp...>, To>
{
    typedef To<Tp...> type;
};

///--------------------------------------------------------------------
// @brief Sort the types of a tuple in order of the types idefined Tag  (will not work unless this is defines)
// @details the types to be ordered must have a numerical Tag defined to describe ordering
// --------------------------------------------------------------------
template<typename>
struct Sort;

template<typename... Types>
struct Sort<std::tuple<Types...>> {
    typedef typename TypeTransfer<typename boost::mpl::sort<boost::mpl::list<Types...>>::type, std::tuple>::type type;
};


///--------------------------------------------------------------------
// @brief create a mixin object
// @details create a mixin given a list of types
//
// @code
// MixinTemplateClassA<MixinTemplateClassB<BaseClass>>>* my_object_ptr = Mixin<BaseClass, MininTemplateClassA, MixinTemplateClassB>;
// @endcode
///--------------------------------------------------------------------

/*
template<int I, typename Base, template<typename> class... Tp>
struct MixinTypeGenerator;

template<int I, typename Base, template<typename> class T, template<typename> class... Tp>
struct MixinTypeGenerator<I, Base, T, Tp...> {
    typedef T<typename MixinTypeGenerator<I - 1, Base, Tp...>::type> type;
};

template<typename Base, template<typename> class... Tp>
struct MixinTypeGenerator<0, Base, Tp...>
{
    typedef Base type;
};

template<typename Base, template<typename> class... Tp>
struct Mixin
{
    typedef typename MixinTypeGenerator<sizeof...(Tp), Base, Tp...>::type type;

    static type* create() {
        return new type;
    }

    template<typename... Args>
    static type* create(Args&&... args) {
        return new type(std::forward<Args>(args)...);
    }
};
*/

///--------------------------------------------------------------------
// @brief create a mixin object by extending a base object
// @details the original object is subsummed by the new object
//          n.b. Extension Type must have a copy constructor that takes the BaseType
///--------------------------------------------------------------------

template<template<typename> class Extension, typename BaseType>
inline Extension<BaseType>* extend(BaseType base)
{
    return new Extension<BaseType>(base);
}

///--------------------------------------------------------------------
// @brief checks to see if it is a tuple
// --------------------------------------------------------------------
template<typename T>
struct is_tuple : public std::false_type
{
    static constexpr bool value=false;
};

template<typename... T>
struct is_tuple<std::tuple<T...>> : public std::true_type
{
    static constexpr bool value=true;
};

///--------------------------------------------------------------------
// @brief produces a single tuple from two or more tuples (compile time version of std::tuple_cat)
// --------------------------------------------------------------------
template<typename Tuple1, typename... Tuples>
struct join_tuples;

template<typename... Tuple1, typename... Tuple2>
struct join_tuples<std::tuple<Tuple1...>, std::tuple<Tuple2...>>
{
    typedef std::tuple<Tuple1..., Tuple2...> type;
};

template<typename... Tuple1, typename... Tuple2, typename... Tuples>
struct join_tuples<std::tuple<Tuple1...>, std::tuple<Tuple2...>, Tuples...>
{
    typedef typename join_tuples<std::tuple<Tuple1...>, typename join_tuples<std::tuple<Tuple2...>, Tuples...>::type>::type type;
};

///--------------------------------------------------------------------
// @brief produces a tuple that cntains all elements in Tuple1 that are not in Tuple2
// --------------------------------------------------------------------
template<typename Tuple1, typename Tuple2>
struct tuple_diff;

template<typename T, typename... Tuple2>
struct tuple_diff<std::tuple<T>, std::tuple<Tuple2...>>
{
    typedef typename std::conditional<HasType<T, std::tuple<Tuple2...>>::value,
                             std::tuple<>,
                             std::tuple<T>>::type type;
};

template<typename T, typename... Tuple1, typename... Tuple2>
struct tuple_diff<std::tuple<T, Tuple1...>, std::tuple<Tuple2...>>
{
    private:
        typedef tuple_diff<std::tuple<Tuple1...>, std::tuple<Tuple2...>> Next;

    public:
        typedef typename std::conditional<HasType<T, std::tuple<Tuple2...>>::value,
                            typename Next::type,
                            typename join_tuples<std::tuple<T>, typename Next::type>::type>::type type;

};


///--------------------------------------------------------------------
// @brief macro to generate typedef existence checkers
// @code
// TYPEDEF_TESTER(HasMyType, MyType);
//
// template<typename T>
// typename std::enable_if<HasMyType<T>::value, void>::type do_something(T const& t)
// {
//      typedef T::MyType type; // guaranteed to exist in this specialisation
//      // ... do something with type
// }
//
// @endcode
// --------------------------------------------------------------------
#define TYPEDEF_TESTER(Name, TypeDef) \
template<class T> \
class Name {                                                              \
    private:                                                              \
        typedef char Yes;                                                 \
        typedef char (&No)[2];                                            \
                                                                          \
        template <class U>                                                \
        static No tester(...);                                            \
                                                                          \
        template <class U>                                                \
        static Yes tester(typename U::TypeDef *);                         \
                                                                          \
    public:                                                               \
        static bool const value = (sizeof(tester<T>(0)) == sizeof(Yes));  \
        typedef std::integral_constant<bool, value> type;                 \
}


} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TUPLEUTILITIES_H
