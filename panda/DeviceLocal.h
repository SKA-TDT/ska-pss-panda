/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICELOCAL_H
#define SKA_PANDA_DEVICELOCAL_H
#include "panda/DeviceId.h"
#include <map>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *   idea is similar to thread_local but using device id's and as we can't add a keyword we instead use a class
 *
 * @details
 *    Will ensure that there is a copy of the object for each device.
 *    If the copy does not exist it will create it for you.
 *
 *    The class uses the DeviceId template class to determine a unique identifer for the device type passed
 *
 * @param T_Factory : A factory object that creates objects of the type T. The return type of this factory is what
 *                    is stored and returned by the operator()(DeviceType).
 *
 * @example
 * @code
 * class MyAlgoWorker
 * {
 *     public:
 *         MyAlgoWorker(DeviceType& device);
 *         std::shared_ptr<OutputData> do_something(InputData const& input_data);
 * };
 *
 * class PrimaryApi
 * {
 *     public:
 *         PrimaryApi();
 *         std::shared_ptr<OutputData> do_something(DeviceType& device, InputData const& input_data)
 *         {
 *             return _worker(device)(input_data);
 *         }
 *
 *     private:
 *         struct MyAlgoWorkerFactory {
 *             MyAlgoWorker* operator()(DeviceType& device) {
 *                 return new MyAlgoWorker(device);
 *             }
 *         };
 *
 *     private:
 *         DeviceLocal<DeviceType, MyAlgoWorkerFactory> _worker;
 * };
 *
 * int main(int, cha**) {
 *     PrimaryApi api;
 *     DeviceType device;
 *     InputData data;
 *
 *     std::shared_ptr<OutputData> out = api.do_something(device, data);
 *     ...
 * }
 *
 * @endcode
 *
 * @endexample
 */
template<typename DeviceType, typename T_Factory>
class DeviceLocal
{
    private:
        // work out the type from the return type of the factory
        typedef typename std::remove_reference<decltype(std::declval<T_Factory>()(std::declval<DeviceType const&>()))>::type FactoryReturnT;

    public:
        typedef typename std::remove_pointer<FactoryReturnT>::type T;

    public:
        DeviceLocal(T_Factory);
        DeviceLocal(DeviceLocal&&);
        ~DeviceLocal();

        inline T& operator()(DeviceType const&);

    private:
        // work out the id type from the return type of the DeviceId<DeviceType>::id function
        typedef typename std::remove_reference<decltype(std::declval<DeviceId<DeviceType>>().id(std::declval<DeviceType const&>()))>::type IdType;
        std::map<IdType, FactoryReturnT> _objects;
        T_Factory _factory;
};

} // namespace panda
} // namespace ska
#include "detail/DeviceLocal.cpp"

#endif // SKA_PANDA_DEVICELOCAL_H
