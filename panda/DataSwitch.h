/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATASWITCH_H
#define SKA_PANDA_DATASWITCH_H

#include "panda/DataChannel.h"
#include "panda/DataSwitchConfig.h"
#include "panda/TupleUtilities.h"
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *    The safe Routing of data to different DataChannels
 *
 * @details
 *    Allows runtime activation/deactivation of channels, by both channel identifier and type of data passed
 *    Routing is asynchronous, and will ensure data passed is kept in scope as required.
 *
 *  \ingroup resource
 *  @{
 */

template<typename... DataTypes>
class DataSwitch
{
        typedef std::tuple<DataChannel<DataTypes>*...> ChannelTuple;

    public:
        DataSwitch(DataSwitchConfig& config);
        DataSwitch(DataSwitch const&) = delete;
        ~DataSwitch();

        /**
         * @brief send data to a named channel
         * @details for async operation the data has to be owned by a shared_ptr
         *          and support shared_from_this()
         */
        template<typename DataType>
        typename std::enable_if<HasType<DataChannel<DataType>*, ChannelTuple>::value, void>::type
        send(ChannelId const& channel, DataType& data);

        // version for const data
        template<typename DataType>
        typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, ChannelTuple>::value, void>::type
        send(ChannelId const& channel, DataType const& data);

        /**
         * @brief send data to a named channel
         */
        template<typename DataType>
        typename std::enable_if<HasType<DataChannel<DataType>*, ChannelTuple>::value, void>::type
        send(ChannelId const& channel, std::shared_ptr<DataType> const& data);

        /**
         * @brief send data to a named channel
         */
        template<typename DataType>
        typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, ChannelTuple>::value, void>::type
        send(ChannelId const& channel, std::shared_ptr<DataType> const& data);

        /**
         * @brief add data handler to a channel
         */
        template<typename DataType>
        typename std::enable_if<HasType<DataChannel<DataType>*
                               , typename DataSwitch<DataTypes...>::ChannelTuple>::value
                               , typename DataChannel<DataType>::DataChannelHandler&>::type
        add(ChannelId const& channel, typename DataChannel<DataType>::DataChannelHandler handler);

        /**
         * @brief add data handler to a channel
         * @details specialisation (syntactical suger)
         *          to allow calls of type add<T> to channels accepting only const T.
         */
        template<typename DataType>
        typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*
                               , typename DataSwitch<DataTypes...>::ChannelTuple>::value
                               , typename DataChannel<const DataType>::DataChannelHandler&>::type
        add(ChannelId const& channel, typename DataChannel<const DataType>::DataChannelHandler handler);

        /**
         * @brief return true if the named channel is active for the specified type
         */
        template<typename DataType>
        bool is_active(ChannelId const& channel);

        /**
         * @brief activate the channel for the specified DataType
         */
        template<typename DataType>
        void activate(ChannelId const& channel);

        /**
         * @brief activate the channel for all DataTypes
         */
        void activate_all(ChannelId const& channel);

        /**
         * @brief deactivate the channel for the specified DataType
         */
        template<typename DataType>
        void deactivate(ChannelId const& channel);

        /**
         * @brief deactivate the channel for all DataTypes
         */
        void deactivate_all(ChannelId const& channel);

    protected:
        template<typename DataType>
        inline typename std::enable_if<HasType<DataChannel<DataType>*, ChannelTuple>::value, DataChannel<DataType>*>::type channel();

        // specialisation for case to match a request for DataType when the actual type is cont DataType
        template<typename DataType>
        inline typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, ChannelTuple>::value, DataChannel<const DataType>*>::type channel();

    private:
        DataSwitchConfig _config; // make a copy to keep all engines in scope whilst exporters are active
        ChannelTuple _switches;
};

} // namespace panda
} // namespace ska
#include "panda/detail/DataSwitch.cpp"

#endif // SKA_PANDA_DATASWITCH_H
/**
 *  @}
 *  End Document Group
 **/
