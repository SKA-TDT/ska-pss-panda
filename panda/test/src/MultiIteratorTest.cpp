/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "MultiIteratorTest.h"
#include "panda/MultiIterator.h"
#include <algorithm>


namespace ska {
namespace panda {
namespace test {


MultiIteratorTest::MultiIteratorTest()
    : ::testing::Test()
{
}

MultiIteratorTest::~MultiIteratorTest()
{
}

void MultiIteratorTest::SetUp()
{
}

void MultiIteratorTest::TearDown()
{
}

TEST_F(MultiIteratorTest, test_vector_1d)
{
    typedef std::vector<int> Container;
    Container vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto it = panda::make_MultiIterator<Container>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    // exercies the iterator
    for(unsigned i=0; i < vec.size(); ++i) {
        ASSERT_FALSE(it == end);
        ASSERT_EQ(vec[i], *it++) << i;
    }
}

TEST_F(MultiIteratorTest, test_vector_2d)
{
    typedef std::vector<std::vector<int>> Container;
    Container vec = { {0,1,2,3}, { 4,5,6}, {7,8,9}};
    auto it = panda::make_MultiIterator<Container>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    for(int i=0; i < 10; ++i) {
        ASSERT_TRUE(it != end);
        ASSERT_EQ(i, *(it++));
    }
    ASSERT_FALSE(it != end);
    ASSERT_TRUE(it.end());
}

TEST_F(MultiIteratorTest, test_vector_3d)
{
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, { 4,5,6}}, {{7,8,9}, {10}} };
    auto it = panda::make_MultiIterator<Container>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    for(int i=0; i < 11; ++i) {
        ASSERT_FALSE(it == end);
        ASSERT_EQ(i, *(it++));
    }
    ASSERT_FALSE(it != end);
}

TEST_F(MultiIteratorTest, test_vector_2d_pre_increment_operator_not_equal_op)
{
    typedef std::vector<std::vector<int>> Container;
    Container vec = { {0,1,2,3}, { 4,5,6}, {7,8,9}};
    auto it = panda::make_MultiIterator<Container>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    int i=0;
    while(++it!=end) {
        ASSERT_EQ(++i, *it);
    }
    ASSERT_EQ(9, i);
}

TEST_F(MultiIteratorTest, test_vector_3d_max_depth_override)
{
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10}} };
    // depth 1
    {
        auto it = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,1>(vec.begin(), vec.end());
        auto end = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,1>(vec.end(), vec.end());
        int i=0;
        while(it!=end) {
            std::vector<std::vector<int>> const& arrays =*it;
            for(auto const& vals : arrays)
            {
                for(auto const& val : vals) {
                    ASSERT_EQ(val, i);
                    ++i;
                }
            }
            ++it;
        }
    }
    // depth 2
    {
        auto it = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,2>(vec.begin(), vec.end());
        auto end = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,2>(vec.end(), vec.end());
        int i=0;
        while(it!=end) {
            std::vector<int> const& vals =*it;
            for(auto const& val : vals) {
                ASSERT_EQ(val, i);
                ++i;
            }
            ++it;
        }
    }
    // depth 3
    {
        auto it = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,3>(vec.begin(), vec.end());
        auto end = panda::make_MultiIterator<Container,panda::DefaultIteratorDepthTraits,3>(vec.end(), vec.end());
        int i=0;
        while(it!=end) {
            ASSERT_EQ(*it, i);
            ++i;
            ++it;
        }
    }
}

TEST_F(MultiIteratorTest, test_vector_2d_shared_ptr_types)
{
    typedef std::shared_ptr<std::vector<int>> SharedType;
    typedef std::vector<SharedType> Container;
    Container vec = { SharedType(new std::vector<int>({0,1,2,3})),
                      SharedType(new std::vector<int>({4,5,6})),
                      SharedType(new std::vector<int>({7,8,9})) };
    auto it = panda::make_MultiIterator<Container>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    int i=0;
    while(it!=end) {
        ASSERT_EQ(i++, *it);
        ASSERT_FALSE(it.end());
        ++it;
    }
    ASSERT_EQ(10, i);
}
template<typename T>
struct TestDepthTraitsEqual : public DefaultIteratorDepthTraits<T>
{
};

TEST_F(MultiIteratorTest, test_comparison_different_handler_types)
{
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10}} };
    auto it = panda::make_MultiIterator<Container, TestDepthTraitsEqual>(vec.begin(), vec.end());
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    static_assert(!std::is_same<decltype(it), decltype(end)>::value, "expecting different types");
    ASSERT_TRUE(it != end); // should compile even though types are different
    ASSERT_FALSE(it == end); // should compile even though types are different
    while(it!=end) {
        ASSERT_FALSE(it.end());
        ++it;
    }
    ASSERT_TRUE(it.end());
}


// template speiclisations for handler_callback test
struct TestHandlerType {
    public:
        TestHandlerType()
            : _called(0)
        {
        }

        void operator()() {
            ++_called;
        };

        std::size_t called() const {
            return _called;
        }

    private:
        std::size_t _called;
};

template<typename T>
struct TestDepthTraits : public DefaultIteratorDepthTraits<T>
{
};

template<>
struct TestDepthTraits<std::vector<int>> : public IteratorDepthTraits<std::vector<int>, TestHandlerType&>
{
        typedef IteratorDepthTraits<std::vector<int>, TestHandlerType&> BaseT;

    public:
        TestDepthTraits(TestHandlerType& h) : BaseT(h) {}
};

TEST_F(MultiIteratorTest, test_handler_callback_single_config_passthrough)
{
    TestHandlerType handler;
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10}} };
    auto it = panda::make_MultiIterator<Container, TestDepthTraits>(
                                        vec.begin(), vec.end()
                                        , TestDepthTraits<std::vector<int>>(handler)
                                        );
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    static_assert(!std::is_same<decltype(it), decltype(end)>::value, "expecting different types");
    while(it!=end) {
        ++it;
    }
    ASSERT_EQ(4U, handler.called());
}


template<typename T>
struct TestDepthTraits2 : public DefaultIteratorDepthTraits<T>
{
};

template<>
struct TestDepthTraits2<std::vector<int>> : public IteratorDepthTraits<std::vector<int>, TestHandlerType&>
{
        typedef IteratorDepthTraits<std::vector<int>, TestHandlerType&> BaseT;

    public:
        TestDepthTraits2(TestHandlerType& h) : BaseT(h) {}
};

template<>
struct TestDepthTraits2<std::vector<std::vector<int>>> : public IteratorDepthTraits<std::vector<std::vector<int>>, TestHandlerType&>
{
        typedef IteratorDepthTraits<std::vector<std::vector<int>>, TestHandlerType&> BaseT;

    public:
        TestDepthTraits2(TestHandlerType& h) : BaseT(h) {}
};


TEST_F(MultiIteratorTest, test_handler_callback_multi_config)
{
    TestHandlerType handler;
    TestHandlerType handler_outer;
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10}} };
    auto it = panda::make_MultiIterator<Container, TestDepthTraits2>(
                                        vec.begin(), vec.end()
                                        , TestDepthTraits2<std::vector<int>>(handler)
                                        , TestDepthTraits2<std::vector<std::vector<int>>>(handler_outer)
                                        );
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    static_assert(!std::is_same<decltype(it), decltype(end)>::value, "expecting different types");
    while(it!=end) { // ensure we can compare types with different handlers
        ++it;
    }
    ASSERT_EQ(4U, handler.called());
}

TEST_F(MultiIteratorTest, test_apply_algorithm)
{
    TestHandlerType handler;
    TestHandlerType handler_outer;
    typedef std::vector<std::vector<std::vector<int>>> Container;
    typedef std::vector<int>::iterator IteratorType;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10}} };
    auto it = panda::make_MultiIterator<Container, TestDepthTraits2>(
                                        vec.begin(), vec.end()
                                        , TestDepthTraits2<std::vector<int>>(handler)
                                        , TestDepthTraits2<std::vector<std::vector<int>>>(handler_outer)
                                        );
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    std::vector<int> dst;
    it.apply_algorithm(end, [&](IteratorType& begin, IteratorType const& end){ std::copy(begin, end, std::back_inserter(dst)); });
    ASSERT_EQ(11U, dst.size());
    for(int i=0; i<11; ++i) {
        ASSERT_EQ(i, dst[i]);
    }
    ASSERT_EQ(4U, handler.called());
}

TEST_F(MultiIteratorTest, test_copy)
{
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10, 11}} };
    TestHandlerType handler;
    TestHandlerType handler_outer;
    auto it = panda::make_MultiIterator<Container, TestDepthTraits2>(
                              vec.begin(), vec.end()
                            , TestDepthTraits2<std::vector<int>>(handler)
                            , TestDepthTraits2<std::vector<std::vector<int>>>(handler_outer)
                        );
    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    std::vector<int> dst;
    panda::copy(it, end, std::back_inserter(dst));
    ASSERT_EQ(12U, dst.size());
    for(int i=0; i<12; ++i) {
        ASSERT_EQ(i, dst[i]);
    }
    ASSERT_EQ(4U, handler.called());
}

TEST_F(MultiIteratorTest, test_performance)
{
/*
    // a comaprision between simple pointers and a multi-iterator
    typedef std::vector<std::vector<std::vector<int>>> Container;
    Container vec = { {{0,1,2,3}, {4,5,6}}, {{7,8,9}, {10, 11}} };

    // do some work in the outer loop by changing this vector
    std::vector<bool> outer_loop_vec(vec.size());
    unsigned middle_loop_count = 0;
    unsigned mi_middle_loop_count = 0;

    // normal vector iteration
    for(unsigned i=0; i < vec.size(); ++i) {
        for(unsigned j=0; j < vec[i].size(); ++j) {
            for(unsigned k=0; k < vec[i][k].size(); ++k) {
                vec[i][j][k] += 1;
            }
            ++middle_loop_count;
        }
        outer_loop_vec[i] = outer_loop_vec[i] ^ true;
    }

    // iteration with a MultiVector
    auto handler_outer = []() {} ;
    auto mid_handler = [&]() { ++mi_middle_loop_count; } ;


    auto it = panda::make_MultiIterator<Container>(
                              vec.begin()
                            , vec.end()
                            , DepthTag<std::vector<float>, decltype(mid_handler)&>(mid_handler)
                            , DepthTag<std::vector<std::vector<float>>, decltype(handler_outer)&>(handler_outer)
                        );

    auto end = panda::make_MultiIterator<Container>(vec.end(), vec.end());
    while(it!=end) {
        *it +=1;
        ++it;
    }

    ASSERT_EQ(middle_loop_count, mi_middle_loop_count);
*/
}

} // namespace test
} // namespace panda
} // namespace ska
