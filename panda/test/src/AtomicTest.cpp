/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "AtomicTest.h"


namespace ska {
namespace panda {
namespace test {


template<typename T>
AtomicTest<T>::AtomicTest()
    : BaseT()
{
}

TYPED_TEST(AtomicTest, test_assignment)
{
    TypeParam atom(0);
    typename TypeParam::value_type value = static_cast<typename TypeParam::value_type>(12);
    atom = value;
    ASSERT_EQ(value, atom);
}

TYPED_TEST(AtomicTest, test_post_increment)
{
    TypeParam atom(0);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(0), atom);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(0), atom++);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(1), atom);
}

TYPED_TEST(AtomicTest, test_pre_increment)
{
    TypeParam atom(0);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(1), ++atom);
}

TYPED_TEST(AtomicTest, test_pre_decrement)
{
    TypeParam atom(1);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(0), --atom);
}

TYPED_TEST(AtomicTest, test_post_decrement)
{
    TypeParam atom(1);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(1), atom--);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(0), atom);
}

TYPED_TEST(AtomicTest, test_plus_equal)
{
    TypeParam atom(1);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(7), atom+=6);
}

TYPED_TEST(AtomicTest, test_minus_equal)
{
    TypeParam atom(8);
    ASSERT_EQ(static_cast<typename TypeParam::value_type>(2), atom-=6);
}

TYPED_TEST(AtomicTest, test_and_equal)
{
    TypeParam atom(8);
    typename TypeParam::value_type val = 6;
    typename TypeParam::value_type result = atom;
    ASSERT_EQ(result&=val, atom&=val);
}

TYPED_TEST(AtomicTest, test_or_equal)
{
    TypeParam atom(8);
    typename TypeParam::value_type val = 6;
    typename TypeParam::value_type result = atom;
    ASSERT_EQ(result|=val, atom|=val);
}

TYPED_TEST(AtomicTest, test_xor_equal)
{
    TypeParam atom(8);
    typename TypeParam::value_type val = 6;
    typename TypeParam::value_type result = atom;
    ASSERT_EQ(result^=val, atom^=val);
}
} // namespace test
} // namespace panda
} // namespace ska
