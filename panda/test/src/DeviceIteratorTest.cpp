/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceIteratorTest.h"
#include "panda/DeviceIterator.h"


namespace ska {
namespace panda {
namespace test {


DeviceIteratorTest::DeviceIteratorTest()
    : BaseT()
{
}

DeviceIteratorTest::~DeviceIteratorTest()
{
}

void DeviceIteratorTest::SetUp()
{
}

void DeviceIteratorTest::TearDown()
{
}

struct ArchA;

TEST_F(DeviceIteratorTest, test_is_device_iterator)
{
    static_assert(panda::is_device_iterator<DeviceIterator<ArchA, int>>::value, "expecting to recognise as a DeviceIterator");
    static_assert(panda::is_device_iterator<DeviceIterator<ArchA, int*>>::value, "expecting to recognise as a DeviceIterator");
    static_assert(panda::is_device_iterator<const DeviceIterator<ArchA, int*>>::value, "expecting to recognise as a DeviceIterator");
    static_assert(!panda::is_device_iterator<int*>::value, "not expecting to recognise as a DeviceIterator");
}

/*
TEST_F(DeviceIteratorTest, test_make_device_iterator_ref)
{
    int some_type;
    auto device_ptr = panda::make_device_iterator<ArchA>(some_type);
    static_assert(std::is_same<decltype(device_ptr), DeviceIterator<ArchA, int>>::value, "unexpected type");
}
*/

TEST_F(DeviceIteratorTest, test_make_device_iterator_ptr)
{
    int* ptr = nullptr;
    auto device_ptr = panda::make_device_iterator<ArchA>(ptr);
    static_assert(std::is_same<decltype(device_ptr), DeviceIterator<ArchA, int*>>::value, "unexpected type");
}

TEST_F(DeviceIteratorTest, test_make_device_iterator_recursive)
{
    DeviceIterator<ArchA, int*> type(nullptr);
    auto device_ptr = panda::make_device_iterator<ArchA>(type);
    static_assert(std::is_same<decltype(device_ptr), DeviceIterator<ArchA, int*>>::value, "unexpected type");
}

} // namespace test
} // namespace panda
} // namespace ska
