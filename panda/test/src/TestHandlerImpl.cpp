/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/detail/TestHandlerImpl.h"
#include <iostream>

namespace ska {
namespace panda {
namespace test {

TestHandlerImpl::TestHandlerImpl(bool blocking)
    : _blocking(blocking)
    , _verbose(false)
    , _do_throw(false)
{
    reset();
}

TestHandlerImpl::~TestHandlerImpl()
{
}

void TestHandlerImpl::set_verbose(bool v)
{
    _verbose = v;
}

void TestHandlerImpl::reset()
{
    _called = false;
    _waiting = false;
    _caller_thread_id = std::thread::id();
}

void TestHandlerImpl::operator()()
{
    std::unique_lock<std::mutex> lock(_mutex);
    msg("operator()() - locked");

    _called = true;
    _caller_thread_id = std::this_thread::get_id();

    if(_blocking && !_waiting) {
        while(!_waiting) {
            msg("operator()() - blocking");
           _blocking_wait.wait(lock);
        }
    }

    // tell anybody waiting we have finished
    msg("operator()() - notify");
    _waiting = false;
    _wait.notify_all();

   if( _do_throw ) {
       _object();
   }
}

void TestHandlerImpl::wait()
{
    {
        std::unique_lock<std::mutex> lock(_mutex);
        msg("wait() - lock");

        _waiting = true;
        while(!_called) {
            msg("TestHandler::wait() - waiting");
            _wait.wait(lock);
        }
        msg("wait() - notify");
        _blocking_wait.notify_all();
    }
    // this lock will prevent us from returning until the operator() has completed as well
    std::unique_lock<std::mutex> lock(_mutex);
    msg("wait() - exit");
}

bool TestHandlerImpl::wait(std::chrono::seconds const& timeout)
{
    {
        std::unique_lock<std::mutex> lock(_mutex);
        msg("wait() - lock");

        _waiting = true;
        bool timed_out = !_wait.wait_for(lock, timeout, [this]{
                                            msg("TestHandler::wait() - waiting");
                                            return _called;
                                         });
        msg("wait() - notify");
        _blocking_wait.notify_all();
        if(timed_out)
        {
            msg("wait() - timeout");
            return false;
        }
    }
    // this lock will prevent us from returning until the operator() has completed as well
    std::unique_lock<std::mutex> lock(_mutex);
    msg("wait() - exit");
    return true;
}

void TestHandlerImpl::msg(std::string const& msg) const {
    if( _verbose ) {
        std::cout << "TestHandlerImpl(" << this << ")::" << msg << std::endl;
    }
}

std::thread::id const& TestHandlerImpl::caller_thread_id() const
{
    return _caller_thread_id;
}

bool TestHandlerImpl::blocking() const
{
    return _blocking;
}

bool TestHandlerImpl::executed() const
{
    msg("executed()");
    return _called;
}

} // namespace test
} // namespace panda
} // namespace ska
