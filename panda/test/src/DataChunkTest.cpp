/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DataChunkTest.h"
#include "panda/DataChunk.h"
#include "panda/test/TestChunk.h"


namespace ska {
namespace panda {
namespace test {


DataChunkTest::DataChunkTest()
    : ::testing::Test()
{
}

DataChunkTest::~DataChunkTest()
{
}

void DataChunkTest::SetUp()
{
}

void DataChunkTest::TearDown()
{
}

class ProtectedTestChunk : public TestChunkBase<std::string>, public panda::DataChunk<ProtectedTestChunk>
{
    protected:
        ProtectedTestChunk() : TestChunkBase<std::string>("ProtectedTestChunk") {}
};

TEST_F(DataChunkTest, test_make_shared_no_args)
{
    std::shared_ptr<TestChunk_A> ptr = TestChunk_A::make_shared();
    ASSERT_NE(nullptr, ptr.get());
    ASSERT_EQ(ptr->data(), "A");
}

TEST_F(DataChunkTest, test_make_shared_protected_constructor)
{
    std::shared_ptr<ProtectedTestChunk> ptr = ProtectedTestChunk::make_shared();
    ASSERT_NE(nullptr, ptr.get());
    ASSERT_EQ(ptr->data(), "ProtectedTestChunk");
}

TEST_F(DataChunkTest, test_make_shared_with_args)
{
    std::shared_ptr<TestChunk_A> ptr = TestChunk_A::make_shared("wibble");
    ASSERT_NE(nullptr, ptr.get());
    ASSERT_EQ(ptr->data(), "wibble");
}

TEST_F(DataChunkTest, test_make_shared_with_deleter)
{
    bool deleted = false;
    {
        std::shared_ptr<TestChunk_A> ptr = TestChunk_A::make_shared_with_deleter([&](TestChunk_A* ptr) {
                                                                                       delete ptr;
                                                                                       deleted = true;
                                                                                     }, "wibble");
        ASSERT_NE(nullptr, ptr.get());
        ASSERT_EQ(ptr->data(), "wibble");
    }
    ASSERT_TRUE(deleted);
}

TEST_F(DataChunkTest, test_make_shared_with_deleter_protected_constructor)
{
    std::shared_ptr<ProtectedTestChunk> ptr = ProtectedTestChunk::make_shared_with_deleter([](ProtectedTestChunk*) {});
    ASSERT_NE(nullptr, ptr.get());
    ASSERT_EQ(ptr->data(), "ProtectedTestChunk");
}

} // namespace test
} // namespace panda
} // namespace ska
