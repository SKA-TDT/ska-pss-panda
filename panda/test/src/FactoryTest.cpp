/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "FactoryTest.h"
#include "panda/Factory.h"
#include <memory>


namespace ska {
namespace panda {
namespace test {


FactoryTest::FactoryTest()
    : ::testing::Test()
{
}

FactoryTest::~FactoryTest()
{
}

void FactoryTest::SetUp()
{
}

void FactoryTest::TearDown()
{
}

class TestBase {
    public: 
        TestBase(std::string s) : _msg(std::move(s)) {}
        virtual ~TestBase() {}
        virtual std::string operator()() { return _msg; };

    private:
        std::string _msg;
};

class TestTypeA final : public TestBase {
    public: 
        TestTypeA() : TestBase("A") {}
    
};

class TestTypeB final : public TestBase {
    public: 
        TestTypeB() : TestBase("B") {}
};

TEST_F(FactoryTest, test_factory_create_zero_args)
{
    panda::Factory<TestBase> factory;
    std::string type_name_a = "testtype_a";
    std::string type_name_b = "testtype_b";

    ASSERT_EQ(0U, factory.available().size());
    factory.add_type(type_name_a, [](){ return new TestTypeA(); } );
    ASSERT_EQ(1U, factory.available().size());
    factory.add_type(type_name_b, [](){ return new TestTypeB(); } );

    std::unique_ptr<TestBase> a(factory.create(type_name_a));
    std::unique_ptr<TestBase> b(factory.create(type_name_b));
    ASSERT_EQ("A",(*a)());
    ASSERT_EQ("B",(*b)());

/*
    std::unique_ptr<panda::MixInTimer<TestBase>> a_timed(factory.create_timed(type_name_a));
    ASSERT_EQ("A",(*a_timed)());

    panda::TimeDurationFormatter<std::chrono::high_resolution_clock> handler;
    std::unique_ptr<panda::MixInTimer<TestBase>> b_timed(factory.create_timed(type_name_b, handler));
    ASSERT_EQ("B",(*b_timed)());
*/
}

} // namespace test
} // namespace panda
} // namespace ska
