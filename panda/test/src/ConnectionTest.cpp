/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ConnectionTest.h"
#include "panda/Connection.h"
#include "panda/Engine.h"
#include "panda/Buffer.h"
#include "panda/test/TestConnectionErrorPolicy.h"
#include "panda/test/TestSocket.h"
#include <iostream>


namespace ska {
namespace panda {
namespace test {

typedef ska::panda::Tcp TestProtocol;
struct TestConnectionTraits : public ska::panda::ConnectionTraits<TestProtocol, TestConnectionErrorPolicy, panda::Engine>
{
    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<TestConnectionErrorPolicy>));

    typedef TestSocket<TestProtocol> SocketType;
};

typedef ska::panda::Connection<TestConnectionTraits> TestConnection;

ConnectionTest::ConnectionTest()
    : ::testing::Test()
    , _expected_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()))
    , _expected_connection_canceled_error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category()))
{
    _expected_connection_error << " (" << TestConnection::EndPointType() << ") trying to connect";
}

ConnectionTest::~ConnectionTest()
{
}

void ConnectionTest::SetUp()
{
}

void ConnectionTest::TearDown()
{
}

TEST_F(ConnectionTest, send_post_halt)
{
    TestConnection::BufferType buffer(10);
    {
        // send called after close()
        TestConnection connection(engine());
        connection.close();

        int called = 0;
        connection.send(buffer, [&](Error const& e) { 
            ++called; 
            ASSERT_TRUE(e);
            ASSERT_EQ(_expected_connection_canceled_error, e);
        } );
        do { engine().run(); } while(!called);
        ASSERT_FALSE( connection.policy()._connected );
        // expect policy object to not be called as a generic error
        ASSERT_EQ(unsigned(0), connection.policy().transmission_errors());
        ASSERT_EQ(unsigned(0), connection.policy().connection_errors());
    }
}

TEST_F(ConnectionTest, halt_with_pending_send)
{
    TestConnection::BufferType buffer(10);
    // send called but does not connect before close()
    TestConnection connection(engine());

    auto & socket = connection.socket(); 
    socket.set_on_connect_function([&socket](ska::panda::TestSocketBase&) { 
            // hold the connection up so it dosn't complete
            unsigned count=0;
            while(socket.is_open() && count < 1000) {
                ++count;
                std::this_thread::sleep_for(std::chrono::duration<float, std::milli>(1.0));
            }
            ASSERT_GT(1000U, count) << "test timed out. expecting send to be cancelled";
            socket.set_connection_error(boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category()));
    });

    // initialising send call
    int called = 0;
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_TRUE(e);
        ASSERT_EQ(_expected_connection_canceled_error, e);
    } );

    // stacked send call
    int called2 = 0;
    connection.send(buffer, [&](Error const& e) { 
        ++called2; 
        ASSERT_TRUE(e);
        ASSERT_EQ(_expected_connection_canceled_error, e);
    } );

    // stacked receive call
    int called3 = 0;
    connection.receive(buffer, [&](Error const& e, std::size_t received_bytes, TestConnection::BufferType) { 
        ++called3; 
        ASSERT_TRUE(e);
        ASSERT_EQ(0U, received_bytes);
        ASSERT_EQ(_expected_connection_canceled_error, e);
    } );

    // now close connection and check if send operations get cancelled
    connection.close();

    do { engine().run(); } while(!called);
    do { engine().run(); } while(!called2 && !called3);

    // expect policy object to not be called as this is not a generic error
    ASSERT_EQ(unsigned(0), connection.policy().transmission_errors());
    ASSERT_EQ(unsigned(0), connection.policy().connection_errors());
}

TEST_F(ConnectionTest, send_connection_error_policy_disconnect)
{
    TestConnection::BufferType buffer(10);

    // New connection with a connection error - policy set to return false on connection error
    TestConnection connection(engine());
    int called = 0;
    connection.socket().set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
    connection.policy()._connection_error_return = false;
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_TRUE(e);
        ASSERT_EQ(_expected_connection_error, e);
    } );
    do { engine().run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(unsigned(0), connection.policy().transmission_errors());
    ASSERT_EQ(unsigned(1), connection.policy().connection_errors());
    ASSERT_EQ(1, called);
    ASSERT_EQ( _expected_connection_error, connection.policy()._last_connection_error);
}

TEST_F(ConnectionTest, send_connection_error_policy_retry)
{
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    // New connection with a connection error - policy set to return true on connection error
    int called = 0;
    TestConnection connection(engine);
    connection.socket().set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
    connection.policy()._connection_error_return = true;
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_TRUE(e);
        ASSERT_EQ(_expected_connection_error, e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(unsigned(2), connection.policy().connection_errors());
    ASSERT_EQ(1, called);
    ASSERT_EQ(_expected_connection_error, connection.policy()._last_connection_error);

}

TEST_F(ConnectionTest, send_transmission_error_policy_retry_fail)
{
    // connection with a transmission error - policy set to return true on error
    // so we should expect to see a retry
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().reset_connection_error();
    connection.socket().set_send_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    unsigned connection_call=0;
    connection.socket().set_on_connect_function(
        [&](TestSocketBase& socket)
        {
            if(++connection_call >1) {
                socket.set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
            }
            connection.policy()._connection_error_return = false;
        });
    connection.policy()._error_return = true; // try again after transmission failure

    // we expect the connection to succeed, but the transmission to fail. Should retry to establish the connection
    // which is also set to fail. At this point the handler should be called with an error
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_TRUE(e);
    } );

    do { engine.run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(1, called);
    ASSERT_EQ(unsigned(2), connection_call);
    panda::Error expected(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    expected << " (0.0.0.0:0)";
    ASSERT_EQ(expected, connection.policy()._last_error);
}

TEST_F(ConnectionTest, send_transmission_error_policy_retry_success)
{
    // connection with a transmission error - policy set to return true on error
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().set_send_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    connection.socket().reset_connection_error();
    unsigned send_call=0;
    connection.socket().set_on_send_function(
        [&](TestSocketBase& socket)
        {
            if(++send_call >1) {
                socket.reset_send_error();
            }
        }
    );
    connection.policy()._error_return = true;
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_FALSE(e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_TRUE( connection.policy()._connected );
    ASSERT_EQ(1, called);
    panda::Error expected(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    expected << " (0.0.0.0:0)";
    ASSERT_EQ(expected, connection.policy()._last_error);
}

TEST_F(ConnectionTest, send_transmission_error_policy_no_retry)
{
    // connection with a transmission error - policy set to return false on error
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().reset_connection_error();
    connection.socket().set_send_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    connection.policy()._error_return = false;
    connection.send(buffer, [&](Error const& e) { 
        ++called; 
        ASSERT_TRUE(e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_EQ(1, called);
    ASSERT_FALSE( connection.policy()._connected );
    panda::Error expected(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    expected << " (0.0.0.0:0)";
    ASSERT_EQ(expected, connection.policy()._last_error);
}

TEST_F(ConnectionTest, recv_transmission_error_policy_retry_success)
{
    // connection with a transmission error - policy set to return true on error
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().set_recv_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    connection.socket().reset_connection_error();
    unsigned recv_call=0;
    connection.socket().set_on_recv_function(
        [&](TestSocketBase& socket)
        {
            if(++recv_call > 1 ) {
                socket.reset_recv_error();
            }
        }
    );
    connection.policy()._error_return = true;
    connection.receive(buffer, [&](Error err, std::size_t received_bytes, TestConnection::BufferType buffer) { 
        ++called; 
        ASSERT_LE(received_bytes, buffer.size());
        ASSERT_FALSE(err);
    } );
    do { engine.run(); } while(!called);
    ASSERT_TRUE( connection.policy()._connected );
    ASSERT_EQ(1, called);
    ASSERT_EQ(Error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category())),
              connection.policy()._last_error);
}

TEST_F(ConnectionTest, recv_transmission_error_policy_no_retry)
{
    // connection with a transmission error - policy set to return false on error
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().reset_connection_error();
    connection.socket().set_recv_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    connection.policy()._error_return = false;
    connection.receive(buffer, [&](Error e, std::size_t received_bytes, TestConnection::BufferType buffer) { 
        ++called; 
        ASSERT_LE(received_bytes, buffer.size());
        ASSERT_TRUE(e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_EQ(1, called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(Error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category())),
              connection.policy()._last_error);
}

TEST_F(ConnectionTest, recv_transmission_error_policy_retry_fail)
{
    // connection with a transmission error - policy set to return true on error
    // so we should expect to see a retry
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    TestConnection connection(engine);
    int called = 0;
    connection.socket().reset_connection_error();
    connection.socket().set_recv_error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    unsigned connection_call=0;
    connection.socket().set_on_connect_function(
        [&](TestSocketBase& socket)
        {
            if(++connection_call >1) {
                socket.set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
            }
            connection.policy()._connection_error_return = false;
        });
    connection.policy()._error_return = true; // try again after transmission failure

    // we expect the connection to succeed, but the transmission to fail. Should retry to establish the connection
    // which is also set to fail. At this point the handler should be called with an error
    connection.receive(buffer, [&](Error e, std::size_t received_bytes, TestConnection::BufferType buffer) { 
        ++called; 
        ASSERT_LE(received_bytes, buffer.size());
        ASSERT_TRUE(e);
    } );

    do { engine.run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(1, called);
    ASSERT_EQ(unsigned(2), connection_call);
    ASSERT_EQ(Error(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category())),
              connection.policy()._last_error);
}

TEST_F(ConnectionTest, recv_connection_error_policy_disconnect)
{
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    // New connection with a connection error - policy set to return false on connection error
    TestConnection connection(engine);
    int called = 0;
    connection.socket().set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
    connection.policy()._connection_error_return = false;
    connection.receive(buffer, [&](Error e, std::size_t received_bytes, TestConnection::BufferType buffer) { 
        ++called; 
        ASSERT_TRUE(e);
        ASSERT_LE(received_bytes, buffer.size());
        ASSERT_EQ(_expected_connection_error, e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(unsigned(0), connection.policy().transmission_errors());
    ASSERT_EQ(unsigned(1), connection.policy().connection_errors());
    ASSERT_EQ(1, called);
    ASSERT_EQ(_expected_connection_error, connection.policy()._last_connection_error);
}

TEST_F(ConnectionTest, recv_connection_error_policy_retry)
{
    panda::Engine engine;
    TestConnection::BufferType buffer(10);

    // New connection with a connection error - policy set to return true on connection error
    int called = 0;
    TestConnection connection(engine);
    connection.socket().set_connection_error(boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category()));
    connection.policy()._connection_error_return = true;
    connection.receive(buffer, [&](Error e, std::size_t received_bytes, TestConnection::BufferType buffer) { 
        ++called; 
        ASSERT_TRUE(e);
        ASSERT_LE(received_bytes, buffer.size());
        ASSERT_EQ(_expected_connection_error, e);
    } );
    do { engine.run(); } while(!called);
    ASSERT_FALSE( connection.policy()._connected );
    ASSERT_EQ(unsigned(2), connection.policy().connection_errors());
    ASSERT_EQ(1, called);
    ASSERT_EQ(_expected_connection_error, connection.policy()._last_connection_error);

}

} // namespace test
} // namespace panda
} // namespace ska
