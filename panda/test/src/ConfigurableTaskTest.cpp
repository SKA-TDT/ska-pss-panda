/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ConfigurableTaskTest.h"
#include "panda/ConfigurableTask.h"
#include "panda/Error.h"
#include "panda/test/TestChunk.h"
#include "panda/test/TestChunkHandler.h"
#include "panda/test/TestPoolManager.h"


namespace ska {
namespace panda {
namespace test {
struct ArchA {};
struct ArchB {};
struct SubArchB : ArchB {};
}

namespace test {

ConfigurableTaskTest::ConfigurableTaskTest()
    : ::testing::Test()
{
}

ConfigurableTaskTest::~ConfigurableTaskTest()
{
}

void ConfigurableTaskTest::SetUp()
{
}

void ConfigurableTaskTest::TearDown()
{
}

typedef TestChunk<char> DataType;

struct Algo1 {
    Algo1() : _called(false) {}
    typedef ArchA Architecture;

    template<typename... DataTypes>
    void operator()(panda::PoolResource<Architecture>&, DataTypes&&...) { _called = true;}

    bool _called;
};

struct Algo2 {
    Algo2() : _called(false) {}
    typedef ArchB Architecture;

    template<typename... DataTypes>
    void operator()(panda::PoolResource<Architecture>&, DataTypes&&...) { _called = true; }

    bool _called;
};

// Algorithm requires ArchB architecture but different capability
struct Algo3 {
    Algo3() : _called(false) {}
    typedef ArchB Architecture;
    typedef SubArchB ArchitectureCapability;

    template<typename... DataTypes>
    void operator()(panda::PoolResource<Architecture>&, DataTypes&&...) { _called = true; }

    bool _called;
};

} //namespace test

namespace test {

TEST_F(ConfigurableTaskTest, test_submit_on_unselected)
{
    TestHandler finish_handler(false);
    TestPoolManager<> config;
    panda::ConfigurableTask<TestPoolManager<>&, TestHandler, DataType> task(config, finish_handler);

    // submit with data
    DataType data("some_data");
    task.submit(data);
}

TEST_F(ConfigurableTaskTest, test_submit_single_algo)
{
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();

    TestHandler finish_handler(false);
    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> task(config, finish_handler);
    Algo1 algo1;
    task.set_algorithms(algo1);

    // submit with data
    DataType data("some_data");
    task.submit(data);
    ASSERT_TRUE(config.executed()); // ensure pool recieved the submit call
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_multi_algo)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> task(config, finish_handler);
    Algo1 algo1;
    Algo2 algo2;
    task.set_algorithms(algo1, algo2);

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

struct TestAlgoFactory {
    public:
        template<typename AlgoT>
        static inline
        AlgoT create()
        {
            return AlgoT();
        }
};

TEST_F(ConfigurableTaskTest, test_call_with_resource)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager pool_manager;
    pool_manager.add_resource<ArchA>();
    pool_manager.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(pool_manager, finish_handler);
    DataType data_a("some_data a");

    ASSERT_THROW(task(pool_manager.get_resource<ArchA>(), data_a), panda::Error);
    ASSERT_THROW(task(pool_manager.get_resource<ArchB>(), data_a), panda::Error);

    Algo1 algo1;
    Algo2 algo2;
    task.set_algorithms(algo1, algo2);
    task(pool_manager.get_resource<ArchA>(), data_a);
    task(pool_manager.get_resource<ArchB>(), data_a);
    ASSERT_FALSE(pool_manager.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_single_algo_configure)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager pool_manager;
    pool_manager.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(pool_manager, finish_handler);
    TestAlgoFactory factory;
    ASSERT_TRUE(task.configure(factory, TaskType::Select<Algo1>(false), TaskType::Select<Algo2>(true)));

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_TRUE(pool_manager.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_no_algos_configured)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager pool_manager;
    pool_manager.add_resource<ArchA>();
    pool_manager.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(pool_manager, finish_handler);
    TestAlgoFactory factory;
    ASSERT_FALSE(task.configure(factory, TaskType::Select<Algo1>(false), TaskType::Select<Algo2>(false)));

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_FALSE(pool_manager.executed());
}

TEST_F(ConfigurableTaskTest, test_submit_multi_algo_configure)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(config, finish_handler);
    TestAlgoFactory factory;
    task.configure(factory, TaskType::Select<Algo1>(true), TaskType::Select<Algo2>(true));

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

struct TestAlgoFactoryWithCallback : public TestAlgoFactory
{
    private:
        template<typename T>
        struct exec_tag {};

    public:

        template<typename T, typename T2, typename... Ts>
        inline
        void exec() {
            this->exec(exec_tag<T>());
            this->template exec<T2, Ts...>();
        }

        template<typename T>
        inline
        void exec() {
            this->exec(exec_tag<T>());
        }

        bool algo1_activated() const { return _algo_1; }
        bool algo2_activated() const { return _algo_2; }

    protected:
        void exec(exec_tag<Algo1> const&)
        {
            _algo_1 = true;
        }

        void exec(exec_tag<Algo2> const&)
        {
            _algo_2 = true;
        }

        void exec(exec_tag<Algo3> const&)
        {
            _algo_3 = true;
        }

    private:
        bool _algo_1 = false;
        bool _algo_2 = false;
        bool _algo_3 = false;
};

static_assert(HasMethod<TestAlgoFactoryWithCallback, ConfigureUserCallBack<TestAlgoFactoryWithCallback, Algo1>::HasExecuteMethodHelperT>::value, "callback not recognised");
static_assert(HasMethod<TestAlgoFactoryWithCallback, ConfigureUserCallBack<TestAlgoFactoryWithCallback, Algo2>::HasExecuteMethodHelperT>::value, "callback not recognised");
static_assert(HasMethod<TestAlgoFactoryWithCallback, ConfigureUserCallBack<TestAlgoFactoryWithCallback, Algo1, Algo2>::HasExecuteMethodHelperT>::value, "callback not recognised");

TEST_F(ConfigurableTaskTest, test_submit_multi_algo_configure_with_callback)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(config, finish_handler);
    TestAlgoFactoryWithCallback factory;
    task.configure(factory, TaskType::Select<Algo1>(true), TaskType::Select<Algo2>(true));

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    ASSERT_TRUE(factory.algo1_activated());
    ASSERT_TRUE(factory.algo2_activated());
}

struct TestAlgoFactoryWithNonSelectedCallback : public TestAlgoFactory
{
    public:
        void none_selected() { _called = true; }
        bool called() const { return _called; }

    private:
        bool _called = false;
};

TEST_F(ConfigurableTaskTest, test_submit_configure_with_none_selected_callback)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager pool_manager;
    pool_manager.add_resource<ArchA>();
    pool_manager.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(pool_manager, finish_handler);
    TestAlgoFactoryWithNonSelectedCallback factory;
    task.configure(factory, TaskType::Select<Algo1>(false), TaskType::Select<Algo2>(false));

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_FALSE(pool_manager.executed());

    ASSERT_TRUE(factory.called());
}


TEST_F(ConfigurableTaskTest, test_submit_multi_algo_configure_same_arch)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    typedef panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> TaskType;
    TaskType task(config, finish_handler);
    TestAlgoFactory factory;
    ASSERT_THROW(task.configure(factory, TaskType::Select<Algo2>(true), TaskType::Select<Algo3>(true)), panda::Error);

}

TEST_F(ConfigurableTaskTest, test_submit_multi_algo_multi_data)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType, DataType> task(config, finish_handler);
    Algo1 algo1;
    Algo2 algo2;
    task.set_algorithms(algo1, algo2);

    // submit with data
    DataType data_a("some_data a");
    DataType data_b("some_data b");
    task.submit(data_a, data_b);
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_multi_algo_diff_capability)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    config.add_resource<ArchB>();

    TestHandler finish_handler(false);
    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> task(config, finish_handler);
    Algo2 algo2;
    Algo3 algo3;
    task.set_algorithms(algo2, algo3);

    // submit with data
    DataType data_a("some_data a");
    task.submit(data_a);
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_multi_algo_no_devices)
{
    typedef TestPoolManager<ArchA,ArchB> PoolManager;
    PoolManager config;
    TestHandler finish_handler(false);
    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType> task(config, finish_handler);
    Algo1 algo1;
    Algo2 algo2;
    task.set_algorithms(algo1, algo2);
    DataType data_a("some_data a");
    ASSERT_THROW(task.submit(data_a), Error);
}

namespace {
    struct NonCopyableDataType
    {
        NonCopyableDataType() {}
        NonCopyableDataType(NonCopyableDataType const&) = delete;
        NonCopyableDataType(NonCopyableDataType&&) = default;
    };
} // namespace

TEST_F(ConfigurableTaskTest, test_submit_algo_move_only_data_types)
{
    TestHandler finish_handler(false);
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;

    panda::ConfigurableTask<PoolManager&, TestHandler&, NonCopyableDataType> task(config, finish_handler);
    task.set_algorithms(algo1);

    NonCopyableDataType data_a;
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

TEST_F(ConfigurableTaskTest, test_submit_algo_l_value_ref_data_types)
{
    TestHandler finish_handler(false);
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;

    panda::ConfigurableTask<PoolManager&, TestHandler&, DataType&&> task(config, finish_handler);
    task.set_algorithms(algo1);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}

struct TaskMethodFunctorTester
{
    public:
        void operator()(Algo1 const&) {
            _algo_one = true;
        }

        void operator()(Algo2 const&) {
            _algo_two = true;
        }

        bool algo_one() const { return _algo_one; }
        bool algo_two() const { return _algo_two; }

    private:
        bool _algo_one = false;
        bool _algo_two = false;
};

struct TaskMethodFunctorTesterNonConst
{
    public:
        void operator()(Algo1&) {
            _algo_one = true;
        }

        void operator()(Algo2&) {
            _algo_two = true;
        }

        bool algo_one() const { return _algo_one; }
        bool algo_two() const { return _algo_two; }

    private:
        bool _algo_one = false;
        bool _algo_two = false;
};

struct TaskMethodFunctorTesterWithArg : public TaskMethodFunctorTester
{
        typedef TaskMethodFunctorTester BaseT;

    public:
        void operator()(Algo1 const& algo, bool& arg) {
            arg = true;
            BaseT::operator()(algo);
        }

        void operator()(Algo2 const& algo, bool& arg) {
            arg = true;
            BaseT::operator()(algo);
        }
};

struct TaskMethodFunctorTesterWithArgNonConst : public TaskMethodFunctorTesterNonConst
{
        typedef TaskMethodFunctorTesterNonConst BaseT;

    public:
        void operator()(Algo1& algo, bool& arg) {
            arg = true;
            this->BaseT::operator()(algo);
        }

        void operator()(Algo2& algo, bool& arg) {
            arg = true;
            this->BaseT::operator()(algo);
        }
};

TEST_F(ConfigurableTaskTest, test_single_extra_method_non_const_setup)
{
    TestHandler finish_handler(false);
    //auto method_tester = [&](Algo1 const&) { method_called = true; };
    TaskMethodFunctorTesterNonConst method_tester;
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;

    panda::ConfigurableTask<PoolManager&, TestHandler&, Method<decltype(method_tester)&>, DataType&&> task(config, finish_handler, method_tester);
    task.set_algorithms(algo1);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // can we call the method?
    task();
    ASSERT_TRUE(method_tester.algo_one());
    ASSERT_FALSE(method_tester.algo_two());
}


TEST_F(ConfigurableTaskTest, test_single_extra_method_setup)
{
    TestHandler finish_handler(false);
    //auto method_tester = [&](Algo1 const&) { method_called = true; };
    TaskMethodFunctorTester method_tester;
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;

    panda::ConfigurableTask<PoolManager&, TestHandler&, Method<decltype(method_tester)&>, DataType&&> task(config, finish_handler, method_tester);
    task.set_algorithms(algo1);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // can we call the method?
    auto const& const_task_ref = task;
    const_task_ref();
    ASSERT_TRUE(method_tester.algo_one());
    ASSERT_FALSE(method_tester.algo_two());
}


TEST_F(ConfigurableTaskTest, test_single_extra_method_with_functor_args)
{
    TestHandler finish_handler(false);
    TaskMethodFunctorTesterWithArg method_tester;
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;

    panda::ConfigurableTask<PoolManager&, TestHandler&, Method<decltype(method_tester)&, bool&>, DataType&&> task(config, finish_handler, method_tester);
    task.set_algorithms(algo1);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // can we call the method?
    bool argument=false;
    task(argument);
    ASSERT_TRUE(method_tester.algo_one());
    ASSERT_FALSE(method_tester.algo_two());
    ASSERT_TRUE(argument);
}

TEST_F(ConfigurableTaskTest, test_multiple_extra_method_with_functor_args)
{
    TestHandler finish_handler(false);
    TaskMethodFunctorTesterWithArg method_tester;
    TaskMethodFunctorTester method_tester2;
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;
    Algo2 algo2;

    panda::ConfigurableTask<PoolManager&, TestHandler&
                          , Method<decltype(method_tester)&, bool&>
                          , Method<decltype(method_tester2)&>
                          , DataType&&> task(config, finish_handler, method_tester, method_tester2);
    task.set_algorithms(algo1, algo2);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // can we call the method?
    bool argument=false;
    task(argument);
    ASSERT_TRUE(method_tester.algo_one());
    ASSERT_TRUE(method_tester.algo_two());
    ASSERT_TRUE(argument);

    // can we call the second method?
    ASSERT_FALSE(method_tester2.algo_one());
    ASSERT_FALSE(method_tester2.algo_two());
    task();
    ASSERT_TRUE(method_tester2.algo_one());
    ASSERT_TRUE(method_tester2.algo_two());
}

TEST_F(ConfigurableTaskTest, test_multiple_extra_method_with_const_functor_args)
{
    TestHandler finish_handler(false);
    TaskMethodFunctorTesterWithArgNonConst method_tester;
    TaskMethodFunctorTesterNonConst method_tester2;
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager config;
    config.add_resource<ArchA>();
    Algo1 algo1;
    Algo2 algo2;

    panda::ConfigurableTask<PoolManager&, TestHandler&
                          , Method<decltype(method_tester)&, bool&>
                          , Method<decltype(method_tester2)&>
                          , DataType&&> task(config, finish_handler, method_tester, method_tester2);
    task.set_algorithms(algo1, algo2);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(config.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // can we call the method?
    bool argument=false;
    task(argument);
    ASSERT_TRUE(method_tester.algo_one());
    ASSERT_TRUE(method_tester.algo_two());
    ASSERT_TRUE(argument);

    // can we call the second method?
    ASSERT_FALSE(method_tester2.algo_one());
    ASSERT_FALSE(method_tester2.algo_two());
    auto const& const_task_ref = task;
    const_task_ref();
    ASSERT_TRUE(method_tester2.algo_one());
    ASSERT_TRUE(method_tester2.algo_two());
}

TEST_F(ConfigurableTaskTest, test_single_extra_submit_setup)
{
    TestHandler finish_handler(false);
    //auto method_tester = [&](Algo1 const&) { method_called = true; };
    typedef TestPoolManager<ArchA> PoolManager;
    PoolManager pool_manager;
    pool_manager.add_resource<ArchA>();
    Algo1 algo1;

    typedef TestChunk_A ExtraDataType;
    panda::ConfigurableTask<PoolManager&, TestHandler&, SubmitMethod<ExtraDataType&&>, DataType&&> task(pool_manager, finish_handler);
    task.set_algorithms(algo1);

    DataType data_a("some_data a");
    task.submit(std::move(data_a));
    ASSERT_TRUE(pool_manager.executed());
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
    finish_handler.reset();

    ExtraDataType extra_data;
    ASSERT_NO_THROW(task.submit(std::move(extra_data)));
    while(!finish_handler.executed()) {} // ensure pool was able to call the handler

    // test direct operator()(poolResource, ExtraDataType) is also defined
    finish_handler.reset();
    ASSERT_NO_THROW(task(pool_manager.get_resource<ArchA>(), extra_data));

    while(!finish_handler.executed()) {} // ensure pool was able to call the handler
}


} // namespace test
static std::string ArchA_tag("ArchA");
template<>
constexpr std::string const& to_string<test::ArchA>() { return ArchA_tag; }
static std::string ArchB_tag("ArchB");
template<>
constexpr std::string const& to_string<test::ArchB>() { return ArchB_tag; }
} // namespace panda
} // namespace ska
