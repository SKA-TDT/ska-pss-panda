/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DataManagerTest.h"
#include "panda/DataManager.h"
#include "panda/test/TestProducer.h"
#include "panda/test/TestChunk.h"

namespace ska {
namespace panda {
namespace test {


DataManagerTest::DataManagerTest()
    : ::testing::Test()
{
}

DataManagerTest::~DataManagerTest()
{
}

void DataManagerTest::SetUp()
{
}

void DataManagerTest::TearDown()
{
}

struct TestData_A : public TestChunk<char>
{
    TestData_A(std::string const& s = std::string()) : TestChunk<char>(s) {}
};

struct TestData_B : public TestChunk<char>
{
    TestData_B(std::string const& s = std::string()) : TestChunk<char>(s) {}
};

TEST_F(DataManagerTest, test_producer_eof)
{
    typedef TestProducer<TestData_A> ProducerType;
    ProducerType producer;
    DataManager<ProducerType> dm(producer);
    producer.set_process_return(true);

    // ensure abort exception is propagated to the consumer
    ASSERT_THROW(dm.next(), std::exception);
}


TEST_F(DataManagerTest, test_producer_process_single_type)
{
    typedef TestProducer<TestData_A> ProducerType;
    ProducerType producer;
    DataManager<ProducerType> dm(producer);
    std::string msg="TestChunk_A_data";
    std::string msg_2="TestChunk_A_data 2";

    // get_writable
    // allow the object and allow it to go out of scope without invalidating it
    // The object should be added to the data queue.
    {
        std::shared_ptr<TestData_A> chunk = dm.get_writable<TestData_A>();
        chunk->data(msg); // fill the chunk with data
        ASSERT_TRUE((bool)chunk);
    }
    {
        std::shared_ptr<TestData_A> chunk2 = dm.get_writable<TestData_A>();
        chunk2->data(msg_2); // fill the chunk with data
    }
    ASSERT_EQ(unsigned(2), dm.size());

    // should not block as data should be available
    // ensure the expected data is returned and the consumed data has
    // been removed from the queue
    auto result=dm.next();
    ASSERT_EQ(unsigned(1), dm.size());
    ASSERT_NE(TestData_A(), *std::get<0>(result));
    ASSERT_EQ(TestData_A(msg), *std::get<0>(result));
    auto result2=dm.next();
    ASSERT_EQ(TestData_A(msg_2), *std::get<0>(result2));
}

TEST_F(DataManagerTest, test_producer_process_exception)
{
    typedef TestProducer<TestData_A> ProducerType;
    ProducerType producer;
    DataManager<ProducerType> dm(producer);
    producer.set_throw(std::exception());

    // ensure any exception from the producer is propagated to the consumer
    ASSERT_THROW(dm.next(), std::exception);
}

TEST_F(DataManagerTest, test_producer_process_multi_type)
{
    typedef TestProducer<TestData_A, TestData_B> ProducerType;
    ProducerType producer;
    DataManager<ProducerType> dm(producer);
    std::string msg_a="TestChunk_A_data";
    std::string msg_b="TestChunk_B_data";

    // get_writable
    // allow the object and allow it to go out of scope without invalidating it
    // The object should be added to the data queue.
    {
        std::shared_ptr<TestData_A> chunk_a = dm.get_writable<TestData_A>();
        chunk_a->data(msg_a); // fill the chunk with data
        std::shared_ptr<TestData_B> chunk_b = dm.get_writable<TestData_B>(msg_b);
    }
    // -- the queue should contain both the objects
    ASSERT_EQ(unsigned(1), dm.size());
    auto result=dm.next();
    ASSERT_NE(TestData_A(), *std::get<0>(result));
    ASSERT_EQ(TestData_A(msg_a), *std::get<0>(result));
    ASSERT_NE(TestData_B(), *std::get<1>(result));
    ASSERT_EQ(TestData_B(msg_b), *std::get<1>(result));
    ASSERT_EQ(unsigned(0), dm.size());

    // -- second iteration should not trigger until both data types have been refreshed
    // A dataset should be added to the data queue only when both elements arrive.
    {
        {
            std::shared_ptr<TestData_A> chunk_a = dm.get_writable<TestData_A>();
            chunk_a->data(msg_a + "2"); // fill the chunk with data
        }
        ASSERT_EQ(unsigned(0), dm.size());
        std::shared_ptr<TestData_B> chunk_b = dm.get_writable<TestData_B>(msg_b + "2");
    }
    ASSERT_EQ(unsigned(1), dm.size());
}

TEST_F(DataManagerTest, test_producer_process_multi_type_service_data)
{
    // -- test service data (i.e unsynced streams)

    typedef TestProducer<TestData_A, panda::ServiceData<TestData_B>> ProducerType;
    ProducerType producer;
    DataManager<ProducerType> dm(producer);
    //dm.mark_as_service_data<TestData_B>();

    std::string msg_a="TestChunk_A_data";
    std::string msg_a_2="TestChunk_A_data_2";
    std::string msg_b="TestChunk_B_data";
    std::string msg_b_2="TestChunk_B_data_2";

    // first iteration we expect to wait for a full set
    {
        {
            std::shared_ptr<TestData_B> chunk_b = dm.get_writable<TestData_B>(msg_b);
            ASSERT_EQ(unsigned(0), dm.size());
        }
        std::shared_ptr<TestData_A> chunk_a = dm.get_writable<TestData_A>();
        chunk_a->data(msg_a); // fill the chunk with data
    }
    ASSERT_EQ(unsigned(1), dm.size());

    // add some non-service data.
    // we expect no syncing of the streams, but a copy of existing service data to be passed
    {
        std::shared_ptr<TestData_A> chunk_a = dm.get_writable<TestData_A>();
        chunk_a->data(msg_a_2); // fill the chunk with data
    }
    ASSERT_EQ(unsigned(2), dm.size());
    auto result=dm.next();
    ASSERT_NE(TestData_B(), *std::get<1>(result));
    ASSERT_EQ(TestData_B(msg_b), *std::get<1>(result));
    ASSERT_NE(TestData_A(), *std::get<0>(result));
    ASSERT_EQ(TestData_A(msg_a), *std::get<0>(result));
    result=dm.next();
    ASSERT_NE(TestData_B(), *std::get<1>(result));
    ASSERT_EQ(TestData_B(msg_b), *std::get<1>(result));
    ASSERT_NE(TestData_A(), *std::get<0>(result));
    ASSERT_EQ(TestData_A(msg_a_2), *std::get<0>(result));
    ASSERT_EQ(unsigned(0), dm.size());

    // update the service data first
    // we do not expect a new entry on the queue until we receive more non-service data
    {
        std::shared_ptr<TestData_B> chunk_b = dm.get_writable<TestData_B>();
        chunk_b->data(msg_b_2); // fill the chunk with data
    }
    ASSERT_EQ(unsigned(0), dm.size());
    {
        std::shared_ptr<TestData_A> chunk_a = dm.get_writable<TestData_A>();
        chunk_a->data(msg_a); // fill the chunk with data
    }
    ASSERT_EQ(unsigned(1), dm.size());
    result=dm.next();
    ASSERT_NE(TestData_B(), *std::get<1>(result));
    ASSERT_EQ(TestData_B(msg_b_2), *std::get<1>(result));
    ASSERT_NE(TestData_A(), *std::get<0>(result));
    ASSERT_EQ(TestData_A(msg_a), *std::get<0>(result));
    ASSERT_EQ(unsigned(0), dm.size());
}

} // namespace test
} // namespace panda
} // namespace ska
