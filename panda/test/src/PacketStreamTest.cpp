/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PacketStreamTest.h"
#include "panda/Log.h"
#include "panda/test/TestSocket.h"
#include "panda/test/TestChunk.h"
#include "panda/test/TestPacket.h"
#include "panda/test/TestPacketStreamDataTraits.h"
#include "panda/PacketStream.h"
#include "panda/DataManager.h"
#include "panda/ConnectionTraits.h"
#include "panda/IpAddress.h"
#include "panda/ErrorPolicyConcept.h"
#include <mutex>
#include <type_traits>
#include <deque>

namespace ska {
namespace panda {
namespace test {


PacketStreamTest::PacketStreamTest()
    : ::testing::Test()
{
}

PacketStreamTest::~PacketStreamTest()
{
}

void PacketStreamTest::SetUp()
{
}

void PacketStreamTest::TearDown()
{
}

typedef TestPacketStreamDataTraits<uint8_t> DataTraits;

template<typename ConnectionTraits>
class TestPacketStream : public panda::PacketStream<TestPacketStream<ConnectionTraits>, ConnectionTraits, DataTraits>
{
        BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));
        BOOST_CONCEPT_ASSERT((ChunkerContextDataTraitsConcept<DataTraits>));

        typedef panda::PacketStream<TestPacketStream<ConnectionTraits>, ConnectionTraits, DataTraits> BaseT;
        typedef TestPacketStream<ConnectionTraits> SelfType;
        struct Wrap : BaseT { // struct to allow us to extraact type info for ContextType from protected member fn
            auto context() -> decltype(std::declval<Wrap>().BaseT::context());
        };

        std::string _chunk_init;

    public:
        typedef typename std::remove_reference<decltype(std::declval<Wrap>().context())>::type ContextType;

    public:
        template<typename... Ts>
        TestPacketStream(unsigned packets_per_chunk, Ts&&... args)
            : BaseT(DataTraits(packets_per_chunk), std::forward<Ts>(args)...)
            , _chunk_init(packets_per_chunk * DataTraits::packet_size(), 'A')
        {
            this->start();
        }

        ~TestPacketStream()
        {
            PANDA_LOG_DEBUG << "~TestPacketStream()";
        }

        auto context() -> decltype(std::declval<SelfType>().BaseT::context()) {
            ContextType* volatile ctx;
            do {
               ctx=&this->BaseT::context();
            }
            while(!ctx->is_ready()); // _start_chunk is never initialised
            return *ctx;
        }

        // required method
        template<typename DataType>
        std::shared_ptr<DataType> get_chunk(typename BaseT::LimitType, typename DataTraits::PacketInspector const&)
        {
            return this->BaseT::template get_chunk<DataType>(_chunk_init);
        }

};

template<typename ConnectionTraits>
struct LoopBackSendSocket {
        BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));

    public:
        LoopBackSendSocket(unsigned port)
            : _send_socket(_engine)
        {
            panda::IpAddress remote_address("127.0.0.1"); // loopback
            remote_address.port(port);
            boost::system::error_code ec;
            auto endpoint = remote_address.template end_point<typename ConnectionTraits::EndPointType>();
            PANDA_LOG << endpoint;
            _send_socket.open(endpoint.protocol(), ec);
            if(ec) {
                PANDA_LOG_ERROR << "unable to open LoopBackSocket: " << ec.message();
                throw ec;
            }
            _send_socket.connect(endpoint, ec);

            if(ec) {
                PANDA_LOG_ERROR << "unable to connect LoopBackSocket: " << ec.message();
                throw ec;
            }
        }

        template<std::size_t PayloadSize>
        void send(TestPacket<PayloadSize> const& packet) {
            _send_socket.send(boost::asio::buffer(reinterpret_cast<const char*>(&packet), sizeof(TestPacket<PayloadSize>)));
        }

        unsigned port() {
            return _send_socket.remote_endpoint().port();
        }

    private:
        panda::Engine _engine;
        typename ConnectionTraits::SocketType _send_socket;
};

template<typename ErrorPolicy=CumulativeDelayErrorPolicy>
class PacketStreamTester {

        BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<ErrorPolicy>));

        typedef PacketStreamTester<ErrorPolicy> SelfType;
        typedef panda::ConnectionTraits<panda::Udp, ErrorPolicy> ConnectionTraits;
        typedef TestPacketStream<ConnectionTraits> PacketStreamType;
        typedef panda::DataManager<PacketStreamType> DataManagerType;

    public:
        PacketStreamTester(unsigned number_of_threads, unsigned packets_per_chunk)
            : _local_address(0, "127.0.0.1")
            , _engine_config(number_of_threads)
            , _engine(_engine_config)
            , _packet_stream(packets_per_chunk, _engine, std::move(_local_address.template end_point<typename ConnectionTraits::EndPointType>()))
            , _send_socket(_packet_stream.local_end_point().port())
            , _data_mgr(_packet_stream)
        {
        }

        ~PacketStreamTester()
        {
        }

        template<std::size_t PayloadSize>
        void send(TestPacket<PayloadSize> const& packet) {
            PANDA_LOG_DEBUG << "==== SENDING PACKET " << packet.packet_count() << " =====";
            _send_socket.send(packet);
        }

        auto next() -> typename std::result_of<decltype(&DataManagerType::next)(DataManagerType)>::type {
            return _data_mgr.next();
        }

        PacketStreamType& packet_stream() {
            return _packet_stream;
        }

        void stop() {
            _packet_stream.stop();
        }

    private:
        panda::IpAddress _local_address;
        panda::ProcessingEngineConfig _engine_config;
        panda::ProcessingEngine _engine;
        PacketStreamType _packet_stream;
        LoopBackSendSocket<ConnectionTraits> _send_socket;
        DataManagerType _data_mgr;
};

TEST_F(PacketStreamTest, test_continuous_ordered_complete_packets)
{
    // turn on debug messages
    //panda::log_debug.enable(true);

    // Use Case: continuous stream of well formed packets
    TestPacket<2> test_packet_1(1);
    TestPacket<2> test_packet_2(2);
    TestPacket<2> test_packet_3(3);
    TestPacket<2> test_packet_4(4);

    // set up packet stream receiver
    //for(unsigned num_threads=1; num_threads < 2; ++num_threads)
    unsigned packets_per_chunk = 2;
    for(unsigned num_threads=0; num_threads < 3; ++num_threads)
    {
        SCOPED_TRACE(num_threads);
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);

        tester.send(test_packet_1);
        tester.send(test_packet_2);

        auto data_out = tester.next();

        // send out another complete chunk
        tester.send(test_packet_3);
        tester.send(test_packet_4);
        auto data_out_2 = tester.next();
        ASSERT_TRUE(data_out != data_out_2);
    }
}

TEST_F(PacketStreamTest, test_out_of_order_packet_in_subsequent_chunk)
{
    //
    // Use Case: Where the first packet of the second chunk we get does not correspond to the start of the next chunk
    //
    TestPacket<2> test_packet_1(1);
    TestPacket<2> test_packet_2(2);
    TestPacket<2> test_packet_3(3);
    TestPacket<2> test_packet_4(4);
    unsigned packets_per_chunk = 2;
    for(unsigned num_threads=1; num_threads < 3; ++num_threads)
    {
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);
        tester.send(test_packet_1);
        tester.send(test_packet_2);
        auto data_out_1 = tester.next();
        tester.send(test_packet_4); // out of order
        tester.send(test_packet_3);
        auto data_out_2 = tester.next();
        ASSERT_TRUE(data_out_1 != data_out_2);
    }
}

TEST_F(PacketStreamTest, test_subsequent_packet_long_delay)
{
    //
    // Use Case: Where the first packet of the next chunk we get does not correspond to the the expected next chunk
    //           Should reject the current chunk and start a new one with a fresh timestamp etc.
    //
    TestPacket<2> test_packet_1(1);
    TestPacket<2> test_packet_2(2);
    TestPacket<2> test_packet_30(30); // n.b not aligned to previous boundary
    TestPacket<2> test_packet_31(31);
    TestPacket<2> test_packet_32(32); // n.b not aligned to previous boundary
    TestPacket<2> test_packet_33(33);
    unsigned packets_per_chunk=2;
    for(unsigned num_threads=1; num_threads < 3; ++num_threads)
    {
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);
        tester.send(test_packet_1);
        tester.send(test_packet_2);
        auto data_out_1 = tester.next();
        tester.send(test_packet_30); // packets not in sequence and not on aligned boundary
        tester.send(test_packet_31);
        tester.send(test_packet_32);
        tester.send(test_packet_33);
        auto data_out_2 = tester.next();
        ASSERT_TRUE(data_out_1 != data_out_2);
    }
}

TEST_F(PacketStreamTest, test_missing_packets)
{
    for(unsigned packets_per_chunk = 3; packets_per_chunk < 5; ++packets_per_chunk)
    {
        SCOPED_TRACE(packets_per_chunk);
        SCOPED_TRACE("packets_per_chunk: ");
        for(unsigned num_threads=1; num_threads < 4; ++num_threads)
        {
            SCOPED_TRACE(num_threads);
            SCOPED_TRACE("num_threads: ");
            PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);
            // miss last packet of first chunk
            unsigned packet_num=0;
            for(; packet_num<packets_per_chunk-1; ++packet_num) {
                tester.send(TestPacket<2>(packet_num));
            }
            ++packet_num; // skip packet
            for(; packet_num<(2 *packets_per_chunk); ++packet_num) {
                tester.send(TestPacket<2>(packet_num));
            }

            // check data is what we expect
            auto data_out_1 = std::get<0>(tester.next());
            ASSERT_TRUE(data_out_1->missing_called());

            // cater for arbitrary realignment
            // by sending just enough packets to fill the next chunk
            unsigned realign_offset=*data_out_1->begin();
            for(unsigned i=0; i<realign_offset; ++i) {
                tester.send(TestPacket<2>(packet_num));
                ++packet_num;
            }

            auto data_out_2 = std::get<0>(tester.next());
            ASSERT_NE(&*data_out_1, &*data_out_2);
            ASSERT_FALSE(data_out_2->missing_called());
        }
    }
}

TEST_F(PacketStreamTest, test_missing_small_chunk)
{
    // Use case: enough packets are missing to cover one or more small chunks, but not so many to trigger a
    // realignment
    for(unsigned packets_per_chunk = 3; packets_per_chunk < 5; ++packets_per_chunk)
    {
        SCOPED_TRACE(packets_per_chunk);
        SCOPED_TRACE("packets_per_chunk: ");
        for(unsigned num_threads=1; num_threads < 4; ++num_threads)
        {
            SCOPED_TRACE(num_threads);
            SCOPED_TRACE("num_threads: ");
            PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);
            tester.packet_stream().missing_packets_before_realign(4*packets_per_chunk); // ensure we don't invoke realign
            // miss last packet of first chunk
            unsigned packet_num=0;
            for(; packet_num<packets_per_chunk-1; ++packet_num) {
                tester.send(TestPacket<2>(packet_num));
            }
            packet_num += (2 * packets_per_chunk) + 1; // skip over next 2 chunks
            // send the next, next chunk
            for(; packet_num<(4 *packets_per_chunk); ++packet_num) {
                tester.send(TestPacket<2>(packet_num));
            }

            // check data is what we expect
            auto data_out_1 = std::get<0>(tester.next());
            ASSERT_TRUE(data_out_1->missing_called()) << " chunk=" << &*data_out_1;;

            // cater for arbitrary realignment
            // by sending just enough packets to fill the next chunk
            unsigned realign_offset=*data_out_1->begin();
            for(unsigned i=0; i<realign_offset; ++i) {
                tester.send(TestPacket<2>(++packet_num));
            }

            auto data_out_2 = std::get<0>(tester.next());
            ASSERT_NE(&*data_out_1, &*data_out_2);
            ASSERT_TRUE(data_out_2->missing_called()) << " chunk=" << &*data_out_2;;

            auto data_out_3 = std::get<0>(tester.next());
            PANDA_LOG << "got chunk 3";
            ASSERT_NE(&*data_out_2, &*data_out_3);
            ASSERT_TRUE(data_out_3->missing_called()) << " chunk=" << &*data_out_3;;

            auto data_out_4 = std::get<0>(tester.next());
            PANDA_LOG << "got chunk 2";
            ASSERT_NE(&*data_out_3, &*data_out_4);
            ASSERT_FALSE(data_out_4->missing_called()) << " chunk=" << &*data_out_4;;
        }
    }
}

TEST_F(PacketStreamTest, test_last_packet_late_arrival_during_purge)
{
    unsigned num_threads = 2;
    for(unsigned packets_per_chunk = 2; packets_per_chunk < 3; ++packets_per_chunk)
    {
        SCOPED_TRACE(packets_per_chunk);
        SCOPED_TRACE("packets_per_chunk: ");
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);

        // miss last packet of first chunk
        unsigned packet_num=0;
        for(; packet_num<packets_per_chunk-1; ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }
        auto& ctx=tester.packet_stream().context(); // will block until valid context arrives
        unsigned missing_packet_number = packet_num;
        ++packet_num; // skip packet

        PANDA_LOG_DEBUG << "blocking thread:" << ctx;
        ctx.data().block_in_missing_packet(true);

        for(; packet_num<(2 * packets_per_chunk ); ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }
        ctx.data().wait_process_missing_slice_called();

        auto purged_chunk_ptr = ctx.data().get_missing_chunk();

        // now thread blocked in processing_missing_packet
        // we send the missing packet
        tester.send(TestPacket<2>(missing_packet_number)); // send the missing packet of first block

        // send some more data
        for(; packet_num<(2 *packets_per_chunk); ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }

        // release the missing packets block
        PANDA_LOG_DEBUG << "unblocking thread:" << ctx;
        ctx.data().block_in_missing_packet(false);

        tester.packet_stream().context().data().block_in_missing_packet(false);
        auto data_out_1 = std::get<0>(tester.next()); // wait till the data arrives
        ASSERT_EQ(purged_chunk_ptr, data_out_1.get()); // verify we got the right chunk

        // we should not have the old context still alive
        ASSERT_FALSE(tester.packet_stream().context().data().process_missing_slice_called());
    }
}

TEST_F(PacketStreamTest, test_packets_late_arrival_after_purge)
{
    unsigned num_threads = 1;
    for(unsigned packets_per_chunk = 2; packets_per_chunk < 5; ++packets_per_chunk)
    {
        SCOPED_TRACE(packets_per_chunk);
        SCOPED_TRACE("packets_per_chunk: ");
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);

        // miss last packet of first chunk
        unsigned packet_num=0;
        for(; packet_num<packets_per_chunk-1; ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }
        auto& ctx=tester.packet_stream().context(); // will block until valid context arrives
        unsigned missing_packet=packet_num;
        ++packet_num; // skip packet

        // send another chunks worth of packets
        for(; packet_num<(2 *packets_per_chunk); ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }

        ctx.data().wait_process_missing_slice_called();
        auto purged_chunk_ptr = ctx.data().get_missing_chunk();
        auto& ctx2=tester.packet_stream().context(); // will block until valid context arrives
        ASSERT_FALSE(ctx2.data().process_missing_slice_called());

        auto data_out_1 = std::get<0>(tester.next()); // wait till the data arrives
        ASSERT_EQ(data_out_1.get(), purged_chunk_ptr); // verify we got the right chunk
        auto data_out_2 = std::get<0>(tester.next()); // wait till the data arrives
        ASSERT_NE(data_out_2.get(), purged_chunk_ptr); // verify we got the right chunk

        // send the missing packets
        auto ctx_ptr = &tester.packet_stream().context();
        tester.send(TestPacket<2>(missing_packet)); // send the missing packet of first block

        auto ctx_ptr_2 = &tester.packet_stream().context();
        ASSERT_EQ(ctx_ptr, ctx_ptr_2); // we should not of switched contexts
    }
}

TEST_F(PacketStreamTest, test_trailing_purge)
{
    unsigned num_threads = 3;
    for(unsigned skip_packets=1; skip_packets<5; ++skip_packets)
    {
        // Use Case: chunk is large enough that the trailing purge mechanism comes in to play
        unsigned packets_per_chunk = 16;
        PacketStreamTester<panda::CumulativeDelayErrorPolicy> tester(num_threads, packets_per_chunk);
        tester.packet_stream().purge_cadence(4);

        unsigned packet_num=0;
        // send 4 packets OK
        for(; packet_num<4; ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }

        // skip some packets
        for(; packet_num<4+skip_packets; ++packet_num) {
        }

        // send the remaining packets
        for(; packet_num<packets_per_chunk; ++packet_num) {
            tester.send(TestPacket<2>(packet_num));
        }
        auto data_out_1 = std::get<0>(tester.next()); // wait till the data arrives
        ASSERT_TRUE(data_out_1->missing_called()) << " data=" << (void*)&*data_out_1;
        PANDA_LOG << "data=" << (void*)&*data_out_1;
    }
}

TEST_F(PacketStreamTest, test_concepts)
{

    // Because concepts are a compile-time issue, this is not a real test.
    // It is here to illustrate the use of concepts

    unsigned num_threads=3;

    unsigned packets_per_chunk=2;

    // Swap these two lines over to see concepts in action...
    PacketStreamTester<panda::PassiveErrorPolicy> tester(num_threads, packets_per_chunk);
//  PacketStreamTester<panda::Engine> tester(num_threads);
}

} // namespace test
} // namespace panda
} // namespace ska
