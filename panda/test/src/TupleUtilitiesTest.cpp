/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "TupleUtilitiesTest.h"
#include "panda/TupleUtilities.h"
#include "panda/test/TestChunk.h"


namespace ska {
namespace panda {
namespace test {


TupleUtilitiesTest::TupleUtilitiesTest()
    : ::testing::Test()
{
}

TupleUtilitiesTest::~TupleUtilitiesTest()
{
}

void TupleUtilitiesTest::SetUp()
{
}

void TupleUtilitiesTest::TearDown()
{
}

struct Base {
    int data;
};


template<typename Base>
struct MixinClass : Base { 
    MixinClass() {}
    MixinClass(const Base& b) : Base(b) {} // copy base data
};

template<typename Base>
struct MixinClassB : Base { 
};

TEST_F(TupleUtilitiesTest, test_index_ignore_case)
{
    typedef std::tuple<TestChunk_A, const TestChunk_B, TestChunk_C> TupleType;
    std::size_t index = IndexIgnoreConst<TestChunk_B, TupleType>::value;
    ASSERT_EQ(index, 1U);
    index = IndexIgnoreConst<TestChunk_A, TupleType>::value;
    ASSERT_EQ(index, 0U);
    index = IndexIgnoreConst<TestChunk_C, TupleType>::value;
    ASSERT_EQ(index, 2U);

    typedef std::tuple<const TestChunk_A, TestChunk_B, TestChunk_C> TupleTypeB;
    index = IndexIgnoreConst<TestChunk_A, TupleTypeB>::value;
    ASSERT_EQ(index, 0U);
    index = IndexIgnoreConst<TestChunk_B, TupleTypeB>::value;
    ASSERT_EQ(index, 1U);
    index = IndexIgnoreConst<TestChunk_C, TupleType>::value;
    ASSERT_EQ(index, 2U);
}

TEST_F(TupleUtilitiesTest, test_has_type)
{
    typedef std::tuple<TestChunk_A, TestChunk_B> TupleType;
    bool val = HasType<TestChunk_A, TupleType>::value;
    ASSERT_TRUE(val);

    val = HasType<TestChunk_B, TupleType>::value;
    ASSERT_TRUE(val);

    val = HasType<Base, TupleType>::value;
    ASSERT_FALSE(val);
}

template<typename... Types> struct generic_template_type {};

TEST_F(TupleUtilitiesTest, test_has_type_generic)
{
    typedef generic_template_type<TestChunk_A, TestChunk_B> TupleType;
    bool val = HasType<TestChunk_A, TupleType>::value;
    ASSERT_TRUE(val);

    val = HasType<TestChunk_B, TupleType>::value;
    ASSERT_TRUE(val);

    val = HasType<Base, TupleType>::value;
    ASSERT_FALSE(val);
}

TEST_F(TupleUtilitiesTest, test_create_mixin)
{
/*
    MixinClass<Base>* m1 = panda::Mixin<Base, MixinClass>::create();
    delete m1;
    MixinClass<MixinClassB<Base>>* m2 = panda::Mixin<Base, MixinClass, MixinClassB>::create();
    delete m2;
    Base* base = panda::Mixin<Base>::create(); // Base only
    delete base;
*/
}

TEST_F(TupleUtilitiesTest, test_extend_copy_constructor)
{
    Base base;
    base.data = 101;

    MixinClass<Base>* extended = panda::extend<MixinClass, Base>(base);
    ASSERT_EQ(101, extended->data); // ensure content of base is transfered
    delete extended;
}

TEST_F(TupleUtilitiesTest, test_tuple_diff)
{
    static_assert(std::is_same<typename tuple_diff<std::tuple<int, float>, std::tuple<float>>::type, std::tuple<int>>::value, "test");
    static_assert(std::is_same<typename tuple_diff<std::tuple<int, float>, std::tuple<int>>::type, std::tuple<float>>::value, "test");
    static_assert(std::is_same<typename tuple_diff<std::tuple<unsigned, int, float>, std::tuple<int, unsigned>>::type, std::tuple<float>>::value, "test");
}

} // namespace test
} // namespace panda
} // namespace ska
