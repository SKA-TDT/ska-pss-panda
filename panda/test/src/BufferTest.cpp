/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "BufferTest.h"
#include "panda/Buffer.h"
#include "panda/Copy.h"
#include <numeric>

namespace ska {
namespace panda {
namespace test {


BufferTest::BufferTest()
    : ::testing::Test()
{
}

BufferTest::~BufferTest()
{
}

void BufferTest::SetUp()
{
}

void BufferTest::TearDown()
{
}

TEST_F(BufferTest, test_copy)
{
    Buffer<int> const p1(10);
    int const* d1 = p1.data();

    Buffer<int> p2(p1);
    auto d2 = p2.data();
    ASSERT_EQ(d1, d2);
    ASSERT_EQ(p2.size(), p1.size());
}

TEST_F(BufferTest, test_panda_copy)
{
    Buffer<int> p1(10);
    std::iota(p1.begin(), p1.end(), 0);
    Buffer<int> p2(10);
    panda::copy(p1.begin(), p1.end(), p2.begin());
    for(std::size_t i=0; i < p1.size(); ++i) {
        ASSERT_EQ(*p1.data(i), *p2.data(i));
    }
}

TEST_F(BufferTest, test_move)
{
    Buffer<int> p1(10);
    int* d1 = p1.data();
    std::size_t s1=p1.size();
    ASSERT_EQ(s1, 10U); // test size is consistent with constructor

    Buffer<int> p2(std::move(p1));
    int* d2 = p2.data();
    ASSERT_EQ(d1, d2);
    ASSERT_EQ(p2.size(), s1);
}

TEST_F(BufferTest, test_swap)
{
    Buffer<int> p1(10);
    Buffer<int> p2(100);
    int* d1 = p1.data();
    int* d2 = p2.data();
    p1.swap(p2);
    ASSERT_EQ(p1.data(), d2);
    ASSERT_EQ(p2.data(), d1);
    ASSERT_EQ(p1.size(), 100U);
    ASSERT_EQ(p2.size(), 10U);
}

} // namespace test
} // namespace panda
} // namespace ska
