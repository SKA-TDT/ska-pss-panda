/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CpuTest.h"
#include "panda/Cpu.h"


namespace ska {
namespace panda {
namespace test {


CpuTest::CpuTest()
    : ::testing::Test()
{
}

CpuTest::~CpuTest()
{
}

void CpuTest::SetUp()
{
}

void CpuTest::TearDown()
{
}

TEST_F(CpuTest, test_resource_discovery)
{
    Cpu cpu; // check construction

    DiscoverResources<Cpu> discover;

    std::vector<std::shared_ptr<PoolResource<Cpu>>> resources(discover());

    ASSERT_TRUE(resources.size()>0);
}

TEST_F(CpuTest, test_device_memory)
{
    DiscoverResources<Cpu> discover;
    std::vector<std::shared_ptr<PoolResource<Cpu>>> resources(discover());
    ASSERT_NE(100U, resources[0]->device_memory());
}

} // namespace test
} // namespace panda
} // namespace ska
