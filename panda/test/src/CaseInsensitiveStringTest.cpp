/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CaseInsensitiveStringTest.h"
#include "panda/CaseInsensitiveString.h"
#include <sstream>


namespace ska {
namespace panda {
namespace test {


CaseInsensitiveStringTest::CaseInsensitiveStringTest()
    : ::testing::Test()
{
}

CaseInsensitiveStringTest::~CaseInsensitiveStringTest()
{
}

void CaseInsensitiveStringTest::SetUp()
{
}

void CaseInsensitiveStringTest::TearDown()
{
}

TEST_F(CaseInsensitiveStringTest, test_comparison)
{
    CaseInsensitiveString s1("AbcD");
    CaseInsensitiveString s2("aBcD");
    CaseInsensitiveString s3("1aBcD");
    ASSERT_EQ(s1, s2);
    ASSERT_NE(s1, s3);
    ASSERT_TRUE(s1 == "abcd");
}

TEST_F(CaseInsensitiveStringTest, test_add_std_string)
{
    CaseInsensitiveString s1("AbcD");
    std::string s2("plus1");
    CaseInsensitiveString s3 = s1 + s2;
    std::string s4 = s2 + s1;
    ASSERT_EQ(s3, CaseInsensitiveString("AbcDplus1"));
    ASSERT_EQ(s4, std::string("plus1AbcD"));
}

TEST_F(CaseInsensitiveStringTest, test_string_input_stream)
{
    std::string line("abc");
    std::istringstream ss(line);
    CaseInsensitiveString ci_str;
    ss >> ci_str;
    ASSERT_EQ(ci_str, "ABC");
}

TEST_F(CaseInsensitiveStringTest, test_output_stream)
{
    std::string buffer;
    std::ostringstream ss(buffer);
    CaseInsensitiveString ci_str("abd");
    ss << ci_str;
    ss.flush();
    ASSERT_EQ(ss.str(), "abd");
}

} // namespace test
} // namespace panda
} // namespace ska
