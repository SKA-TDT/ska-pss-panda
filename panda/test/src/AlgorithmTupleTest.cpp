/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "AlgorithmTupleTest.h"
#include "panda/AlgorithmTuple.h"
#include <string>

namespace ska {
namespace panda {
namespace test {


AlgorithmTupleTest::AlgorithmTupleTest()
    : ::testing::Test()
{
}

AlgorithmTupleTest::~AlgorithmTupleTest()
{
}

void AlgorithmTupleTest::SetUp()
{
}

void AlgorithmTupleTest::TearDown()
{
}

struct ArchA {
};

struct ArchB {
};

struct A {
    A() {}
    A(A const&) =  delete;
    A(A&&) =  default;
    typedef ArchA Architecture;
    static std::string data() { return "A"; }
};

struct B {
    typedef ArchB Architecture;
    static std::string data() { return "B"; }
};

TEST_F(AlgorithmTupleTest, test_get_with_algorithm_move_constructor)
{
    typedef AlgorithmTuple<A, B> TupleType;
    A a;
    B b;
    TupleType algo_tuple(std::move(a), std::move(b));
    auto& algo_a = algo_tuple.get<ArchA>();
    auto& algo_b = algo_tuple.get<ArchB>();
    ASSERT_EQ(algo_a.data(), std::string("A")); 
    ASSERT_EQ(algo_b.data(), std::string("B")); 
}

TEST_F(AlgorithmTupleTest, test_get_with_tuple_move_constructor)
{
    typedef AlgorithmTuple<A, B> TupleType;
    A a;
    B b;
    std::tuple<A,B> n_tuple(std::move(a), std::move(b));
    TupleType algo_tuple(std::move(n_tuple));
    auto& algo_a = algo_tuple.get<ArchA>();
    auto& algo_b = algo_tuple.get<ArchB>();
    ASSERT_EQ(algo_a.data(), std::string("A")); 
    ASSERT_EQ(algo_b.data(), std::string("B")); 
}

TEST_F(AlgorithmTupleTest, test_algotuple_move_constructor)
{
    typedef AlgorithmTuple<A, B> TupleType;
    A a;
    B b;
    std::tuple<A,B> n_tuple(std::move(a), std::move(b));
    TupleType algo_tuple(std::move(n_tuple));
    TupleType algo_tuple_b(std::move(algo_tuple));
    auto& algo_a = algo_tuple_b.get<ArchA>();
    ASSERT_EQ(algo_a.data(), std::string("A")); 
}

TEST_F(AlgorithmTupleTest, test_architecture_info)
{
    typedef AlgorithmTuple<A, B> TupleType;
    static_assert(std::is_same<std::tuple<ArchA, ArchB>, typename TupleType::Architectures>::value, "architectures not exported");
}

} // namespace test
} // namespace panda
} // namespace ska
