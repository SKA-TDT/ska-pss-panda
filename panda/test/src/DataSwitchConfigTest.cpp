/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DataSwitchConfigTest.h"
#include "panda/DataSwitchConfig.h"
#include "panda/ProcessingEngine.h"


namespace ska {
namespace panda {
namespace test {


DataSwitchConfigTest::DataSwitchConfigTest()
    : ::testing::Test()
    , _channel_1("channel_1")
    , _channel_2("channel_2")
    , _channel_3("channel_3")
{
}

DataSwitchConfigTest::~DataSwitchConfigTest()
{
}

void DataSwitchConfigTest::SetUp()
{
}

void DataSwitchConfigTest::TearDown()
{
}

TEST_F(DataSwitchConfigTest, test_default_engine)
{
    DataSwitchConfig config;

    /** 
     * @test 
     *   no special engine  config, so check we get the same engine back for each call irrespective of channel
     */
    ProcessingEngine& e1=config.engine(_channel_1);
    ProcessingEngine& e2=config.engine(_channel_1);
    ProcessingEngine& e3=config.engine(_channel_2);
    ASSERT_EQ(&e1, &e2);
    ASSERT_EQ(&e1, &e3);
}

TEST_F(DataSwitchConfigTest, test_setting_default_engine)
{
    DataSwitchConfig config;

    /** 
     * @test 
     *   default engine config supplied, so check we get an approproately configured engine 
     *   and this is returned for each channel.
     *   one channel has its own config and we expect an appropriate engine
     */
    ProcessingEngineConfig p_config;
    ProcessingEngineConfig p_config_2;
    p_config.number_of_threads(0);
    p_config_2.number_of_threads(4);

    ASSERT_NE(0U, config.engine_config().number_of_threads()); // default should be async
    config.set_engine_config(p_config);
    ASSERT_EQ(0U, config.engine_config().number_of_threads()); // changed to sync
    ASSERT_EQ(0U, config.engine_config(_channel_1).number_of_threads()); // changed to sync
    ASSERT_EQ(0U, config.engine_config(_channel_2).number_of_threads()); // not yet set so should use default

    config.set_engine_config(_channel_2, p_config_2);
    ASSERT_EQ(4U, config.engine_config(_channel_2).number_of_threads());

    ProcessingEngine& e1=config.engine(_channel_1);
    ASSERT_EQ(0U, e1.number_of_threads());
    ProcessingEngine& e2=config.engine(_channel_1);
    ASSERT_EQ(&e1, &e2);
    ProcessingEngine& e3=config.engine(_channel_2);
    ASSERT_NE(&e2, &e3);
    ASSERT_EQ(4U, e3.number_of_threads());
    ProcessingEngine& e4=config.engine(_channel_2);
    ASSERT_EQ(&e4, &e3);
    ProcessingEngine& e5=config.engine(_channel_3);
    ASSERT_EQ(&e1, &e5);

}

} // namespace test
} // namespace panda
} // namespace ska
