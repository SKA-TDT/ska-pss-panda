/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "TypeTraitsTest.h"
#include "panda/TypeTraits.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {
namespace test {


TypeTraitsTest::TypeTraitsTest()
    : BaseT()
{
}

TypeTraitsTest::~TypeTraitsTest()
{
}

void TypeTraitsTest::SetUp()
{
}

void TypeTraitsTest::TearDown()
{
}

namespace {
    class A {
        public:
            void void_function();
            int begin(float);
    };
    class B
    {
        public:
            int begin();
            void void_function(std::size_t);
    };
    class C
    {
        public:
            int begin();
            int begin() const;
    };
    class D
    {
        protected:
            int begin();
            int begin() const;
    };

    // define testers
    template<typename T>
    using has_begin_t = decltype(std::declval<T>().begin());
    static_assert(std::is_same<has_begin_t<B>, int>::value, "");

    template<typename T>
    struct has_begin : panda::HasMethod<T, has_begin_t> {};

    template<typename T>
    using has_const_begin_t = decltype(std::declval<T const&>().begin());

    template<typename T>
    using has_const_begin = panda::HasMethod<T, has_const_begin_t>;

    template<typename T>
    using has_void_function_t = decltype(std::declval<T&>().void_function());

    template<typename T>
    using has_void_function_with_arg_t = decltype(std::declval<T&>().void_function(std::declval<std::size_t>()));
} // namespace

TEST_F(TypeTraitsTest, test_has_method)
{
    static_assert(!has_begin<A>::value, "error");
    static_assert(!has_const_begin<A>::value, "error");
    static_assert(has_begin<B>::value, "error");
    static_assert(!has_const_begin<B>::value, "error");
    static_assert(has_begin<C>::value, "error");
    static_assert(has_const_begin<C>::value, "error");
    static_assert(!has_begin<D>::value, "error: did not expect to see protected members");
    static_assert(!has_const_begin<D>::value, "error: did not expect to see protected members");
    // void functions
    static_assert(HasMethod<A, has_void_function_t>::value, "error: function returingn void not detected");
    static_assert(!HasMethod<B, has_void_function_t>::value, "error: function returning void reported when it doesn't exist");
    static_assert(!HasMethod<A, has_void_function_with_arg_t>::value, "error: function returning void reported when it doesn't exist");
    static_assert(HasMethod<B, has_void_function_with_arg_t>::value, "error: function returning void, taking an argument is not reported");
}

struct SelectMethodCallBack
{
        typedef std::tuple<int, float, double> TupleT;

    public:
        SelectMethodCallBack()
            : _count_tuple(0, 0.0, 0.0)
            , _null_count(0)
        {}

        template<typename T>
        T& count() { return std::get<panda::Index<T, TupleT>::value>(_count_tuple); }


        template<typename T, typename T2, typename... Ts>
        void exec() {
            ++count<T>();
            this->template exec<T2, Ts...>();
        }

        template<typename T>
        void exec() {
            ++this->count<T>();
        }

        void exec() {
            ++this->_null_count;
        }

        int null_count() const {
            return _null_count;
        }

    private:
        TupleT _count_tuple;
        int _null_count;

};

TEST_F(TypeTraitsTest, test_select_method)
{
    SelectMethodCallBack callback;
    ASSERT_TRUE(SelectMethod::exec(callback, SelectMethod::Select<int>(true), SelectMethod::Select<float>(true), SelectMethod::Select<double>(false)));
    ASSERT_EQ(callback.count<int>(), 1);
    ASSERT_FLOAT_EQ(callback.count<float>(), 1);
    ASSERT_DOUBLE_EQ(callback.count<double>(), 0.0);
    ASSERT_EQ(callback.null_count(), 0);
}

TEST_F(TypeTraitsTest, test_select_method_all_false)
{
    SelectMethodCallBack callback;
    ASSERT_FALSE(SelectMethod::exec(callback, SelectMethod::Select<int>(false), SelectMethod::Select<float>(false), SelectMethod::Select<double>(false)));
    ASSERT_EQ(callback.count<int>(), 0);
    ASSERT_FLOAT_EQ(callback.count<float>(), 0.0);
    ASSERT_DOUBLE_EQ(callback.count<double>(), 0.0);
    ASSERT_EQ(callback.null_count(), 1);
}
} // namespace test
} // namespace panda
} // namespace ska
