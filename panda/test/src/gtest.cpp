/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/gtest.h"
#include "panda/Log.h"
#include "panda/FileSearchPath.h"

namespace ska {
namespace panda {
namespace test {

std::string& test_data_dir()
{
    static std::string data_dir = ".";
    return data_dir;
}

panda::FileSearchPath& test_data_path()
{
    static panda::FileSearchPath data_path;
    return data_path;
}

std::string test_file(std::string const& filename, bool fail_if_not_found) {
    std::string file = test_data_path().find(filename);
    if(file == "" && fail_if_not_found) {
        FAIL() << "Could not find test data file. Please specify directory it can be found in with --test_data option : " << filename, "";
    }
    PANDA_LOG << "using test file: " << file;
    return test_data_path().find(filename);
}

int panda_gtest_main(int argc, char** argv)
{
    // will process gtest options and pass on the rest
    testing::InitGoogleTest(&argc, argv);

    // process extra command line options;
    bool help=false;
    for(int i=0; i < argc; ++i ) {
        std::string const arg(argv[i]);
        if( arg == "--help")
        {
            help=true;
            std::cout << "\npanda test options:" << "\n";
            std::cout << "\t" << "--test_data <dir>" << "\t" << "specify the directory to find the tests required data files" << "\n";
            std::cout << "\t" << "--log_debug      " << "\t" << "activate debug logging" << "\n";
        }
        else if( arg == "--test_data") {
            if(++i < argc) {
                std::string const val(argv[i]);
                test_data_dir() = val;
                test_data_path().push_back(boost::filesystem::path(val));
            }
        }
        else if( arg == "--log_debug") {
            panda::log_debug.enable(true);
        }
    }
    // -- add the test executables directory to the data search path
    test_data_path().push_back(boost::filesystem::system_complete(argv[0]).parent_path());

    if(help)
    {
        std::cout << "test data search path :\n";
        for(auto const& path : test_data_path().paths()) {
            std::cout << " " << path << "\n";
        }
    }
    return RUN_ALL_TESTS();
}

} // namespace testg
} // namespace panda
} // namespace ska
