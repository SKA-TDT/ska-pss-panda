/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestDir.h"
#include "panda/Error.h"
#include <thread>
#include <chrono>
#include <iostream>


namespace ska {
namespace panda {
namespace test {

TestDir::TestDir(bool cleanup)
    : _cleanup(cleanup)
{
    _path = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
}

TestDir::~TestDir()
{
    if(boost::filesystem::is_directory(_path)) {
        if(!_cleanup) {
            std::cout << "leaving temp path " << _path << std::endl;
        }
        else {
            boost::filesystem::remove_all(_path);
        }
    }
    else if(boost::filesystem::exists(_path)) {
        boost::filesystem::remove(_path);
    }
}

boost::filesystem::path const& TestDir::path() const
{
    return _path;
}

std::string const& TestDir::dir_name() const
{
    return _path.native();
}

void TestDir::create()
{
    std::lock_guard<std::mutex> lock(_mutex);
    if(!boost::filesystem::create_directory(_path)) {
        panda::Error e("could not create path ");
        e << _path;
        throw e;
    }
}

} // namespace test
} // namespace panda
} // namespace ska
