/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "BasicAppConfigTest.h"
#include "panda/BasicAppConfig.h"
#include "panda/Log.h"

namespace ska {
namespace panda {
namespace test {


BasicAppConfigTest::BasicAppConfigTest()
    : ::testing::Test()
{
}

BasicAppConfigTest::~BasicAppConfigTest()
{
}

void BasicAppConfigTest::SetUp()
{
}

void BasicAppConfigTest::TearDown()
{
}



class Args: public std::vector<char*> {
public:
    typedef std::initializer_list<const char*> ListType;

    Args(ListType c) {for(auto i:c) std::vector<char*>::push_back(const_cast<char*>(i));}

    int argc() const {return size();}

    char** argv() const {return const_cast<char**>(data());}
}; // Args



class TestBasicAppConfig: public BasicAppConfig
{
    public:
        TestBasicAppConfig(std::string const& app)
            : BasicAppConfig(app, "test class")
        {
        }
 
}; // TestBasicAppConfig

class TestBasicAppConfigParser: public TestBasicAppConfig
{
    public:
        template<typename... T>
        TestBasicAppConfigParser(Args::ListType c)
            : TestBasicAppConfig(*c.begin())
        {
            Args args(c);

            parse(args.argc(),args.argv());
        }
}; // TestBasicAppConfigParser

// test use case where extra options are defined at the top level
class TestBasicAppConfigWithOptions : public BasicAppConfig
{
    public:
        TestBasicAppConfigWithOptions() 
            : BasicAppConfig("TestBasicAppConfigWithOptions", "test class")
            , _extra_option(false)
        {
        }

        void add_options(OptionsDescriptionEasyInit& add_options) override
        {
            add_options
            ("extra-option", boost::program_options::bool_switch()->notifier([this](bool b)
                {
                    _extra_option = b;
                })
                , "extra option doc");
        }

        bool extra_option() const {
            return _extra_option;
        }

    private:
        bool _extra_option;
};

class TestBasicAppConfigWithSubOptions : public TestBasicAppConfigWithOptions
{
        class TestSubOptions : public panda::ConfigModule
        {
            public:
                TestSubOptions()
                    : panda::ConfigModule("sub") 
                    , _sub_option(false)
                {}

                void add_options(OptionsDescriptionEasyInit& add_options) override
                {
                    add_options
                        ("option", boost::program_options::bool_switch()->notifier([this](bool b)
                                                                                   {
                                                                                   _sub_option = b;
                                                                                   })
                         , "sub option doc");
                }

                bool sub_option() const {
                    return _sub_option;
                }

            private:
                bool _sub_option;
            
        };

    public:
        TestBasicAppConfigWithSubOptions()
        {
            add(_sub_options);
        }
   
        bool sub_option() const {
            return _sub_options.sub_option();
        }

    private:
        TestSubOptions _sub_options;
};

TEST_F(BasicAppConfigTest, test_basic_options)
{
    TestBasicAppConfigParser t1{"app","--list-modules"};
    TestBasicAppConfigParser t2{"app","--help"};
    TestBasicAppConfigParser t3{"app","--version"};
    TestBasicAppConfigParser t4{"app","--log-level","debug"};
    TestBasicAppConfigParser t5{"app","--log-level","error"};
    TestBasicAppConfigParser t6{"app","--log-level","warn"};
    TestBasicAppConfigParser t7{"app","--log-level","log"};
    TestBasicAppConfigParser t8{"app","--help-module","arg"};

    {
        bool no_such_file=false;

        try {
            TestBasicAppConfigParser t{"app","--config","no_such_file.conf"};
        } catch(std::exception&) {
            no_such_file=true;
        }

        ASSERT_TRUE(no_such_file);
    }

    {
        bool no_such_config_file=false;

        try {
            TestBasicAppConfigParser t{"app","--help-config-file","no_such_config_file"};
        } catch(std::exception&) {
            no_such_config_file=true;
        }

        ASSERT_TRUE(no_such_config_file);
    }
}

TEST_F(BasicAppConfigTest, test_garbage_option)
{
    bool garbage=false;

    try {
       TestBasicAppConfigParser garbage{"app","--totalg-arbageo-ption"};
    }
    catch(std::exception& e) {
       garbage=true;
    }

    ASSERT_TRUE(garbage);
}

TEST_F(BasicAppConfigTest, test_parse_extra_add_options)
{
    TestBasicAppConfigWithOptions config;
    std::vector<char*> argv;
    const char* app_name="test_app";
    argv.push_back(const_cast<char*>(app_name));

    // parse without option specified
    config.parse(argv.size(), &argv[0]);
    ASSERT_FALSE(config.extra_option());

    // parse with option specified
    const char* extra_option="--extra-option";
    argv.push_back(const_cast<char*>(extra_option));
    config.parse(argv.size(), &argv[0]);
    ASSERT_TRUE(config.extra_option());
}

TEST_F(BasicAppConfigTest, test_parse_sub_options)
{
    TestBasicAppConfigWithSubOptions config;
    std::vector<char*> argv;
    const char* app_name="test_app";
    argv.push_back(const_cast<char*>(app_name));

    // parse without option specified
    config.parse(argv.size(), &argv[0]);
    ASSERT_FALSE(config.extra_option());
    ASSERT_FALSE(config.sub_option());

    // parse with sub-option specified
    const char* sub_option="--sub.option";
    argv.push_back(const_cast<char*>(sub_option));
    config.parse(argv.size(), &argv[0]);
    ASSERT_FALSE(config.extra_option());
    ASSERT_TRUE(config.sub_option());
    
    // parse with top level option specified
    const char* extra_option="--extra-option";
    argv.push_back(const_cast<char*>(extra_option));
    config.parse(argv.size(), &argv[0]);
    ASSERT_TRUE(config.extra_option());
}

} // namespace test
} // namespace panda
} // namespace ska
