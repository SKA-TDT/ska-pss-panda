/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "FileSearchPathTest.h"
#include "panda/FileSearchPath.h"
#include "panda/test/TestDir.h"
#include <fstream>
#include <tuple>


namespace ska {
namespace panda {
namespace test {


FileSearchPathTest::FileSearchPathTest()
    : BaseT()
{
}

FileSearchPathTest::~FileSearchPathTest()
{
}

void FileSearchPathTest::SetUp()
{
}

void FileSearchPathTest::TearDown()
{
}

TEST_F(FileSearchPathTest, test_empty)
{
    FileSearchPath path;
    std::string filename = path.find("");
    boost::filesystem::path file_path;
    bool found;
    std::tie(file_path, found) = path.find_path("");
    ASSERT_FALSE(found);
    ASSERT_EQ(boost::filesystem::path(), file_path);
    ASSERT_EQ("", filename);
}

TEST_F(FileSearchPathTest, test_search_non_existing_file)
{
    TestDir dir1;
    dir1.create();

    std::string filename="file1";
    boost::filesystem::path full_file_path = dir1.path() / filename;

    FileSearchPath path(dir1.path());
    boost::filesystem::path file_path;
    bool found;
    std::tie(file_path, found) = path.find_path(filename);
    ASSERT_FALSE(found);
    ASSERT_EQ(boost::filesystem::path(), file_path);
    ASSERT_EQ("", file_path.string());
}

TEST_F(FileSearchPathTest, test_search_existing_file)
{
    TestDir dir1;
    dir1.create();

    std::string filename="file1";
    boost::filesystem::path full_file_path = dir1.path() / filename;
    std::ofstream file(full_file_path.string());
    file << "hello";
    file.flush();

    FileSearchPath path(dir1.path());
    boost::filesystem::path file_path;
    bool found;
    std::tie(file_path, found) = path.find_path(filename);
    ASSERT_TRUE(found);
    ASSERT_EQ(full_file_path.string(), file_path.string());
    ASSERT_EQ(boost::filesystem::canonical(file_path).string(), path.find(filename));

    // insert a directory into the search path
    TestDir dir2;
    dir2.create();
    path.push_front(dir2.path());

    boost::filesystem::path file_path2;
    std::tie(file_path2, found) = path.find_path(filename);
    ASSERT_TRUE(found);
    ASSERT_EQ(file_path, file_path2);

    // insert a directory at end of the search path
    TestDir dir3;
    dir3.create();
    path.push_front(dir3.path());

    boost::filesystem::path file_path3;
    std::tie(file_path3, found) = path.find_path(filename);
    ASSERT_TRUE(found);
    ASSERT_EQ(file_path, file_path3);
}

} // namespace test
} // namespace panda
} // namespace ska
