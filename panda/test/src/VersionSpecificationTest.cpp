/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "VersionSpecificationTest.h"
#include "panda/VersionSpecification.h"


namespace ska {
namespace panda {
namespace test {


VersionSpecificationTest::VersionSpecificationTest()
    : ::testing::Test()
{
}

VersionSpecificationTest::~VersionSpecificationTest()
{
}

void VersionSpecificationTest::SetUp()
{
}

void VersionSpecificationTest::TearDown()
{
}

TEST_F(VersionSpecificationTest, test_comparision_operators)
{
   panda::VersionSpecification<int, int> v0(0,3);
   panda::VersionSpecification<int, int> v1(1,3);
   panda::VersionSpecification<int, int> v2(1,3);
   panda::VersionSpecification<int, int> v3(1,4);
   panda::VersionSpecification<int, int> v4(1,2);

   ASSERT_EQ( v0, v0 );
   ASSERT_EQ( v1, v2 );
   ASSERT_NE( v0, v1 );

   ASSERT_TRUE( v3 > v2 );
   ASSERT_TRUE( v3 > v1 );
   ASSERT_TRUE( v3 > v0 );
   ASSERT_TRUE( v3 > v4 );
   ASSERT_FALSE( v4 > v3 );
   ASSERT_FALSE( v0 > v1 ); // same minor version
   ASSERT_FALSE( v2 > v1 );

   ASSERT_TRUE( v3 >= v2 );
   ASSERT_TRUE( v3 >= v1 );
   ASSERT_TRUE( v3 >= v0 );
   ASSERT_TRUE( v3 >= v4 );
   ASSERT_TRUE( v2 >= v1 );
   ASSERT_FALSE( v2 >= v3 );
}

} // namespace test
} // namespace panda
} // namespace ska
