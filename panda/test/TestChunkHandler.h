/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_TESTCHUNKHANDLER_H
#define SKA_PANDA_TEST_TESTCHUNKHANDLER_H

#include "panda/test/TestHandler.h"
#include <memory>
#include <type_traits>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *     A TestHandler that accepts a DataChunk as its argument
 *
 * @details
 *     The chunk type passed is retained
 *     Ensure to set blocking to false if using sync mode
 */
template<class ChunkType>
class TestChunkHandler : public TestHandler
{
    public:
        TestChunkHandler(bool blocking=false);
        ~TestChunkHandler();

        /**
         * @brief function to be called on execution
         */
        void operator()(ChunkType& chunk);

        /**
         * @brief return the chunk that was sent when handler was executed
         */
        typename std::remove_const<ChunkType>::type const& chunk() const;

        /**
         * @brief reset the handler to its pre-executed state
         */
        void reset();

    private:
        // make sure we cannot call this by mistake by making it private
        void operator()();

    protected:
        std::shared_ptr<typename std::remove_const<ChunkType>::type> _chunk; // share data between handler copies
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestChunkHandler.cpp"

#endif // SKA_PANDA_TEST_TESTCHUNKHANDLER_H
