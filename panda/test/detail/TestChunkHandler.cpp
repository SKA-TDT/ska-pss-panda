/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestChunkHandler.h"


namespace ska {
namespace panda {
namespace test {


template<class ChunkType>
TestChunkHandler<ChunkType>::TestChunkHandler(bool blocking)
    : TestHandler(blocking)
    , _chunk(std::make_shared<typename std::remove_const<ChunkType>::type>("not set"))
{
}

template<class ChunkType>
TestChunkHandler<ChunkType>::~TestChunkHandler()
{
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::operator()(ChunkType& chunk)
{
    *_chunk = chunk; // store a copy of the chunk we were passed.
    (*this)();       // do the usual Testhandler things
}

template<class ChunkType>
typename std::remove_const<ChunkType>::type const& TestChunkHandler<ChunkType>::chunk() const 
{ 
    return *_chunk;
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::reset()
{
    TestHandler::reset();
    *_chunk=ChunkType("not set");
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::operator()() 
{
    TestHandler::operator()();
}

} // namespace test
} // namespace panda
} // namespace ska
