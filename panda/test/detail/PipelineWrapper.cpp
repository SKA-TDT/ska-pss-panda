/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/PipelineWrapper.h"

namespace ska {
namespace panda {
namespace test {

template<class Pipeline>
template<typename... Args>
PipelineWrapper<Pipeline>::PipelineWrapper(Args&&... args)
 : Pipeline(std::forward<Args>(args)...)
 , _run_count(0)
{
}

template<class Pipeline>
PipelineWrapper<Pipeline>::~PipelineWrapper()
{
}

template<class Pipeline>
template<typename... RunArgs>
void PipelineWrapper<Pipeline>::run(RunArgs&&... args)
{
    ++_run_count;  // track how many times run is called  
    static_cast<Pipeline&>(*this).run(std::forward<RunArgs>(args)...);
    if(_max_count && (_run_count > _max_count) ) {
        Pipeline::stop();
    }
}

template<class Pipeline>
unsigned PipelineWrapper<Pipeline>::run_count() const
{
    return _run_count;
}

template<class Pipeline>
int PipelineWrapper<Pipeline>::exec()
{
    _max_count = 0;
    return Pipeline::exec();  
}

template<class Pipeline>
int PipelineWrapper<Pipeline>::exec(unsigned iterations)
{
    _max_count = iterations;
    return Pipeline::exec();  
}

template<class Pipeline>
void PipelineWrapper<Pipeline>::start(unsigned iterations)
{
    _max_count = iterations;
    Pipeline::start();  
}

template<class Pipeline>
void PipelineWrapper<Pipeline>::start()
{
    _max_count = 0;
    Pipeline::start();  
}

} // namespace test
} // namespace panda
} // namespace ska
