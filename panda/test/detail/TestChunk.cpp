/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace panda {
namespace test {

template <typename T>
TestChunk<T>::TestChunk(std::vector<T> const & data, unsigned index)
    : BaseT(data, index)
{
}

template <typename T>
TestChunk<T>::TestChunk(std::size_t size, unsigned index)
    : BaseT(typename BaseT::Container(size), index)
{
}

template <typename T>
TestChunk<T>::~TestChunk()
{
}

template<typename T>
TestChunkBase<T>::TestChunkBase(TestChunkBase<T>::Container const & data, unsigned index)
    : _data(data)
    , _index(index)
    , _missing_called(false)
{
}


template<typename T>
TestChunkBase<T>::TestChunkBase(unsigned index, std::initializer_list<T> list)
    : _data(list)
    , _index(index)
    , _missing_called(false)
{
}

template<typename T>
TestChunkBase<T>::~TestChunkBase()
{
}

template<typename T>
typename TestChunkBase<T>::Container const & TestChunkBase<T>::data() const
{
    return _data;
}

template<typename T>
void TestChunkBase<T>::data(typename TestChunkBase<T>::Container const& data)
{
    _data = data;
}

template<typename T>
unsigned TestChunkBase<T>::index() const
{
    return _index;
}

template<typename T>
std::size_t TestChunkBase<T>::size() const
{
    return _data.size();
}

template<typename T>
bool TestChunkBase<T>::missing_called() const
{
    return _missing_called;
}

template<typename T>
void TestChunkBase<T>::set_missing_called()
{
    _missing_called = true;
}

template<typename T>
bool operator==(TestChunkBase<T> const& chunk_A, TestChunkBase<T> const& chunk_B)
{
    return chunk_A.data() == chunk_B.data();
}

template<typename T>
bool operator!=(TestChunkBase<T> const& chunk_A, TestChunkBase<T> const& chunk_B)
{
    return !(chunk_A == chunk_B);
}

template<typename T>
std::ostream& operator<<(std::ostream& os, TestChunkBase<T> const& chunk)
{
    os << chunk.data();
    return os;
}

template<typename T>
typename TestChunkBase<T>::const_iterator TestChunkBase<T>::begin() const
{
    return _data.begin();
}

template<typename T>
typename TestChunkBase<T>::iterator TestChunkBase<T>::begin()
{
    return _data.begin();
}

template<typename T>
typename TestChunkBase<T>::const_iterator TestChunkBase<T>::end() const
{
    return _data.end();
}

template<typename T>
typename TestChunkBase<T>::iterator TestChunkBase<T>::end()
{
    return _data.end();
}

} // namespace test
} // namespace panda
} // namespace ska
