/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTHANDLERIMPL_H
#define SKA_PANDA_TESTHANDLERIMPL_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <chrono>

namespace ska {
namespace panda {
namespace test {

/**
 * @brief
 *   Implementation details for the TestHandler
 * @details
 *   By having details in a seperate object, we can allow out TestHandler to be copy constructible
 *   and so use mutex and conditional_variables
 */
class TestHandlerImpl
{
    public:
        TestHandlerImpl(bool blocking);
        ~TestHandlerImpl();

        /**
         * @brief the handler execution function. 
         * @details blocks until wait() is called if blocking is true
         */
        void operator()();

        /**
         * @brief wait (block) until the handler has completed
         */
        void wait();

        /**
         * @brief wait (block) until the handler has completed or timeout is reached
         * @returns true if completed succesfully, false if the timeout has been reached
         */
        bool wait(std::chrono::seconds const& timeout);

        /**
         * @brief cause the handler to terminate by throwing.
         */
        template<typename T> void create_exception( const T object );

        /**
         * @returns the thread id of the handler caller
         */
        std::thread::id const& caller_thread_id() const;

        /**
         * @returns true if this handler is set to block when called
         */
        bool blocking() const;

        /**
         * @returns true if this handler has been executed
         */
        bool executed() const;

        /**
         * @brief reset the object state ready for another call
         */
        void reset();

        /**
         * @brief if true will print debugging messages to std::out 
         */
        void set_verbose(bool);

        /**
         * @brief output msg tp std::cout if verbose mode is set
         */
        void msg(std::string const&) const;

    protected:
        template<typename T> void do_throw( const T object ) {
            throw object;
        }

    private:
        bool _called;
        bool _blocking;
        bool _verbose;
        bool _waiting;
        std::thread::id _caller_thread_id;

        std::mutex _mutex;
        std::condition_variable _wait;
        std::condition_variable _blocking_wait;

        std::function<void()> _object;
        bool _do_throw;
};

template<typename T> 
void TestHandlerImpl::create_exception( const T object ) {
    _do_throw = true;
    _object = std::bind( &TestHandlerImpl::do_throw<T>, this, object );
    wait();
}

} // namespace test
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTHANDLERIMPL_H 
