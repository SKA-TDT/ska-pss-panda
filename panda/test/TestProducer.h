/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTPRODUCER_H
#define SKA_PANDA_TESTPRODUCER_H

#include "panda/Producer.h"
#include <tuple>
#include <deque>
#include <mutex>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A DataManger pass through producer
 */
template<typename... DataTypes>
class TestProducer : public Producer<TestProducer<DataTypes...>, DataTypes...>
{
    public:
        TestProducer();

        template<typename IgnoredConfigType>
        TestProducer(IgnoredConfigType const&);

        ~TestProducer();

        /**
         * @brief the next process() will throw the passed exception
         */
        void set_process_return(bool rv);

        /**
         * @brief the next process() will throw the passed exception
         */
        template<typename ExceptionType>
        void set_throw(ExceptionType e);

        /**
         * @brief set data of the required type
         */
        template<typename DataType>
        void data(DataType);

        bool process();

        template<typename T>
        void process_type(std::deque<T>&);

        void stop();

    private:
        std::exception_ptr _exception_ptr;
        bool _rv;
        std::tuple<std::deque<typename panda::DataType<DataTypes>::type>...> _data;
        std::mutex _mutex;
};

/**
 * @brief send a TestChunk to the stream as data to be served
 */
template<typename... Types, typename ChunkType>
TestProducer<Types...>& operator<<(TestProducer<Types...>& p, ChunkType const & type);

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestProducer.cpp"

#endif // SKA_PANDA_TESTPRODUCER_H
