/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTCONNECTIONERRORPOLICY_H
#define SKA_PANDA_TESTCONNECTIONERRORPOLICY_H

#include "panda/Error.h"
#include "panda/ErrorPolicyConcept.h"

namespace ska {
namespace panda {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    Policy that allows monitoring ans setting of the states
 *
 * @details
 * 
 */
class TestConnectionErrorPolicy
{
    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<TestConnectionErrorPolicy>));

    public:
        TestConnectionErrorPolicy();
        ~TestConnectionErrorPolicy();

        // ----------------------
        // -- Required Methods --
        // ----------------------
        bool connection_error(Error const& error);
        bool error(Error const& error);
        bool send_error(Error const& error);
        bool try_connect();
        void connected();

        /**
         * @brief return the number of times the connection_error method has been called since the last call.
         */
        unsigned connection_errors();

        /**
         * @brief return the number of times the error method has been called since the last call.
         */
        unsigned transmission_errors();

    public:
        Error _last_connection_error;
        Error _last_error;

        bool _connected;
        bool _try_connect;
        bool _connection_error_return;
        bool _error_return;
        bool _error;

        unsigned _connection_error_count;
        unsigned _error_count;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTCONNECTIONERRORPOLICY_H
