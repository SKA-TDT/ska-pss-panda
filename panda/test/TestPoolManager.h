/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTPOOLMANAGER_H
#define SKA_PANDA_TESTPOOLMANAGER_H
#include "panda/test/TestHandler.h"
#include "panda/test/TestResourcePool.h"

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief A mock PoolManager class
 *
 * @details
 *
 */

template<typename... ArchitecturesT>
class TestPoolManager : public TestHandler
{
        template<typename Arch>
        struct Resource : panda::Resource {
            void abort() override {};
        };
    public:
        static const unsigned MaxPriority = 2;
        typedef std::tuple<ArchitecturesT...> Architectures;

    public:
        TestPoolManager() : TestHandler(false) {}

        template<typename Arch>
        void add_resource()
        {
            _pool.template add_resource<Arch>(std::make_shared<Resource<Arch>>());
        }

        template<typename Arch>
        std::shared_ptr<panda::PoolResource<Arch>> get_resource()
        {
            return _pool.template free_resources<Arch>()[0];
        }

        template<typename TraitsType, typename... DataTypes>
        std::shared_ptr<panda::ResourceJob> submit(TraitsType&& traits, DataTypes&&... data) {
            (*this)();      // mark we have been called
            return _pool.template submit<0>(std::forward<TraitsType>(traits), std::forward<DataTypes>(data)...); // ensure the task meets the requirements for submission to a pool
        }

    private:
        TestResourcePool<ArchitecturesT...> _pool;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTPOOLMANAGER_H
