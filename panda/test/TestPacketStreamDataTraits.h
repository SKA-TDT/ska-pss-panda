/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTPACKETSTREAMDATATRAITS_H
#define SKA_PANDA_TESTPACKETSTREAMDATATRAITS_H
#include "panda/test/TestChunk.h"
#include "panda/test/TestPacket.h"
#include "panda/concepts/ChunkerContextDataTraitsConcept.h"
#include <condition_variable>
#include <memory>
#include <deque>
#include <vector>
#include <mutex>

namespace ska {
namespace panda {
namespace test {

/**
 * @brief A test class matching the PacketStream DataTraits interface requirements
 */
template<typename LimitType, typename PacketSizeType=unsigned>
class TestPacketStreamDataTraits
{
    BOOST_CONCEPT_ASSERT((ChunkerContextDataTraitsConcept<TestPacketStreamDataTraits<LimitType>>));

    public:
        typedef TestChunk_A DataType;
        typedef TestPacketInspector<2> PacketInspector;

    public:
        TestPacketStreamDataTraits(double packets_per_chunk);
        TestPacketStreamDataTraits(TestPacketStreamDataTraits const&);
        ~TestPacketStreamDataTraits();

        // setup interface
        void chunk_size(std::size_t size);
        void block_in_missing_packet(bool block_if_true);
        void block_in_deserialise_packet(bool block_if_true);

        // test info interface
        bool deserialise_called() const;
        bool process_missing_slice_called() const;

        // required concept interface
        static LimitType max_sequence_number();
        std::size_t chunk_size(DataType const&);
        static LimitType sequence_number(PacketInspector const& p);
        bool align_packet(PacketInspector const&);

        template<typename ContextType>
        void deserialise_packet(ContextType& ctx, PacketInspector const& p);

        template<typename ContextType>
        void process_missing_slice(ContextType& ctx);

        template<typename ContextType>
        void resize_chunk(ContextType& ctx);

        static PacketSizeType packet_size(); // the data payload of each packet

        void packet_stats(uint64_t missing, uint64_t expected);

        /**
         * @brief returns the parameters that were passed down to resize_chunk() call
         */
        std::vector<std::pair<DataType*, PacketSizeType>> const& resize_params() const;

        void wait_process_missing_slice_called() const;
        void wait_resize_called() const;

        /// return the chunk that was passed to the process_missing_slice
        //  each call will provide a different chunks in the order that they were received
        DataType* get_missing_chunk();

    private:
        struct SharedMembers {
            SharedMembers()
                : _block_ds(false)
                , _block_mp(false)
            {}
            ~SharedMembers()
            {
                _block_ds = false;
                _block_ds_cv.notify_all();
                _block_mp = false;
                _block_mp_cv.notify_all();
                _resize_cv.notify_all();
                _cv.notify_all();
            }

            std::mutex _mutex;
            std::mutex _resize_mutex;
            std::condition_variable _resize_cv;
            std::condition_variable _cv;
            std::mutex _block_ds_mutex;
            std::condition_variable _block_ds_cv;
            bool _block_ds;
            std::mutex _block_mp_mutex;
            std::condition_variable _block_mp_cv;
            bool _block_mp;
        };

    private:
        std::size_t _chunk_size;
        std::unique_ptr<std::deque<DataType*>> _deserialise_chunks;
        std::unique_ptr<std::deque<DataType*>> _missing_slice_chunks;
        std::unique_ptr<std::vector<std::pair<DataType*, PacketSizeType>>> _resize_passed_params;
        mutable std::shared_ptr<SharedMembers> _shared;
};

} // namespace test
} // namespace panda
} // namespace ska
#include "detail/TestPacketStreamDataTraits.cpp"

#endif // SKA_PANDA_TESTPACKETSTREAMDATATRAITS_H
