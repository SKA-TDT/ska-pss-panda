/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTHANDLER_H
#define SKA_PANDA_TESTHANDLER_H

#include <thread>
#include <memory>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * TODOdoc
 */
class TestHandlerImpl;

/**
 * @brief
 *    An async handler that allows for testing
 * @details
 *    inspection of attibutes of the passed type
 */
class TestHandler
{
    public:
        /**
         * @brief set a blocking or non-blocking handler
         * @details note that wait() will always block until the handler has been executed
         *          whatever this setting. This setting controls the blocking of the handler execution
         *          thread.
         */
        TestHandler(bool blocking=true);
        ~TestHandler();

        /**
         * @brief if true will print debugging messages to std::out
         */
        void set_verbose(bool);

        /**
         * @brief the handler execution function.
         * @blocks until wait() is called if blocking is true
         */
        void operator()();

        /**
         * @brief wait (block) until the handler has completed
         */
        void wait();
        bool wait(std::chrono::seconds const& timeout);

        /**
         * @returns the thread id of the handler caller
         */
        std::thread::id const& caller_thread_id() const;

        /**
         * @brief reset to an uncalled state
         */
        void reset();

        /**
         * @returns true if this handler has been executed, otherwise false
         */
        bool executed() const;

        /**
         * @brief cause the handler to throw the provided execption when called (after block is released)
         */
        template<typename T>
        void create_exception( const T object );

    private:
        std::shared_ptr<TestHandlerImpl> _pimpl;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestHandler.cpp"

#endif // SKA_PANDA_TESTHANDLER_H
