/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_GPUNVIDIA_H
#define SKA_PANDA_NVIDIA_GPUNVIDIA_H


#include "panda/Resource.h"
#include <vector>

#ifdef ENABLE_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif

namespace ska {
namespace panda {
namespace nvidia {

/**
 * @brief
 *    An NVidia GPU card resource
 *
 */
class GpuNvidia : public Resource
{
    public:
        GpuNvidia(unsigned int device_id);
        ~GpuNvidia();

        template<typename TaskType>
        void run( TaskType& job );

        void abort() {}; //TODO

        unsigned device_id() const;

        std::size_t device_memory() const;
#ifdef ENABLE_CUDA
        cudaDeviceProp const& device_properties() const;
#endif
    private:
#ifdef ENABLE_CUDA
        cudaDeviceProp _device_prop;
#endif
        unsigned int _device_id;
};

} // namespace nvidia
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_NVIDIA_GPUNVIDIA_H 
