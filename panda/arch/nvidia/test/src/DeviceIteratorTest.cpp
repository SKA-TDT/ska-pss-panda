/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceIteratorTest.h"
#include "panda/arch/nvidia/DeviceIterator.h"


namespace ska {
namespace panda {
namespace nvidia {
namespace test {


DeviceIteratorTest::DeviceIteratorTest()
    : BaseT()
{
}

DeviceIteratorTest::~DeviceIteratorTest()
{
}

void DeviceIteratorTest::SetUp()
{
}

void DeviceIteratorTest::TearDown()
{
}

TEST_F(DeviceIteratorTest, test_operator_add)
{
    int val[10];
    panda::DeviceIterator<Cuda, int*> it(val);
    auto it_2 = it + 9;
    static_assert(std::is_same<panda::DeviceIterator<Cuda, int*>, decltype(it_2)>::value, "unexpected type returned from operator+");
    ASSERT_EQ(&*it_2, &val[9]);
}

} // namespace test
} // namespace nvidia
} // namespace panda
} // namespace ska
