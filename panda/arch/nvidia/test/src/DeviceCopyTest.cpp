/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceCopyTest.h"
#include "panda/test/CopyTester.h"
#include "panda/arch/nvidia/DeviceCopy.h"
#include "panda/arch/nvidia/test/TestDevice.h"
#include <vector>
#include <list>

namespace ska {
namespace panda {
namespace nvidia {
namespace test {


DeviceCopyTest::DeviceCopyTest()
    : ::testing::Test()
{
}

DeviceCopyTest::~DeviceCopyTest()
{
}

void DeviceCopyTest::SetUp()
{
}

void DeviceCopyTest::TearDown()
{
}

TEST_F(DeviceCopyTest, test_copy_to_CudaDevicePointer_ramdom_access)
{
    std::vector<double> const input = { 0, 1, 2 , 3, 4 };
    TestDevice system; 
    for(auto & device : system.devices()) {
        device->run([&](typename TestDevice::DeviceRefType&) 
        {
            panda::nvidia::CudaDevicePointer<double> cuda_ptr(input.size());
            panda::copy(input.begin(), input.end(), cuda_ptr);
            std::vector<double> returned_data(input.size());
            cuda_ptr.read(returned_data.begin());
            for(unsigned i=0; i<input.size(); ++i) {
                ASSERT_EQ(input[i], returned_data[i]);
            }
        });
    }
}

TEST_F(DeviceCopyTest, test_copy_to_CudaDevicePointer_non_ramdom_access)
{
    std::list<double> const input = { 0, 1, 2 , 3, 4 };
    TestDevice system; 
    for(auto & device : system.devices()) {
        device->run([&](typename TestDevice::DeviceRefType&) 
        {
            panda::nvidia::CudaDevicePointer<double> cuda_ptr(input.size());
            panda::copy(input.begin(), input.end(), cuda_ptr);
            // copy back from the device to a new vector
            std::vector<double> returned_data(input.size());
            cuda_ptr.read(returned_data.begin());
            auto list_it = input.begin();
            auto vec_it = returned_data.begin();
            while(list_it != input.end()) {
                ASSERT_EQ(*list_it, *vec_it);
                ++list_it;
                ++vec_it;
            }
        });
    }
}

TEST_F(DeviceCopyTest, test_device_copy_auto_alloc_random_access)
{
    std::vector<double> const input = { 0, 1, 2 , 3, 4 };
    TestDevice system; 
    for(auto & device : system.devices()) {
        device->run([&](typename TestDevice::DeviceRefType&) 
        {
            auto cuda_ptr = panda::nvidia::device_copy(input.begin(), input.end());
            std::vector<double> returned_data(input.size());
            cuda_ptr.read(returned_data.begin());
            for(unsigned i=0; i<input.size(); ++i) {
                ASSERT_EQ(input[i], returned_data[i]);
            }
        });
    }
}

TEST_F(DeviceCopyTest, test_device_copy_auto_alloc_non_random_access)
{
    std::list<double> const input = { 0, 1, 2 , 3, 4 };
    TestDevice system; 
    for(auto & device : system.devices()) {
        device->run([&](typename TestDevice::DeviceRefType&) 
        {
            auto cuda_ptr = panda::nvidia::device_copy(input.begin(), input.end());
            std::vector<double> returned_data(input.size());
            cuda_ptr.read(returned_data.begin());
            auto list_it = input.begin();
            auto vec_it = returned_data.begin();
            while(list_it != input.end()) {
                ASSERT_EQ(*list_it, *vec_it);
                ++list_it;
                ++vec_it;
            }
        });
    }
}
} // namespace test
} // namespace nvidia

namespace test {
    // standard panda::copy test suite for Cuda Device Pointers
    struct CudaCopyTraits {
        typedef nvidia::Cuda Arch;
        template<typename T>
        static nvidia::CudaDevicePointer<T> contiguous_device_memory(std::size_t size) {
            return nvidia::CudaDevicePointer<T>(size);
        }
    };
    INSTANTIATE_TYPED_TEST_CASE_P(Cuda, CopyTester, CudaCopyTraits);

} // namespace test
} // namespace panda
} // namespace ska
