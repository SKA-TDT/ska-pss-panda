/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceAllocator.h"
#ifdef ENABLE_CUDA
#include <cuda_runtime.h>
#endif //ENABLE_CUDA
#include <exception>

namespace ska {
namespace panda {

template<typename T>
DeviceAllocator<T, nvidia::Cuda>::DeviceAllocator(Device<nvidia::Cuda> const&)
{
}

template<typename T>
template<typename U>
DeviceAllocator<T, nvidia::Cuda>::DeviceAllocator(DeviceAllocator<U, nvidia::Cuda> const&)
{
}

template<typename T>
T* DeviceAllocator<T, nvidia::Cuda>::allocate(std::size_t n)
{
#ifdef ENABLE_CUDA
    T* d_ptr;
    if(cudaMalloc((void**)&d_ptr, n*sizeof(T)) != cudaSuccess)
        throw std::bad_alloc();
    return d_ptr;
#else
    return nullptr;
#endif // ENABLE_CUDA
}

template<typename T>
void DeviceAllocator<T, nvidia::Cuda>::deallocate( T* d_ptr, std::size_t)
{
#ifdef ENABLE_CUDA
    cudaFree(d_ptr);
#else
    (void*) d_ptr;
#endif // ENABLE_CUDA
}

} // namespace panda
} // namespace ska
