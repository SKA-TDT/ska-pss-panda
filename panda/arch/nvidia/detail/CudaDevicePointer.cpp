/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/nvidia/CudaDevicePointer.h"
#include "panda/Error.h"
#ifdef ENABLE_CUDA
#include <cuda_runtime.h>
#endif //ENABLE_CUDA
#include <iterator>
#include <type_traits>
#include <cassert>


namespace ska {
namespace panda {
namespace nvidia {

template<typename DataType, class Allocator>
CudaDevicePointer<DataType, Allocator>::CudaDevicePointer(std::size_t n)
    : _d_ptr(_allocator.allocate(n))
    , _size(n)
{
}

template<typename DataType, class Allocator>
CudaDevicePointer<DataType, Allocator>::CudaDevicePointer(std::size_t n, Allocator const& allocator)
    : _allocator(allocator)
    , _d_ptr(_allocator.allocate(n))
    , _size(n)
{
}

template<typename DataType, class Allocator>
CudaDevicePointer<DataType, Allocator>::~CudaDevicePointer()
{
    _allocator.deallocate(_d_ptr, _size);
}

template<typename DataType, class Allocator>
CudaDevicePointer<DataType, Allocator>::CudaDevicePointer(CudaDevicePointer&& c)
{
    _d_ptr = c._d_ptr;
    c._d_ptr=nullptr;
}

template<typename DataType, class Allocator>
DataType* CudaDevicePointer<DataType, Allocator>::operator()() { return _d_ptr; }

template<typename DataType, class Allocator>
panda::DeviceIterator<Cuda, DataType*> CudaDevicePointer<DataType, Allocator>::begin()
{
    return static_cast<panda::DeviceIterator<Cuda, DataType*>>(_d_ptr);
}

template<typename DataType, class Allocator>
panda::DeviceIterator<Cuda, DataType*> const CudaDevicePointer<DataType, Allocator>::end() const
{
    return static_cast<panda::DeviceIterator<Cuda, DataType*> const>(_d_ptr + _size);
    //return _d_ptr + _size;
}

template<typename DataType, class Allocator>
std::size_t CudaDevicePointer<DataType, Allocator>::size() const
{
    return _size;
}

template<typename DataType, class Allocator>
template<typename IteratorType>
void CudaDevicePointer<DataType, Allocator>::read(IteratorType&& it) const
{
    typedef std::iterator_traits<typename std::remove_reference<IteratorType>::type> IteratorTraits;
    static_assert(std::is_same<std::random_access_iterator_tag, typename IteratorTraits::iterator_category>::value, "Write directly to non-contiguous iterator type not supported (as this would lead to inefficient coding)");
#ifdef ENABLE_CUDA
    cudaMemcpy((void *)&*it, (const void*)_d_ptr, _size * sizeof(DataType), cudaMemcpyDeviceToHost);
#endif
}

template<typename DataType, class Allocator>
template<typename IteratorType, typename EndIteratorType>
void CudaDevicePointer<DataType, Allocator>::write(IteratorType&& begin, EndIteratorType&& end)
{
    typedef std::iterator_traits<typename std::remove_reference<IteratorType>::type> IteratorTraits;
    static_assert(std::is_same<std::random_access_iterator_tag, typename IteratorTraits::iterator_category>::value, "Read directly from non-contiguous iterator type not supported (use a preallocated buffer instead)");
#ifdef ENABLE_CUDA
    std::size_t size = (std::size_t)(end - begin);
    assert(size <= _size);
    cudaMemcpy((void*)_d_ptr,(const void*)&(begin[0]), size * sizeof(typename IteratorTraits::value_type), cudaMemcpyHostToDevice);
#endif
}

} // namespace nvidia
} // namespace panda
} // namespace ska
