/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/nvidia/GpuNvidia.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <utility>


namespace ska {
namespace panda {

template<typename TaskType>
void PoolResource<nvidia::Cuda>::run( TaskType&& job )
{
#ifndef ENABLE_CUDA
    job(*this);
#else // ENABLE_CUDA
    cudaSetDevice( device_id() );
    job(*this);

    // check for errors
    cudaDeviceSynchronize();
    if( cudaPeekAtLastError() ) {
        PANDA_LOG_ERROR << cudaGetErrorString( cudaPeekAtLastError() );
    }
#endif // ENABLE_CUDA
}

/// specialised operator>= for comparing resources and capabilities
template<typename DeviceCapability>
bool operator>=( PoolResource<nvidia::Cuda> const& r, DeviceCapability const& c ) {
#ifdef ENABLE_CUDA
    if( typename DeviceCapability::CapabilityVersion(r.device_properties().major, r.device_properties().minor) >= c.compute_capability() &&
        r.device_properties().totalGlobalMem >= c.total_memory ) {
        return true;
    } else {
        return false;
    }
#else
    // Avoid compiler warning
    (void)r;
    (void)c;
    return false;
#endif
}

/// specialised operator<= for comparing resources and capabilities
template<typename DeviceCapability>
bool operator<=( PoolResource<nvidia::Cuda> const& r, DeviceCapability const& c) {
#ifdef ENABLE_CUDA
    if( c.compute_capability() >= typename DeviceCapability::CapabilityVersion(r.device_properties().major, r.device_properties().minor) ) {
        return true;
    } else {
        return false;
    }
#else
    // Avoid compiler warning
    (void)r;
    (void)c;
    return false;
#endif
}

template<int Major, int Minor, size_t TotalMemory>
bool ResourceSupports<nvidia::Cuda, nvidia::DeviceCapability<Major, Minor, TotalMemory>>::compatible( PoolResource<nvidia::Cuda> const& ) {
    return true;
}

template<int Major, int Minor, size_t TotalMemory>
bool ResourceSupports<nvidia::DeviceCapability<Major, Minor, TotalMemory>, nvidia::Cuda>::compatible( PoolResource<nvidia::Cuda> const& resource ) {
#ifdef ENABLE_CUDA
    using CudaCapability = nvidia::DeviceCapability<Major, Minor, TotalMemory>;
    if(typename CudaCapability::CapabilityVersion(resource.device_properties().major, resource.device_properties().minor) >= CudaCapability::compute_capability()
       && (CudaCapability::total_memory <= resource.device_properties().totalGlobalMem)) {
        return true;
    } else {
        return false;
    }
#else
    // Avoid compiler warning
    (void)resource;
    return false;
#endif
}


namespace nvidia {
template<typename ResourcePoolType>
std::size_t min_gpu_memory(ResourcePoolType& pool)
{
#ifdef ENABLE_CUDA
    if(pool.template free_resources_count<nvidia::Cuda>() == 0)
    {
        return 0;
    }
    else {
        return (*std::min_element(pool.template free_resources<nvidia::Cuda>().begin(), pool.template free_resources<nvidia::Cuda>().end(),
                                   [](std::shared_ptr<panda::PoolResource<nvidia::Cuda>> const& a, std::shared_ptr<panda::PoolResource<nvidia::Cuda>> const& b) {
                                        return a->device_properties().totalGlobalMem < b->device_properties().totalGlobalMem;
                                   }))->device_properties().totalGlobalMem;
    }
#else //ENABLE_CUDA
    (void) pool;
    return 0;
#endif //ENABLE_CUDA
}
} // namespace nvidia

} // namespace panda
} // namespace ska
