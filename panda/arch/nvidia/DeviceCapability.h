/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICECAPABILITY_H
#define SKA_PANDA_DEVICECAPABILITY_H
#include "panda/VersionSpecification.h"
#include "panda/arch/nvidia/Architecture.h"


namespace ska {
namespace panda {
namespace nvidia {

/**
 * @brief
 *    Represents the capabiliites of an nvidia device
 * @details
 *
 */
template <int Major, int Minor, std::size_t TotalMemory>
class DeviceCapability : public Cuda
{
    public:
        typedef VersionSpecification<int, int> CapabilityVersion;

        static CapabilityVersion compute_capability();

    public:
        static constexpr int major_version = Major;
        static constexpr int minor_version = Minor;
        static constexpr std::size_t total_memory = TotalMemory;
};

/// Non-member operator>
template<int MajorT1, int MinorT1, std::size_t MemT1,
         int MajorT2, int MinorT2, std::size_t MemT2>
static constexpr bool operator>(DeviceCapability<MajorT1, MinorT1, MemT1> const& c1,
                                DeviceCapability<MajorT2, MinorT2, MemT2> const& c2);

/// Non-member operator>=
template<int MajorT1, int MinorT1, std::size_t MemT1,
         int MajorT2, int MinorT2, std::size_t MemT2>
static constexpr bool operator>=(DeviceCapability<MajorT1, MinorT1, MemT1> const& c1,
                                 DeviceCapability<MajorT2, MinorT2, MemT2> const& c2);

// some predefined cards
static const unsigned mega(1000000);
static const std::size_t giga(1000000000);
typedef DeviceCapability<2,0,3*giga> TeslaC2070;
typedef DeviceCapability<3,5,6*giga> TeslaK40;

} // namespace nvidia
} // namespace panda
} // namespace ska
#include "panda/arch/nvidia/detail/DeviceCapability.cpp"

#endif // SKA_PANDA_DEVICECAPABILITY_H
