/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTUREUTILITIES_H
#define SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTUREUTILITIES_H

#include "panda/ArchitectureUtilities.h"
#include "panda/arch/nvidia/DeviceCapability.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/mpl/equal_to.hpp>
#include <boost/mpl/greater.hpp>
#include <boost/mpl/greater_equal.hpp>
#include <boost/mpl/int.hpp>
#pragma GCC diagnostic pop

namespace {
/**
 * @brief Is Actual capability compatible with Required
 */
template <class Required, class Actual>
struct _IsCompatibleHelper {
    typedef boost::mpl::int_<Required::major_version> _maj_req;
    typedef boost::mpl::int_<Required::minor_version> _min_req;
    typedef boost::mpl::int_<Actual::major_version> _maj_act;
    typedef boost::mpl::int_<Actual::minor_version> _min_act;

    static constexpr bool _vers_ok = (boost::mpl::greater<_maj_act, _maj_req>::value ||
                                      (boost::mpl::equal_to<_maj_act, _maj_req>::value &&
                                       boost::mpl::greater_equal<_min_act, _min_req>::value));
    // This duplicates the operators in DeviceCapability
    static constexpr bool value = (Actual::total_memory >= Required::total_memory && _vers_ok);
};
}

namespace ska {
namespace panda {

/**
 * Specialisations for compare Cuda architecture compatiblity
 *
 * Any specific DeviceCapability will match against generical Cuda. When comparing
 * specific capabilities IsCompatible will return true iff
 *   - (other.major > requirement.major && other.memory >= requirement.memory) or
 *   - (other.major == requirement.major && other.minor >= requirement.minor && other.memory >= requirement.memory)
 */

/// @brief Specialisation for any DeviceCapability automatically matching cuda
template <int Major, int Minor, std::size_t TotalMemory>
struct IsCompatible<nvidia::Cuda, nvidia::DeviceCapability<Major, Minor, TotalMemory>>
    : std::true_type {};

/// @brief Specialisation for two DeviceCapability checks.
/// @details
/// Enabled only if they are not the same to disambiguate the template with the general version
/// matching the same types
template <int MajorRequired, int MinorRequired, std::size_t TotalMemoryRequired,
          int MajorActual, int MinorActual, std::size_t TotalMemoryActual>
struct IsCompatible<nvidia::DeviceCapability<MajorRequired, MinorRequired, TotalMemoryRequired>,
                    nvidia::DeviceCapability<MajorActual, MinorActual, TotalMemoryActual>,
                    typename std::enable_if<!std::is_same<nvidia::DeviceCapability<MajorRequired, MinorRequired, TotalMemoryRequired>,
                                                          nvidia::DeviceCapability<MajorActual, MinorActual, TotalMemoryActual>>::value>::type>
  : std::integral_constant<bool,
                           _IsCompatibleHelper<nvidia::DeviceCapability<MajorRequired, MinorRequired, TotalMemoryRequired>,
                                               nvidia::DeviceCapability<MajorActual, MinorActual, TotalMemoryActual>>::value> {};


} // namespace panda
} // namespace ska

#endif // SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTUREUTILITIES_H
