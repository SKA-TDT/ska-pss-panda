/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_NVIDIA_POOLRESOURCE_H
#define SKA_PANDA_NVIDIA_NVIDIA_POOLRESOURCE_H

#include "panda/arch/nvidia/GpuNvidia.h"
#include "panda/arch/nvidia/DeviceCapability.h"
#include "panda/DeviceConfig.h"
#include "panda/Resource.h"
#include "panda/ResourcePool.h"
#include "panda/ResourceUtilities.h"
#include <cstddef>


namespace ska {
namespace panda {

/**
 * @brief
 *    NVidia card definition conforming to ResourcePool requirements
 */
template<>
class PoolResource<nvidia::Cuda> : public nvidia::GpuNvidia
{
    public:
        PoolResource(unsigned int device_id);
        ~PoolResource();


        template<typename TaskType>
        void run(TaskType&& job);

        void configure(DeviceConfig<nvidia::Cuda> const&);
};

/// specialised operator<= for comparing resources and capabilities
template<typename DeviceCapability>
bool operator>=(PoolResource<nvidia::Cuda> const&, DeviceCapability const&);

/// specialised operator<= for comparing resources and capabilities. Note that this
/// ignores the memory and only checks the version requirements
template<typename DeviceCapability>
bool operator<=(PoolResource<nvidia::Cuda> const&, DeviceCapability const&);

/**
 * @brief
 *     ResourceSupports specialisation. If required capability is Cuda then any version matches
 */
template<int Major, int Minor, size_t TotalMemory>
struct ResourceSupports<nvidia::Cuda, nvidia::DeviceCapability<Major, Minor, TotalMemory>> {
    static bool compatible( PoolResource<nvidia::Cuda> const& );
};

/**
 * @brief
 *     ResourceSupports specialisation for a DeviceCapability requirement
 * @details
 *     Cuda GPU device is considered sufficeient if the compute capabilities and the memory are sufficient
 */
template<int Major, int Minor, size_t TotalMemory>
struct ResourceSupports<nvidia::DeviceCapability<Major, Minor, TotalMemory>, nvidia::Cuda> {
    static bool compatible( PoolResource<nvidia::Cuda> const& );
};

namespace nvidia {
/// find the card with the smallest memory available in a given pool
template<typename ResourcePoolType>
std::size_t min_gpu_memory(ResourcePoolType const&&);
} // namespace nvidia

} // namespace panda
} // namespace ska
#include "panda/arch/nvidia/detail/PoolResource.cpp"

#endif // SKA_PANDA_NVIDIA_NVIDIA_POOLRESOURCE_H
