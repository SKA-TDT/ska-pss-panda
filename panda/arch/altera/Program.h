/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_PROGRAM_H
#define SKA_PANDA_ALTERA_PROGRAM_H

#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/Aocx.h"
#ifdef ENABLE_OPENCL


namespace ska {
namespace panda {
namespace altera {

/**
 * @brief: manages OpenCL cl_program
 */
class Program
{
    public:
        Program(Aocx const& aocx, Fpga const& device);
        Program(Program const&) = delete;
        ~Program();

        cl_program const& program() const;

        Fpga const& device() const;

    private:
        cl_program _program;
        Fpga const& _device;

};

} //namespace altera
} // namespace panda
} // namespace ska

#endif //ifdef ENABLE_OPENCL
#endif // SKA_PANDA_ALTERA_PROGRAM_H
