/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_DEVICEMEMORY_H
#define SKA_PANDA_NVIDIA_DEVICEMEMORY_H

#include "panda/arch/altera/Architecture.h"
#include "panda/arch/altera/DeviceAllocator.h"
#include "panda/DeviceMemory.h"
#include "panda/arch/altera/Fpga.h"

namespace ska {
namespace panda {

template<typename DataType, typename Allocator>
class DeviceMemory<altera::OpenCl, DataType, Allocator>
{
        static_assert(!std::is_same<typename std::decay<Allocator>::type, std::allocator<DataType>>::value, "std::allocator is not suitable for FPGA memory allocation)");

    public:
        typedef altera::OpenCl Architecture;
        typedef typename altera::DevicePointer<DataType>::ConstIterator ConstIterator;
        typedef typename altera::DevicePointer<DataType>::Iterator Iterator;

    public:
        template<typename... AllocatorArgs>
        DeviceMemory(std::size_t size, AllocatorArgs&&... allocator_constructor_args);
        DeviceMemory(DeviceMemory&& copy_to_move);
        ~DeviceMemory();

        /**
         * @brief the size of the allocated memory
         */
        std::size_t const& size() const;

        /**
         * @brief forced deallocation and reallocation of memory of the sepcified size
         */
        void reset(std::size_t size);

        /**
         * @brief return a pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        Iterator begin();

        /**
         * @brief return a pointer to the last element + 1 (on the device)
         * @details do not attempt to access this memory on the host
         */
        Iterator end();

        /**
         * @brief return a pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        ConstIterator begin() const;

        /**
         * @brief return a pointer to the last element + 1 (on the device)
         * @details do not attempt to access this memory on the host
         */
        ConstIterator end() const;

        /**
         * @brief return a const pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        ConstIterator cbegin() const;

        /**
         * @brief return a const pointer to the last element + 1 on the device
         */
        ConstIterator cend() const;

    private:
        Allocator _allocator;
        altera::DevicePointer<DataType>* _d_ptr;
};

} // namespace panda
} // namespace ska

#include "detail/DeviceMemory.cpp"
#endif // SKA_PANDA_NVIDIA_DEVICEMEMORY_H
