/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/Aocx.h"
#include "panda/Error.h"
#include <fstream>


namespace ska {
namespace panda {
namespace altera {

Aocx::Aocx(boost::filesystem::path const& kernel_filename)
{
    // opens binary image file(.aocx)
    std::ifstream ifs;
    ifs.open(kernel_filename.c_str(), std::ifstream::binary);
    if (!ifs) {
        panda::Error e("Error opening file: ");
        e << kernel_filename;
        throw e;
    }

    //saves aocx file in a string
    _binary_str = std::string(std::istreambuf_iterator<char>(ifs), (std::istreambuf_iterator<char>()));
    ifs.close();
    if (!_binary_str.data()) {
        panda::Error e("failed to read binary file: ");
        e << kernel_filename;
        throw e;
    }

    //size of file used to build program
    if (!_binary_str.size()) {
        panda::Error e("Error getting size of file: ");
        e << kernel_filename;
        throw e;
    }

}
Aocx::~Aocx()
{
}

std::size_t Aocx::binary_size() const
{
    return _binary_str.size();
}

std::string const& Aocx::binary_string() const
{
    return _binary_str;
}

} // namespace altera
} // namespace panda
} // namespace ska
