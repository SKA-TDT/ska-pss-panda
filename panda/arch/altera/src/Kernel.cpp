/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/Kernel.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <fstream>
#include <vector>

namespace ska {
namespace panda {
namespace altera {


Kernel::Kernel(std::string const& kernel_name, Program const& program)
    : _owner(true)
{
    cl_int status;
    _kernel_name=kernel_name;

    _kernel = clCreateKernel(program.program(), kernel_name.c_str(), &status);
    if (status != CL_SUCCESS) {
        panda::Error e("failed to build altera opencl kernel: ");
        e << kernel_name;
        throw e;
    }
    clRetainKernel(_kernel);
}

Kernel::Kernel(Kernel&& other)
    : _kernel(std::move(other._kernel))
    , _owner(true)
{
    other._owner = false;
}

Kernel& Kernel::operator=(Kernel&& other)
{
    if(_owner) {
        clReleaseKernel(_kernel);
    }

    _kernel = std::move(other._kernel);
    _owner = true;
    other._owner = false;
    return *this;
}

Kernel::~Kernel()
{
    //clFinish completes kerenl execution
    if(_owner) {
        clReleaseKernel(_kernel);
    }
}

} // namespace altera
} // namespace panda
} // namespace ska
