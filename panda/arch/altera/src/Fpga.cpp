/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/CommandQueue.h"
#include "panda/arch/altera/OpenClError.h"
#include "panda/Error.h"

namespace ska {
namespace panda {
namespace altera {

Fpga::Fpga(std::shared_ptr<OpenClPlatform> const& shutdown, cl_device_id device_id)
    : _platform(shutdown)
    , _device_id(device_id)
{
    for(unsigned int i=0; i < sizeof(_device_name); ++i)
    {
        _device_name[i] = '\0';
    }

    cl_int status;
    _cl_context = clCreateContext(NULL, 1, &_device_id, NULL, NULL, &status);
    if(status != CL_SUCCESS)
    {
        panda::Error e("ALTERA: unable to create CL context: ");
        e << error_msg(status);
        throw e;
    }

    clRetainContext(_cl_context);

    _queue.reset(new CommandQueue(*this));
}

Fpga::~Fpga()
{
    _queue.reset(nullptr);
    clReleaseContext(_cl_context);
}

cl_device_id const& Fpga::device_id() const
{
    return _device_id;
}

const char* Fpga::device_name() const
{
    if(_device_name[0] == '\0')
    {
        // we pass the sizeof(_device_name) -1 to ensure char array will always be terminated with \0 whatever this fn does
        clGetDeviceInfo(const_cast<cl_device_id>(_device_id), CL_DEVICE_NAME, sizeof(_device_name) -1 , _device_name, NULL);

    }
    return _device_name;
}

cl_ulong const& Fpga::device_memory()
{
    clGetDeviceInfo(_device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(_max_memory), &_max_memory, NULL);
    return _max_memory;
}

cl_context const& Fpga::context() const
{
    return _cl_context;
}

CommandQueue& Fpga::default_queue() const
{
    return *_queue;
}

const char *Fpga::error_msg(cl_int error)
{
    return OpenClError::error_msg(error);
}

} // namespace altera
} // namespace panda
} // namespace ska
