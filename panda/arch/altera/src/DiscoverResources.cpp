/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/DiscoverResources.h"
#include "panda/arch/altera/PoolResource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#ifdef ENABLE_OPENCL
#include "panda/arch/altera/OpenClPlatform.h"
#endif // ENABLE_OPENCL


namespace ska {
namespace panda {

std::vector<std::shared_ptr<PoolResource<altera::OpenCl>>> DiscoverResources<altera::OpenCl>::operator()()
{
    std::vector<std::shared_ptr<PoolResource<altera::OpenCl>>> res;

#ifndef ENABLE_OPENCL
    PANDA_LOG_WARN << "Not compiled with Altera support";
#else
    // Search all platforms for the first platform whose name contains the search string (case-insensitive)

    cl_int status;
    cl_uint num_platforms;
    std::string search = "Intel";

    std::transform(search.begin(), search.end(), search.begin(), tolower);

    // Get number of platforms
    status = clGetPlatformIDs(0, NULL, &num_platforms);
    if (status != CL_SUCCESS) {
        PANDA_LOG_ERROR << "Query for number of platforms failed!";
        return res;
    }

    // Get a list of all platform ids
    std::vector<cl_platform_id> pids(num_platforms);
    status = clGetPlatformIDs(num_platforms, pids.data(), NULL);
    PANDA_LOG_DEBUG << "Number of platforms:"<<num_platforms;
    if (status != CL_SUCCESS) {
        PANDA_LOG_ERROR << "Query for all platform ids failed!";
        return res;
    }

    // For each platform, get name and compare against the search string
    for (unsigned iplat = 0; iplat < num_platforms; ++iplat) {
        try {
            std::shared_ptr<altera::OpenClPlatform> platform(new altera::OpenClPlatform(pids[iplat]));

            // Convert platfrom name to lower case
            std::string name(platform->name());
            std::transform(name.begin(), name.end(), name.begin(), tolower);

            if (name.find(search) != std::string::npos) {

                std::vector<cl_device_id> dids = platform->device_ids();
                for (unsigned idev = 0; idev < dids.size(); ++idev) {
                    res.emplace_back(std::make_shared<PoolResource<altera::OpenCl>>(platform, std::move(dids[idev])));
                }
            }
        } catch(std::exception const& e)
        {
            PANDA_LOG_ERROR << e.what();
        }
    }

    if(res.size() == 0) {
        PANDA_LOG_WARN << "unable to find Intel(Altera) OpenCl platform!";
    }

#endif // ENABLE_OPENCL
    return res;
}

} // namespace panda
} // namespace ska
