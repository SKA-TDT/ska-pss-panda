/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceIterator.h"
#include <cassert>

namespace ska {
namespace panda {

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>::DeviceIterator(std::size_t offset, Pointer& dev)
    : _dev_ptr(&dev)
    , _offset(offset)
{
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>::~DeviceIterator()
{
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>::operator cl_mem const& () const
{
    assert(_offset == 0);
    return static_cast<cl_mem const&>(*_dev_ptr);
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>& DeviceIterator<altera::OpenCl, DataType>::operator=(DeviceIterator const& other)
{
    _dev_ptr=other._dev_ptr;
    _offset=other._offset;
    return *this;
}

template<typename DataType>
cl_int DeviceIterator<altera::OpenCl, DataType>::write_to_device(DataType const& data, std::size_t size) const
{
    return _dev_ptr->enque_write(_offset, data, size);
}


template<typename DataType>
cl_int DeviceIterator<altera::OpenCl, DataType>::read_from_device(typename std::remove_const<DataType>::type& data, std::size_t size) const
{
    return _dev_ptr->enque_read(_offset, data, size);
}


template<typename DataType>
bool DeviceIterator<altera::OpenCl, DataType>::operator==(DeviceIterator const& o) const
{
    return _dev_ptr == o._dev_ptr && _offset == o._offset;
}

template<typename DataType>
bool DeviceIterator<altera::OpenCl, DataType>::operator!=(DeviceIterator const& o) const
{
    return _dev_ptr != o._dev_ptr || _offset != o._offset;
}

template<typename DataType>
std::size_t  DeviceIterator<altera::OpenCl, DataType>::distance(DeviceIterator const& o) const
{
    return o._offset - _offset;
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>& DeviceIterator<altera::OpenCl, DataType>::operator++()
{
    ++_offset;
    return *this;
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType> DeviceIterator<altera::OpenCl, DataType>::operator++(int)
{
    DeviceIterator tmp(_offset, *_dev_ptr);
    ++_offset;
    return tmp;
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType>& DeviceIterator<altera::OpenCl, DataType>::operator+=(std::size_t n)
{
    _offset += n;
    return *this;
}

template<typename DataType>
std::size_t DeviceIterator<altera::OpenCl, DataType>::operator-(DeviceIterator<altera::OpenCl, DataType> const & other) const
{
    return _offset - other._offset;
}

template<typename DataType>
DeviceIterator<altera::OpenCl, DataType> DeviceIterator<altera::OpenCl, DataType>::operator+(std::size_t size) const
{
    return DeviceIterator<altera::OpenCl, DataType>(_offset + size, *_dev_ptr);
}

} // namespace panda
} // namespace ska
