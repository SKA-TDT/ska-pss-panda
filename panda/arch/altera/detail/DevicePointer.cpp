/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/DevicePointer.h"
#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/PoolResource.h"
#include "panda/Error.h"

namespace ska {
namespace panda {
namespace altera {

template<typename DataType>
DevicePointer<DataType>::DevicePointer(Fpga const& device, std::size_t n, CommandQueue& queue)
    : _size(n)
    , _device(&device)
    , _queue(&queue)
{
#ifdef ENABLE_OPENCL
    if (n==0)
        return;
    cl_context const& context = device.context();
    cl_int status;
    _cl_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, n * sizeof(DataType), NULL, &status);
    if (status) {
        panda::Error e("Altera FPGA: unable to allocate memory :");
        e << n *sizeof(DataType) << " bytes";
        throw e;
    }
    clRetainMemObject(_cl_mem);
    _tag=static_cast<void*>(&*_cl_mem);
#else
    (void) device;
    (void) queue;
#endif
}

template<typename DataType>
DevicePointer<DataType>::DevicePointer(DevicePointer const& cp)
    : _size(cp._size)
    , _device(cp._device)
    , _cl_mem(cp._cl_mem)
    , _queue(cp._queue)
    , _tag(cp._tag)
{
    if (_size!=0)
        clRetainMemObject(_cl_mem);
}

template<typename DataType>
DevicePointer<DataType>::DevicePointer(DevicePointer&& cp)
    : _size(cp._size)
    , _device(cp._device)
    , _cl_mem(std::move(cp._cl_mem))
    , _queue(cp._queue)
    , _tag(cp._tag)
{
    if (_size!=0)
        clRetainMemObject(_cl_mem);
}

template<typename DataType>
DevicePointer<DataType>::~DevicePointer()
{
    if (_size!=0)
        clReleaseMemObject(_cl_mem);
}

template<typename DataType>
DevicePointer<DataType>& DevicePointer<DataType>::operator=(DevicePointer&& cp)
{
    clReleaseMemObject(_cl_mem);
    _size=cp._size;
    _device=cp._device;
    _cl_mem=std::move(cp._cl_mem);
    _queue=cp._queue;
    _tag=cp._tag;
    clRetainMemObject(_cl_mem);
    return *this;
}

template<typename DataType>
void* DevicePointer<DataType>::tag() const
{
    return _tag;
}

template<typename DataType>
typename DevicePointer<DataType>::Iterator DevicePointer<DataType>::begin()
{
    return Iterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::Iterator DevicePointer<DataType>::end()
{
    return Iterator(_size, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::begin() const
{
    return ConstIterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::end() const
{
    return ConstIterator(_size, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::cbegin() const
{
    return ConstIterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::cend() const
{
    return ConstIterator(_size, *this);
}

template<typename DataType>
cl_int DevicePointer<DataType>::enque_write(cl_command_queue queue, std::size_t offset, DataType const& data, std::size_t size) const
{
    assert(offset < _size);
    assert(_size - offset >= size);
    return clEnqueueWriteBuffer(static_cast<cl_command_queue&>(queue), _cl_mem, CL_TRUE, offset * sizeof(DataType), sizeof(DataType) * size, (const void*)(&data), 0, NULL, NULL);
}

template<typename DataType>
cl_int DevicePointer<DataType>::enque_write(std::size_t offset, DataType const& data, std::size_t size) const
{
    return enque_write(static_cast<cl_command_queue&>(*_queue), offset, data, size);
}

template<typename DataType>
cl_int DevicePointer<DataType>::enque_read(cl_command_queue queue, std::size_t offset, DataType& data, std::size_t size) const
{
    assert(offset < _size);
    assert(_size - offset >= size);
    return clEnqueueReadBuffer(queue, _cl_mem, CL_TRUE, offset * sizeof(DataType), sizeof(DataType) * size, (void*)(&data), 0, NULL, NULL);
}

template<typename DataType>
cl_int DevicePointer<DataType>::enque_read(std::size_t offset, DataType& data, std::size_t size) const
{
    return enque_read(static_cast<cl_command_queue&>(*_queue), offset, data, size);
}

template<typename DataType>
std::size_t const& DevicePointer<DataType>::size() const
{
    return _size;
}

template<typename DataType>
Fpga const& DevicePointer<DataType>::device() const{
    return *_device;
}

template<typename DataType>
DevicePointer<DataType>::operator cl_mem const& () const { return _cl_mem; }

} // namespace altera
} // namespace panda
} // namespace ska
