/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_AOCX_H
#define SKA_PANDA_ALTERA_AOCX_H

#include "panda/arch/altera/Fpga.h"
#ifdef ENABLE_OPENCL
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wpedantic"
#include <boost/filesystem/path.hpp>
#pragma GCC diagnostic pop


namespace ska {
namespace panda {
namespace altera {

/**
 * @brief: imports binary image file (.aocx) of FPGA OpenCL kernel and saves in a string
 *
 */
class Aocx
{
    public:
        Aocx(boost::filesystem::path const& kernel_filename);
        Aocx(Aocx const&) = delete;
        ~Aocx();

        std::size_t binary_size() const;
        std::string const& binary_string() const;

    private:
        std::string _binary_str;
};

} // namespace altera
} // namespace panda
} // namespace ska

#endif //ifdef ENABLE_OPENCL
#endif // SKA_PANDA_ALTERA_AOCX_H
