/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_ALTERA_FPGA_H
#define SKA_PANDA_ARCH_ALTERA_FPGA_H

#include "panda/Resource.h"
#ifdef ENABLE_OPENCL
#include "panda/arch/altera/OpenCl.h"
#include "panda/arch/altera/CommandQueue.h"
#include <memory>

namespace ska {
namespace panda {
namespace altera {
class OpenClPlatform;

/**
 * @brief
 *    An Altera FPGA card resource
 *
 */
class Fpga : public Resource
{
    public:
        Fpga(std::shared_ptr<OpenClPlatform> const&, cl_device_id device_id);
        Fpga(Fpga const&) = delete;
        ~Fpga();

        /**
         * @brief execute the provided task on the device
         */
        template<typename TaskType>
        void run( TaskType& job );

        void abort() {}; //TODO

        /**
         * @brief The opencl device_id
         */
        cl_device_id const& device_id() const;

        /**
         * @brief tje name of the device
         */
        const char* device_name() const;

        /**
         * @brief the maximum available memory on the deivce
         */
        cl_ulong const& device_memory();

        /**
         * @brief the default context for this device
         */
        cl_context const& context() const;

        /**
         * @brief the default queue to be assoiciated with this device
         */
        CommandQueue& default_queue() const;

        /**
         * @brief The error message assoicated with an opencl error return value
         */
        static const char* error_msg(cl_int error);

    private:
        std::shared_ptr<OpenClPlatform> _platform;
        cl_device_id _device_id;
        mutable char _device_name[1025];
        size_t _max_memory;
        cl_context _cl_context;
        std::unique_ptr<CommandQueue> _queue;

};

} // namespace altera
} // namespace panda
} // namespace ska
#endif // ENABLE_OPENCL

#endif // SKA_PANDA_ARCH_ALTERA_FPGA_H
