/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_KERNEL_H
#define SKA_PANDA_ALTERA_KERNEL_H

#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/Program.h"
#include "panda/arch/altera/CommandQueue.h"

#include <initializer_list>

#ifdef ENABLE_OPENCL

namespace ska {
namespace panda {
namespace altera {

/**
 * @brief: Creates opencl cl_kernel for the kernel name
 *
 * @details: A program created for the device can have multiple kernels.
 */
//template<>
class Kernel
{
    public:
        Kernel(std::string const& kernel_name, Program const& program);
        Kernel(Kernel const&) = delete;
        Kernel(Kernel&&);
        ~Kernel();

        Kernel& operator=(Kernel&& other);

        /**
         * @brief: Executes the NDRange kernel with a 1-dimensional index space
         * @param: n_local (local_work_size) describes the number of work-items/threads in a work-group
         * @param: n_global is number of global work-items/total threads that will excecute the kernel function
         * @tparam: kernel arguments to be passed same as opencl kernel compiled
         */
        template<typename... Args>
        void operator()(CommandQueue& queue, size_t n_local, size_t n_global, Args&&... args);

        /**
         * @brief: Executes the NDRange kernel with the given index space dimensions
         * @detail: Note: OpenCL supports between 1 and 3 dimensions
         * @param: local_work_size describes the number of work-items/threads per dimension in a work-group
         * @param: global_work_size is number of global work-items/total threads per dimension that will excecute the kernel function
         * @tparam: kernel arguments to be passed same as opencl kernel compiled
         */
        template<typename... Args>
        void operator()(CommandQueue& queue, std::initializer_list<size_t> local_work_size, std::initializer_list<size_t> global_work_size, Args&&... args);

    private:
        cl_kernel _kernel;
        bool _owner;
        std::string _kernel_name;

};

} // namespace altera
} // namespace panda
} // namespace ska

#include "panda/arch/altera/detail/Kernel.cpp"
#endif // ifdef ENABLE_OPENCL
#endif // SKA_PANDA_ALTERA_KERNEL_H
