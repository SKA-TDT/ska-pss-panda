/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_COMMANDQUEUE_H
#define SKA_PANDA_ALTERA_COMMANDQUEUE_H

#include "panda/arch/altera/OpenCl.h"


namespace ska {
namespace panda {
namespace altera {

class Fpga;

/**
 * @brief safe wrpper around a cl_command_queue object
 *
 * @details
 *
 */
class CommandQueue
{
    public:
        explicit CommandQueue(Fpga const& device);
        CommandQueue(CommandQueue const&);
        CommandQueue(CommandQueue&&) = delete;
        ~CommandQueue();

        void finish();

        operator cl_command_queue& ();
        cl_command_queue const& queue() const;

    private:
        cl_command_queue _queue;
};

} // namespace altera
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ALTERA_COMMANDQUEUE_H
