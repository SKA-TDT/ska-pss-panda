/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SERVERCLIENT_H
#define SKA_PANDA_SERVERCLIENT_H

#include <functional>
#include "panda/Buffer.h"

namespace ska {
namespace panda {
class Error;

/**
 * @brief
 *    Maintain access to a client
 * @details
 *
 * \ingroup connectivity
 * @{
 */
template<typename ServerTraits>
class ServerClient
{

    public:
        typedef std::function<void(Error)> SendHandler;
        typedef typename ServerTraits::EndPoint EndPointType;
        typedef typename ServerTraits::Socket SocketType;
        typedef Buffer<char> BufferType;

    public:
        ServerClient(SocketType& socket);
        ~ServerClient();
        ServerClient(ServerClient const&) = delete;

        /// a unique id associated with the client/session
        std::string id() const;

        EndPointType& end_point();

        void read(BufferType& buffer);
        void send(BufferType const buffer);
        void send(BufferType const buffer, SendHandler const& handler);

    private:
        void do_write(Buffer<char> const& buffer, unsigned offset, SendHandler const & handler);

    private:
        SocketType& _socket;
        EndPointType _end_point;

};

} // namespace panda
} // namespace ska

#include "panda/detail/ServerClient.cpp"

#endif // SKA_PANDA_SERVERCLIENT_H
/**
 *  @}
 *  End Document Group
 **/
