/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MULTIPLECONFIGMODULE_H
#define SKA_PANDA_MULTIPLECONFIGMODULE_H


namespace ska {
namespace panda {

/**
 * @brief Create leaf-nodes in a ConfigurationModule tree with a templated
 *        accessor interface.
 *
 * @details
 *   Provides a standardised config<ConfigType>() interface to access each leaf node.
 *   Note that each ConfigModuleType must be unique.
 *
 * @tparam BaseType The ConfigModule type to which the other nodes should be added as leaves
 */
template<typename BaseType, typename... ConfigModuleTypes>
class MultipleConfigModule
{
    public:
        MultipleConfigModule();
        ~MultipleConfigModule();

        /**
         * @brief get a reference to the module of the specified type
         */
        template<class ConfigModuleType>
        ConfigModuleType& config();

        /**
         * @brief get a reference to the module of the specified type (const version)
         */
        template<class ConfigModuleType>
        ConfigModuleType const& config() const;

        /**
         * @brief deactivate any nodes in the ConfigModuleType list that support the active(bool) method
         */
        void deactivate_all();
};

} // namespace panda
} // namespace ska
#include "panda/detail/MultipleConfigModule.cpp"

#endif // SKA_PANDA_MULTIPLECONFIGMODULE_H
