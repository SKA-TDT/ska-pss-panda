/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLMODULECONFIG_H
#define SKA_PANDA_POOLMODULECONFIG_H

#include "panda/PoolSelector.h"
#include "panda/Config.h"
#include <tuple>

namespace ska {
namespace panda {

/**
 * @brief
 *    Enable a collection of Config objects to be pool enabled
 *
 * @details
 *    This is a helper class for building configuration trees
 *    from many modules configuration classes
 */
template<typename PoolManagerType, typename... Configs>
class PoolModuleConfig
{
        typedef std::tuple<std::unique_ptr<PoolSelector<PoolManagerType, Configs>>...> TupleType;

    public:
        /**
         * @brief the configurations will be added as subsections to the parent
         */
        PoolModuleConfig(PoolManagerType& pm, ConfigModule& parent);

        template<typename... Arch>
        PoolModuleConfig(Config<Arch...>& parent);

        ~PoolModuleConfig();

        /**
         * @brief get the pool enabled configuration node based on the config type T
         * @details T must be one of the Configs passed in the template argument list
         */
        template<typename T>
        PoolSelector<PoolManagerType, T> const& config() const;

        template<typename T>
        PoolSelector<PoolManagerType, T>& config();

    private:
        TupleType _configs;
};

} // namespace panda
} // namespace ska
#include "detail/PoolModuleConfig.cpp"

#endif // SKA_PANDA_POOLMODULECONFIG_H 
