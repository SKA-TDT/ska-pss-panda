/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICEITERATOR_H
#define SKA_PANDA_DEVICEITERATOR_H
#include "panda/Architecture.h"
#include "panda/ArchitectureConcept.h"
#include <boost/iterator/iterator_adaptor.hpp>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *    Wraps an iterator giving a typedef for the Architecture
 *    for use with panda;:copy.
 *
 * @details
 *
 */
template<class Arch, typename IteratorType>
class DeviceIterator : public IteratorType
{
    BOOST_CONCEPT_ASSERT((ArchitectureConcept<Arch>));

    public:
        typedef Arch Architecture;
        typedef DeviceIterator<Arch, IteratorType> SelfType; // useful for breaking recursive DeviceIterator<Arch, DeviceIterator<...

    public:
        explicit DeviceIterator(IteratorType const& iterator);
        DeviceIterator(IteratorType&& iterator);
};

// specialisation for pointer types
template<class Arch, typename T>
class DeviceIterator<Arch, T*> :  public boost::iterator_adaptor<
                                  DeviceIterator<Arch, T*>      // Derived
                                , T*                            // Base
                                >
{
    BOOST_CONCEPT_ASSERT((ArchitectureConcept<Architecture>));

    private:
        typedef boost::iterator_adaptor<DeviceIterator<Arch, T*>, T*> BaseT;
        struct Enabler {}; // used for enable_if in copy constructor

    public:
        typedef typename BaseT::value_type value_type;
        typedef typename BaseT::pointer pointer;

    public:
        explicit DeviceIterator(T* ptr);

        /// copy constructor
        template <class OtherType>
        DeviceIterator( DeviceIterator<Arch, OtherType> const& other
                      , typename std::enable_if<
                              std::is_convertible<OtherType*,T*>::value
                            , Enabler
                        >::type = Enabler()
        );

    private:
       friend class boost::iterator_core_access;
};

/**
 * @brief compile time function to detemrine if a type is a DeviceIterator
 */
template<typename T>
struct is_device_iterator : public std::false_type
{
};

template<class Arch, typename T>
struct is_device_iterator<DeviceIterator<Arch,T>> : public std::true_type
{
};

template<typename T>
struct is_device_iterator<const T> : public is_device_iterator<T>
{
};

/**
 * @brief convenience function for creating a DeviceIteratorType from a provided iterator object
 */
template<class Arch
        ,typename T
        ,typename std::enable_if<!is_device_iterator<typename std::remove_reference<T>::type>::value, bool>::type = true>
DeviceIterator<Arch, typename std::remove_reference<T>::type> make_device_iterator(T&&);


/**
 * @brief specialisation to extract Arch type from DeviceIterator classes
 */
template<class Arch, typename T>
struct ArchitectureType<DeviceIterator<Arch, T>>
{
    typedef Arch type;
};

} // namespace panda
} // namespace ska
#include "panda/detail/DeviceIterator.cpp"

#endif // SKA_PANDA_DEVICEITERATOR_H
