/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCHITECTURE_H
#define SKA_PANDA_ARCHITECTURE_H

#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *   Base class for all tags
 *   tag for marking architecture of accelerators
 */
class Architecture
{
    protected:
        Architecture(std::string const& id);

    public:
        std::string const& tag() const;

    private:
        const std::string _tag;
};

/**
 * @brief generate a string to identify a type (such as an Architecture)
 */
static std::string const default_string("ska::panda::to_string not defined");
template<typename T>
constexpr std::string const& to_string() { return default_string; }

class Cpu;

static std::string const cpu_string("panda::Cpu");
template<>
constexpr std::string const& to_string<Cpu>() { return cpu_string; }

/**
 * @brief static time function to return the Arch associated with any given type
 * @details specialise for any specific types
 * defualt type is Cpu unless otherwise overridden
 */
template<typename T, typename Enable=void>
struct ArchitectureType
{
    typedef Cpu type;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ARCHITECTURE_H
