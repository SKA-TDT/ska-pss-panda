/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_BASIC_APP_CONFIG_H
#define SKA_PANDA_BASIC_APP_CONFIG_H

#include "panda/ConfigModule.h"
#include "panda/PoolManagerConfig.h"
#include "panda/PoolManager.h"
#include "panda/System.h"
#include <string>
#include <vector>
#include <utility>

namespace ska {
namespace panda {

/**
 * @brief
 *    Top level BasicAppConfiguration Options and confoguration file parser.
 *
 * @details
 *   Convenience class for standard applications
 *   Provides command line access to help functions, versioning and config files
 *
 */
class BasicAppConfig : protected ConfigModule
{
    protected:
        /**
         * @brief you must inherit from this class
         */
        BasicAppConfig(std::string app_name, std::string short_description);
        BasicAppConfig(BasicAppConfig&&) = delete;
        BasicAppConfig(BasicAppConfig const&) = delete;

   public:
        virtual ~BasicAppConfig();

        /**
         * @brief a string representing the version of the project and its dependencies
         */
        virtual std::string version() const;

        /**
         * @brief parse the command line options/config file
         * @returns 0 to indicate no parse halting options specified
         * @returns >0 to indicate parse failed or parse indicates program halt
         */
        int parse(int argc, char** argv);

    protected:
        /**
         * @brief override if you want direct access to the varaibale map during a parse.
         *        n.b it is better to use notifiers instead where possible
         * @return 0 to continue parsing, any other value to return that value as an error code
         */
        virtual int parse_vm(boost::program_options::variables_map& vm);

        // needs to exist for constructor
        void add_options(OptionsDescriptionEasyInit& add_options) override;

        void add(ConfigModule& config);
        void add(std::string section_name, ConfigModule& config);

    protected:
        boost::program_options::options_description _desc;
        boost::program_options::positional_options_description _options_pod;

    private:
        void init();

    private:
        bool _init;
        boost::program_options::options_description _all_desc;

        std::string _short_description;
        std::string _app_name;

        std::vector<std::pair<std::string, ConfigModule*>> _add_modules;

        template<typename P, typename...> friend class PoolModuleConfig;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_BASIC_APP_CONFIG_H
