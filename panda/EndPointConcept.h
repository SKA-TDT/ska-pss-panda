/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ENDPOINTCONCEPT_H
#define SKA_PANDA_ENDPOINTCONCEPT_H

#include "panda/Concept.h"

#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/basic_endpoint.hpp>



/**
 * @brief The EndPointConcept class check the requirements on an EndPoint type
 *
 * @details The EndPoint must be a valid boost endpoint such as boost::asio::ip::basic_endpoint<Protocol> which takes an ip address and port number.
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct EndPointConcept: Concept<X> {
        typedef Concept<X> Base;

        boost::asio::ip::address address();

        unsigned port();

    public:
        BOOST_CONCEPT_USAGE(EndPointConcept) {
            /// Classes modelling the EndPoint concept must support a construtor taking a boost::asio::ip::address and an unsigned int
            X x(address(),port());
            Base::use(x);
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_ENDPOINTCONCEPT_H
