/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TYPETRAITS_H
#define SKA_PANDA_TYPETRAITS_H
#include <memory>
#include <type_traits>


namespace ska {
namespace panda {

/**
 * @class is_pointer_wrapper
 * @brief return true is its a std::shared_ptr or std::unique_ptr
 *        false otherwise
 *        @param type is typedefed to either the full object or the wrapped type
 *        @method extract return a ref to the value (by deref if necessary)
 */
template<typename T>
struct is_pointer_wrapper {
    static constexpr bool value=false;
    typedef T type;

    inline static T& extract(T& t) {
        return t;
    }
};

template<typename T>
struct is_pointer_wrapper<std::shared_ptr<T>> {
    static constexpr bool value=true;
    typedef T type;

    inline static T& extract(std::shared_ptr<T> const& t) {
        return *t;
    }
};

template<typename T>
struct is_pointer_wrapper<std::unique_ptr<T>> {
    static constexpr bool value=true;
    typedef T type;

    inline static T& extract(std::unique_ptr<T> const& t) {
        return *t;
    }
};

/**--------------------------------------------------------------------
 * @class SelectMethod
 * @brief call a templated method on a CallBack object according to
 *        runtime parameters
 * @code
 *     struct CallBack
 *     {
 *         template<typename... Ts>
 *         void exec() { std::cout << "generic call"; }
 *
 *         // specialisation
 *         template<float, time_t> void exec() { std::cout << "specialisation"; }
 *
 *         // non-selected handler
 *         void exec();
 *
 *     };
 *
 *     // the following call will invoke the specialisation as only the corresponding selects are marked "true"
 *     if(!SelectMethod::exec(SelectMethod::Select<int>(false), SelectMethod::Select<float>(true), SelectMethod::Select<time_t>(true))) {
 *        std::cerr << "warning: no types were selected\n";
 *     }
 *
 * @endcode
 * --------------------------------------------------------------------
 */
struct SelectMethod
{
    public:
        template<typename T>
        struct Select {
            public:
                Select(bool select)
                    : _selected(select)
                {}

                bool operator()() const { return _selected; }

            private:
                bool _selected;
        };

    public:
        SelectMethod() = delete;
        SelectMethod(SelectMethod const&) = delete;
        /**
         * @brief from the list of provided Select objects, call the exec method on CallBack corresponnding to those marked as true
         * @return true if at least one Select<T> was passed with a true value, false otherwise
         */
        template<typename CallBack, typename... Ts>
        static inline
        bool exec(CallBack& callback, Select<Ts> const&...);
};

/**
 * @brief tests if a Type has a method
 * @tparam Type is the type to test for the method
 * @tparam Tester is a template taking a single Template Parameter that exercises the method
 *         e.g. for to test for a method ```begin() const``` taking no arguments
 *         @code
 *            template<typename T>
 *            using has_const_begin_t = decltype(std::declval<T const&>().begin());
 *
 *            template<typename T>
 *            struct has_const_begin : HasMethod<T, has_const_begin_t> {};
 *
 *         @endcode
 *
 *         e.g. for to test for a method that takes an argument ```void execute(int)```
 *         @code
 *            template<typename T>
 *            using has_exec_t = decltype(std::declval<T const&>().exec(std::declval<int>()));
 *
 *            template<typename T>
 *            struct has_exec : HasMethod<T, has_exec_t> {};
 *
 *         @endcode
 *
 * @result value will be either true or false
 */
template<typename Type, template<typename> class Tester>
struct HasMethod
{
    private:
        template<typename T> struct TestWrap;
        template<typename T> static std::true_type test( TestWrap<Tester<T>>* );
        template<typename T> static std::false_type test(...);

    public:
        /// true if the method exists, false otherwise
        static constexpr bool value = decltype(test<Type>(nullptr))::value;

        /// std::true_type if the method exists, std::false_type otherwise
        typedef typename std::conditional<value, std::true_type, std::false_type>::type type;
};


} // namespace panda
} // namespace ska
#include "detail/TypeTraits.cpp"

#endif // SKA_PANDA_TYPETRAITS_H
