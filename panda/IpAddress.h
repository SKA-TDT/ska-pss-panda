/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_IPADDRESS_H
#define SKA_PANDA_IPADDRESS_H

#include "panda/ConceptChecker.h"
#include "panda/EndPointConcept.h"

#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/basic_endpoint.hpp>
#include <string>

namespace ska {
namespace panda {

/**
 * @class IpAddress
 *
 * @brief
 *    Basic ip address and port
 *    Will accept hostnames and attempt to resolve when end_point called if necessary
 */
class IpAddress
{
    public:
        IpAddress(std::string const& address="127.0.0.1");
        IpAddress(boost::asio::ip::address const& address, unsigned port=0);
        IpAddress(unsigned port, std::string const& address = "127.0.0.1");
        template<typename ProtocolType>
        IpAddress(boost::asio::ip::basic_endpoint<ProtocolType> const&);
        ~IpAddress();

        boost::asio::ip::address const & address() const;
        void address(boost::asio::ip::address const &);
        void port(unsigned port);
        unsigned port() const;

        template<typename EndPointType>
        EndPointType end_point() const;

        bool operator==(IpAddress const& addr) const;
        bool operator!=(IpAddress const& addr) const;

    private:
        void init(std::string const& address);

    private:
        mutable unsigned _port;
        mutable boost::asio::ip::address _address;
        std::string  _hostname;
};

} // namespace panda
} // namespace ska
#include "detail/IpAddress.cpp"

#endif // SKA_PANDA_IPADDRESS_H
