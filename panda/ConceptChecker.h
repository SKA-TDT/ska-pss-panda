/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONCEPT_H
#define SKA_PANDA_CONCEPT_H

#include "Concept.h"
#include "panda/detail/ConceptCheckParameterPack.h"


/**
 * @brief Sometimes it is necessary to check the concept of every type in a parameter pack.
 * Unfortunately, as of v1.62, BOOST does not not support this.
 *
 * The Concept class provides this (meta) functionality
 *
 * @details To check that every type in parameter pack, P... satisfies concept, C,
 * you can use a declaration like this:-
 *
 * @code
 *     typedef ::ska::panda::Concept<C>::Check<P...> MyConceptCheck;
 * @endcode
 *
 * For an example of this in action, refer to ProducerAggregator.h or AlgorithmTuple.h
 *
 * Of course it would be possible to use this for a single Policy like so:
 *
 * @code
 *     ::ska::panda::Concept<C>::Check<P>::use();
 * @endcode
 *
 * in which case, it is the same as:
 *
 * @code
 *     BOOST_CONCEPT_ASSERT((C<P>));
 * @endcode
 *
 * except that an unnecessary type, MyConceptCheck, would have been declared.
 *
 * \ingroup concepts
 * @{
 */



namespace ska {
namespace panda {

template<template<typename> class ConceptType>
struct ConceptChecker {
    template<typename... PolicyTypeList>
    class Check: detail::ConceptCheckParameterPack<ConceptType,PolicyTypeList...> {
        public:
            static void use() {}
    };
}; // ConceptChecker

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_CONCEPT_H
