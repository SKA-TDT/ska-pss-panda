/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATASWITCHCONFIG_H
#define SKA_PANDA_DATASWITCHCONFIG_H

#include "panda/ProcessingEngineConfig.h"
#include <string>
#include <map>
#include <memory>

namespace ska {
namespace panda {
class ProcessingEngine;
class ChannelId;

/**
 * @brief Configuration information for the DataSwitch class
 *
 */
class DataSwitchConfig
{
    public:
        explicit DataSwitchConfig();
        ~DataSwitchConfig();

        /**
         * @brief mark the named channel to be active
         */
        void activate_channel(ChannelId const& channel_name);

        /**
         * @brief mark the named channel as deactive
         */
        void deactivate_channel(ChannelId const& channel_name);

        /**
         * @brief return true if the named channel has been explicitly marked as active, false otherwise
         */
        bool is_active(ChannelId const& channel_name) const;

        /**
         * @brief return the processing engine configuration object associated with a specifc channel
         */
        ProcessingEngineConfig const& engine_config(ChannelId const& channel_id) const;

        /**
         * @brief set the processing engine configuration params associated with a specifc channel
         * @details can only be called prior to a call to method engine(channel_id);
         * @throw panda::Error if called after the relevant engine has already been provisioned
         */
        void set_engine_config(ChannelId const& channel_id, ProcessingEngineConfig const&);

        /**
         * @brief return the processing engine configuration associated with the default engine
         */
        ProcessingEngineConfig const& engine_config() const;

        /**
         * @brief set the processing engine configuration params associated with default engine
         * @details can only be called prior to a call to method engine() for non-specialised channels
         * @throw panda::Error if called after the default engine has already been provisioned
         */
        void set_engine_config(ProcessingEngineConfig const&);

        /**
         * @brief return the processing engine configuration object associated with a specific channel
         * @details if the set_engine_config(channel_id) has been called prior to a call to this
         *          method for the same id, then a special engine will be assigned corresponding
         *          to these settings. Otherwise the default engine will be assigned.
         */
        ProcessingEngine& engine(ChannelId const& channel_name) const;

    private:
        struct DataConfigHolder {
            ~DataConfigHolder();
            std::map<ChannelId, bool> _active;

            // specialist engines
            std::map<ChannelId, ProcessingEngineConfig> _engine_config;
            mutable std::map<ChannelId, std::shared_ptr<ProcessingEngine>> _engines;

            // default engine
            ProcessingEngineConfig _default_engine_config;
            mutable std::shared_ptr<ProcessingEngine> _default_engine;
        };
        std::shared_ptr<DataConfigHolder> _holder;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_DATASWITCHCONFIG_H 
