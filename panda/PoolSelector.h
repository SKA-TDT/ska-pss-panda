/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLSELECTOR_H
#define SKA_PANDA_POOLSELECTOR_H
#include <string>
#include "panda/ConfigModule.h"


namespace ska {
namespace panda {

/**
 * @brief
 *    Wraps a ConfigModule and adds options to select a pool by name
 *
 * @details
 *    If selected then the requested will be marked reserved on the PoolManagerConfig object
 *    The appropriate pool will be madea availabe to the call to pool().
 *
 * @exmaple
 *    as a wrappea (for mixin style configuration). This gives the advantage of being able to compile
 *    most options into a library whilst leaving the pool part as a template to adapt to different PoolMaagers.
 *    @code
 *      class MyConfigClass : public panda::ConfigModule
 *      {
 *        MyConfigClass() : panda::ConfigModule("my_config_name: {}
 *
 *        void add_options(ConfigModule::OptionsDescriptionEasyInit& add_options) override
 *        {
 *             // non-pool selection options here.... 
 *        }
 *      };
 *
 *      // define a template type for defining the full configuration type to be passed
 *      template<typename PoolManagerType>
 *      using MyConfigType = PoolSelector<PoolManagerType, MyConfigClass>;
 *
 *      // now use these defined types
 *      typdef panda::PoolManager<MyArchitectures...> PoolManagerType;
 *      PoolManagerType pool_manager;
 *      MyConfigType<PoolManagerType> config(pool_manager);
 *
 *    @endcode
 *      
 * @trait ConfigModuleType - must suppor the COnfigModule interface
 */
template<typename PoolManagerType, typename ConfigModuleType=void>
class PoolSelector : public ConfigModuleType
{
    public:
        typedef typename PoolManagerType::PoolType PoolType;

    public:
        /** 
         * @brief constructor for the PoolSeelctor
         * @param pool_manager a suitable reference to an exisiting PoolManager object
         * @param config_args argumnet to be passed in to the underlying ConfigModuleType constructor
         */
        template<typename... Args>
        PoolSelector(PoolManagerType& pool_manager, Args&&... config_args);
        ~PoolSelector();

        /** 
         * @brief adds the options to select a pool
         *        --pool-id
         */
        void add_options(ConfigModule::OptionsDescriptionEasyInit& add_options) override;
        std::string const& pool_id() const;

        /**
         * @brief return the selected pool (n.b. shared_ptr under the hood)
         * @details for efficiency reasons it is best if you can store this reference
         *          rather than make multuiple calls to this call
         */
        PoolType pool() const;

        /**
         * @brief set the priority configuration (0=highest priority)
         */
        void set_priority(unsigned);

    private:
        PoolManagerType& _pool_manager;
        unsigned _priority;
        std::string _pool_id;
};

} // namespace panda
} // namespace ska
#include "panda/detail/PoolSelector.cpp"

#endif // SKA_PANDA_POOLSELECTOR_H 
