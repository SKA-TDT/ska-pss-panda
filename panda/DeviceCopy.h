/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICECOPY_H
#define SKA_PANDA_DEVICECOPY_H


namespace ska {
namespace panda {

/**
 * @brief provide specialisations of this class to extend panda::copy
 * @details
 *    specialise by the Architectres and type tags 
 *    Tags or an iterator are std::remove_reference<IteratorType>::type.
 *    Note that this is not necessarily the type passed in the copy call which may be a l or r value
 *    and is left for the coiler to deduce
 */
template<typename SrcArch, typename DstArch, typename SrcIteratorTypeTag=void, typename EndIteratorTypeTag=void, typename DstIteratorTypeTag=void>
struct DeviceCopy
{
    public:
        /**
         * @brief perform the copy form source to destination architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline DstIteratorType copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

};

} // namespace panda
} // namespace ska

#ifdef ENABLE_CUDA
#include "panda/arch/nvidia/DeviceCopy.h"
#endif // ENABLE_CUDA
#endif // SKA_PANDA_DEVICECOPY_H 
