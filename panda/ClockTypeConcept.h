/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CLOCKTYPECONCEPT_H
#define SKA_PANDA_CLOCKTYPECONCEPT_H

#include "panda/Concept.h"
#include <chrono>

/**
 * @brief The ClockTypeConcept class check the requirements on an ClockType type
 * @details Expected functionality to be similar to that of std::chrono library clocks - in particular, a static now() method must be provided
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct ClockTypeConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(ClockTypeConcept)
        {
            /// duration type required
            typedef typename X::duration duration;

            /// X::time_point
            Base::template type<typename X::time_point>::exists();

            /// X::duration must be std::chrono::duration_castable to std::chrono::duration<double>
            duration& d(Base::template reference<duration>());
            std::chrono::duration_cast<std::chrono::duration<double>>(d);

            /// The now() method returns the current time.
            typename X::time_point now(X::now());
            Base::use(now);
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_CLOCKTYPECONCEPT_H
