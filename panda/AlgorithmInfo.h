/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALGORITHMINFO_H
#define SKA_PANDA_ALGORITHMINFO_H

namespace ska {
namespace panda {

/**
 * @brief Extract the Architecture information for a specific Algorithm
 * @tparsm Alorithm the Algorithm with one or more of the following typedefs defined:
 *         Architecture
 *         Architectures
 *         ArchitectureCapability
 * @details
 *         Any typedef not found will be substituted with default values
 */
template<typename Algorithm, typename Enable=void>
struct AlgorithmInfo {
    typedef typename Algorithm::Architecture Architecture;
    typedef typename Algorithm::Architecture ArchitectureCapability;

};

/**
 * @brief Compound AlgoithmInfo Traits of a composition of Algorithms
 */
template<class AlgorithmTuple>
struct MixedAlgoTraits;

} // namespace panda
} // namespace ska

#include "detail/AlgorithmInfo.cpp"

#endif // SKA_PANDA_ALGORITHMINFO_H
