/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_OUTPUTFILESTREAMER_H
#define SKA_PANDA_OUTPUTFILESTREAMER_H

#include "panda/concepts/OutputFileConcept.h"
#include <boost/filesystem.hpp>
#include <fstream>
#include <string>
#include <mutex>

namespace ska {
namespace panda {

/**
 * @brief
 *    Implementation base class for templated OutputFileStreamer class
 *    Don't use this class except via the template class.
 */
class OutputFileStreamerBase
{
    public:
        OutputFileStreamerBase(boost::filesystem::path const& dir);

    protected:
        ~OutputFileStreamerBase();

        /**
         * @brief set the directory to write files to
         */
        void set_dir(boost::filesystem::path const& path);

        /**
         * @brief check if file already exists, and if so modify the filename with a number count
         * @details override if you want different behaviour
         */
        virtual boost::filesystem::path unique_filename(boost::filesystem::path const&);

        /**
         * @brief open a unique file (based on unique_filename()) for future stream ops
         */
        void open_unique_filename(boost::filesystem::path const&);

        /**
         * @brief set the extension to add to any filename (n.b should include the "." if required
         */
        void extension(std::string const& extension);

        /**
         * @brief set any prefix to add to the filename
         */
        void prefix(std::string const& prefix);

    private:
        static std::mutex _fs_mutex;
        boost::filesystem::path _dir;

    protected:
        std::ofstream _file_stream;
        std::string  _prefix;
        std::string  _extension;
};

/**
 * @brief
 *    class for output streamers that write to files
 *
 * @tparam DataType the DataType supported by the streamer
 * @tparam OutputFileStreamer Class supporting the OutputFileConcept
 *
 * @details
 *    FileFormatTraits class must support the OutputFileConcept
 */
template<class DataType, class FileFormatTraits> //, class FormatAdapter=PassThroughAdapter<DataType>>
class OutputFileStreamer : protected OutputFileStreamerBase
{

        BOOST_CONCEPT_ASSERT((OutputFileConcept<FileFormatTraits, DataType>));
        typedef OutputFileStreamerBase BaseT;

    public:
        OutputFileStreamer(boost::filesystem::path const& dir);

    protected:
        ~OutputFileStreamer();

    public:
        /**
         * @brief the streaming operator that ulimately writes to a file
         * @details the actual file written to will depend on the next_file() operator
         *          it will be created in the currently set directory directory.
         */
        OutputFileStreamer& operator<<(DataType const& t);

    protected:
        /**
         * @brief by default will create a filename _prefix + current_time + "." + _suffix
         * @details override for other formats, or if you need to take the DataType state into account
         */
        virtual boost::filesystem::path next_filename(DataType const&);

    protected:
        FileFormatTraits _format_traits;

};

} // namespace panda
} // namespace ska
#include "panda/detail/OutputFileStreamer.cpp"

#endif // SKA_PANDA_OUTPUTFILESTREAMER_H
