/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_COPY_H
#define SKA_PANDA_COPY_H

#include "panda/DeviceIterator.h"
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief similar to std::copy but with specialisations for transferring to devices
 * @details
 *    Copy to and from external devices will occur based on the types of iterator that is passed.
 *    This is determined by the result of ArchitectureType<IteratorType>::type struct (in Architecture.h)
 *
 * example
 * @code
 * std::vector<int> input(10);
 * DeviceMemory<MyArch> device_ptr;
 * panda::copy(input.begin(), input.end(), device_ptr.begin());
 * @endcode
 *
 * example if using a raw pointer type and you want to mark it as being assoicated with a specific device
 * @code
 * device_ptr_type* device_memory_ptr = device_malloc(10); // beware of leaksy!
 * panda::copy(input.begin(), input.end(), panda::make_device_iterator<MyArch>(device_ptr));
 * @endcode
 *
 * @section extending for new architectures
 * To extend the copy function to support a specfic device
 * specialise
 * @code
 * template<typename SrcArch, typename DstArch>
 * class DeviceCopy<SrcArch, DstArch>
 * @endcode
 * this class should support a single method
 * @code
 * template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
 * static DstIterator copy(begin, end, dest)
 * @endcode
 * the transfer.
 * The copy method can assume contiguous memory to from the Cpu host.
 * This is because the DeviceCopyDelegate will run host -> device copies through a pre-copy processor that will buffer if required
 * Similarly device -> host methods will be buffered if the destination iterator is not contiguous.
 *
 * If this behaviour is not required, supply a suitable specialisation of the DeviceCopyDelegate that calls the DeviceCopy::copy directly
 *
 */
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&&);

} // namespace panda
} // namespace ska

#include "panda/detail/Copy.cpp"

#endif // SKA_PANDA_COPY_H
