/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PROCESSINGENGINECONFIG_H
#define SKA_PANDA_PROCESSINGENGINECONFIG_H

#include "panda/Cpu.h"

namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration options for a ProcessingEngine (number of threads an their affinities)
 */
class ProcessingEngineConfig : public DeviceConfig<Cpu>
{
        typedef DeviceConfig<Cpu> BaseT;

    public:
        ProcessingEngineConfig(unsigned number_of_threads=1);
        ProcessingEngineConfig(ProcessingEngineConfig const&) = default;
        ~ProcessingEngineConfig();

        /**
         * @brief assignment operator - number of threads
         */
        ProcessingEngineConfig& operator=(unsigned number_of_threads);

        /**
         * @brief set the total number of standard threads required that require no affinity settins.
         * @details a convenience method to avoid having to create number_of_threads
         */
        ProcessingEngineConfig& number_of_threads(unsigned number_of_threads);

        /**
         * @brief return the total number of threads required
         */
        unsigned number_of_threads() const;

};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_PROCESSINGENGINECONFIG_H
