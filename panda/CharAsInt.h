/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CHARASINT_H
#define SKA_PANDA_CHARASINT_H


namespace ska {
namespace panda {

/**
 * @brief helpers for template code outputing uint8_t type variables to std::ostream
 *
 * @detail
 * std iostreams have overrides for char types.
 * uint8_t is ofter defined with a typdef to a char or unsigned char and so outputing the
 * value with an std::ostream is difficult.
 * These helpers allow us to transform char types to unsigned to output the actual value.
 *
 */
template<typename T>
struct CharAsInt {
    typedef T type;
};

template<>
struct CharAsInt<unsigned char> {
    typedef unsigned type;
};

template<>
struct CharAsInt<char> {
    typedef unsigned type;
};

template<typename T>
using CharAsInt_t = typename CharAsInt<T>::type;

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CHARASINT_H
