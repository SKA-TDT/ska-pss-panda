/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TICKTOCKTIMER_H
#define SKA_PANDA_TICKTOCKTIMER_H

#include "panda/ClockTypeConcept.h"


namespace ska {
namespace panda {

/**
 * @brief
 *   A simple Tick Tock timer to return time duration between the tick() and the tock()
 *   Optionally a Handler can be provided which will be called each time a time duration is measured
 *
 * @details
 *   If tock() is called before tick(), the time of object contruction is returned.
 */

/// the default - non handler implementation
template<typename ClockType, typename Handler=void*>
class TickTockTimer;

template<typename ClockType>
class TickTockTimer<ClockType, void*>
{
        BOOST_CONCEPT_ASSERT((ClockTypeConcept<ClockType>));

    private:
        typedef typename ClockType::time_point TimePointType;

    public:
        typedef typename ClockType::duration TimeDurationType;

    public:
        TickTockTimer();
        ~TickTockTimer();

        /**
         * @brief set the reference time to now
         */
        void tick();

        /**
         * @brief alias for operator()
         */
        TimeDurationType tock();

        /**
         * @brief return the time since the last call to any method of this class
         */
        TimeDurationType operator()();

    private:
        TimePointType _last;
};

/// specialisation for a Handler
template<typename ClockType, typename Handler>
class TickTockTimer : public TickTockTimer<ClockType, void*>
{
    private:
        typedef TickTockTimer<ClockType, void*> BaseT;

    public:
        typedef typename BaseT::TimeDurationType TimeDurationType;

    public:
        TickTockTimer(Handler handler);

        /**
         * @brief return the time since the last call to any method of this class
         */
        TimeDurationType operator()();

        /**
         * @brief alias for operator()
         */
        TimeDurationType tock();

    private:
        Handler _handler;
};

template<typename ClockType, typename Handler>
TickTockTimer<ClockType, Handler> make_tick_tock_timer(Handler&& h)
{
    return TickTockTimer<ClockType, Handler>(std::forward<Handler>(h));
}

} // namespace panda
} // namespace ska

#include "panda/detail/TickTockTimer.cpp"
#endif // SKA_PANDA_TICKTOCKTIMER_H 
