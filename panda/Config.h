/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIG_H
#define SKA_PANDA_CONFIG_H

#include "panda/BasicAppConfig.h"
#include "panda/PoolManagerConfig.h"
#include "panda/PoolManager.h"
#include "panda/System.h"
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *    Top level Configuration Options and confoguration file parser.
 *
 * @details
 *   Convenience class for standard panda applications that require pools
 *   This is the same as the BasicAppConfig with the addition of the pool resources
 *
 *   Provides access to help functions and pool configuration
 *   and assoicated configured pool manager.
 *
 *   Also contains the System object that handels device discovery etc.
 *
 *   to use inherit from this class.
 */
template<typename... Architectures>
class Config : public BasicAppConfig
{
    public:
        typedef panda::System<Architectures...> SystemType;
        typedef panda::PoolManager<SystemType> PoolManagerType;
        typedef typename PoolManagerType::PoolType PoolType;

    protected:
        /**
         * @brief you must inherit from this class
         */
        Config(std::string app_name, std::string short_description);
        Config(Config&&) = delete;
        Config(Config const&) = delete;

   public:
        virtual ~Config();

        /**
         * @brief return the pool manager object
         */
        PoolManagerType const& pool_manager() const;
        PoolManagerType& pool_manager();

        /**
         * @brief return the sysstem accelerator description
         */
        SystemType& system();

    protected:
        // needs to exist for constructor
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        static SystemType _system; // this is a system wide object - only require a single instance

        typedef panda::PoolManagerConfig<SystemType> PoolManagerConfigType;
        PoolManagerConfigType _pool_manager_config;
        PoolManagerType _pool_manager;
};

} // namespace panda
} // namespace ska
#include "panda/detail/Config.cpp"

#endif // SKA_PANDA_CONFIG_H 
